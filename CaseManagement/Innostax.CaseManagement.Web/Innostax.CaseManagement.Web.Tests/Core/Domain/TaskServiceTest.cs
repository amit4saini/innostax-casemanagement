﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using AutoMapper;
using Innostax.Common.Infrastructure.Utility;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private TaskService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandler;
        private Mock<CommonUtils> _commonUtils;
        private Mock<ITaskFileAssociationService> _taskFileAssociationService;
        ODataQueryOptions<Models.Task> options;
        private Mock<ICommonService> _commonService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<IMailSenderService> _mailSenderService;
        private Mock<INotificationService> _notificationService;
        private Mock<ITaskActivityService> _taskActivityService;
        private Mock<ITaskDescriptionTaggedUserService> _taskDescriptionTaggedUserService;
        private Mock<IReportService> _reportService;
        Guid clientId1;
        Guid clientId2;
        Guid caseId;
        Guid userId;    
        Guid taskId;
        Guid clientContactUserId;


        [TestInitialize]
        public void TestInitialize()
        {
            userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            taskId = System.Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandler = new ErrorHandlerService();
            _taskFileAssociationService = new Mock<ITaskFileAssociationService>();
            _mailSenderService = new Mock<IMailSenderService>();
            _notificationService = new Mock<INotificationService>();
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            userId = Guid.NewGuid();
            caseId = Guid.NewGuid();
            clientContactUserId = Guid.NewGuid();
            _mailSenderService = new Mock<IMailSenderService>();
            _commonService = new Mock<ICommonService>();
            _taskDescriptionTaggedUserService = new Mock<ITaskDescriptionTaggedUserService>();
            _commonUtils = new Mock<CommonUtils>();
            _mailSenderService = new Mock<IMailSenderService>();
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Task>("Task");
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _taskActivityService = new Mock<ITaskActivityService>();
            options = new ODataQueryOptions<Models.Task>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Task)), new HttpRequestMessage());
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _userService.Setup(x => x.GetUser(It.IsAny<Guid>())).Returns(new Models.Result<Models.Account.User> { Data = new Models.Account.User { Role = new Models.Role { Name = Roles.Client_Participant.ToString() } } });
            _taskFileAssociationService.Setup(x => x.GetTaskAssociatedFiles(It.IsAny<Guid>())).Returns(new List<Models.FileModel>
            {
                new Models.FileModel
                {
                    FileName="test",
                    FileType="test",
                    FileId=System.Guid.NewGuid()
                }
            });
            _reportService = new Mock<IReportService>();
            sut = new TaskService(
                innostaxDbMock.Object,
                 _userService.Object, 
                 _commonUtils.Object, 
                 _errorHandler, 
                 _taskFileAssociationService.Object, 
                 _commonService.Object,
                 _caseHistoryService.Object,
                 _mailSenderService.Object,
                 _taskActivityService.Object,
                 _notificationService.Object,
                 _taskDescriptionTaggedUserService.Object,
                 _reportService.Object
                );
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _caseHistoryService.Setup(x => x.SaveCaseHistory(It.IsAny<Guid>(), It.IsAny<Innostax.Common.Database.Enums.Action>(),null));
           // _commonService.Setup(x => x.ValidateCaseAccess(It.IsAny<Guid>())).Returns(true);           
        }

        private Client GetADummyClient()
        {
            var dummyClient = new Client
            {
                ClientId = clientId1,
                IndustryId = 1,
                IndustrySubCategoryId = 1,
                SalesRepresentativeId = userId,
                ClientName = "clientName2",
                Notes = "notes2",
                SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "firstName2", LastName = "lastName2"},
                IsDeleted = false
            };
            return dummyClient;
        }

        private ClientContactUser GetAClientContactUser()
        {
            var dummyClientContactUser = new ClientContactUser
            {
                Id = clientContactUserId,
                CountryId = 1,
                StateId = 1,
                Client = new Client { ClientName = "clientName", Notes = "notes", SalesRepresentativeEmail = "abc@bcd.com" },
                PositionHeldbyContact = "positionHeldBy",
                Street = "street",
                City = "city",
                ZipCode = "zipCode",
                IsDeleted = false
            };
            return dummyClientContactUser;
        }

        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=caseId,
                    Client = GetADummyClient(),
                    ClientContactUser = GetAClientContactUser(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                },
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = GetADummyClient(),
                    ClientContactUser = GetAClientContactUser(),
                    ClientId=clientId2,
                    IsDeleted=false,
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                }
            };
            return dummyCase;
        }

        private Case GetADummyCase()
        {
            var dummyCase = new Case
            {
                CaseId = Guid.NewGuid(),
                Client = GetADummyClient(),
                ClientContactUser = GetAClientContactUser(),
                ClientId = clientId2,
                IsDeleted = false,
                AdditionalSalesRepNotes = "notes",
                ClientPO = "clientPO",
                ClientProvidedInformation = "hgdjmd",
                HoldReason = "holdReason",
                NickName = "matter",
                StateOfEmployment = new State { Country = new Country { CountryName = "india" } }
            };
            return dummyCase;
        }

        private Innostax.Common.Database.Models.UserManagement.User GetAssignTo()
        {
            var dummyAssignedTo = new Innostax.Common.Database.Models.UserManagement.User
            {
                FirstName = "firstName",
                LastName = "lastName",
                Email = "email",
                UserName = "username"
            };
            return dummyAssignedTo;
        }
        private List<Task> GetDummyTask()
        {
            var dummyTask = new List<Task>
            {
                new Task
                {
                    CaseId=caseId,
                    AssignedTo=userId,
                    DueDate=new DateTime(),
                    Status = 0,
                    Title="fgvb",
                    TaskId=Guid.NewGuid(),
                    Description="description",
                    AssignedToUser= GetAssignTo(),
                    IsDeleted=false,
                    Case = GetADummyCase()
                },
                new Task
                {
                    CaseId=caseId,
                    AssignedTo=new Guid("2D2958E1-54FD-E511-B5D7-FCAA14866D0F"),
                    DueDate=new DateTime(),
                    Status=0,
                    Title="title",
                    Description="details",
                    TaskId=taskId,
                     AssignedToUser= GetAssignTo(),
                    IsDeleted=false,
                     Case = GetADummyCase()
                }
            };
            return dummyTask;
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = Guid.NewGuid() }
                            };
            return dummyUser;
        }

        private List<TaskComment> GetDummyTaskComments()
        {
            var dummyTasksComments = new List<TaskComment>
            {
                new TaskComment
                {
                    Comment = "Comment",
                    Task = new Task { Description= "description", Title = "title" }
                }
            };
            return dummyTasksComments;
        }

        [TestMethod]
        public void GetAllCaseCorrespondingTaskDetails_WhenCaseIdIsNotFound_ThenReturnEmptyResult()
        {
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            var actual = sut.GetAllTasksByCaseId(options, new HttpRequestMessage(), new Guid());
            Assert.AreEqual(actual.Result.Count,0);
        }

        [TestMethod]
        public void GetAllCaseCorrespondingTaskDetails_WhenCaseIdIsValid_ThenTaskDetailsIsReturn()
        {
            var dummyTask = GetDummyCase();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyTask);
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            var actual = sut.GetAllTasksByCaseId(options, new HttpRequestMessage(), caseId);
            Assert.IsNotNull(actual.Result.Items);
        }

        [TestMethod]
        public void GivenCalling_ArchiveUnarchiveTask_WhenDataIsValid_MappedResultReturn()
        {

            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            try
            {
                sut.ArchiveUnarchiveTask(taskId,true);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetAllTaskForAUser_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            try
            {
                sut.GetAllTasksForAUser(options, new HttpRequestMessage(), userId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetAllTaskForAUser_WhenNoUserCorrespondingTaskIsFound_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());            
            var result=sut.GetAllTasksForAUser(options, new HttpRequestMessage(), Guid.NewGuid());
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void GetCalling_SearchTasksByKeyword_WhenKeywordIsFound_ReturnTasks()
        {
            var dummyTasks = GetDummyTask();
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(dummyTasks);
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(GetDummyTaskComments());
            var taskData = sut.SearchTasksByKeyword(options, new HttpRequestMessage(), "title",caseId);
            Assert.AreEqual(taskData.AllTasksCount, 2);
        }

        [TestMethod]
        public void GetCalling_SearchTasksByKeyword_WhenKeywordisNotFound_ThenResultIsEmpty()
        {
            var dummyTasks = GetDummyTask();
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(dummyTasks);
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(GetDummyTaskComments());
            var tasksData = sut.SearchTasksByKeyword(options, new HttpRequestMessage(), "jkgkjf", Guid.NewGuid());
            Assert.AreEqual(tasksData.AllTasksCount, 0);
        }
    }
}
