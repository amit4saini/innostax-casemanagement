﻿using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Models.EmailModels
{
    public class NewCaseEmailModel
    {
        public string CaseNumber { get; set; }
        public User User { get; set; }        
    }
}
