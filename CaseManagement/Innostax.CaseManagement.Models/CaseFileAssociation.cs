﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class CaseFileAssociation
    {
        public Guid CaseId { get; set; }
        public Case Case { get; set; }
        public FileModel UploadedFile { get; set; }
    }
}
