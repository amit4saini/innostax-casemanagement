﻿using CacheManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Infrastructure.Cache
{
    public interface ICacheManager
    {
        object Get(string key);
        void set(string key, object value, int expiresInMinutes);
    }

    public class CacheManager : ICacheManager
    {
        private ICacheManager<object> _cache;

        public CacheManager()
        {
            if (_cache == null)
            {
                _cache = CacheFactory.Build("cache", settings =>
                {
                    settings.WithSystemRuntimeCacheHandle("handleName");
                });
            }
        }

        public object Get(string key)
        {
            return _cache.Get(key);
        }

        public void set(string key, object value, int expiresInMinutes = 10)
        {
            var item = new CacheItem<object>(key, value, ExpirationMode.Absolute, TimeSpan.FromMinutes(expiresInMinutes));
            _cache.Add(item);
        }
    }
}
