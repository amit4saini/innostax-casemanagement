﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innostax.Common.Database.Enums;

namespace Innostax.Common.Database.Models.RolesAndPermissionManagement
{
    public class RolePermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RolePermissionId { get; set; }

        public Guid RoleId { get; set; }

        public Permission Permission { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }
    }
}
