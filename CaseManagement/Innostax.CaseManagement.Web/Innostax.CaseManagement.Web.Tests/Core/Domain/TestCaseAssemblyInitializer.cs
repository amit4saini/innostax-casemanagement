﻿using AutoMapper;
using Innostax.CaseManagement.Web.DependencyResolution;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TestCaseAssemblyInitializer
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            var profiles = IoC.Container.GetAllInstances<Profile>();
            Mapper.Initialize(cfg =>
            {
                foreach (var profile in profiles)
                    cfg.AddProfile(profile);
            });
        }
    }
}
