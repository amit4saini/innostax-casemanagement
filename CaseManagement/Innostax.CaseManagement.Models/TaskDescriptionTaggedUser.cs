﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class TaskDescriptionTaggedUser
    {  
            public Guid Id { get; set; }
            public Guid UserId { get; set; }
            public Guid TaskCommentId { get; set; }
        }
    }
