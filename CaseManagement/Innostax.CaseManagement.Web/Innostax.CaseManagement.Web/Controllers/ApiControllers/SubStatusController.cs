﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    public class SubStatusController : ApiController
    {
        private readonly ISubStatusService _caseSubStatusService;
        public SubStatusController(ISubStatusService caseSubStatusService)
        {
            _caseSubStatusService = caseSubStatusService;
        }

        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.CASE_READ_OWN, Permission.CASE_READ_ALL })]
        public List<SubStatus> Get()
        {
            return _caseSubStatusService.GetAllSubStatus();
        }

        [HttpGet]
        [Route("api/CaseStatus/{statusId}/CaseSubStatus")]
        public List<SubStatus> Get(Guid statusId)
        {
            return _caseSubStatusService.GetSubStatusByStatusId(statusId);
        }
    }
}
