namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsEligibleForInvoiceColumnToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "IsEligibleForInvoice", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "IsEligibleForInvoice");
        }
    }
}
