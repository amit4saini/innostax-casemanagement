using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using Innostax.Common.Database.Models.RolesAndPermissionManagement;
using Innostax.Common.Database.Models.UserManagement;
using Innostax.Common.Database.Models;

namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;
    using MigrationSeedData;

    public sealed class Configuration : DbMigrationsConfiguration<InnostaxDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InnostaxDb context)
        {
            var ddSupervisorId = AddDDSupervisorRole(context);
            //var userRoleId = AddUserRole(context);
            var salesRepRoleId = AddSalesRepRole(context);
            AddInCountryInvestigatorRole(context);
            AddInvestigatorRole(context);
            AddNFCRole(context);
            var clientParticipantRoleId = AddClientParticipantRole(context);
            var clientRoleId = AddClientRole(context);
            AddBillingRole(context);
            AddUsersAndUserRoles(context, ddSupervisorId, salesRepRoleId, clientRoleId, clientParticipantRoleId);
            AddRegions(context);
            AddCountries(context);
            AddStates(context);
            AddReports(context);
            AddIndustry(context);
            AddIndustrySubCategories(context);
            AddClientIndustry(context);
            AddDiscounts(context);
            AddTimeSheetDescriptions(context);
            AddCaseStatus(context);
            AddSubStatus(context);
            AddVendor(context);
        }

        private void AddTimeSheetDescriptions(InnostaxDb context)
        {
            if (!context.TimeSheetDescriptions.Any())
            {
                var timeSheetDescriptionList = new TimeSheetDescriptionList();
                var timeSheetDescriptions = timeSheetDescriptionList.TimeSheetDescription();
                foreach (var timeSheetDescription in timeSheetDescriptions)
                {
                    if (context.TimeSheetDescriptions.Count(x => x.Description == timeSheetDescription) == 0)
                    {
                        context.TimeSheetDescriptions.AddOrUpdate(new TimeSheetDescription { Description = timeSheetDescription });
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddUsersAndUserRoles(InnostaxDb context, Guid ddSupervisorRoleId, Guid salesRepRoleId, Guid clientRoleId, Guid clientParticipantRoleId)
        {
            var ddSupervisorId = Guid.Parse("aff6fcea-23e9-e511-82af-6036dd4de34b");
            var salesRepId = Guid.Parse("beb2314f-c8cf-4c2d-b31b-2ff85c43ee9c");
            var supervisorId = Guid.Parse("beb5424f-c8cf-4c2d-b31b-2ff87c43ee9c");
            var clientId = Guid.Parse("aeb5424f-c8cf-4c2d-b31b-2ff87c43ee9c");
            var clientParticipantId = Guid.Parse("beb5424f-c8cf-4c2d-b31b-2ff87c43ee9d");
            if (!context.Users.Any())
            {
                context.Users.AddOrUpdate(
                new User
                {
                    FirstName = "DDSupervisorFirstName",
                    LastName = "DDSupervisorLastName",
                    UserName = "ddsupervisor@vicesoftware.com",
                    Id = ddSupervisorId,
                    PasswordHash = "AMCvvQQAqe/yuE5ZTyaohOWYxny4o6QlA/fn1lDhTJrjlogBrJubIc1HzpZ8L7/grA==",
                    SecurityStamp = "0c767b3d-de4e-4210-a1bc-63be37e50934",
                    Email = "ddsupervisor@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                },
                new User
                {
                    FirstName = "ClientFirstName",
                    LastName = "ClientLastName",
                    UserName = "client@vicesoftware.com",
                    Id = clientId,
                    PasswordHash = "AMCvvQQAqe/yuE5ZTyaohOWYxny4o6QlA/fn1lDhTJrjlogBrJubIc1HzpZ8L7/grA==",
                    SecurityStamp = "0c767b3d-de4e-4210-a1bc-63be37e50934",
                    Email = "client@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                },
                new User
                {
                    FirstName = "SalesRepFirstName",
                    LastName = "SalesRepLastName",
                    UserName = "salesrep@vicesoftware.com",
                    Id = salesRepId,
                    PasswordHash = "AFcLOxXJXm/YxLHJUq2BZlPiHnuk6RODEy+E0JiAps/hmij39+e10tA0dSNPDbyVdw==",
                    SecurityStamp = "d4e34bb0-75ed-4734-940c-79aa685be06c",
                    Email = "salesrep@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                },
                new User
                {
                    FirstName = "ClientParticipantFirstName",
                    LastName = "ClientParticipantLastName",
                    UserName = "clientparticipant@vicesoftware.com",
                    Id = clientParticipantId,
                    PasswordHash = "AMCvvQQAqe/yuE5ZTyaohOWYxny4o6QlA/fn1lDhTJrjlogBrJubIc1HzpZ8L7/grA==",
                    SecurityStamp = "0c767b3d-de4e-4210-a1bc-63be37e50934",
                    Email = "clientparticipant@vicesoftware.com",
                    LastEditedDateTime = DateTime.UtcNow
                }
            );
            }
            if (!context.UserRoles.Any())
            {
                context.UserRoles.AddOrUpdate(
                new UserRole
                {
                    RoleId = ddSupervisorRoleId,
                    UserId = ddSupervisorId
                },
                new UserRole
                {
                    RoleId = salesRepRoleId,
                    UserId = salesRepId
                },
                new UserRole
                {
                    RoleId = clientRoleId,
                    UserId = clientId
                },
                new UserRole
                {
                    RoleId = clientParticipantRoleId,
                    UserId = clientParticipantId
                }
            );
            }
            var existingSuperVisorList = context.UserRoles.Where(x => x.RoleId == supervisorId).ToList();
            foreach (var existingSuperVisor in existingSuperVisorList)
            {
                existingSuperVisor.RoleId = ddSupervisorId;
                context.UserRoles.AddOrUpdate(existingSuperVisor);
            }
        }

        private static Guid AddDDSupervisorRole(InnostaxDb context)
        {
            RenameAdminRole(context);
            var roleName = Roles.DDSupervisor.ToString();
            var roleId = Guid.Parse("1aa58124-c50f-4773-ad62-694fad3ff939");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_DELETE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_UPDATE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_DELETE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_DELETE },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_UPDATE },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_TIMESHEET_COST },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_OWN_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_ALL_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ICIUSER },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_PARTICIPANT },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_DASHBOARD },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_FILES },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_TASKS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_TASKS },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_HISTORY },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CALENDARS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ANALYTICS },
                new RolePermission { RoleId = roleId, Permission = Permission.UPLOAD_CASE_FILE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.ASSIGN_TO_USER },
                new RolePermission { RoleId = roleId, Permission = Permission.DOWNLOAD_ALL_FILES },
                new RolePermission { RoleId = roleId, Permission = Permission.DOWNLOAD_OWN_FILES },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_GET_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                new RolePermission { RoleId=roleId, Permission = Permission.CAN_DELETE_A_TASK},
                new RolePermission { RoleId=roleId, Permission = Permission.CAN_DELETE_A_DISCUSSION},
                new RolePermission { RoleId=roleId, Permission = Permission.CASE_INVOICE_APPROVE },
                new RolePermission { RoleId = roleId, Permission = Permission.NFCUSER_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASEHEADER_IHI },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_TIMESHEET_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_EXPENSE_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_STATUS_BUTTONS_SIDEBAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_SEND_TO_NFC_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_APPROVE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_INFORMATION_PENDING_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_REQUEST_UPDATE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_IN_QC_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_REPORT_TO_SALES_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_QC_REVIEW },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_STATUS_SIDEBAR_POSITION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_QC_FIELDS_VALIDATION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_USERS_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_CLIENT_PARTICIPANT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_ICI_USER_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_MARK_AS_REPORT_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ARCHIVE_DISCUSSION_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_CLIENTNAME },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_REPORTTYPE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_SALES_REP },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_INTAKE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_MARGIN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_GLOBAL_SEARCH_BAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_DELETE_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_USERS },
                new RolePermission { RoleId = roleId, Permission = Permission.EDIT_IN_QC_STATUS },
                new RolePermission { RoleId = roleId, Permission = Permission.EDIT_INFO_PENDING_STATUS },
                new RolePermission { RoleId = roleId, Permission = Permission.EDIT_CLIENT_REQUEST_UPDATE_STATUS },
                new RolePermission { RoleId = roleId, Permission = Permission.EDIT_ON_HOLD_STATUS },
                new RolePermission { RoleId = roleId, Permission = Permission.EDIT_CANCEL_STATUS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DUEDATE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_FILTER_DROPDOWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_READY_FOR_INVOICE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ICI_REPORT_RECEIVED_BUTTON },

              };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }

        private static Guid AddClientRole(InnostaxDb context)
        {
            RenameSupervisorRole(context);
            var roleName = Roles.Client.ToString();
            var roleId = Guid.Parse("1aa58754-c50f-4773-ad62-694fad3ff727");
            var listOfRolePermission = new List<RolePermission>
            {
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_CREATE },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_OWN },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_DELETE_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_CREATE },
                //new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_UPDATE_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_READ_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_DELETE_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.USER_DELETE },
                //new RolePermission { RoleId = roleId, Permission = Permission.USER_CREATE },
                //new RolePermission { RoleId = roleId, Permission = Permission.USER_READ_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.USER_UPDATE },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_OWN },
                //new RolePermission { RoleId = roleId, Permission = Permission.ADD_TIMESHEET_COST },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_OWN_TIMESHEET },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_ALL_TIMESHEET },
                //new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ICIUSER },
                //new RolePermission { RoleId = roleId, Permission = Permission.INVITE_PARTICIPANT },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_DASHBOARD },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_FILES },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_DISCUSSIONS },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_TASK },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_TASKS },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_TASKS },
                //new RolePermission { RoleId = roleId, Permission = Permission.ADD_TIMESHEET },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_TIMESHEET },
                //new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_HISTORY },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CALENDARS },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ANALYTICS },
                //new RolePermission { RoleId = roleId, Permission = Permission.UPLOAD_CASE_FILE },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_TASK },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_DISCUSSION },
                //new RolePermission { RoleId = roleId, Permission = Permission.ASSIGN_TO_USER },
                //new RolePermission { RoleId = roleId, Permission = Permission.DOWNLOAD_ALL_FILES },
                //new RolePermission { RoleId = roleId, Permission = Permission.DOWNLOAD_OWN_FILES },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY },
                //new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                //new RolePermission { RoleId = roleId, Permission = Permission.USER_GET_ALL },
                //new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_A_TASK},
                //new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_A_DISCUSSION},
                //new RolePermission { RoleId=roleId, Permission = Permission.CASE_INVOICE_APPROVE },
                //new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS }
            };
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }
        private static void RenameSupervisorRole(InnostaxDb context)
        {
            var roleName = "Supervisor";
            var existingSupervisorRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingSupervisorRole != null)
            {
                existingSupervisorRole.Name = Roles.Client.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameAdminRole(InnostaxDb context)
        {
            var roleName = "Admin";
            var existingAdminRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingAdminRole != null)
            {
                existingAdminRole.Name = Roles.DDSupervisor.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameParticipantRole(InnostaxDb context)
        {
            var roleName = "Participant";
            var existingParticipantRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingParticipantRole != null)
            {
                existingParticipantRole.Name = Roles.Client_Participant.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameIHIUserRole(InnostaxDb context)
        {
            var roleName = "IHIUser";
            var existingParticipantRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingParticipantRole != null)
            {
                existingParticipantRole.Name = Roles.Investigator.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameICIUserRole(InnostaxDb context)
        {
            var roleName = "ICIUser";
            var existingICIUserRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingICIUserRole != null)
            {
                existingICIUserRole.Name = Roles.In_Country_Investigator.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameNFCUserRole(InnostaxDb context)
        {
            var roleName = "NFCUser";
            var existingNFCUserRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingNFCUserRole != null)
            {
                existingNFCUserRole.Name = Roles.NFC.ToString();
            }
            context.SaveChangesWithErrors();
        }

        private static void RenameAccountantRole(InnostaxDb context)
        {
            var roleName = "Accountant";
            var existingNFCUserRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingNFCUserRole != null)
            {
                existingNFCUserRole.Name = Roles.Billing.ToString();
            }
            context.SaveChangesWithErrors();
        }
        private static void RenameSalesRole(InnostaxDb context)
        {
            var roleName = "Sales";
            var existingSalesRole = context.Roles.FirstOrDefault(x => x.Name == roleName);
            if (existingSalesRole != null)
            {
                existingSalesRole.Name = Roles.Sales_Rep.ToString();
            }
            context.SaveChangesWithErrors();
        }

        //private static Guid AddUserRole(KrellerDb context)
        //{
        //    string roleName;
        //    Guid roleId;
        //    roleName = Roles.User.ToString();
        //    roleId = Guid.Parse("cb7b51f1-9e76-4986-b829-dc19b85c2329");
        //    if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
        //    {
        //        context.Roles.AddOrUpdate(
        //        new Role { Id = roleId, Name = roleName }
        //        );
        //    }
        //    var listOfRolePermission =
        //          new List<RolePermission> {
        //        new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
        //        new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_OWN },
        //        new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_OWN },
        //        new RolePermission { RoleId = roleId, Permission = Permission.CASE_OWN_TIMESHEET },
        //        new RolePermission { RoleId = roleId, Permission = Permission.VIEW_DASHBOARD },
        //        new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_FILES }
        //     };
        //    foreach (var rolePermission in listOfRolePermission)
        //    {
        //        if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
        //        {
        //            context.RolePermissions.AddOrUpdate(rolePermission);
        //        }
        //    }
        //    return roleId;
        //}

        private static void AddInCountryInvestigatorRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameICIUserRole(context);
            roleName = Roles.In_Country_Investigator.ToString();
            roleId = Guid.Parse("cb7b51f1-9e76-4986-b829-dc19b85c3287");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_TIMESHEET_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_EXPENSE_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_USERS_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_CLIENT_PARTICIPANT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_ICI_USER_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_CLIENTNAME },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_REPORTTYPE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_SALES_REP },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_INTAKE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_GLOBAL_SEARCH_BAR },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_GET_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_FILTER_DROPDOWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_REPORT_TURN_AROUND_DATE_END_BUTTON }
                };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
        }

        private static void AddBillingRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameAccountantRole(context);
            roleName = Roles.Billing.ToString();
            roleId = Guid.Parse("cb7b51f1-9e76-4986-b829-dc19b85c3567");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_ALL_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_OWN_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_FILTER_DROPDOWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
        }

        private static void AddInvestigatorRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameIHIUserRole(context);
            roleName = Roles.Investigator.ToString();
            roleId = Guid.Parse("810737C6-C4A0-4702-ABC8-F3CE89BCE2DC");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                 new RolePermission { RoleId = roleId, Permission = Permission.CASE_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_DELETE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_UPDATE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_DELETE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_FILES },                
                new RolePermission { RoleId = roleId, Permission = Permission.UPLOAD_CASE_FILE },
                new RolePermission { RoleId = roleId, Permission = Permission.DOWNLOAD_OWN_FILES },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_TASKS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_OWN_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_GET_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASEHEADER_IHI },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_TIMESHEET_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_EXPENSE_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_STATUS_BUTTONS_SIDEBAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_SEND_FOR_REVIEW_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_STATUS_SIDEBAR_POSITION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_USERS_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_CLIENT_PARTICIPANT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_ICI_USER_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_HISTORY },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_CLIENTNAME },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_REPORTTYPE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_SALES_REP },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_INTAKE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_GLOBAL_SEARCH_BAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_USERS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_TASKS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ALL_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_ALL_TIMESHEET },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_ALL_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_INFORMATION_PENDING_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_REQUEST_UPDATE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_IN_QC_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_FILTER_DROPDOWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_READY_FOR_INVOICE_BUTTON },
                };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }

        }

        private static void AddNFCRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameNFCUserRole(context);
            roleName = Roles.NFC.ToString();
            roleId = Guid.Parse("C8B7A4D9-5FB4-46A5-8498-FEF98F61F018");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
               new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_TASKS },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
               new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
               new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
               new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
               new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
               new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
               new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },

                };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
        }

        private static Guid AddSalesRepRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameSalesRole(context);
            roleName = Roles.Sales_Rep.ToString();
            roleId = Guid.Parse("8b68991c-0bd3-4ee7-a5ed-f7377746e13b");
            if (context.Roles.FirstOrDefault(x => x.Id == roleId) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
            }
            var listOfRolePermission = new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_CREATE },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_UPDATE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CLIENT_DELETE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_UPDATE_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_DELETE_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.NFCUSER_READ_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_NFCUSER },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CALENDAR },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_TASKS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_TASK },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_TAG_USER_ON_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.ADD_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_DELETE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_UPDATE_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_OWN_CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_EXPENSE },
                new RolePermission { RoleId = roleId, Permission = Permission.USER_GET_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.GET_ALL_ROLES },
                new RolePermission { RoleId = roleId, Permission = Permission.INVITE_ALL },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_EXPENSE_ACTION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_VALIDATION_SIDEBAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_STATUS_BUTTONS_SIDEBAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_READY_FOR_INVOICE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_SEND_FOR_APRROVAL_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_USERS_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_CLIENT_PARTICIPANT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_NFC_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_HISTORY },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_CLIENTNAME },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_REPORTTYPE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_SALES_REP },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_HEADER_INTAKE },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_GLOBAL_SEARCH_BAR },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_USER_TASKS_LIST },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_NICKNAME_VALIDATION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_INFORMATION_PENDING_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CLIENT_REQUEST_UPDATE_BUTTON },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_HISTORY_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ONHOLD_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CANCEL_REASONS },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_FILTER_DROPDOWN },
            };
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }

        private static Guid AddClientParticipantRole(InnostaxDb context)
        {
            string roleName;
            Guid roleId;
            RenameParticipantRole(context);
            roleName = Roles.Client_Participant.ToString();
            roleId = Guid.NewGuid();
            List<RolePermission> listOfRolePermission = new List<RolePermission>();
            if (context.Roles.FirstOrDefault(x => x.Name == roleName) == null)
            {
                context.Roles.AddOrUpdate(
                new Role { Id = roleId, Name = roleName }
                );
                listOfRolePermission =
                    new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.REQUEST_CASE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_DISCUSSION_REPORT},
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS_CLIENTINFO },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_NOTIFIERS_CLIENT_PARTICIPANT },
                
                 };
            }
            else
            {
                roleId = context.Roles.FirstOrDefault(x => x.Name == roleName).Id;
                listOfRolePermission =
                  new List<RolePermission> {
                new RolePermission { RoleId = roleId, Permission = Permission.REQUEST_CASE },
                new RolePermission { RoleId = roleId, Permission = Permission.CASE_READ_OWN },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_VIEW_OWN_DISCUSSIONS },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_CREATE_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.CAN_EDIT_DISCUSSION },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_TAB },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_DISCUSSION_REPORT},
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_CASE_DETAILS_CLIENTINFO },
                new RolePermission { RoleId = roleId, Permission = Permission.VIEW_ADD_NOTIFIERS_CLIENT_PARTICIPANT },
               };
            }
            foreach (var rolePermission in listOfRolePermission)
            {
                if (context.RolePermissions.Count(x => x.RoleId == rolePermission.RoleId && x.Permission == rolePermission.Permission) == 0)
                {
                    context.RolePermissions.AddOrUpdate(rolePermission);
                }
            }
            return roleId;
        }

        private void AddRegions(InnostaxDb context)
        {
            if (!context.Regions.Any())
            {
                var regionList = new RegionList();
                var regions = regionList.Region();
                foreach (var region in regions)
                {
                    if (context.Regions.Count(x => x.RegionName == region) == 0)
                    {
                        context.Regions.AddOrUpdate(new Region { RegionName = region });
                    }
                }
                context.SaveChanges();
            }
        }
        private void AddIndustry(InnostaxDb context)
        {
            if (!context.Industries.Any())
            {
                var IndustryList = new IndustryList();
                var industries = IndustryList.Industry();
                foreach (var industry in industries)
                {
                    if (context.Industries.Count(x => x.Name == industry) == 0)
                    {
                        context.Industries.AddOrUpdate(new Industry { Name = industry, Description = "Dummy Description" });
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddVendor(InnostaxDb context)
        {
            var vendorList = new VendorList();
            foreach (var vendor in vendorList.GetVendorList())
            {
                if (context.Vendors.Count(x => x.VendorName == vendor.VendorName) == 0)
                {
                    context.Vendors.AddOrUpdate(vendor);
                }
                else
                {
                    var existingVendor = context.Vendors.FirstOrDefault(x => x.VendorName == vendor.VendorName);
                    if (existingVendor.Cost != vendor.Cost)
                    {
                        vendor.VendorId = existingVendor.VendorId;
                        context.Vendors.AddOrUpdate(vendor);
                    }
                }
            }
            context.SaveChanges();
        }

        private void AddClientIndustry(InnostaxDb context)
        {
            if (!context.ClientIndustries.Any())
            {
                var clientIndustryList = new ClientIndustryList();
                var clientIndustries = clientIndustryList.ClientIndustry();
                foreach (var clientIndustry in clientIndustries)
                {
                    if (context.ClientIndustries.Count(x => x.Name == clientIndustry) == 0)
                    {
                        context.ClientIndustries.AddOrUpdate(new ClientIndustry { Name = clientIndustry });
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddIndustrySubCategories(InnostaxDb context)
        {
            if (!context.IndustrySubCategories.Any())
            {
                var IndustrySubCategoryList = new IndustrySubCategoryList();
                var industrySubCategories = IndustrySubCategoryList.IndustrySubCategory();
                foreach (var industrySubCategory in industrySubCategories)
                {
                    if (context.IndustrySubCategories.Count(x => x.Name == industrySubCategory) == 0)
                    {
                        context.IndustrySubCategories.AddOrUpdate(new IndustrySubCategory { Name = industrySubCategory });
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddCountries(InnostaxDb context)
        {
            if (!context.Countries.Any())
            {
                var countryList = new CountryList();
                var countries = countryList.Country();
                foreach (var country in countries)
                {
                    var countryDetails = country.Split('~');
                    var countryName = countryDetails[0];
                    var regionName = countryDetails[1];
                    var region = context.Regions.FirstOrDefault(x => x.RegionName == regionName);
                    if (region != null)
                    {
                        if (context.Countries.Count(x => x.CountryName == countryName) == 0)
                        {
                            context.Countries.AddOrUpdate(new Country { CountryName = countryName, RegionId = region.RegionId });
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddStates(InnostaxDb context)
        {
            if (!context.States.Any())
            {
                var stateList = new StateList();
                var states = stateList.States();
                foreach (var state in states)
                {
                    var stateDetails = state.Split('~');
                    var stateName = stateDetails[0];
                    var countryName = stateDetails[1];

                    var country = context.Countries.FirstOrDefault(x => x.CountryName == countryName);
                    if (country != null)
                    {
                        if (context.States.Count(x => x.StateName == stateName) == 0)
                        {
                            context.States.AddOrUpdate(new State { StateName = stateName, CountryId = country.CountryId });
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        private void AddReports(InnostaxDb context)
        {
            if (!context.Reports.Any())
            {
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Level A",
                    Description = "15-18 business days\r\n\r\nFor a Subject Company, research includes one(1) principal and the following: \r\n\r\n� Verification of Subject Company�s corporate registration at local registry\r\n� Research to identify Business Affiliations such as directorships, partnerships, shareholdings, subsidiaries, parent companies, branches, etc.\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Criminal records check in local jurisdiction, where publicly available\r\n� Legal filings check of available civil records & bankruptcies in local jurisdiction\r\n� Research of pertinent government and industry - based regulatory agencies\r\n� Basic check of Subject Company and principal�s government and political affiliations\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)\r\n\r\nFor an Subject Individual, research includes(may require consent and curriculum vitae):\r\n\r\n� Verification of Subject�s identifying information such as DOB, ID number, and registered address through available resources in country\r\n� Research to identify Business Affiliations such as directorships, partnerships, and shareholdings, etc.\r\n� Employment verification\r\n� Licensure verification\r\n� Education verification\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Criminal records check in local jurisdiction, where publicly available\r\n� Legal filings check of available civil records & bankruptcies in local jurisdiction\r\n� Research of pertinent government and industry - based regulatory agencies\r\n� Basic check of Subject�s government and political affiliations\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)",
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Level B",
                    Description = "20-25 business days\r\n\r\nUsually conducted on a Subject Company, research includes one(1) principal.The following is in addition to research conducted for a Level A: \r\n\r\n� Expanded financial research on Subject Company(credit report & financial statements where available)\r\n� Comprehensive check of Subject Company and principal�s government and political affiliations\r\n� Character and reputation comments developed though discreet contact within local business community\r\n� Site visit verification and photographic confirmation of Subject Company�s location\r\n� Property ownership research regarding Subject Company�s location, where publicly available\r\n� Focused open source research to locate corporate entity affiliations with the Sanctioned Countries of Sudan, North Korea, Cuba, Iran and Syria\r\n� Basic check of government affiliations for Subject Company�s 3 largest shareholders or parent company\r\n� Targeted derogatory media research on Subject Company�s 3 largest shareholders or parent company\r\n\r\nFor an Individual, in addition to research conducted for a Level A research includes(may require consent and curriculum vitae):\r\n\r\n� Expanded financial research on Subject through available resources(requires consent in most jurisdictions)\r\n� Comprehensive check of Subject�s government and political affiliations\r\n� Character and reputation comments developed through discreet contact within local business community\r\n� Site visit verification and photographic confirmation of Subject�s registered / reported address\r\n� Property ownership research regarding Subject�s registered / reported address, where publicly available\r\n� Focused open source research to identify possible ties to the Sanctioned Countries of Sudan, North Korea, Cuba, Iran and Syria\r\n� Basic check of government and political affiliations for up to 3 of Subject�s major business affiliations\r\n� Targeted derogatory media research on up to 3 of Subject�s major business affiliations"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Level C",
                    Description = "5-10 business days\r\n\r\nUsually on a Subject Company, research includes:\r\n\r\n� Open source research(Nexis, media, etc.) to obtain company details\r\n� Review of the company's website and internet presence\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Pre-Employment",
                    Description = "5-18 business days\r\n\r\nResearch on a Subject Individual, signed consent required and may require curriculum vitae.Research includes: \r\n\r\n� Verification of Subject�s identifying information such as DOB, ID number, and registered address through available resources in country\r\n� Research to identify Business Affiliations such as directorships, partnerships, and shareholdings, etc.\r\n� Employment verification\r\n� Licensure verification\r\n� Education verification\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Criminal records check in local jurisdiction, where publicly available\r\n� Legal filings check of available civil records and bankruptcies in local jurisdiction\r\n� Research of pertinent government and industry - based regulatory agencies\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Media/DPL",
                    Description = "5-7 business days\r\n\r\nOn a Subject Company or Subject Individual, research includes:\r\n\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Media/DPL + Government Affiliations",
                    Description = "5-7 business days\r\n\r\nOn a Subject Company or Subject Individual, research includes:\r\n\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)\r\n� Basic check of Subject Company / Subject's government and political affiliations."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "DPL-Single",
                    Description = "3 business days\r\n\r\nComprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) on one company or individual."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "DPL-Multiple",
                    Description = "Turn Time TBD\r\n\r\nComprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) on multiple companies or individuals."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Asset Investigation",
                    Description = "Turn Time TBD\r\n\r\nPrior to submitting this case, the scope of the case and Kreller�s capabilities pertaining to the case must be approved by the Investigations Department.Specific instructions from the client must be provided in the Sales Rep Notes and / or Client Provided Information section(s)."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Broad Media Sweep",
                    Description = "Turn Time TBD\r\n\r\nConducted on a large, well known business / entity(for example, UPS).Media research is narrowed to a specific time frame and specific keywords with a focus on the most significant / derogatory issues."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Conflict of Interest",
                    Description = "Turn Time TBD\r\n\r\nPrior to submitting this case, the scope of the case and Kreller�s capabilities pertaining to the case must be approved by the Investigations Department.Specific instructions from the client must be provided in the Sales Rep Notes and / or Client Provided Information section(s)."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Corporate Registration",
                    Description = "Turn Time TBD\r\n\r\nVerification of a Subject Company�s corporate registration at the local registry."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Civil/Criminal",
                    Description = "Turn Time TBD\r\n\r\nCriminal records check in local jurisdiction, where publicly available; Legal filings check of available civil records and bankruptcies in local jurisdiction."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Civil",
                    Description = "Turn Time TBD\r\n\r\nLegal filings check of available civil records and bankruptcies in local jurisdiction."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Criminal",
                    Description = "Turn Time TBD\r\n\r\nCriminal records check in local jurisdiction, where publicly available."
                });

                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Open Source Intelligence Report (New)",
                    Description = "5-7 Business Days\r\n\r\nResearch on a Subject Company only.A principal is included for an additional fee.Research on the Subject Company is conducted through open source intelligence records including publicly available databases, Internet and media sources for information regarding:\r\n\r\n� Company details\r\n� Business affiliations\r\n� References to derogatory information\r\n� Criminal activity history\r\n� Litigation / Lawsuit history\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.), including Politically Exposed Persons & Entities"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Open Source Intelligence Report (Original)",
                    Description = "15-18 business days\r\n\r\nResearch on a Subject Company and principal through open source intelligence records, including publicly available databases, Internet and media sources for information regarding:\r\n\r\n� Company registration, principals / shareholders, company history\r\n� Products, markets and clients\r\n� Business Affiliations such as directorships, partnerships, shareholdings, subsidiaries, parent companies, etc. (company only)\r\n� References to derogatory information regarding Subject Company and principal \r\n� Criminal activity related to Subject Company and principal\r\n� Litigation related to Subject Company and principal\r\n� Regulatory issues in jurisdiction where Subject Company is being vetted(including sanctions lists)\r\n� Government / political affiliations on Subject Company and principal\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) on Subject Company and principal"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Government Affiliations",
                    Description = "5-7 business days\r\n\r\nResearch conducted on one(1) name, does not include a principal.Research includes:\r\n\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Sanctioned Countries",
                    Description = "Turn Time TBD\r\n\r\nFocused open source research on one Subject Company or Subject Individual to locate affiliations with the Sanctioned Countries of Sudan, North Korea, Cuba, Iran and Syria."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Site Visit",
                    Description = "Turn Time TBD\r\n\r\nSite visit verification and photographic confirmation(where possible) of a company."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Media Monitoring",
                    Description = "Turn Time TBD\r\n\r\nWeekly or monthly monitoring for negative information pertaining to a specific company or individual through open source media review and analysis of domestic and foreign press sources.Depending on the nature of the situation being monitoring and / or the online presence of the company / indivdiual, research may be focused using specific keywords."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Aon",
                    Description = "Turn Time TBD\r\n\r\nSpecific report format provided by Aon, USA only."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Noble/Paragon Level A",
                    Description = "15-18 business days\r\n\r\nA standard Level A report on a Subject Company with a principal, including the following additional research: \r\n\r\n� Additional DPL names(pricing determined by the number of names)\r\n� References\r\n� Information from the US Department of Commerce Country Commercial Guide regarding corruption and doing business in the country in which the report is being conducted\r\n� Contact with the relevant US Department of State desk officer to conduct a reputation inquiry regarding the Subject Company \r\n� Summary of information provided by the US State Department's website regarding corruption in the country in which the report is being conducted"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Cameron Level A",
                    Description = "15-18 business days\r\n\r\nA standard Level A report on a Subject Company with a principal, including the following additional research: \r\n\r\n� References\r\n� Public Profile & Reputation Analysis determined by the derogatory information located, includes a risk level(High, Medium, Low)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Cameron Medium Risk",
                    Description = "8-12 business days\r\n\r\nResearch on a Subject Company and principal which includes:\r\n\r\n� Verification of Subject Company�s corporate registration at local registry \r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Anadarko Level 1",
                    Description = "Turn Time TBD\r\n\r\nResearch on a Subject Individual which includes:\r\n\r\n� Criminal records check in local jurisdiction, where publicly available(with consent)\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Anadarko Level 2",
                    Description = "15-20 business days\r\n\r\nResearch on a Subject Individual which includes:\r\n\r\n� Education verification(if institution is provided)\r\n� Criminal records check in local jurisdiction, where publicly available (with consent)\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Anadarko Level 2.5",
                    Description = "15-20 Business Days\r\n\r\nResearch on a Subject Individual which includes:\r\n\r\n� References\r\n� Employment verification\r\n� Education verification(if institution is provided)\r\n� Criminal records check in local jurisdiction, where publicly available (with consent)\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Anadarko Level 3",
                    Description = "15-20 Business Days\r\n\r\nResearch on a Subject Individual which includes:\r\n\r\n� References\r\n� Research to identify Business Affiliations such as directorships, partnerships, and shareholdings, etc.\r\n� Employment verification\r\n� Education verification(if institution is provided)\r\n� Licensure verification (if applicable)\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Criminal records check in local jurisdiction, where publicly available(with consent)\r\n� Legal filings check of available civil records and bankruptcies in local jurisdiction\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Baker Hughes with References",
                    Description = "Research on a Subject Company which includes a principal and the following:\r\n\r\n� Verification of Subject Company�s corporate registration at local registry\r\n� Research to identify Business Affiliations such as directorships, partnerships, shareholdings, subsidiaries, parent companies, branches, etc.\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Research to identify Business Affiliations such as directorships, partnerships, shareholdings, subsidiaries, parent companies, branches, etc.\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) for company and shareholders\r\n� References(2 business / person and 1 bank)\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) for company and shareholders\r\n� Legal filings check of available civil records and bankruptcies in local jurisdiction for recertification requests.\r\n\r\n\r\nThe following research is conducted on the principal: \r\n\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) for company and shareholders\r\n� Criminal records check in local jurisdiction, where publicly available(with consent)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Papa Johns Patriot Act",
                    Description = "The following research, conducted on an individual:\r\n\r\n� Verification of Subject�s identifying information such as DOB, ID number, and registered address through available resources in country\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Criminal records check in local jurisdiction, where publicly available(with consent)\r\n� Legal filings check of available civil records and bankruptcies in local jurisdiction\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.)\r\n\r\nFor a company, a full Level A is typically conducted."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Miller Thomson Legal",
                    Description = "Legal filings check of available civil records and bankruptcies in local jurisdiction, includes tax liens if available."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Getz Prince Wells Legal",
                    Description = "The following research, conducted on a company or individual:\r\n\r\n� Criminal records check in local jurisdiction, where publicly available\r\n� Legal filings check of available civil records(including tax liens, if available) and bankruptcies in local jurisdictions\r\n\r\nFor US cases, client is charged per jurisdiction -research is only conducted in those jurisdictions specified by the client."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Helmerich & Payne Renewal",
                    Description = "The following research, conducted on a company or individual:\r\n\r\n� Verification of Subject Company�s corporate registration at local registry OR Verification of Subject�s identifying information such as DOB, ID number, and registered address through available resources in country\r\n� Research to identify Business Affiliations such as directorships, partnerships, shareholdings, subsidiaries, parent companies, branches, etc.\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Research of pertinent government and industry - based regulatory agencies\r\n� Basic check of government and political affiliations through media and business database resources\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) \r\n� US database and / or foreign publicly available records research"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "MI Group",
                    Description = "For a company only (does not include a principal), research includes the following: \r\n\r\n� Verification of Subject Company�s corporate registration at local registry, including ownership / senior management\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) for company and shareholders\r\n\r\n\r\nFor a principal, research includes the following: \r\n\r\n� Review and analysis of domestic and foreign press sources for information of a derogatory nature\r\n� Comprehensive government sanctions and watch lists check(OFAC, FCPA, BIS, etc.) for company and shareholders\r\n� Research to identify Business Affiliations such as directorships, partnerships, and shareholdings, etc.\r\n� Criminal records check in local jurisdiction, where publicly available(with consent)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Custom",
                    Description = "Turn Time TBD\r\n\r\nOffers numerous possibilties of research conducted on companies and / or individuals.Prior to submitting this case, the scope of the case and Kreller�s capabilities pertaining to such a case must be discussed with the Investigations department.Specific instructions from the client must be provided in the Sales Rep Notes and / or Client Provided Information section(s)."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Charitable Organizations",
                    Description = "Research includes verification of the corporate organization (board, officers), functioning place of business and 501(c)(3) non-profit organization status. Review of corporate website for program expenses (% of budget spend on programs/services), administrative expenses, Form 990 info (independent board voting members, available financial info, CEO salary, board compensation), donor privacy policy, board members, key staff, and financials."
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Schnitzer Steel Level II",
                    Description = "Everything from Level I plus: \r\n\r\n� Expanded background and financial info on the Subject\r\n� In - country inquiries conducted with local business communities\r\n� Site visit\r\n� Property record checks(where available)"
                });
                context.Reports.AddOrUpdate(new Report
                {
                    ReportType = "Other",
                    Description = ""
                });
                context.SaveChanges();
            }
        }

        private void AddDiscounts(InnostaxDb context)
        {
            if (!context.Discounts.Any())
            {
                var discountList = new DiscountList();
                var discounts = discountList.Discount();
                foreach (var discount in discounts)
                {
                    if (context.Discounts.Count(x => x.DiscountAmount == discount) == 0)
                    {
                        context.Discounts.AddOrUpdate(new Discount { DiscountAmount = discount });
                    }
                }
                context.SaveChanges();
            }
        }
        private void AddCaseStatus(InnostaxDb context)
        {
            var caseStatusList = new CaseStatusList();
            var allCaseStatus = caseStatusList.CaseStatus();
            foreach (var eachCaseStatus in allCaseStatus)
            {
                if (context.CaseStatus.Count(x => x.Status == eachCaseStatus.Status) == 0)
                {
                    context.CaseStatus.AddOrUpdate(eachCaseStatus);
                }
            }
            context.SaveChanges();
        }

        private void AddSubStatus(InnostaxDb context)
        {
            var subStatusList = new SubStatusList();
            var allSubStatus = subStatusList.SubStatus();
            foreach (var eachSubStatus in allSubStatus)
            {
                var eachSubStatusDetails = eachSubStatus.Split('~');
                var subStatus = eachSubStatusDetails[0];
                var status = eachSubStatusDetails[1];

                var caseStatus = context.CaseStatus.FirstOrDefault(x => x.Status == status);
                if (caseStatus != null)
                {
                    if (context.SubStatus.Count(x => x.Name == subStatus && x.StatusId == caseStatus.Id) == 0)
                    {
                        if (caseStatus != null)
                        {
                            context.SubStatus.AddOrUpdate(new SubStatus { Name = subStatus, StatusId = caseStatus.Id });

                        }
                    }
                }
            }
            context.SaveChanges();
        }
    }
}

