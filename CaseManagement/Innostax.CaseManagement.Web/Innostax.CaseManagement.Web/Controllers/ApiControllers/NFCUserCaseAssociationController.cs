﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    public class NFCUserCaseAssociationController : ApiController
    {
            private readonly INFCUserCaseAssociationService _nfcUserCaseAssociationService;
            public NFCUserCaseAssociationController(INFCUserCaseAssociationService nfcUserCaseAssociationService)
            {
            _nfcUserCaseAssociationService = nfcUserCaseAssociationService;
            }

            [HttpGet]
            public List<NFCUserCaseAssociation> Get(Guid id)
            {
                return _nfcUserCaseAssociationService.GetNFCUserCaseAssociationsByCaseId(id);
            }

            [HttpGet]
            [Route("api/NFCUserCaseAssociation/{id}/Cases")]
            public List<Case> GetCasesByNFCUserId(Guid id)
            {
                return _nfcUserCaseAssociationService.GetCasesByNFCUserId(id);
            }

            [HttpPost]
            [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
            public void Post(NFCUserCaseAssociation nfcUserCaseAssociationData)
            {
                _nfcUserCaseAssociationService.Save(nfcUserCaseAssociationData);
            }

            [HttpPut]
            [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
            public void Put(int id, NFCUserCaseAssociation nfcUserCaseAssociationData)
            {
                nfcUserCaseAssociationData.Id = id;
                _nfcUserCaseAssociationService.Save(nfcUserCaseAssociationData);
            }
        }
    }
