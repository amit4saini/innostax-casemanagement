﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Core.Domain;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class TimeSheetDescriptionController:ApiController
    {
        private readonly ITimeSheetDescriptionService _timeSheetDescriptionService;
        public TimeSheetDescriptionController(ITimeSheetDescriptionService timeSheetDescriptionService)
        {
            _timeSheetDescriptionService = timeSheetDescriptionService;
        }

        [HttpGet]
        public List<TimeSheetDescription> Get()
        {
            return _timeSheetDescriptionService.GetAllTimeSheetDescriptions();
        }
    }
}