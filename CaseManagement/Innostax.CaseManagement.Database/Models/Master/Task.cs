﻿using Innostax.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innostax.Common.Database.Enums;
namespace Innostax.Common.Database.Models
{
    public class Task
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public System.Guid TaskId { get; set; }
        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TaskStatusType Status { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public Guid AssignedTo { get; set; }
        [ForeignKey("AssignedTo")]
        public User AssignedToUser { get; set; }

        public Guid CreatedBy { get; set; }
         [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid EditedBy { get; set; }
        [ForeignKey("EditedBy")]
        public User EditedByUser { get; set; }
        public DateTime EditedOn { get; set; }
      
        public Boolean IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }

        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }

        public TaskPriority Priority { get; set; }
        public DateTime OriginalDueDate { get; set; }

    }
}
