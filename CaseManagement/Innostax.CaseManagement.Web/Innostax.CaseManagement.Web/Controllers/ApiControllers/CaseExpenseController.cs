﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseExpenseController : ApiController
    {
        private readonly ICaseExpenseService _caseExpenseService;
        public CaseExpenseController(ICaseExpenseService caseExpenseService)
        {
            _caseExpenseService = caseExpenseService;
        }

        [HttpGet]
        [Route("api/Case/{id}/CaseExpenses")]
        [HasPermission(new Permission[] { Permission.VIEW_ALL_CASE_EXPENSE, Permission.VIEW_OWN_CASE_EXPENSE })]
        public List<CaseExpense> Get(Guid id)
        {
            return _caseExpenseService.GetCaseExpenseByCaseId(id);
        }

        [HttpPost]
        [HasPermission(new Permission[] { Permission.ADD_CASE_EXPENSE })]
        public void Post(CaseExpense caseExpenseData)
        {
            _caseExpenseService.Save(caseExpenseData);
        }

        [HasPermission(new Permission[] { Permission.CAN_UPDATE_CASE_EXPENSE })]
        [HttpPut]
        public void Put(Guid id, CaseExpense caseExpenseData)
        {
            caseExpenseData.Id = id;
            _caseExpenseService.Save(caseExpenseData);
        }

        [HttpDelete]
        [HasPermission(new Permission[] { Permission.CAN_DELETE_CASE_EXPENSE})]
        public void Delete(Guid id)
        {
            _caseExpenseService.Delete(id);
        }

        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
        [HttpPut]
        public void UpdateCaseExpensesList(List<CaseExpense> caseExpenseList)
        {
            _caseExpenseService.UpdateCaseExpensesList(caseExpenseList);
        }
    }
}
