﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Core.Domain
{
    public class LogCaseStatusHistoryService:IObserver
    {
        private readonly ICaseHistoryService _caseHistoryService;
        public LogCaseStatusHistoryService(ICaseHistoryService caseHistoryService)
        {
            _caseHistoryService = caseHistoryService;
        }

        public List<Guid> Execute(Guid caseId, string status)
        {            
            _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_STATUS_CHANGED, status);
            return new List<Guid> { };
        }
    }
}
