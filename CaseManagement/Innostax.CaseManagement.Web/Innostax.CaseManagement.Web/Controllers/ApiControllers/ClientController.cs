﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System.Web;
using System.Linq;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class ClientController : ApiController
    {
        private readonly IClientService _clientService;
        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        // GET api/<controller>
        [Route("api/Clients")]
        [HasPermission(new Permission[] { Permission.CLIENT_READ_ALL })]
        public PageResult<Client> Get(ODataQueryOptions<Client> options)
        {
            Dictionary<string, string> filterParameters = HttpContext.Current.Request.QueryString.Keys.Cast<string>().ToDictionary(k => k, v => HttpContext.Current.Request.QueryString[v]);
            if (filterParameters.First().Key == "$skip")
            {
                return _clientService.GetAllClientsDetails(options, Request);
            }
            else
            {
                return _clientService.GetClientsBasedOnClientName(Request, filterParameters.First().Value);
            }
        }

        [Route("api/GetAllClients")]
        [HasPermission(new Permission[] { Permission.CLIENT_READ_ALL })]
        public List<Client> Get()
        {
            return _clientService.GetAllClientsForCases();
        }

        [System.Web.Http.Route("api/Client/{clientId}/Cases")]
        [HasPermission(new Permission[] { Permission.CASE_READ_ALL })]
        public PageResult<Case> GetAllCasesForAClient(ODataQueryOptions<Case> options, Guid clientId)
        {
            return _clientService.GetAllCasesForAClient(options, Request, clientId);               
        }

        [HasPermission(new Permission[] { Permission.CLIENT_READ_ALL })]
        public Client Get(string id)
        {
            // TODO: Prashanth - Let's do all error checking in the domain servcies to make testing easier
            var result = new Result<Client>();
            var receivedClientId = _clientService.ValidateClientId(id);
            var getClientDetailsFromClientIdResult = _clientService.GetClientDetailsFromClientId(receivedClientId);
            return getClientDetailsFromClientIdResult;
        }

        // POST api/<controller>
        [HttpPost]
        [HasPermission(new Permission[] { Permission.CLIENT_CREATE })]
        public Client Post(Client client)
        {
            var saveClientResult = _clientService.Save(client);
            return saveClientResult;
        }

        // PUT api/<controller>/5
        [HasPermission(new Permission[] { Permission.CLIENT_UPDATE_ALL })]
        public Client Put(string id, Client client)
        {
            var receivedClientId = _clientService.ValidateClientId(id);
            client.ClientId = receivedClientId;
            var updateExistingClientResult = _clientService.Save(client);
            return updateExistingClientResult;
        }

        // DELETE api/<controller>/5
        [HasPermission(new Permission[] { Permission.CLIENT_DELETE_ALL })]
        public void Delete(string id)
        {
            var receivedClientId = _clientService.ValidateClientId(id);
            _clientService.Delete(receivedClientId);
        }

        [HttpGet]
        [Route("api/Search/Clients")]
        [HasPermission(new Permission[] { Permission.CLIENT_READ_ALL })]
        public PageResult<Client> SearchClients(ODataQueryOptions<Client> options, string keyword)
        {
            return _clientService.SearchClientsByKeyword(options, Request, keyword);
        }

    }
}