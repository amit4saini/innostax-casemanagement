﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    public class TaskFileAssociationController : ApiController
    {
        private readonly ITaskFileAssociationService _taskFileAssociationService;

        public TaskFileAssociationController(ITaskFileAssociationService taskFileAssociationService)
        {
            _taskFileAssociationService = taskFileAssociationService;
        }

        [HttpGet]
        [Route("api/TaskFiles/{taskId}")]
        public List<FileModel> Get(Guid taskId)
        {
          return  _taskFileAssociationService.GetTaskAssociatedFiles(taskId);
        }

        [HttpPost]
        [Route("api/TaskFiles")]
        public void Post(Task selectedTask)
        {
            _taskFileAssociationService.SaveTaskAssociatedFiles(selectedTask);
        }

        [HttpDelete]
        [Route("api/TaskFiles/{taskId}/File/{fileId}")]
        public void Delete(Guid taskId, Guid fileId)
        {
            _taskFileAssociationService.RemoveTaskFileAssociation(taskId, fileId);
        }
    }
}
