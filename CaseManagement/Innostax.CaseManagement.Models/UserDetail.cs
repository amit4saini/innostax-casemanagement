﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class UserDetail
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string CompanyName { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
    }
}
