﻿namespace Innostax.Common.Database.Enums
{
    public enum Workflow
    {
        NEW = 1,
        ASSIGNED,
        IN_PROGRESS,
        COMPLETED,
        UNDER_REVIEW,
        DONE
    }
}
