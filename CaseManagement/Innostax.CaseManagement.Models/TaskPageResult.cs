﻿using System;
using System.Web.Http.OData;

namespace Innostax.CaseManagement.Models
{
    public class TaskPageResult
    {
        public PageResult<Task> Result { get; set; }
        public int AllTasksCount { get; set; }
    }
}
