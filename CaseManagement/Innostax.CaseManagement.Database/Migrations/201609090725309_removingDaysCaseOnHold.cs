namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingDaysCaseOnHold : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cases", "DaysOnHold");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "DaysOnHold", c => c.Int());
        }
    }
}
