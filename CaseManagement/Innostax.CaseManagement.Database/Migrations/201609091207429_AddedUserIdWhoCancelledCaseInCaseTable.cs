namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserIdWhoCancelledCaseInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "UserIdWhoCancelledCase", c => c.Guid());
            CreateIndex("dbo.Cases", "UserIdWhoCancelledCase");
            AddForeignKey("dbo.Cases", "UserIdWhoCancelledCase", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "UserIdWhoCancelledCase", "dbo.Users");
            DropIndex("dbo.Cases", new[] { "UserIdWhoCancelledCase" });
            DropColumn("dbo.Cases", "UserIdWhoCancelledCase");
        }
    }
}
