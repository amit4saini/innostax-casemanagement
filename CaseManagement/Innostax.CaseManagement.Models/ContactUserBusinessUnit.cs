﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class ContactUserBusinessUnit
    {      
        public Guid Id { get; set; }
        public Guid ClientContactUserId { get; set; }
        public ClientContactUser ClientContactUser { get; set; }
        public string BusinessUnit { get; set; }
        public string AccountCode { get; set; }
        public DateTime? CreationDateTime { get; set; }
    }
}
