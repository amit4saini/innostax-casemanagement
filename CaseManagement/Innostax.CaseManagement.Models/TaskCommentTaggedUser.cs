﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class TaskCommentTaggedUser
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid TaskCommentId { get; set; }
    }
}
