﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CasePrincipalServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CasePrincipalService sut;
        private Mock<ICaseHistoryService> caseHistoryService;
        Guid caseId;
        [TestInitialize]
        public void TestInitialize()
        {
            caseId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            caseHistoryService = new Mock<ICaseHistoryService>();
            sut = new CasePrincipalService(innostaxDbMock.Object, caseHistoryService.Object);
            caseHistoryService.Setup(x => x.SaveCaseHistory(It.IsAny<Guid>(), It.IsAny<Innostax.Common.Database.Enums.Action>(),null));           
        }
        private CasePrincipal[] GetDummyCasePrincipal()
        {
            var dummyCasePrincipal = new CasePrincipal[]
            {
                new CasePrincipal
                {
                    Id=Guid.NewGuid(),
                    CaseId=caseId,
                    PrincipalName="Ankita"
                },
                new CasePrincipal
                {
                    Id=Guid.NewGuid(),
                    CaseId=caseId,
                    PrincipalName="Aditi"
                }
            };
            return dummyCasePrincipal;
        }
        [TestMethod]
        public void GivenCalling_GetCaseAssociatedPrincipals_WhenPrincipalsIsPresent_ThenMappedResultReturn()
        {
            var dummyCasePrincipal = GetDummyCasePrincipal();
            innostaxDbMock.Setup(c => c.CasePrincipals).ReturnsDbSet(dummyCasePrincipal);
            var actual = sut.GetCaseAssociatedPrincipals(caseId);
            Assert.IsNotNull(actual);
        }
        [TestMethod]
        public void GivenCalling_Save_WhenPrincipalsForCaseIdIsPresent_ThenMappedResultReturn()
        {
            var dummyCasePrincipal = GetDummyCasePrincipal();
            innostaxDbMock.Setup(c => c.CasePrincipals).ReturnsDbSet(dummyCasePrincipal);
            var dummyCasePrincipalModel = Mapper.Map<CaseManagement.Models.CasePrincipal[]>(dummyCasePrincipal);
            try
            {
                sut.Save(dummyCasePrincipalModel, caseId); 
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_Save_WhenPrincipalsForCaseIdIsNotPresent_ThenMappedResultReturn()
        {
            var dummyCasePrincipal = GetDummyCasePrincipal();
            innostaxDbMock.Setup(c => c.CasePrincipals).ReturnsDbSet(dummyCasePrincipal);
            var dummyCasePrincipalModel = Mapper.Map<CaseManagement.Models.CasePrincipal[]>(dummyCasePrincipal);
            try
            {
                sut.Save(dummyCasePrincipalModel,Guid.NewGuid());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
