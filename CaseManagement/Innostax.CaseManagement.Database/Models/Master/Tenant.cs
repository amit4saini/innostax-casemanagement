﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Database.Models.Master
{
    public class Tenant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TenantId { get; set; }

        public string TenantDomain { get; set; }
        public string ConnectionString { get; set; }
        public Guid CreatedByUserId { get; set; }

        [ForeignKey("CreatedByUserId")]
        public UserManagement.User CreatedByUser { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? LastEditedDateTime { get; set; }

        public Guid? LastUpdatedByUserId { get; set; }

        [ForeignKey("LastUpdatedByUserId")]
        public UserManagement.User LastUpdatedByUser { get; set; }

        public string Serverip { get; set; }

        public string PortNumber { get; set; }

        public string Dbname { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }

        public bool IsDeleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
