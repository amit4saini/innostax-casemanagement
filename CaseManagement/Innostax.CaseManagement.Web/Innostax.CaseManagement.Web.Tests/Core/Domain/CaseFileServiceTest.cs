﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseFileServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CaseFilesService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandler;
        private Mock<ICommonService> _commonService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICaseFileAssociationService> _caseFileAssociationService;
        private Mock<IDiscussionFileAssociationService> _discussionFileAssociationService;
        private Mock<IDiscussionMessageFileAssociationService> _discussionMessageFileAssociationService;
        Guid caseId;


        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            caseId = System.Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _discussionFileAssociationService = new Mock<IDiscussionFileAssociationService>();
            _discussionMessageFileAssociationService = new Mock<IDiscussionMessageFileAssociationService>();
            _errorHandler = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _caseFileAssociationService = new Mock<ICaseFileAssociationService>();
            _caseFileAssociationService.Setup(x => x.GetCaseAssociatedFiles(It.IsAny<Guid>())).Returns(GetDummyDomainUploadedFile());
            _discussionFileAssociationService.Setup(x => x.GetDiscussionAssociatedFiles(It.IsAny<Guid>())).Returns(GetDummyDomainUploadedFile());
            _discussionMessageFileAssociationService.Setup(x => x.GetDiscussionMessageAssociatedFiles(It.IsAny<Guid>())).Returns(GetDummyDomainUploadedFile());
            _userService.Setup(x => x.GetUser(It.IsAny<Guid>())).Returns(new Models.Result<User> { Data = new User { FirstName = "sdfdf", LastName = "dsdfdf" } });
            _userService.SetupGet(x => x.UserId).Returns(userId);
            sut = new CaseFilesService(
                innostaxDbMock.Object,
                 _caseFileAssociationService.Object, _discussionFileAssociationService.Object,
                _discussionMessageFileAssociationService.Object, _commonService.Object
                );
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
           // _commonService.Setup(x => x.ValidateCaseAccess(It.IsAny<Guid>())).Returns(true);
        }
        private List<UploadedFile> GetDummyUploadedFile()
        {
            var dummyUploadedFile = new List<UploadedFile>
            {
                new UploadedFile
                {
                   FileId=Guid.NewGuid(),
                   FileUrl="www.test.com",
                   UserId=Guid.NewGuid(),
                   AzureKey="test",
                   FileName="test",
                    IsDeleted=false
                },
                new UploadedFile
                {
                   FileId=Guid.NewGuid(),
                   FileUrl="www.test2.com",
                   UserId=Guid.NewGuid(),
                   AzureKey="test2",
                   FileName="test2",
                    IsDeleted=false
                },
            };
            return dummyUploadedFile;
        }
        private List<Models.FileModel> GetDummyDomainUploadedFile()
        {
            var dummyUploadedFile = new List<Models.FileModel>
            {
                new Models.FileModel
                {
                   FileId=Guid.NewGuid(),
                   FileUrl="www.test.com",
                   UserName="test",
                   AzureKey="test",
                   FileName="test",
                },
                new Models.FileModel
                {
                   FileId=Guid.NewGuid(),
                   FileUrl="www.test2.com",
                    UserName="test",
                   AzureKey="test2",
                   FileName="test2",
                },
            };
            return dummyUploadedFile;
        }

        [TestMethod]
        public void GivenCalling_GetAllCaseRelatedFiles_WhenDataIsValid_ThenNoExceptionOccurs()
        {
            innostaxDbMock.Setup(c => c.CaseFileAssociations).ReturnsDbSet(new List<CaseFileAssociation> { new CaseFileAssociation { CaseId = caseId, FileId = new Guid(), IsDeleted = false, Id = Guid.NewGuid() } });
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(new List<Discussion> { new Discussion { Id=Guid.NewGuid(),CaseId = caseId, IsDeleted = false } });
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(new List<DiscussionMessage> { new DiscussionMessage { DiscussionId=Guid.NewGuid(), IsDeleted = false, Id = Guid.NewGuid() } });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Task> { new Task { TaskId = Guid.NewGuid() } });
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(new List<TaskComment> { new TaskComment { } });
            innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(new List<TaskFileAssociation> { });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Task> { new Task { } });
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(new List<TaskCommentFileAssociation> { });
            try
            {
                sut.GetAllCaseRelatedFiles(caseId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetAllCaseRelatedFiles_WhenNoFilesAreFound_ThenNoExceptionOccurs()
        {
            innostaxDbMock.Setup(c => c.CaseFileAssociations).ReturnsDbSet(new List<CaseFileAssociation> { new CaseFileAssociation { } });           
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(new List<Discussion> { new Discussion { Id = Guid.NewGuid(), CaseId = caseId, IsDeleted = false } });
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(new List<DiscussionMessage> { new DiscussionMessage { DiscussionId = Guid.NewGuid(), IsDeleted = false, Id = Guid.NewGuid() } });
            _caseFileAssociationService.Setup(x => x.GetCaseAssociatedFiles(It.IsAny<Guid>())).Returns(new List<Models.FileModel> {  });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Task> { new Task { } });
            innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(new List<TaskFileAssociation> { });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Task> { new Task { } });
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(new List<TaskCommentFileAssociation> { });
            _discussionFileAssociationService.Setup(x => x.GetDiscussionAssociatedFiles(It.IsAny<Guid>())).Returns(new List<Models.FileModel> {  });
            _discussionMessageFileAssociationService.Setup(x => x.GetDiscussionMessageAssociatedFiles(It.IsAny<Guid>())).Returns(new List<Models.FileModel> {  });          
           var  result=sut.GetAllCaseRelatedFiles(caseId);
            Assert.AreEqual(result.Count, 0);         
        }
    
    }
}
    
