﻿using System;
using System.Collections.Generic;
using System.Linq;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IReportPriceService
    {
        List<ReportPrice> GetReportPricesByCaseId(Guid caseId);
    }

    public class ReportPriceService:IReportPriceService
    {
        private IInnostaxDb _innostaxDb;

        public ReportPriceService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public List<ReportPrice> GetReportPricesByCaseId(Guid caseId)
        {
            var reportPriceList = new List<ReportPrice>();
            var caseReportAssociations = _innostaxDb.CaseReportAssociations.Where(x => x.CaseId == caseId && x.IsDeleted != true).ToList();
            foreach (var eachAssociation in caseReportAssociations)
            {
                var mappedReportPrice = Mapper.Map<ReportPrice>(_innostaxDb.ReportPrices.FirstOrDefault(x => x.ReportId == eachAssociation.ReportId && x.CountryId == eachAssociation.CountryId));
                reportPriceList.Add(mappedReportPrice);
            }
            return reportPriceList;
        }
    }
}
