﻿using AutoMapper;
using Innostax.Common.Database;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionMessageTaggedUserService
    {
        void SaveDiscussionMessageTaggedUsers(Guid discussionMessageId, List<Guid> taggedUserIdsList);
        List<Models.Account.User> GetDiscussionMessageTaggedUsers(Guid discussionMessageId);
        void DeleteDiscussionMessageTaggedUsers(Guid discussionMessageId);
    }

    public class DiscussionMessageTaggedUserService : IDiscussionMessageTaggedUserService
    {
        private IInnostaxDb _innostaxDb;

        public DiscussionMessageTaggedUserService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public void SaveDiscussionMessageTaggedUsers(Guid discussionMessageId, List<Guid> taggedUserIdsList)
        {
            var existingtaggedUsersList = _innostaxDb.DiscussionMessageTaggedUsers.Where(x => x.DiscussionMessageId == discussionMessageId).ToList();
            if (existingtaggedUsersList != null)
            {
                existingtaggedUsersList.ForEach(x => _innostaxDb.DiscussionMessageTaggedUsers.Remove(x));
            }
            foreach (var eachTaggedUser in taggedUserIdsList.Distinct())
            {
                var newDiscussionMessageTag = new DiscussionMessageTaggedUser
                {                    
                    DiscussionMessageId = discussionMessageId,
                    UserId = eachTaggedUser
                };
                _innostaxDb.DiscussionMessageTaggedUsers.Add(newDiscussionMessageTag);
            }
            _innostaxDb.SaveChangesWithErrors();
        }

        public List<Models.Account.User> GetDiscussionMessageTaggedUsers(Guid discussionMessageId)
        {
            var existingTaggedUsersList = _innostaxDb.DiscussionMessageTaggedUsers.Include(y => y.User).Where(x => x.DiscussionMessageId == discussionMessageId).Select(y => y.User).ToList();
            var usersList = new List<Models.Account.User>();
            usersList = Mapper.Map<List<Models.Account.User>>(existingTaggedUsersList);
            return usersList;
        }

        public void DeleteDiscussionMessageTaggedUsers(Guid discussionMessageId)
        {
            var existingTaggedUsersList = _innostaxDb.DiscussionMessageTaggedUsers.Where(x => x.DiscussionMessageId == discussionMessageId).ToList();
            existingTaggedUsersList.ForEach(x => _innostaxDb.DiscussionMessageTaggedUsers.Remove(x));
        }
    }
}
