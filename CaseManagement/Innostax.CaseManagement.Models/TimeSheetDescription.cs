﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class TimeSheetDescription
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
