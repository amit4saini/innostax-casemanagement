// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models.RolesAndPermissionManagement;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using StructureMap;
using User = Innostax.Common.Database.Models.UserManagement.User;

namespace Innostax.CaseManagement.Web.DependencyResolution
{
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using System.Linq;
    public class DefaultRegistry : Registry
    {
        #region Constructors and Destructors

        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.AssembliesFromApplicationBaseDirectory(assembly => assembly.FullName.Contains("Innostax"));
                    scan.WithDefaultConventions();
                    scan.With(new ControllerConvention());
                    scan.AddAllTypesOf<Profile>();
                });
            // For<IUserStore<User, Guid>>().Use<UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>>();
            For<IAuthenticationManager>().Use(() => HttpContext.Current.GetOwinContext().Authentication);
            For<IRoleStore<IdentityRole, string>>().Use(() => new RoleStore<IdentityRole>());           
            For<IInnostaxDb>().Use<InnostaxDb>();           
            For<Common.Database.Database.ITenantDb>().Use(() => new Common.Database.Database.TenantDb(@"Data SourceataDirectory|\Innostax.mdf;Initial Catalog=Innostax;Integrated Security=True; MultipleActiveResultSets=True;"));
            //try
            //{
            //    var connctionString = System.Web.HttpContext.Current.GetOwinContext().Get<Common.Database.Database.TenantDb>("current_tenant_context");
            //}
            //catch (Exception e)
            //{          
            
            //}         
        }

        #endregion
    }
}