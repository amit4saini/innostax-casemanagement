﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using Innostax.Common.Infrastructure.Utility;
using Innostax.CaseManagement.Models.Account;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using System.Linq;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseServiceTests
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CaseService sut;
        private Mock<IClientService> _clientService;
        private Mock<IUserService> _userService;
        private Mock<ICaseReportAssociationService> _caseReportAssociationService;
        private Mock<IReportService> _reportService;
        private Mock<IFileService> _fileUploadService;
        private Mock<ICaseFileAssociationService> _caseFileAssociationService;
        private Mock<ICommonUtils> _commonUtils;
        private ErrorHandlerService _errorHandlerService;
        private Mock<INotificationService> _notificationService;
        private Mock<ICasePrincipalService> _casePrincipalService;
        private Mock<ICommonService> commonService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<IMailSenderService> _mailSenderService;
        private Mock<ITaskService> _taskService;
        private Mock<ICaseStatusService> _caseStatusService;
        private Mock<IParticipantCaseAssociationService> _participantCaseAssociationService;
        Guid clientId1;
        Guid clientId2;
        Guid userId1;
        Guid caseId;
        ODataQueryOptions<Models.Case> options;
        ODataQueryOptions<Models.Account.User> userOptions;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            caseId = Guid.NewGuid();
            userId1 = Guid.NewGuid();
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            innostaxDbMock = new Mock<InnostaxDb>();
            _clientService = new Mock<IClientService>();
            _userService = new Mock<IUserService>();
            _reportService = new Mock<IReportService>();
            _caseStatusService = new Mock<ICaseStatusService>();
            _mailSenderService = new Mock<IMailSenderService>();

            _participantCaseAssociationService = new Mock<IParticipantCaseAssociationService>();
            commonService = new Mock<ICommonService>();
            _reportService.Setup(x => x.GetCaseAssociatedReports(It.IsAny<Guid>())).Returns(new List<Models.Report>
            {
                new Models.Report
                {
                    ReportId=new Guid()
                }
            });
            _caseReportAssociationService = new Mock<ICaseReportAssociationService>();
            _fileUploadService = new Mock<IFileService>();
            _caseFileAssociationService = new Mock<ICaseFileAssociationService>();
            _caseFileAssociationService.Setup(x => x.GetCaseAssociatedFiles(It.IsAny<Guid>())).Returns(new List<Models.FileModel>
            {
                new Models.FileModel
                {
                    FileId=new Guid(),
                    FileName="dfds",
                    FileUrl="fdgfdg"
                }
            });
            _clientService.Setup(x => x.GetClientDetailsFromClientId(It.IsAny<Guid>())).Returns(new Models.Client
            {
                ClientId = new Guid()
            });
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _userService.Setup(x => x.GetUserByUserName(It.IsAny<List<string>>())).Returns(new List<User> { new User { FirstName = "sdfdf", LastName = "dsdfdf", Role = new Models.Role { Name = "name", Id = Guid.NewGuid() } } });
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _userService.Setup(x => x.GetUser(It.IsAny<Guid>())).Returns(new Models.Result<User> { Data = new User { FirstName = "sdfdf", LastName = "dsdfdf", Role = new Models.Role { Name = "name", Id = Guid.NewGuid() } } });
            _userService.Setup(x => x.GetCurrentUser()).Returns(new User { FirstName = "sdfdf", LastName = "dsdfdf", Role = new Models.Role { Name = "name", Id = Guid.NewGuid() } });
            _commonUtils = new Mock<ICommonUtils>();
            _notificationService = new Mock<INotificationService>();
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Case>("Case");
            options = new ODataQueryOptions<Models.Case>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Case)), new HttpRequestMessage());
            ODataModelBuilder userModelBuilder = new ODataConventionModelBuilder();
            userModelBuilder.EntitySet<Models.Account.User>("User");
            userOptions = new ODataQueryOptions<Models.Account.User>(new ODataQueryContext(userModelBuilder.GetEdmModel(), typeof(Models.Account.User)), new HttpRequestMessage());
            _errorHandlerService = new ErrorHandlerService();
            _casePrincipalService = new Mock<ICasePrincipalService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _taskService = new Mock<ITaskService>();
            sut = new CaseService(
                innostaxDbMock.Object,
                 _userService.Object,
                 _clientService.Object, _caseReportAssociationService.Object,
                 _reportService.Object, _fileUploadService.Object, _caseFileAssociationService.Object,
                 _errorHandlerService,
                 _notificationService.Object,
                 _casePrincipalService.Object, commonService.Object, _caseHistoryService.Object,
                 _mailSenderService.Object, _taskService.Object,
                 _participantCaseAssociationService.Object, _caseStatusService.Object

                );
            _caseHistoryService.Setup(x => x.SaveCaseHistory(It.IsAny<Guid>(), It.IsAny<Innostax.Common.Database.Enums.Action>(), null));
        }
        private List<Client> GetDummyClients()
        {

            var dummyClients = new List<Client>
            {
                new Client
                {
                    ClientId = clientId1,
                    SalesRepresentativeId = userId1,
                    ClientName = "abcd",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "first", LastName = "last" },
                    Notes = "notes",
                    Industry = GetDummyIndustry(),
                    IndustrySubCategory = GetDummyIndustrySubCategory(),
                    SalesRepresentativeEmail= "abc@afg.com",
                    IsDeleted =false ,
                },
                new Client
                {
                ClientId = clientId2,
                SalesRepresentativeId = userId1,
                ClientName = "abcdbf",
                SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "first", LastName = "last" },
                Notes = "Mynotes",
                Industry = GetDummyIndustry(),
                IndustrySubCategory = GetDummyIndustrySubCategory(),
                SalesRepresentativeEmail= "asfbc@afg.com",
                IsDeleted =false ,
                }
            };
            return dummyClients;
        }
        private Models.Case GetADummyCase()
        {
            return new Models.Case
            {

                CaseId = caseId,   
                ClientId = clientId1,
                IsDeleted = false,
                AdditionalSalesRepNotes = "notes",
                ClientPO = "clientPO",
                ClientProvidedInformation = "hgdjmd",
                HoldReason = "holdReason",
                NickName = "matter",
                AssignedTo = userId1,
                ActualRetail=10000
            };
        }

        private Models.Case GetADummyCaseWithActualRetailZero()
        {
            return new Models.Case
            {

                CaseId = caseId,
                ClientId = clientId1,
                IsDeleted = false,
                AdditionalSalesRepNotes = "notes",
                ClientPO = "clientPO",
                ClientProvidedInformation = "hgdjmd",
                HoldReason = "holdReason",
                NickName = "matter",
                AssignedTo = userId1,
                ActualRetail = 0
            };
        }

        private Client GetADummyClient()
        {

            var dummyClients = new Client
            {
                ClientId = clientId1,
                SalesRepresentativeId = userId1,
                ClientName = "abcd",
                SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "first", LastName = "last" },
                Notes = "notes",
                Industry = GetDummyIndustry(),
                IndustrySubCategory = GetDummyIndustrySubCategory(),
                SalesRepresentativeEmail = "abc@afg.com",
                IsDeleted = false,
            };
            return dummyClients;
        }
        private Innostax.Common.Database.Models.ClientContactUser GetADummyClientContactUser()
        {
            var dummyClientContactUser = new ClientContactUser
            {
                ContactName = "abc",
                EmailAddress = "email",
                PhoneNumber = "987839",
                Street = "abcd",
                ZipCode = "123242",
                PositionHeldbyContact = "executive",         
                City = "Delhi",
                IsDeleted = false
            };
            return dummyClientContactUser;
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                 new Case
                {
                CaseId = caseId,
                Client = GetADummyClient(),
                ClientContactUser = GetADummyClientContactUser(),
                ClientId = clientId1,
                IsDeleted = false,
                CreatedByUser = GetADummyUser(),
                AdditionalSalesRepNotes = "notes",
                ClientPO = "clientPO",
                ClientProvidedInformation = "hgdjmd",
                HoldReason = "holdReason",
                NickName = "matter",
                StateOfEmployment = new State { Country = new Country { CountryName = "india" } },
                AssignedToUser = new Innostax.Common.Database.Models.UserManagement.User { Id = userId1 },
                AssignedTo = userId1,
                 },
                     new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = GetADummyClient(),
                    ClientContactUser = GetADummyClientContactUser(),
                    ClientId=clientId2,
                    CreatedByUser=GetADummyUser(),
                    AdditionalSalesRepNotes= "AdditinalSalesRepNotes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } },
                    IsDeleted=false,
                    AssignedToUser = new Innostax.Common.Database.Models.UserManagement.User {Id = userId1 },
                    AssignedTo = userId1
                }
            };
            return dummyCase;
        }
        private List<CaseReportAssociation> GetDummyCaseReport()
        {
            var dummyCaseReport = new List<CaseReportAssociation>
            {
                new CaseReportAssociation
                {
                    Case = new Case
                    {
                    CaseId=caseId,
                    Client = GetADummyClient(),
                    ClientContactUser = GetADummyClientContactUser(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    CreatedByUser=GetADummyUser(),
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                    }
                 }
            };
            return dummyCaseReport;
        }
        private List<CasePrincipal> GetDummyCasePrincipal()
        {
            var dummyCasePrincipal = new List<CasePrincipal>
            {
                new CasePrincipal
                  {
                    PrincipalName = "principal",
                    Case = new Case
                    {
                    CaseId=caseId,
                    Client = GetADummyClient(),
                    ClientContactUser = GetADummyClientContactUser(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    CreatedByUser=GetADummyUser(),
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                    }
                 }
            };
            return dummyCasePrincipal;
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1, FirstName="first", LastName="last" },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1, FirstName="1st", LastName="lst" }
                            };
            return dummyUser;
        }
        private Innostax.Common.Database.Models.UserManagement.User GetADummyUser()
        {
            var dummyUser = new Innostax.Common.Database.Models.UserManagement.User
            {
                Id = userId1,
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                IsDeleted = false
            };
            return dummyUser;
        }
        private List<Region> GetDummyRegions()
        {
            var dummyRegions = new List<Region>
            {
                new Region { RegionId = 1, RegionName = "DummyRegion" }
            };
            return dummyRegions;
        }
        private List<Country> GetDummyCountries()
        {
            var dummyCountries = new List<Country>
            {
                new Country { CountryId = 1, CountryName = "DummyCountry", RegionId = 1 }
            };
            return dummyCountries;
        }
        private List<State> GetDummyStates()
        {
            var dummyStates = new List<State>
            {
                new State { StateId = 1, StateName = "DummyState", CountryId = 1 }
            };
            return dummyStates;
        }
        private List<Task> GetDummyTasks()
        {
            var dummyTask = new List<Task>
            {
                new Task {TaskId = Guid.NewGuid(),Title= "task",IsDeleted=false,CaseId=caseId}
            };
            return dummyTask;
        }
        private Industry GetDummyIndustry()
        {
            var dummyIndustry = new Industry { Id = 1, Name = "DummyIndustry" };
            return dummyIndustry;
        }
        private IndustrySubCategory GetDummyIndustrySubCategory()
        {
            var dummyIndustrySubCategory = new IndustrySubCategory { Id = 1, Name = "DummyIndustrySubCategory" };
            return dummyIndustrySubCategory;
        }

        private List<DiscussionNotifier> GetDiscussionNotifier()
        {
            var dummyNotifier = new List<DiscussionNotifier>
            {
                new DiscussionNotifier
                {
                    Discussion = new Discussion {Case = new Case {
                    CaseId=caseId,
                    Client = GetADummyClient(),
                    ClientContactUser = GetADummyClientContactUser(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    CreatedByUser=GetADummyUser(),
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                    } },
                    User = GetADummyUser()
                }
            };
            return dummyNotifier;
        }

        private List<Task> GetDummyTask()
        {
            var dummyTask = new List<Task>
            {
                new Task
                {
                    Case = new Case
                    {
                    CaseId=caseId,
                    Client = GetADummyClient(),
                    ClientContactUser = GetADummyClientContactUser(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    CreatedByUser=GetADummyUser(),
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } }
                    },
                    AssignedToUser = new Innostax.Common.Database.Models.UserManagement.User {Id = userId1 },
                    IsDeleted= false   ,
                    CreatedByUser= new Innostax.Common.Database.Models.UserManagement.User {Id = userId1 }
                }
            };
            return dummyTask;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_ValidateCaseId_WhenCaseIdIsNotPresent_ThenThrowBadRequest()
        {
            try
            {
                var dummyCases = GetDummyCase();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
                sut.ValidateCaseId(notPresent);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_ValidateCaseId_WhenCaseIdIsPresent_ThenMappedResultReturn()
        {
            var dummyCases = GetDummyCase();
            var presentCaseId = Guid.NewGuid();
            var dummyCase = new Case
            {
                CaseId = presentCaseId
            };

            dummyCases.Add(dummyCase);
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            try
            {
                sut.ValidateCaseId(presentCaseId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }
        [TestMethod]
        public void GivenCalling_GetAllCases_WhenNoCaseIsPresent_ThenReturnsNoData()
        {
            var dummyCases = new List<Case>();
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            var actual = sut.GetAllCases(options, new HttpRequestMessage());
            Assert.AreEqual(actual.Count, 0);
        }
        [TestMethod]
        public void GivenCalling_GetAllCases_WhenCasesArePresent_ThenMappedResultReturn()
        {
            var dummyCases = GetDummyCase();
            var dummyUsers = GetDummyUser();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTasks());
            commonService.Setup(x => x.GetAuthorizedCasesForUser()).Returns(new List<Case> { new Case { CaseId = Guid.NewGuid() } }.AsQueryable());
            var actual = sut.GetAllCases(options, new System.Net.Http.HttpRequestMessage());
            Assert.AreEqual(actual.Count, 1);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetCaseInfoByCaseId_WhenCaseIdIsNotPresent_ThenThrowNotFoundResponse()
        {
            try
            {
                var dummyCases = GetDummyCase();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(GetDummyClients());
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
                var actual = sut.GetCaseInfoById(notPresent);
            }

            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetCaseInfoByCaseId_WhenCaseIdIsPresent_ThenMappedResultReturn()
        {
            var dummyCases = GetDummyCase();
            var presentCaseId = Guid.NewGuid();
            var dummyCase = new Case
            {
                CaseId = presentCaseId,
                Client = GetADummyClient()
            };
            dummyCases.Add(dummyCase);
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(new List<TimeSheet> { new TimeSheet { } });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Task> { new Task { } });
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(new List<Discussion> { new Discussion { } });
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(new List<DiscussionMessage> { new DiscussionMessage { } });
            innostaxDbMock.Setup(c => c.CaseExpenses).ReturnsDbSet(new List<CaseExpense> { new CaseExpense { } });
            innostaxDbMock.Setup(c => c.DiscussionFileAssociations).ReturnsDbSet(new List<DiscussionFileAssociation> { new DiscussionFileAssociation { } });
            innostaxDbMock.Setup(c => c.DiscussionMessageFileAssociations).ReturnsDbSet(new List<DiscussionMessageFileAssociation> { new DiscussionMessageFileAssociation { DiscussionMessage=new DiscussionMessage { } } });
            innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(new List<TaskFileAssociation> { });
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(new List<TaskComment> { });
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(new List<TaskCommentFileAssociation> { });
            var actual = sut.GetCaseInfoById(presentCaseId);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_ArchiveUnarchiveCase_WhenCaseIdIsNotPresent_ThenThrowBadRequest()
        {
            try
            {
                var dummyCases = GetDummyCase();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
                sut.ArchiveUnarchiveCase(notPresent, true);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_ArchiveUnarchiveCase_WhenCaseIdIsNotPresent_ThenMappedResultReturn()
        {
            var dummyCases = GetDummyCase();
            var presentCaseId = Guid.NewGuid();
            var dummyCase = new Case
            {
                CaseId = presentCaseId
            };

            dummyCases.Add(dummyCase);
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(new CaseReportAssociation[0]);
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            try
            {
                sut.ArchiveUnarchiveCase(presentCaseId, true);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetNewCaseNumber_CheckIfCaseNumberIsNextCaseNumber()
        {
            var latestCaseNumber = "DD-12345-67";
            var dummyCases = GetDummyCase();
            foreach (var eachDummyCase in dummyCases)
            {
                eachDummyCase.CaseNumber = latestCaseNumber;
            }
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);     
            //sut.Setup(x => x.GetCaseNumberInitial("C")).Returns("C");
            var actualNextCaseNumber = sut.GetNewCaseNumber("D");
            Assert.IsNotNull(actualNextCaseNumber);
        }

        [TestMethod]
        public void GivenCalling_GetNewCaseNumber_WhenNoCaseIsPresent_CheckWithDefaultCaseNumber()
        {  
            var dummyCases = new List<Case>();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            //_commonUtils.Setup(x => x.GetCaseNumberInitial("C")).Returns("C");
            var actualNextCaseNumber = sut.GetNewCaseNumber("C");
            Assert.IsNotNull(actualNextCaseNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_AssignUserToCase_WhenCasesAreNotPresent_ThenThrowCaseNotFound()
        {
            var dummyCases = new List<Case>();
            var dummyUsers = GetDummyUser();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            try
            {
                sut.AssignUserToCase(Guid.NewGuid(), dummyUsers[0].Id);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, e.Response.StatusCode);
                throw;
            }
        }

        [TestMethod]
        public void GivenCalling_AssignUserToCase_WhenCasesAndUserArePresent_ThenReturnTrue()
        {
            var dummyCases = GetDummyCase();
            var dummyUsers = GetDummyUser();
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
            try
            {
                sut.AssignUserToCase(dummyCases[0].CaseId, dummyUsers[0].Id);
            }
            catch (Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_SearchCasesByKeyword_WhenCaseIsFound_ReturnCase()
        {
            var dummyCase = GetDummyCase();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCase);
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(GetDummyCaseReport());
            innostaxDbMock.Setup(c => c.CasePrincipals).ReturnsDbSet(GetDummyCasePrincipal());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            commonService.Setup(x => x.GetAuthorizedCasesForUser()).Returns(GetDummyCase().AsQueryable());
            var caseData = sut.SearchCasesByKeyword(options, new HttpRequestMessage(), "notes");
            Assert.AreEqual(caseData.Count, 2);
        }
        [TestMethod]
        public void GivenCalling_SearchCases_WhenCaseIsNotFound_ThenResultIsEmpty()
        {
            var dummyCase = GetDummyCase();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCase);
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(GetDummyCaseReport());
            innostaxDbMock.Setup(c => c.CasePrincipals).ReturnsDbSet(GetDummyCasePrincipal());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            var caseData = sut.SearchCasesByKeyword(options, new HttpRequestMessage(), "fbsmn");
            Assert.AreEqual(caseData.Count, 0);
        }

        [TestMethod]
        public void GivenCalling_GetAllUsersForACase_WhenCaseIdIsPresent_ThenReturnMappedResults()
        {
            var dummyCase = GetDummyCase();
            innostaxDbMock.Setup(c => c.UserDetails).ReturnsDbSet(new List<UserDetail> { new UserDetail() { } });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCase);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.DiscussionNotifiers).ReturnsDbSet(GetDiscussionNotifier());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            innostaxDbMock.Setup(c => c.TaskCommentTaggedUsers).ReturnsDbSet(new List<TaskCommentTaggedUser> { new TaskCommentTaggedUser() { User=new Innostax.Common.Database.Models.UserManagement.User(),TaskComment=new TaskComment { Task=new Task() } ,UserId=Guid.NewGuid()} });
            innostaxDbMock.Setup(c => c.TaskDescriptionTaggedUsers).ReturnsDbSet(new List<TaskDescriptionTaggedUser> { new TaskDescriptionTaggedUser() { User = new Innostax.Common.Database.Models.UserManagement.User(),Task=new Task() , UserId = Guid.NewGuid() } });
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(new List<CaseHistory> { new CaseHistory() });
            innostaxDbMock.Setup(c => c.ParticipantCaseAssociation).ReturnsDbSet(new List<ParticipantCaseAssociation> { new ParticipantCaseAssociation()});
            var actual = sut.GetAllUsersForCase(userOptions,new HttpRequestMessage(),caseId);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetAllUsersForACase_WhenCaseIdIsNotPresent_ThenReturnEmptySet()
        {
            var dummyCase = new List<Case>();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCase);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new Innostax.Common.Database.Models.UserManagement.User[0]);
            try
            {
                sut.GetAllUsersForCase(userOptions, new HttpRequestMessage(), Guid.NewGuid());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, e.Response.StatusCode);
                throw;
            }
        }

        [TestMethod]     
        public void GivenCalling_CalculateCaseMargin_WhenCaseIsPassedAndActualRetailIsNotZero_ThenCalculateMarginForThatCase()
        {
            var dummyCaseDetails = new List<Case> { };
            innostaxDbMock.Setup(c => c.CaseExpenses).ReturnsDbSet(new List<CaseExpense> { new CaseExpense {CaseId=caseId,Quantity=10,Cost=1000 } });
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(new List<TimeSheet> { new TimeSheet { CaseId = caseId,EndTime=DateTime.Now,StartTime=DateTime.Now.AddDays(-1),Cost=1000 } });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new Innostax.Common.Database.Models.UserManagement.User[0]);           
            var caseDetails = sut.CalculateCaseMargin(GetADummyCase());
            Assert.AreNotEqual(caseDetails.Margin, 0);
        }

        [TestMethod]
        public void GivenCalling_CalculateCaseMargin_WhenCaseIsPassedAndActualRetailIsZero_ThenMarginForThatCaseIsZero()
        {
            var dummyCaseDetails = new List<Case> { };
            innostaxDbMock.Setup(c => c.CaseExpenses).ReturnsDbSet(new List<CaseExpense> { new CaseExpense { CaseId = caseId, Quantity = 10, Cost = 1000 } });
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(new List<TimeSheet> { new TimeSheet { CaseId = caseId, EndTime = DateTime.Now, StartTime = DateTime.Now.AddDays(-1), Cost = 1000 } });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new Innostax.Common.Database.Models.UserManagement.User[0]);     
            var caseDetails = sut.CalculateCaseMargin(GetADummyCaseWithActualRetailZero());
            Assert.AreEqual(caseDetails.Margin, 0);
        }

    }
}