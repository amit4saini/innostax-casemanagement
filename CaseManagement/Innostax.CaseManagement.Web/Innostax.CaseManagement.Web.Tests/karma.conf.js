// Karma configuration
// Generated on Fri Apr 22 2016 12:16:24 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
       "../Kreller.CaseManagement.Web/Scripts/textAngular-rangy.min.js",
      '../Kreller.CaseManagement.Web.Tests/UI/Scripts/jasmine/angular.js',
      "../Kreller.CaseManagement.Web/Scripts/textAngular-sanitize.min.js",
      "../Kreller.CaseManagement.Web/Scripts/textAngularSetup.js",
      "../Kreller.CaseManagement.Web/Scripts/textAngular.min.js",
      '../Kreller.CaseManagement.Web.Tests/UI/Scripts/jasmine/angular-mocks.js',
	  '../Kreller.CaseManagement.Web.Tests/UI/Scripts/jasmine/angular-resource.js',	
	  "../Kreller.CaseManagement.Web/Scripts/jquery-2.2.0.min.js",                 
     "../Kreller.CaseManagement.Web/Scripts/toastr.min.js",
      "../Kreller.CaseManagement.Web/Scripts/jquery-ui.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-resource.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-animate.js",
      "../Kreller.CaseManagement.Web/Scripts/AngularUI/ui-router.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-ui/ui-bootstrap.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-ui/ui-bootstrap-tpls.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-file-upload.min.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-toastr.tpls.min.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-cookies.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-route.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-route.min.js",
      "../Kreller.CaseManagement.Web/Scripts/spin.min.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-spinner.min.js",    
      "../Kreller.CaseManagement.Web/Scripts/ng-file-upload-master/dist/ng-file-upload.min.js",
      "../Kreller.CaseManagement.Web/Scripts/select.min.js",
      "../Kreller.CaseManagement.Web/Scripts/moment.min.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-bootstrap-calendar-tpls.min.js",
      "../Kreller.Casemanagement.Web/WebApp/modules/cases/Directives/checkbox-model.directive.js",
      "../Kreller.CaseManagement.Web/Scripts/angular-filter.min.js",
      "../Kreller.CaseManagement.Web/Scripts/tinymce/jquery.tinymce.min.js",
      "../Kreller.CaseManagement.Web/Scripts/tinymce/tinymce.min.js",
       "../Kreller.CaseManagement.Web/Scripts/AngularUI/tinymce.js",
      
       "../Kreller.CaseManagement.Web/Scripts/ment.io/dist/mentio.js",
       "../Kreller.CaseManagement.Web/Scripts/ment.io/dist/templates.js",
	  '../Kreller.CaseManagement.Web/WebApp/config.js',
	  '../Kreller.CaseManagement.Web/WebApp/init.js',

	    '../Kreller.CaseManagement.Web/WebApp/modules/users/users.client.module.js',
	  "../Kreller.CaseManagement.Web/WebApp/modules/users/services/authentication.client.service.js",	
	  '../Kreller.CaseManagement.Web/WebApp/modules/cases/cases.module.js',
	  '../Kreller.CaseManagement.Web/WebApp/modules/core/core.module.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/calendar/calendar.module.js',
	  '../Kreller.CaseManagement.Web/WebApp/modules/clients/clients.module.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/tasks/tasks.module.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/dashboard/dashboard.client.module.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/cases.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseSpec.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/calendar/services/calendar.service.js',
      '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/calendarSpec.js',
       '../Kreller.CaseManagement.Web/WebApp/modules/users/services/users.client.service.js',
       '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/userSpec.js',
       '../Kreller.CaseManagement.Web/WebApp/modules/clients/services/clients.service.js',
       '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/clientSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/case-discussions.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseDiscussionSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/expenses.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/expenseSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/timesheets.service.js',
    '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/timeSheetSpec.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/casetasks.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseTaskSpec.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/casehistory.service.js',
    '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseHistorySpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/case-details.service.js',
    '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseDetailsSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/core/services/header.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/headerSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/participants.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/participantSpec.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/core/services/global.variables.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/globalSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/case-users.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseUserSpec.js',
      '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/case-nfcusers.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/caseNFCUsersSpec.js',
     '../Kreller.CaseManagement.Web/WebApp/modules/dashboard/services/dashboard.service.js',
     '../Kreller.CaseManagement.Web.Tests/UI/Test/spec/dashboardSpec.js',
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/cases.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/clients/services/clients.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/users/services/users.client.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/calendar/services/calendar.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/discussions.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/expenses.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/timesheets.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/casetasks.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/casehistory.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/case-details.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/users/services/usersettings.client.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/core/services/header.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/cases/services/participants.service.js': ['coverage'],
        '../Kreller.CaseManagement.Web/WebApp/modules/tasks/services/tasks.service.js': ['coverage'],
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
