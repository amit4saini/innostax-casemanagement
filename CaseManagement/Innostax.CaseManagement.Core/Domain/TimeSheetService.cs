﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITimeSheetService
    {
        void Save(TimeSheet timeSheetData);
        List<TimeSheet> GetTimeSheetsByCaseId(Guid caseId);
        void Delete(Guid timeSheetId);
    }

    public class TimeSheetService : ITimeSheetService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private IErrorHandlerService _errorHandlerService;
        private ICaseHistoryService _caseHistoryService;
        private ICommonService _commonService;

        public TimeSheetService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService,ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
        }

        public List<TimeSheet> GetTimeSheetsByCaseId(Guid caseId)
        {
            var result = new List<TimeSheet>();
            var existingTimeSheets = _userService.UserPermissions.Contains(Permission.CASE_ALL_TIMESHEET) ? _innostaxDb.TimeSheets.Where(x => x.CaseId == caseId && x.IsDeleted == false).Include(x=>x.User).Include(x=>x.Description).ToList()
                : _innostaxDb.TimeSheets.Where(x => x.CaseId == caseId && x.IsDeleted == false && x.CreatedBy == _userService.UserId).Include(x=>x.User).Include(x=>x.Description).ToList();
            result = Mapper.Map<List<TimeSheet>>(existingTimeSheets);
            return result;
        }

        public void Save(TimeSheet timeSheetData)
        {
            var isNewTimeSheet = false;
            Common.Database.Enums.Action currentAction=new Common.Database.Enums.Action();
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == timeSheetData.CaseId);
            var currentUserId = _userService.UserId;
            if (existingCase == null)
            {
                string caseNotFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(caseNotFoundError)
                };
                throw new HttpResponseException(response);
            }
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_UPDATE_ALL, timeSheetData.CaseId);
            var existingTimeSheet = _innostaxDb.TimeSheets.FirstOrDefault(x => x.CaseId == timeSheetData.CaseId && x.Id == timeSheetData.Id && x.IsDeleted != true);
            if (existingTimeSheet == null)
            {
                currentAction = Common.Database.Enums.Action.CASE_TIMESHEET_ADDED;
                existingTimeSheet = new Innostax.Common.Database.Models.TimeSheet();
                isNewTimeSheet = true;
            }
            var creationDateTime = existingTimeSheet.CreationDateTime;
            var existingTimeSheetCreatedByUser = existingTimeSheet.CreatedBy;
            existingTimeSheet = Mapper.Map<Common.Database.Models.TimeSheet>(timeSheetData);
            if (!isNewTimeSheet)
            {
                currentAction = Common.Database.Enums.Action.CASE_TIMESHEET_EDITED;
            }
            else
            {
                existingTimeSheet.CreationDateTime = _commonService.GetUtcDateTime();
                existingTimeSheet.CreatedBy = currentUserId;
            }
            existingTimeSheet.LastEditedBy = currentUserId;
            existingTimeSheet.LastEditedDateTime = _commonService.GetUtcDateTime();
            _innostaxDb.TimeSheets.AddOrUpdate(existingTimeSheet);
            _innostaxDb.SaveChangesWithErrors();
            _caseHistoryService.SaveCaseHistory(existingTimeSheet.CaseId, currentAction, null);
        }

        public void Delete(Guid timeSheetId)
        {
            var existingTimeSheet = _innostaxDb.TimeSheets.FirstOrDefault(x => x.Id == timeSheetId && (x.IsDeleted != true));
            _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_DELETE_ALL, existingTimeSheet.CaseId);
            if (existingTimeSheet != null)
            {
                existingTimeSheet.IsDeleted = true;
                existingTimeSheet.DeletedBy = _userService.UserId;
                existingTimeSheet.DeletedOn = _commonService.GetUtcDateTime();
                _innostaxDb.SaveChangesWithErrors();
                _caseHistoryService.SaveCaseHistory(existingTimeSheet.CaseId, Common.Database.Enums.Action.CASE_TIMESHEET_DELETED,null);
            }
        }
        
    }
}


