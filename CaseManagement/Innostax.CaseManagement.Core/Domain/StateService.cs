﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Innostax.Common.Database.Database;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IStateService
    {
        List<Innostax.CaseManagement.Models.State> ListOfStatesBasedOnCountryId(string id);
    }

    public class StateService : IStateService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        public StateService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Innostax.CaseManagement.Models.State> ListOfStatesBasedOnCountryId(string id)
        {
            var result = new List<Innostax.CaseManagement.Models.State>();
            int countryId;
            if (!int.TryParse(id, out countryId))
            {
                string countryIdNotValidError = Common.Constants.ErrorMessages.Common.StateError.COUNTRYID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(countryIdNotValidError);
            }
            var existingStates = _innostaxDb.States.Where(x => x.CountryId == countryId).ToList();
            if (existingStates.Count == 0)
            {
                string stateNotFoundError = Common.Constants.ErrorMessages.Common.StateError.STATE_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(stateNotFoundError);
            }
            result = new List<Innostax.CaseManagement.Models.State>();
            foreach (var eachState in existingStates)
            {
                var state = Mapper.Map<Innostax.CaseManagement.Models.State>(eachState);
                result.Add(state);
            }
            return result;
        }
    }
}
