﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class TaskActivity
    {
        public TaskActivity()
        {
            CreationDatetime = DateTime.Now;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public UserManagement.User User { get; set; }
        public Innostax.Common.Database.Enums.TaskActivityAction Action { get; set; }
        public DateTime? CreationDatetime { get; set; }
        public Guid TaskId { get; set; }
        [ForeignKey("TaskId")]
        public Task Task { get; set; }
        public string NewValue { get; set; }
    }
}