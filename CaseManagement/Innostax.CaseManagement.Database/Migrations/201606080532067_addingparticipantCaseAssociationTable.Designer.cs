// <auto-generated />
namespace Innostax.Common.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addingparticipantCaseAssociationTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addingparticipantCaseAssociationTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201606080532067_addingparticipantCaseAssociationTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
