﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class ClientContactUser
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public string ContactName { get; set; }
        public string PositionHeldbyContact { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsCaseAssociatedClientContactUser { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public Country Country { get; set; }
        public State State { get; set; }
        public List<ContactUserBusinessUnit> ContactUserBusinessUnits { get; set; }
        public DateTime CreationDateTime { get; set; }
    }
}
