﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class ICIUserCaseAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }

        public Guid ICIUserId { get; set; }
        [ForeignKey("ICIUserId")]
        public UserManagement.User User  { get; set; }
        public bool IsActive { get; set; }

    }
}
