﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
   public class UserAnayticsModel
    {
        public string UserName { get; set; }
        public int TotalCasesCreatedByAUser { get; set; }
        public List<Task> ListOfTaskDelayed { get; set; }
        public List<TimeSheet> ListOfTimesheet { get; set; }
        public List<CaseExpense> ListOfCaseExpense { get; set; }
    }
}
