﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
    public class Tenant
    {
        public Guid TenantId { get; set; }

        public string TenantDomain { get; set; }
        public string ConnectionString { get; set; }
        public Guid CreatedByUserId { get; set; }

        public Account.User CreatedByUser { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? LastEditedDateTime { get; set; }

        public Guid? LastUpdatedByUserId { get; set; }
 
        public Account.User LastUpdatedByUser { get; set; }

        public string Serverip { get; set; }

        public string PortNumber { get; set; }

        public string Dbname { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }
        public bool IsDeleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }  
        public string Email { get; set; }  
    }
}
