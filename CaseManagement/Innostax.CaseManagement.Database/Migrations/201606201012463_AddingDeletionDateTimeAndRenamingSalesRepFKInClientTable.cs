namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddingDeletionDateTimeAndRenamingSalesRepFKInClientTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Clients", name: "SalesRepresentative", newName: "SalesRepresentativeId");
            RenameIndex(table: "dbo.Clients", name: "IX_SalesRepresentative", newName: "IX_SalesRepresentativeId");
            AddColumn("dbo.Clients", "DeletionDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "DeletionDateTime");
            RenameIndex(table: "dbo.Clients", name: "IX_SalesRepresentativeId", newName: "IX_SalesRepresentative");
            RenameColumn(table: "dbo.Clients", name: "SalesRepresentativeId", newName: "SalesRepresentative");
        }
    }
}
