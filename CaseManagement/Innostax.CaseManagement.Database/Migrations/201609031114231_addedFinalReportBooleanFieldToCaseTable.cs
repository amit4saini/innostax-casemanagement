namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedFinalReportBooleanFieldToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "FinalReportSentToSalesRepDate", c => c.DateTime());
            AddColumn("dbo.Cases", "FinalReportSentToClientDate", c => c.DateTime());
            AddColumn("dbo.Cases", "FinalReportSentToClient", c => c.Boolean(nullable: false));
            AddColumn("dbo.Cases", "FinalReportSentToSalesRep", c => c.Boolean(nullable: false));
            DropColumn("dbo.Cases", "FinalReportSentSalesRepDate");
            DropColumn("dbo.Cases", "FinalReportSentClientDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "FinalReportSentClientDate", c => c.DateTime());
            AddColumn("dbo.Cases", "FinalReportSentSalesRepDate", c => c.DateTime());
            DropColumn("dbo.Cases", "FinalReportSentToSalesRep");
            DropColumn("dbo.Cases", "FinalReportSentToClient");
            DropColumn("dbo.Cases", "FinalReportSentToClientDate");
            DropColumn("dbo.Cases", "FinalReportSentToSalesRepDate");
        }
    }
}
