﻿using Microsoft.Owin;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax;
using Innostax.CaseManagement.Web.DependencyResolution;
using Innostax.Common.Database.TenantResolver;

namespace Innostax.CaseManagement.Web.App_Start
{
    public class MultiTenantMiddleware : OwinMiddleware
    {
        private readonly IMultiTenantEngine engine;

       public MultiTenantMiddleware(OwinMiddleware next)
            : base(next)
        {
            engine = IoC.Container.GetInstance<IMultiTenantEngine>();           
         //   if (engine == null)
         //   {
         //      throw new ArgumentNullException("engine");
         //}
        }

        public async override Task Invoke(IOwinContext context)
        {
            await engine.BeginRequest(context);
            await Next.Invoke(context);
            await engine.EndRequest(context);
        }
    }
}
