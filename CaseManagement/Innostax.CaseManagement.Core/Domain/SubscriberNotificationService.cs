﻿
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Core.Domain
{
    public abstract class ISubject
    {
        private System.Collections.Generic.List<IObserver> observers = new System.Collections.Generic.List<IObserver>();

    public void RegisterObserver(IObserver o)
    {
        observers.Add(o);
    }

    public void RemoveObserver(IObserver o)
    {
        observers.Remove(o);
    }

    public List<Guid> NotifyObservers(Guid caseId, string status)
    {
        var listOfUserIdToBeNotified = new List<Guid> { };
        foreach (IObserver o in observers)
        {
                listOfUserIdToBeNotified= o.Execute(caseId, status);
        }
            return listOfUserIdToBeNotified;
    }

        public abstract List<Guid> ChangeCaseStatus(Guid caseId, Guid statusId);    
          
}

    public interface IObserver
    {      
        List<Guid> Execute(Guid caseId,string status);
    }
}  
