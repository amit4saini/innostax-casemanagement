﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Query;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class SearchServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private SearchService sut;
        private Mock<IUserService> _userService;
        private Mock<ICaseService> _caseService;
        private Mock<IClientService> _clientService;
        private Mock<ITaskService> _taskService;
        private Mock<IDiscussionService> _discussionService;
        private Mock<ICaseFileAssociationService> _caseFileAssociationService;
        private Mock<ICommonService> _commonService;
        ODataQueryOptions<Models.Case> options;
        ODataQueryOptions<Models.Task> taskOptions;
        ODataQueryOptions<Models.Discussion> discussionOptions;
        HttpRequestMessage request;
        Guid clientId1;
        Guid clientId2;
        Guid caseId1;
        Guid caseId2;
        Guid fileId1;
        Guid fileId2;
        Guid taskId1;
        Guid taskId2;
        Guid discussionId1;
        Guid discussionId2;
        Guid clientContactUserId;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            clientContactUserId = Guid.NewGuid();
            ODataModelBuilder caseModelBuilder = new ODataConventionModelBuilder();
            caseModelBuilder.EntitySet<Models.Case>("Case");
            options = new ODataQueryOptions<Models.Case>(new ODataQueryContext(caseModelBuilder.GetEdmModel(), typeof(Models.Case)), new System.Net.Http.HttpRequestMessage());
            request = new HttpRequestMessage();
            ODataModelBuilder taskModelBuilder = new ODataConventionModelBuilder();
            taskModelBuilder.EntitySet<Models.Task>("Task");
            taskOptions = new ODataQueryOptions<Models.Task>(new ODataQueryContext(taskModelBuilder.GetEdmModel(), typeof(Models.Task)), new HttpRequestMessage());
            ODataModelBuilder discussionModelBuilder = new ODataConventionModelBuilder();
            discussionModelBuilder.EntitySet<Models.Discussion>("discussion");
            discussionOptions = new ODataQueryOptions<Models.Discussion>(new ODataQueryContext(discussionModelBuilder.GetEdmModel(), typeof(Models.Discussion)), new HttpRequestMessage());
            _commonService = new Mock<ICommonService>();
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            caseId1 = Guid.NewGuid();
            caseId2 = Guid.NewGuid();
            fileId1 = Guid.NewGuid();
            fileId2 = Guid.NewGuid();
            taskId1 = Guid.NewGuid();
            taskId2 = Guid.NewGuid();
            discussionId1 = Guid.NewGuid();
            discussionId2 = Guid.NewGuid();
            _caseService = new Mock<ICaseService>();
            _discussionService = new Mock<IDiscussionService>();
            _taskService = new Mock<ITaskService>();
            _clientService = new Mock<IClientService>();
            _caseFileAssociationService = new Mock<ICaseFileAssociationService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            sut = new SearchService(
                innostaxDbMock.Object,
                _caseService.Object, _taskService.Object, _discussionService.Object, _commonService.Object, _userService.Object
              );
        }
        private Client GetDummyClients()
        {
            var dummyClient = new Client
            {
                ClientId = clientId2,
                IndustryId = 1,
                IndustrySubCategoryId = 1,
                ClientName = "clientName2",
                Notes = "notes2",
                SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "firstName2", LastName = "lastName2" },
                IsDeleted = false
            };
            return dummyClient;
        }
        private ClientContactUser GetDummyClientContactUser()
        {
            var dummyClientContactUser = new ClientContactUser
            {
                Id = clientContactUserId,
                CountryId = 1,
                StateId = 1,
                PositionHeldbyContact = "positionHeldBy",
                Street = "street",
                City = "city",
                ZipCode = "zipCode",
                IsDeleted = false
            };
            return dummyClientContactUser;
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=caseId1,
                    Client = GetDummyClients(),
                    ClientId=clientId1,
                    IsDeleted=false,
                    ClientPO="clientPo",
                    NickName="New Matter",
                    ReportSubject="subject",
                    CaseNumber="DD-234-123",
                    AdditionalSalesRepNotes="additional notes",
                    ClientContactUser= GetDummyClientContactUser(),
                    
                },
                new Case
                {
                    CaseId=caseId2,
                    Client = GetDummyClients(),
                    ClientContactUser = GetDummyClientContactUser(),
                    ClientId=clientId2,
                    IsDeleted=false,
                    ClientPO="clientPo2",
                    NickName="New Matter2",
                    ReportSubject="subject2",
                    CaseNumber="DD-234-1232",
                    AdditionalSalesRepNotes="additional notes2"
                }
            };
            return dummyCase;
        }
        private Models.Case GetACase()
        {
            var dummyCase = new Models.Case
            {
                CaseId = caseId1,
                ClientId = clientId2,
                IsDeleted = false,
                ClientPO = "clientPo2",
                NickName = "New Matter2",
                ReportSubject = "subject2",
                CaseNumber = "DD-234-1232",
                AdditionalSalesRepNotes = "additional notes2"
            };
            return dummyCase;
        }
        private List<UploadedFile> GetDummyFiles()
        {
            var dummyFiles = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileId=fileId1,
                   AzureKey="test",
                   FileContent="testing",
                   FileName="test.pdf",
                    IsDeleted=false
                },
                new UploadedFile
                {
                    FileId=fileId2,
                    AzureKey ="test2",
                   FileContent="testing2",
                   FileName="test2.pdf",
                    IsDeleted=false
                }
            };
            return dummyFiles;
        }
        private List<CaseFileAssociation> GetDummyCaseFileAssociation()
        {
            var dummyCaseFileAssociation = new List<CaseFileAssociation>
            {
                new CaseFileAssociation
                {
                   CaseId=caseId2,
                   FileId=fileId1,
                   IsDeleted=false,

                    Case = new Case {
                    CaseId =caseId2,
                    ClientId=clientId2,
                    IsDeleted=false,
                    ClientPO="clientPo2",
                    NickName="New Matter2",
                    ReportSubject="NotPresent",
                    CaseNumber="DD-234-1232",
                    AdditionalSalesRepNotes="additional notes2"},

                    UploadedFile = new UploadedFile {
                          FileId =fileId1,
                          AzureKey="NotPresent",
                          FileContent="testing",
                          FileName="test.pdf",
                          IsDeleted=false }
                },
                new CaseFileAssociation
                {
                    CaseId=caseId1,
                   FileId=fileId2,
                    IsDeleted=false,

                    Case = new Case {
                    CaseId =caseId1,
                    ClientId=clientId2,
                    IsDeleted=false,
                    ClientPO="clientPo2",
                    NickName="New Matter2",
                    ReportSubject="NotPresent",
                    CaseNumber="DD-234-1232",
                    AdditionalSalesRepNotes="additional notes2"},

                    UploadedFile = new UploadedFile {
                          FileId =fileId1,
                          AzureKey="test",
                          FileContent="NotPresent",
                          FileName="test.pdf",
                          IsDeleted=false }
                }
            };
            return dummyCaseFileAssociation;
        }
        private List<Innostax.Common.Database.Models.Task> GetDummyTask()
        {
            var dummyTask = new List<Innostax.Common.Database.Models.Task>
            {
                new Innostax.Common.Database.Models.Task
                {
                   CaseId=caseId1,
                   TaskId=taskId1,
                   Title="title1",
                   Description="dummy description1",
                   IsDeleted=false
                },
                new Innostax.Common.Database.Models.Task
                {
                    CaseId=caseId2,
                   TaskId=taskId2,
                   Title="title2",
                   Description="dummy description2",
                   IsDeleted=false
                }
            };
            return dummyTask;
        }
        private List<Innostax.Common.Database.Models.Discussion> GetDummyDiscussion()
        {
            var dummyDiscussion = new List<Discussion>
            {
                new Discussion
                {
                   CaseId=caseId1,
                   Title="title1",
                   Message="dummy Message1",
                   IsDeleted=false
                },
                new Discussion
                {
                   CaseId=caseId2,
                   Title="title2",
                   Message="dummy Message1",
                   IsDeleted=false
                }
            };
            return dummyDiscussion;
        }
         IEnumerable<Models.Case> CaseReturn()
        {
            yield return new Models.Case() {CaseId= caseId1};
            yield return new Models.Case() { CaseId = caseId2};
            yield return new Models.Case() {CaseId= caseId1 };
            }
         IEnumerable<Models.Task> TaskReturn()
        {
            yield return new Models.Task() { CaseId = caseId1, Case = GetACase() };
            yield return new Models.Task() { CaseId = caseId2, Case = GetACase() };
            yield return new Models.Task() {CaseId= caseId1, Case = GetACase() };
        }
         IEnumerable<Models.Discussion> DiscussionReturn()
        {
            yield return new Models.Discussion() { CaseId = caseId1, Case = GetACase() };
            yield return new Models.Discussion() { CaseId = caseId2, Case = GetACase() };
            yield return new Models.Discussion() {CaseId = caseId2, Case = GetACase() };
        }

        [TestMethod]
        public void GivenCalling_SearchByKeyword_WhenKeywordIsPresentInFiles_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(GetDummyDiscussion());
            innostaxDbMock.Setup(c => c.CaseFileAssociations).ReturnsDbSet(GetDummyCaseFileAssociation());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyFiles());
            innostaxDbMock.Setup(c => c.CaseFileAssociations).ReturnsDbSet(GetDummyCaseFileAssociation());
            innostaxDbMock.Setup(c => c.DiscussionFileAssociations).ReturnsDbSet(new List<DiscussionFileAssociation> { new DiscussionFileAssociation()});
            innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(new List<TaskFileAssociation> { new TaskFileAssociation()});
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(new List<TaskCommentFileAssociation> { new TaskCommentFileAssociation() { TaskComment=new TaskComment() { Task=new Innostax.Common.Database.Models.Task()} } });
            innostaxDbMock.Setup(c => c.DiscussionMessageFileAssociations).ReturnsDbSet(new List<DiscussionMessageFileAssociation> { new DiscussionMessageFileAssociation() { DiscussionMessage=new DiscussionMessage() { Discussion=new Discussion()} } });
            _commonService.Setup(c => c.GetAuthorizedDiscussions(null)).Returns(GetDummyDiscussion().AsQueryable());
            _commonService.Setup(c => c.GetAuthorizedTasks(null)).Returns(GetDummyTask().AsQueryable());
            IEnumerable<Models.Case> caseItems = CaseReturn();
            _caseService.Setup(c=>c.SearchCasesByKeyword(It.IsAny<ODataQueryOptions<Models.Case>>(), It.IsAny<HttpRequestMessage>(), "NotPresent")).Returns(new PageResult<Models.Case>(caseItems, new Uri("http://www.google.com"), 12));
            IEnumerable<Models.Task> taskItems = TaskReturn();
            _taskService.Setup(c => c.SearchTasksByKeyword(It.IsAny<ODataQueryOptions<Models.Task>>(), It.IsAny<HttpRequestMessage>(), "NotPresent", null)).Returns(new Models.TaskPageResult() { Result = new PageResult<Models.Task>(taskItems, new Uri("http://www.google.com"), 12) });
            IEnumerable<Models.Discussion> discussionItems = DiscussionReturn();
            _discussionService.Setup(c => c.SearchDiscussionsByKeyword(It.IsAny<ODataQueryOptions<Models.Discussion>>(), It.IsAny<HttpRequestMessage>(), null, "NotPresent")).Returns(() => Task<Models.DiscussionPageResult>.Factory.StartNew(() => new Models.DiscussionPageResult() { Result= new PageResult<Models.Discussion>(discussionItems, new Uri("http://www.google.com"), 12)}));            
            var result = sut.SearchByKeyword(options, request, "NotPresent");
            Assert.AreEqual(result.Result.Count, 2);
        }
      
    }
}
   
