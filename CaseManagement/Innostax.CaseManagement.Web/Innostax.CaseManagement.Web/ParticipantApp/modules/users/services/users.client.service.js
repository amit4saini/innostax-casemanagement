(function () {
    'use strict';
    // Users service used for communicating with the users REST endpoint
    angular.module('users').factory('Users', ['$http', 'usSpinnerService', 'ErrorHandlerService', '$uibModal', 'toastr','$window',
      function ($http, usSpinnerService, ErrorHandlerService, $uibModal, toastr, $window) {
          var vm = this;
          vm.defaultPageSize = $window.defaultPageSize;
          vm.init = init;
          vm.clear = clear;
          vm.editUser = editUser;
          vm.fetchAndStoreUsersList = fetchAndStoreUsersList;
          vm.fetchAndStoreRolesList = fetchAndStoreRolesList;
          vm.openEditUserModal = openEditUserModal;
          vm.closeModal = closeModal;
          vm.onRoleSelected = onRoleSelected;
          vm.deleteUser = deleteUser;
          vm.openAddUserModal = openAddUserModal;
          vm.clearPreFilledValues = clearPreFilledValues;
          vm.validateUser = validateUser;
          vm.saveUser = saveUser;
          vm.getUserDetails = getUserDetails;
          vm.users = [];
          vm.user = {};
          vm.roles = [];
          vm.role = {};
          vm.showUserValidationMessage = false;
          vm.areAllFieldsValid = false;
          vm.allPages = [];
          vm.isUserBeingEdited = false;

          function init() {
              fetchAndStoreUsersList(0);
          }

          function fetchAndStoreUsersList(pageIndex) {
              vm.pageIndex = pageIndex;
              var skipCount = vm.pageIndex * vm.defaultPageSize;             
              $http.get('/api/users?$skip=' + skipCount+'&$top='+vm.defaultPageSize)
                  .success(function (response, status) {
                      vm.users = response.items;
                      vm.totalNumberOfUsers = response.count;
                      vm.allPages.length = Math.ceil(vm.totalNumberOfUsers / vm.defaultPageSize);
                  })
                  .error(function (response, status) {
                      ErrorHandlerService.DisplayErrorMessage(response,status);
                      vm.user = [];
                      vm.totalNumberOfUsers = 0;
                  });
          }

          function editUser(userId) {
              getUserDetails(userId);
              openEditUserModal();
          }

          function openEditUserModal() {
              vm.isUserBeingEdited = true;
              vm.modalInstance = $uibModal.open({
                  templateUrl: '/webapp/modules/users/views/add-user.client.view.html',
                  size: 'lg'
              });
              fetchAndStoreRolesList();
              vm.modalInstance.result.then(function () {
              }, function () {
                  vm.isUserBeingEdited = false;
              });
          }

          function fetchAndStoreRolesList() {
              $http.get('/api/user/getRoles')
                  .success(function (response, status) {
                      vm.roles = response.data;
                  })
                  .error(function (response, status) {
                      ErrorHandlerService.DisplayErrorMessage(response);
                      vm.roles = [];
                  });
          }

          function onRoleSelected($item) {
              vm.user.role = $item;
          }

          function clear($event, $select) {
              $event.stopPropagation();
              $select.selected = undefined;
              $select.search = undefined;
              $select.activate();
          }

          function getUserDetails(userId) {
              $http.get('api/user/' + userId)
              .success(function (response, status) {
                  vm.user = response.data;
                  vm.user.role = response.data.role;
              })
                  .error(function (response, status) {
                      ErrorHandlerService.DisplayErrorMessage(response);
                      vm.user = [];
                  });

          }

          function closeModal() {
              vm.modalInstance.dismiss('cancel');
          }

          function deleteUser(userId) {
              $http.delete('/api/user/' + userId)
                 .success(function (response, status) {
                     
                     toastr.success("User deleted successfully.");
                     vm.users.splice(userId, 1);
                   if (vm.users.length < 1)
                       fetchAndStoreUsersList(vm.pageIndex - 1);
                   else
                       fetchAndStoreUsersList(vm.pageIndex);
                 }).error(function (response, status) {
                     ErrorHandlerService.DisplayErrorMessage(response);
                 });
          }

          function openAddUserModal() {
              vm.modalInstance = $uibModal.open({
                  templateUrl: '/webapp/modules/users/views/add-user.client.view.html',
                  size: 'lg'
              });
              fetchAndStoreRolesList();
              clearPreFilledValues();
          }

          function clearPreFilledValues() {
              vm.user = {}
              vm.showUserValidationMessage = false;
              vm.areAllFieldsValid = false;
          }

          function validateUser() {
              vm.showUserValidationMessage = true;
              return vm.areAllFieldsValid;
          }

          function saveUser(isValid) {
              vm.areAllFieldsValid = isValid;
              if (!validateUser()) { return; }
              if (!vm.isUserBeingEdited) {
                  $http.post('/api/invitation/', vm.user)
                    .success(function (response, status) {
                        toastr.success("Invitation sent");
                        vm.closeModal();
                        fetchAndStoreUsersList(vm.pageIndex);
                    }).error(function (response, status) {
                        ErrorHandlerService.DisplayErrorMessage(response, status);
                    });
              }
              else {
                  $http.put('/api/user/' + vm.user.id, vm.user)
                       .success(function (response, status) {
                           toastr.success("User edited successfully.");
                           fetchAndStoreUsersList(vm.pageIndex);
                           vm.closeModal();
                       }).error(function (response, status) {
                           ErrorHandlerService.DisplayErrorMessage(response);
                       });
              }
          }
          
          return vm;
      }
    ]);
})();
