﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Innostax.Common.Database.Models.UserManagement;

namespace Innostax.Common.Database.Models
{
    public class Discussion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }      
        public string Title { get; set; }
        public string Message { get; set; }     
        public DateTime CreationDateTime { get; set; }
        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public Guid CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public DateTime LastEditedDateTime { get; set; }

        public Guid EditedBy { get; set; }
        [ForeignKey("EditedBy")]
        public User EditedByUser { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        [DefaultValue(false)]
        public bool IsArchive { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsNotes { get; set; }
        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public User DeleteByUser { get; set; }
    }
}
