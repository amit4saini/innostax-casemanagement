﻿(function () {
    'use strict';
    angular.module('dashboard').factory('Dashboard', ['$uibModal', '$http', 'toastr', 'ErrorHandlerService', 'Enums', 'calendarConfig', '$global', '$stateParams', '$location', 'Calendar',
    function ($uibModal, $http, toastr, ErrorHandlerService, Enums, calendarConfig, $global, $stateParams, $location, CalendarService) {
        var vm = this;
        
        function init()
        {           
            vm.getDashboardDetails(function (response, status) {
                vm.dashboardDetails = response;                              
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);               
            });          
        }

        function getDashboardDetails(cbSuccess, cbError) {
            $http.get('api/dashboard')
         .success(function (response, status) {
             cbSuccess(response, status);
         })
         .error(function (response, status) {
             cbError(response, status);
         });
        }      

        vm.init = init;
        vm.getDashboardDetails = getDashboardDetails;
        vm.userCalendarInit = CalendarService.userCalendarInit;
        vm.getRecentTasks = CalendarService.getRecentTasks;
        vm.roleName = $global.roleName;
        return vm;
    }
    ]);
})();
