namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingBusinessUnitForeignKeyToCase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "BusinessUnitId", c => c.Guid());
            CreateIndex("dbo.Cases", "BusinessUnitId");
            AddForeignKey("dbo.Cases", "BusinessUnitId", "dbo.ContactUserBusinessUnits", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "BusinessUnitId", "dbo.ContactUserBusinessUnits");
            DropIndex("dbo.Cases", new[] { "BusinessUnitId" });
            DropColumn("dbo.Cases", "BusinessUnitId");
        }
    }
}
