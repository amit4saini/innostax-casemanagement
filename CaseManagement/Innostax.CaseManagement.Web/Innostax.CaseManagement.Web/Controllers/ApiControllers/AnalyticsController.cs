﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Script.Serialization;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class AnalyticsController : ApiController
    {
        private readonly IAnalyticsService _analyticsService;
        public AnalyticsController(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;   
        }

        //GET api/<controller>
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Analytics/Cases")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS})]    
        public PageResult<Case> GetCaseAnalytics(ODataQueryOptions<Case> options,DateTime? startDate, DateTime? endDate)
        {
            return _analyticsService.GetCaseAnalytics(options, Request, startDate, endDate);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Analytics/Clients")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS})]
        public PageResult<Client> GetClientAnalytics(ODataQueryOptions<Client> options, DateTime? startDate, DateTime? endDate)
        {
            return _analyticsService.GetClientAnalytics(options, Request, startDate, endDate);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/{userId}/Analytics")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS })]
        public UserAnayticsModel GetUserAnalytics( Guid userId)
        {
            return _analyticsService.GetUserAnalytics(userId, null);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Analytics/{userId}/{reportId}/Reports")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS })]
        public AnalyticsConfiguration GetAnalyticsReportById(Guid userId, Guid reportId)
        {
            return _analyticsService.GetAnalyticsReportById(userId, reportId);
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Http.Route("api/AnalyticsConfiguration")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS })]
        public void SaveAnalyticsReport(AnalyticsConfiguration configuration)
        {
            _analyticsService.SaveAnalyticsReport(configuration);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Analytics/{userId}/Reports")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS })]
        public List<AnalyticsConfiguration> GetAnalyticsReportsForUser(Guid userId)
        {
            return _analyticsService.GetAllAnalyticsReportsForUser(userId);
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Http.Route("api/AnalyticsConfiguration/{analyticsType}/ExportToExcel")]
        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.VIEW_ANALYTICS })]
        public string ExportCurrentAnalyticsToExcel(string[] currentColumns, string analyticsType,DateTime? startDate,DateTime? endDate)
        {      
            var fileName = _analyticsService.ExportCurrentAnalyticsToExcel(currentColumns, analyticsType,startDate,endDate);
            return fileName;          
        }
    }
}
