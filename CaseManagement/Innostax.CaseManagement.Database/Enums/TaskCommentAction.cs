﻿namespace Innostax.Common.Database.Enums
{
    public enum TaskCommentAction
    {
        POSTED,
        EDITED,
        ARCHIVED
    }
}
