﻿using Innostax.Common.Database.Database;
using System;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using AutoMapper;
using System.Collections.Generic;
using Innostax.CaseManagement.Models;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseStatusService
    {
        List<Guid> ChangeCaseStatus(Guid caseId,Guid statusId);
        List<CaseStatus> GetCaseStatusList();
    }

    public class CaseStatusService: ISubject,ICaseStatusService
    {                  
        private IInnostaxDb _innostaxDb;
        private ICaseHistoryService _caseHistoryService;
        private INotificationService _notificationService;
        private IUserService _userService;

        public CaseStatusService(IInnostaxDb innostaxDb, ICaseHistoryService caseHistoryService,INotificationService notificationService,IUserService userService)
        {
            _caseHistoryService = caseHistoryService;
            _notificationService = notificationService;  
            _innostaxDb = innostaxDb;
            _userService = userService;         
            RegisterObserver(new LogCaseStatusHistoryService(_caseHistoryService));
            RegisterObserver(new SendNotificationToSubscriberService(_notificationService,_innostaxDb,_userService));
        }

         public override List<Guid> ChangeCaseStatus(Guid caseId, Guid statusId)
        {
            var listOfUserIdToBeNotified = new List<Guid> { };
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId && x.IsDeleted != true);
            if (existingCase != null)
            {
                if(existingCase.StatusId != null)
                {                
                    var caseStatus = _innostaxDb.CaseStatus.FirstOrDefault(x => x.Id == statusId);
                    existingCase.StatusId = statusId;                                     
                    _innostaxDb.Cases.AddOrUpdate(existingCase);
                    _innostaxDb.SaveChangesWithErrors();
                    listOfUserIdToBeNotified = NotifyObservers(caseId, caseStatus.Status);
                }
            }
            return listOfUserIdToBeNotified;
        }

        public List<CaseStatus> GetCaseStatusList()
        {
            var caseStatusList = _innostaxDb.CaseStatus.ToList();
            return Mapper.Map<List<CaseStatus>>(caseStatusList);
        }

    }
}
