﻿using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionTaggedUserService
    {
        void SaveDiscussionTaggedUsers(Guid discussionId, List<Guid> taggedUserIdsList);
        List<Models.Account.User> GetDiscussionTaggedUsers(Guid discussionId);
        void DeleteDiscussionTaggedUsers(Guid discussionId);
    }
    public class DiscussionTaggedUserService: IDiscussionTaggedUserService
    {
        private IInnostaxDb _innostaxDb;

        public DiscussionTaggedUserService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
            
        }
        public void SaveDiscussionTaggedUsers(Guid discussionId, List<Guid> taggedUserIdsList)
        {
            var existingtaggedUsersList = _innostaxDb.DiscussionTaggedUsers.Where(x => x.DiscussionId == discussionId).ToList();
            if (existingtaggedUsersList != null)
            {
                existingtaggedUsersList.ForEach(x => _innostaxDb.DiscussionTaggedUsers.Remove(x));
            }
            foreach (var eachTaggedUser in taggedUserIdsList.Distinct())
            {
                var newDiscussionDescritionTagUser = new DiscussionTaggedUser
                {
                    DiscussionId = discussionId,
                    UserId = eachTaggedUser
                };
                _innostaxDb.DiscussionTaggedUsers.Add(newDiscussionDescritionTagUser);
            }
            _innostaxDb.SaveChangesWithErrors();
        }

        public List<Models.Account.User> GetDiscussionTaggedUsers(Guid discussionId)
        {
            var existingTaggedUsersList = _innostaxDb.DiscussionTaggedUsers.Include(y => y.User).Where(x => x.DiscussionId == discussionId).Select(y => y.User).ToList();
            var usersList = new List<Models.Account.User>();
            usersList = Mapper.Map<List<Models.Account.User>>(existingTaggedUsersList);
            return usersList;
        }

        public void DeleteDiscussionTaggedUsers(Guid discussionId)
        {
            var existingTaggedUsersList = _innostaxDb.DiscussionTaggedUsers.Where(x => x.DiscussionId == discussionId).ToList();
            existingTaggedUsersList.ForEach(x => _innostaxDb.DiscussionTaggedUsers.Remove(x));
        }
    }
}
