namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIHIAssignedUserIdToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "IHIAssignedUserId", c => c.Guid());
            CreateIndex("dbo.Cases", "IHIAssignedUserId");
            AddForeignKey("dbo.Cases", "IHIAssignedUserId", "dbo.Users", "Id");
            DropColumn("dbo.Cases", "IHIAssigned");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "IHIAssigned", c => c.Boolean());
            DropForeignKey("dbo.Cases", "IHIAssignedUserId", "dbo.Users");
            DropIndex("dbo.Cases", new[] { "IHIAssignedUserId" });
            DropColumn("dbo.Cases", "IHIAssignedUserId");
        }
    }
}
