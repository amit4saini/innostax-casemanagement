﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class CaseReportAssociation
    {
        public Guid CaseReportAssociationId { get; set; }
        public Guid CaseId { get; set; }       
        public List<Report> Reports { get; set; }       
       
    }
}
