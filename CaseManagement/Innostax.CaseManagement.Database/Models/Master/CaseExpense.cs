﻿using Innostax.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class CaseExpense
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }

        public string Note { get; set; }

        public decimal Quantity { get; set; }

        public decimal Cost { get; set; }  

        public DateTime CreationDateTime { get; set; }

        public Guid? CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public Guid? LastEditedBy { get; set; }

        [ForeignKey("LastEditedBy")]
        public User LastEditedByUser { get; set; }

        public DateTime? LastEditedDateTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }

        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }

        public Guid? VendorId { get; set; }

        [ForeignKey("VendorId")]
        public Vendor Vendor { get; set; }
        public string OriginalCurrencyCost { get; set; }
        public int ExpensePaymentType { get; set; }
        public bool InvoiceApproved { get; set; }
    }
}