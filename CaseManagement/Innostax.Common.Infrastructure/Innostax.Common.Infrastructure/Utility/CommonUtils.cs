﻿using System;

namespace Innostax.Common.Infrastructure.Utility
{
    public interface ICommonUtils
    {
        DateTime CurrentDateTime();
        
    }
    public class CommonUtils : ICommonUtils
    {
        public DateTime CurrentDateTime()
        {
            return DateTime.Now;
        }
        
    }
}
