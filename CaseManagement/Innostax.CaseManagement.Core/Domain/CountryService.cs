﻿using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICountryService
    {
        List<Innostax.CaseManagement.Models.Country> GetAllCountries();
        List<Innostax.CaseManagement.Models.Country> GetAllCountriesByRegion(string regionId);
    }

    public class CountryService : ICountryService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public CountryService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;

        }

        public List<Innostax.CaseManagement.Models.Country> GetAllCountries()
        {
            var result = new List<Innostax.CaseManagement.Models.Country>();
            var existingCountries = _innostaxDb.Countries.ToList();
            if (existingCountries.Count == 0)
            {
                string countryNotFoundError = Common.Constants.ErrorMessages.Common.CountryError.COUNTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(countryNotFoundError);
            }
            result = new List<Innostax.CaseManagement.Models.Country>();
            foreach (var eachCountry in existingCountries)
            {
                var country = Mapper.Map<Innostax.CaseManagement.Models.Country>(eachCountry);
                result.Add(country);
            }
            return result;
        }

        public List<Innostax.CaseManagement.Models.Country> GetAllCountriesByRegion(string id)
        {
            var result = new List<Innostax.CaseManagement.Models.Country>();
            int regionId;
            if (!int.TryParse(id, out regionId))
            {
                string invalidRegionIdError = Common.Constants.ErrorMessages.Common.RegionError.INVALID_REGION_ID;
                _errorHandlerService.ThrowResourceNotFoundError(invalidRegionIdError);
            }
            var existingCountriesbyRegion = _innostaxDb.Countries.Where(t => t.RegionId == regionId).ToList();
            if (existingCountriesbyRegion.Count == 0)
            {
                string countryNotFoundError = Common.Constants.ErrorMessages.Common.CountryError.COUNTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(countryNotFoundError);

            }
            result = new List<Innostax.CaseManagement.Models.Country>();
            foreach (var eachCountry in existingCountriesbyRegion)
            {
                var country = Mapper.Map<Innostax.CaseManagement.Models.Country>(eachCountry);
                result.Add(country);
            }
            return result;
        }
    }
}
