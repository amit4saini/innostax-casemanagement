﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class CaseHistory
    {       
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Account.User User { get; set; }
        public Common.Database.Enums.Action Action { get; set; }
        public DateTime? CreationDatetime { get; set; }
        public Guid CaseId { get; set; }
        public string NewValue { get; set; }
    }
}
