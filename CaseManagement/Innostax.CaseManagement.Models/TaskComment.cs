﻿using System;
using System.Collections.Generic;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Models
{
    public class TaskComment
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; }
        public string Comment { get; set; }
        public Guid CreatedBy { get; set; }
        public User CreatedByUser { get; set; }
        public DateTime CreationDateTime { get; set; }
        public Guid LastEditedBy { get; set; }
        public DateTime LastEditedDateTime { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? DeletedBy { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public List<FileModel> TaskCommentFiles{get; set;}
        public List<string> TaggedUsersEmailList { get; set; }
        public Task Task { get; set; }
    }
}
