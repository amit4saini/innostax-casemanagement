﻿using Innostax.Common.Database.Database;
using Moq;
using System.Collections.Generic;
using Innostax.CaseManagement.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using Innostax.Common.Database.Models;
using System.Web.Http;
using System.Net;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class RegionServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private RegionService sut;
        private Mock<IRegionService> regionService;
        private ErrorHandlerService errorHandlerService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            regionService = new Mock<IRegionService>();
            errorHandlerService = new ErrorHandlerService();
            sut = new RegionService(innostaxDbMock.Object, errorHandlerService);      
        }
        private List<Region> GetDummyRegions()
        {
            var dummyRegions = new List<Region>
            {
                new Region
                {
                    RegionId = 1,
                    RegionName = "South America"
                },
                 new Region
                {
                    RegionId = 2,
                    RegionName = "Europe"
                }
            };
            return dummyRegions;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingGetAllRegions_NoRegionFound_ThenThrowsNotFound()
        {
            try
            {
                var dummyRegions = new List<Region>();
                innostaxDbMock.Setup(c => c.Regions).ReturnsDbSet(dummyRegions);
                sut.GetAllRegions();
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCallingGetAllRegions_WhenRegionFound_ThenMappedResultReturn()
        {
            var dummyRegions = GetDummyRegions();
            innostaxDbMock.Setup(c => c.Regions).ReturnsDbSet(dummyRegions);
            var actual = sut.GetAllRegions();
            Assert.IsNotNull(actual);
        }
    }
}