﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskFileAssociationService
    {
        void Save(Task selectedTask);
        void SaveTaskAssociatedFiles(Task selectedTask);
        List<Models.FileModel> GetTaskAssociatedFiles(Guid taskId);
        void RemoveTaskFileAssociation(Guid taskId, Guid? fileId);
    }

    public class TaskFileAssociationService : ITaskFileAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly ICommonService _commonService;
        public TaskFileAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
        }

        public void Save(Task selectedTask)
        {
            var existingTask = _innostaxDb.Tasks.FirstOrDefault(x => x.TaskId == selectedTask.TaskId && x.IsDeleted != true);
            if (existingTask != null)
            {
                _commonService.ValidateAccess(Validate.TASK,Permission.CASE_UPDATE_ALL, existingTask.TaskId);
                var existingTaskFileAssociations = _innostaxDb.TaskFileAssociations.Include(y => y.UploadedFile).Where(x => x.TaskId == selectedTask.TaskId && x.IsDeleted == false).ToList();
                var listOfFiles = selectedTask.TaskFiles.ToList();
                if (existingTaskFileAssociations.Count > 0)
                {
                    foreach (var eachAssociation in existingTaskFileAssociations)
                    {
                        if (!listOfFiles.Select(x=>x.FileId).Contains(eachAssociation.FileId))
                        {
                            var fileName = eachAssociation.UploadedFile.FileName;
                            eachAssociation.IsDeleted = true;
                            var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_DELETED, fileName, existingTask.Title);
                            _caseHistoryService.SaveCaseHistory(existingTask.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                        }
                    }
                }
                _innostaxDb.SaveChangesWithErrors();

                if (listOfFiles.Count > 0)
                {
                    foreach (var file in listOfFiles)
                    {
                        var checkIfTaskFileAssociationPresent = _innostaxDb.TaskFileAssociations.FirstOrDefault(x => x.FileId == file.FileId && x.TaskId == selectedTask.TaskId && x.IsDeleted != true);
                        if (checkIfTaskFileAssociationPresent == null)
                        {
                            var newTaskFileAssociation = new Innostax.Common.Database.Models.TaskFileAssociation();
                            newTaskFileAssociation.TaskId = selectedTask.TaskId;
                            newTaskFileAssociation.FileId = file.FileId;
                            newTaskFileAssociation.IsDeleted = false;
                            _innostaxDb.TaskFileAssociations.Add(newTaskFileAssociation);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_ADDED, file.FileName, existingTask.Title);
                            _caseHistoryService.SaveCaseHistory(existingTask.CaseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                        }
                    }
                }
                _innostaxDb.SaveChangesWithErrors();
            }
            else
            {
                var invalidTaskId = Common.Constants.ErrorMessages.Common.TaskFileAssociationError.INVALID_TASK_ID;
                _errorHandlerService.ThrowBadRequestError(invalidTaskId);
            }
        }

        public void SaveTaskAssociatedFiles(Task selectedTask)
        {
            selectedTask.TaskFiles = selectedTask.TaskFiles.Distinct().ToList();
            var existingTask = _innostaxDb.Tasks.FirstOrDefault(x => x.TaskId == selectedTask.TaskId && x.IsDeleted != true);
            if (existingTask != null)
            {
                var existingTaskFileAssociations = _innostaxDb.TaskFileAssociations.Include(y => y.UploadedFile).Where(x => x.TaskId == selectedTask.TaskId && x.IsDeleted == false).ToList();
                var listOfFiles = selectedTask.TaskFiles;                
                    foreach (var eachFile in listOfFiles)
                    {
                        if (!existingTaskFileAssociations.Select(x => x.FileId).Contains(eachFile.FileId))
                        {
                            var newTaskFileAssociation = new Innostax.Common.Database.Models.TaskFileAssociation();
                            newTaskFileAssociation.TaskId = selectedTask.TaskId;
                            newTaskFileAssociation.FileId = eachFile.FileId;
                            newTaskFileAssociation.IsDeleted = false;
                            _innostaxDb.TaskFileAssociations.Add(newTaskFileAssociation);
                            _innostaxDb.SaveChangesWithErrors();
                        }
                    }                                                           
            }
            else
            {
                var invalidTaskId = Common.Constants.ErrorMessages.Common.TaskFileAssociationError.INVALID_TASK_ID;
                _errorHandlerService.ThrowBadRequestError(invalidTaskId);
            }
        }

        public List<Models.FileModel> GetTaskAssociatedFiles(Guid taskId)
        {
            var result = new List<Models.FileModel>();
            var taskFileAssociations = _innostaxDb.TaskFileAssociations.Include(x => x.UploadedFile).Include(x => x.Task).Where(x => x.TaskId == taskId && x.IsDeleted != true).ToList();
            if (taskFileAssociations.Count > 0)
            {
                _commonService.ValidateAccess(Validate.TASK, Permission.CASE_READ_ALL, taskFileAssociations.First().TaskId);
            }
            foreach (var eachAssociation in taskFileAssociations)
            {
                var existingFile = _innostaxDb.UploadFiles.Include(y=>y.User).FirstOrDefault(x => x.FileId == eachAssociation.FileId);
                var mappedFile = Mapper.Map<CaseManagement.Models.FileModel>(existingFile);
                mappedFile.FileUrl = existingFile.FileUrl;
                mappedFile.FileName = existingFile.FileName;
                if (existingFile.User != null)
                {
                    mappedFile.UserName = existingFile.User.FirstName + " " + existingFile.User.LastName;
                }
                result.Add(mappedFile);
            }
            return result.OrderByDescending(x=>x.CreateDateTime).ToList();
        }

        public void RemoveTaskFileAssociation(Guid taskId, Guid? fileId)
        {
            var result = new Result();
            var existingTaskFileAssociations = _innostaxDb.TaskFileAssociations.Include(y=>y.Task).Include(y=>y.Task.Case).Include(y=>y.UploadedFile).Where(x => x.TaskId == taskId && x.IsDeleted == false).ToList();
            if (existingTaskFileAssociations.Count > 0)
            {
                _commonService.ValidateAccess(Validate.TASK, Permission.CASE_DELETE_ALL, existingTaskFileAssociations.First().TaskId);
                if (fileId == null || fileId == Guid.Empty)
                foreach (var eachAssociation in existingTaskFileAssociations)
                {
                    eachAssociation.IsDeleted = true;
                    var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_DELETED, eachAssociation.UploadedFile.FileName, eachAssociation.Task.Title);
                    _caseHistoryService.SaveCaseHistory(existingTaskFileAssociations.First().Task.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                }
                else
                {
                    existingTaskFileAssociations.FirstOrDefault(x => x.FileId == fileId).IsDeleted = true;
                }
                _innostaxDb.SaveChangesWithErrors();
            }
        }
    }
}
