namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedTimesheetsTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TimeSheets", "EndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TimeSheets", "EndTime", c => c.DateTime(nullable: false));
        }
    }
}
