namespace Innostax.CaseManagement.Models
{
    public class Country
    {      
        public int CountryId { get; set; }
        public string CountryName { get; set; }   
        public int? StateId { get; set; }     
        public string StateName { get; set; }
    }
}