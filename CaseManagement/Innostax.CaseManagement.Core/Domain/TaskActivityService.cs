using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskActivityService
    {
        void SaveTaskActivity(Guid taskId, TaskActivityAction action,string newValue);
        List<TaskActivity> GetTaskActivityById( Guid taskId);
    }

    public class TaskActivityService : ITaskActivityService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private IErrorHandlerService _errorHandlerService;
        private ICommonService _commonService;

        public TaskActivityService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _commonService = commonService;
        }

        public void SaveTaskActivity(Guid taskId, TaskActivityAction action, string newValue)
        {
            Common.Database.Enums.TaskActivityAction mappedAction = (Common.Database.Enums.TaskActivityAction)action;
            var taskActivityModel = new Innostax.Common.Database.Models.TaskActivity
            {
                TaskId = taskId,
                Action = mappedAction,
                CreationDatetime = _commonService.GetUtcDateTime(),
                UserId = _userService.UserId,
                NewValue = newValue
            };
            _innostaxDb.TaskActivities.Add(taskActivityModel);         
                _innostaxDb.SaveChangesWithErrors();
        }

        public List<TaskActivity> GetTaskActivityById(Guid taskId)
        {
            var result = new List<TaskActivity>();
            var allTaskActivitiesList = _innostaxDb.TaskActivities.Where(x => x.TaskId == taskId).OrderByDescending(x => x.CreationDatetime).ToList();
            if(allTaskActivitiesList != null)
            {
                foreach (var taskActivity in allTaskActivitiesList)
                {
                    var mappedTaskActivity = Mapper.Map<TaskActivity>(taskActivity);
                    result.Add(mappedTaskActivity);
                }
            }
            return result;
        }
    }
}
