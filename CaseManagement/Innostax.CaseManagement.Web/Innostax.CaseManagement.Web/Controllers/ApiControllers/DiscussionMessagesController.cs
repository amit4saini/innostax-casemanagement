﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Threading.Tasks;
using Innostax.CaseManagement.Web.Hubs;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Web.Attributes;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class DiscussionMessagesController : ApiController
    {
        private readonly IDiscussionMessageService _discussionMessageService;
        private INotificationHub _notificationHub;
        private INotificationService _notificationService;

        public DiscussionMessagesController(IDiscussionMessageService discussionMessageService, INotificationService notificationService, INotificationHub notificationHub)
        {
            _discussionMessageService = discussionMessageService;
            _notificationHub = notificationHub;
            _notificationService = notificationService;
        }

        [System.Web.Mvc.HttpGet]
        [Route("api/Discussion/{discussionId}/DiscussionMessages")]  
        public async Task<PageResult<DiscussionMessage>> Get(ODataQueryOptions<DiscussionMessage> options,Guid discussionId)
        {
            return await _discussionMessageService.GetAllDiscussionMessagesByDiscussionId(options, Request, discussionId);
        }

        [System.Web.Mvc.HttpPost]
        public void Post(DiscussionMessage discussionMessage)
        {
          var listOfUserToBeNotified =  _discussionMessageService.Save(discussionMessage);
            foreach (var userId in listOfUserToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [System.Web.Mvc.HttpPut]   
        public void Put(DiscussionMessage discussionMessage, Guid id)
        {
            discussionMessage.Id = id;
            _discussionMessageService.Save(discussionMessage);
        }

        [System.Web.Mvc.HttpDelete] 
        public void Delete(Guid id)
        {
            _discussionMessageService.ArchiveUnarchiveDiscussionMessage(id, true);
        }

        [Route("api/DiscussionMessages/{discussionId}/Recent")]
        public DiscussionMessage GetRecentDiscussionMessage(Guid discussionId)
        {
            return _discussionMessageService.GetRecentDiscussionMessageByDiscussionId(discussionId);
        }
    }
}
