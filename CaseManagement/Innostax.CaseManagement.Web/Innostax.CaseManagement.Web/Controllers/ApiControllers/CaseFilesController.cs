﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseFilesController : ApiController
    {
        private ICaseFilesService _caseFilesService;
        private ICaseFileAssociationService _caseFileAssociationService;
        public CaseFilesController(ICaseFilesService caseFilesService, ICaseFileAssociationService caseFileAssociationService)
        {
            _caseFilesService = caseFilesService;
            _caseFileAssociationService = caseFileAssociationService;
        }

        [HttpGet]
        [HasPermission(new Permission[] { Permission.VIEW_ALL_FILES })]
        [Route("api/CaseAllFiles/{caseId}")]
        public List<CaseFileModel> Get(Guid caseId)
        {
            return _caseFilesService.GetAllCaseRelatedFiles(caseId);
        }

        [HttpPut]
        public void Put(Case caseData)
        {
            _caseFileAssociationService.Save(caseData);
        }
    }
}
