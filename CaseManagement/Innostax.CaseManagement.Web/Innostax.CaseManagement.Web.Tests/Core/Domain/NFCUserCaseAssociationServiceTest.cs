﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class NFCUserCaseAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private INFCUserCaseAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> userService;
        private Mock<ICaseHistoryService> caseHistoryService;
        private Guid userId;
        private Guid caseId;


        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            userService = new Mock<IUserService>();
            caseId = new Guid();
            caseHistoryService = new Mock<ICaseHistoryService>();
            userId = Guid.NewGuid();
            sut = new NFCUserCaseAssociationService(innostaxDbMock.Object, errorHandlerService, userService.Object, caseHistoryService.Object);
        }

        private Models.NFCUserCaseAssociation GetDummyNFCUserCaseAssociation()
        {

            var dummyAssociation = new Models.NFCUserCaseAssociation
            {
                Id = 1,
                CaseId = caseId,
                IsActive = false,
                NFCUserId = userId,
                User = GetDummyModelOfUser()
            };
            return dummyAssociation;
        }
        private User GetDummyDatabaseModelOfUser()
        {

            var dummyAssociation =
                new User
                {
                    Id = Guid.NewGuid(),
                    Email = "test@test",
                };
            return dummyAssociation;
        }
        private Innostax.CaseManagement.Models.Account.User GetDummyModelOfUser()
        {

            var dummyAssociation =
                new Models.Account.User
                {
                    Id = Guid.NewGuid(),
                    Email = "test@test",
                };
            return dummyAssociation;
        }

        private List<NFCUserCaseAssociation> GetDummyDatabaseModelOfCaseNFCAssociation()
        {

            var dummyAssociation = new List<NFCUserCaseAssociation>
            {
                new NFCUserCaseAssociation
                {
                Id=1,
                CaseId=caseId,
                IsActive=true,
                NFCUserId=userId,
                User=new User { Roles= { new Innostax.Common.Database.Models.RolesAndPermissionManagement.UserRole() { RoleId = Guid.NewGuid() } }}
                },
                  new NFCUserCaseAssociation
                {
                Id=2,
                CaseId=caseId,
                IsActive=true,
                NFCUserId=userId,
                User=new User { Roles= { new Innostax.Common.Database.Models.RolesAndPermissionManagement.UserRole() { RoleId=Guid.NewGuid()} } }
                }
            };
            return dummyAssociation;
        }

        private List<UserDetail> GetDummyUserDetails()
        {
            var listOfUserDetails = new List<UserDetail>
            {
                new UserDetail
                {
                    Id=Guid.NewGuid(),
                    CompanyName="Test",
                    CountryId=1,
                    Country=new Country { CountryId=1,CountryName="Test"},
                    User=new User { UserName="Test" },
                    UserId=userId
                }
            };
            return listOfUserDetails;
        }      

        private List<Innostax.Common.Database.Models.Case> GetDummyInvalidDatabaseModelOfCase()
        {
            var dummyCase = new List<Innostax.Common.Database.Models.Case>
            {
                new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                },
                 new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                }, };
            return dummyCase;
        }
        private List<Innostax.Common.Database.Models.Case> GetDummyValidDatabaseModelOfCase()
        {
            var dummyCase = new List<Innostax.Common.Database.Models.Case>
            {
                new Innostax.Common.Database.Models.Case
            {
                CaseId = caseId
                },
                 new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                }, };
            return dummyCase;
        }
      

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenCaseTableDoesNotContainsSpecifiedCaseId_ThenThrowsBadRequest()
        {
            try
            {
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyInvalidDatabaseModelOfCase());
                sut.Save(GetDummyNFCUserCaseAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }    

        [TestMethod]
        public void GivenCallingGetNFCUserCaseAssociationByCaseId_WhenItContainsNFC_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.NFCUserCaseAssociations).ReturnsDbSet(GetDummyDatabaseModelOfCaseNFCAssociation());
            innostaxDbMock.Setup(c => c.UserDetails).ReturnsDbSet(GetDummyUserDetails());
            innostaxDbMock.Setup(c => c.Roles).ReturnsDbSet(new List<Innostax.Common.Database.Models.RolesAndPermissionManagement.Role>() { new Innostax.Common.Database.Models.RolesAndPermissionManagement.Role() });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            var result = sut.GetNFCUserCaseAssociationsByCaseId(caseId);
            Assert.AreEqual(result.Count, 2);
        }

        [TestMethod]
        public void GivenCallingGetNFCUserCaseAssociationByCaseId_WhenItDoesNotContainsNFC_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.NFCUserCaseAssociations).ReturnsDbSet(new List<NFCUserCaseAssociation>() { });
            innostaxDbMock.Setup(c => c.UserDetails).ReturnsDbSet(GetDummyUserDetails());
            innostaxDbMock.Setup(c => c.Roles).ReturnsDbSet(new List<Innostax.Common.Database.Models.RolesAndPermissionManagement.Role>() { new Innostax.Common.Database.Models.RolesAndPermissionManagement.Role() });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            var result = sut.GetNFCUserCaseAssociationsByCaseId(caseId);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void GivenCallingGetCasesByNFCUserId_WhenItDoesNotContainsNFC_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.NFCUserCaseAssociations).ReturnsDbSet(new List<NFCUserCaseAssociation>() { });
            innostaxDbMock.Setup(c => c.UserDetails).ReturnsDbSet(GetDummyUserDetails());
            innostaxDbMock.Setup(c => c.Roles).ReturnsDbSet(new List<Innostax.Common.Database.Models.RolesAndPermissionManagement.Role>() { new Innostax.Common.Database.Models.RolesAndPermissionManagement.Role() });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            var result = sut.GetCasesByNFCUserId(userId);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void GivenCallingGetCasesByNFCUserId_WhenItContainsNFC_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.NFCUserCaseAssociations).ReturnsDbSet(GetDummyDatabaseModelOfCaseNFCAssociation());
            innostaxDbMock.Setup(c => c.UserDetails).ReturnsDbSet(GetDummyUserDetails());
            innostaxDbMock.Setup(c => c.Roles).ReturnsDbSet(new List<Innostax.Common.Database.Models.RolesAndPermissionManagement.Role>() { new Innostax.Common.Database.Models.RolesAndPermissionManagement.Role() });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            var result = sut.GetCasesByNFCUserId(userId);
            Assert.AreEqual(result.Count, 2);
        }
    }
}
