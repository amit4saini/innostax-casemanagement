namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamingCaseSubStatusToSubStatus : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CaseSubStatus", "StatusId", "dbo.CaseStatus");
            DropForeignKey("dbo.Cases", "SubStatusId", "dbo.CaseSubStatus");
            DropIndex("dbo.CaseSubStatus", new[] { "StatusId" });
            CreateTable(
                "dbo.SubStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        StatusId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaseStatus", t => t.StatusId)
                .Index(t => t.StatusId);
            
            AddForeignKey("dbo.Cases", "SubStatusId", "dbo.SubStatus", "Id");
            DropTable("dbo.CaseSubStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CaseSubStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        StatusId = c.Guid(nullable: false),
                        SubStatus = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Cases", "SubStatusId", "dbo.SubStatus");
            DropForeignKey("dbo.SubStatus", "StatusId", "dbo.CaseStatus");
            DropIndex("dbo.SubStatus", new[] { "StatusId" });
            DropTable("dbo.SubStatus");
            CreateIndex("dbo.CaseSubStatus", "StatusId");
            AddForeignKey("dbo.Cases", "SubStatusId", "dbo.CaseSubStatus", "Id");
            AddForeignKey("dbo.CaseSubStatus", "StatusId", "dbo.CaseStatus", "Id");
        }
    }
}
