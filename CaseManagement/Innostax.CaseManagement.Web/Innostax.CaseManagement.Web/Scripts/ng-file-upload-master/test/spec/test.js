/* global describe, it */

(function () {
  'use strict';

  describe('Give it some context', function () {
    describe('maybe a bit more context here', function () {
      it('should run here few assertions', function () {
          expect(1+1).toEqual(2);
      });
      it('should run', function () {
          expect(true).toBeTruthy();
      });
    });
  });
})();
