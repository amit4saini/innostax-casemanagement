﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class SubStatusServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private SubStatusService sut;
        private ErrorHandlerService errorHandlerService;
        Guid statusId;

        [TestInitialize]
        public void Setup()
        {
            statusId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            sut = new SubStatusService(innostaxDbMock.Object, errorHandlerService);          
        }
        private List<SubStatus> GetDummyCaseSubStatus()
        {
            var dummyCaseSubStatus = new List<SubStatus>
            {
                new SubStatus
                {
                    Id = Guid.NewGuid(),
                    StatusId = statusId,
                    Name = "dummy1"
                },
                 new SubStatus
                {
                    Id = Guid.NewGuid(),
                    StatusId = statusId,
                    Name = "dummy2"
                }
            };
            return dummyCaseSubStatus;
        }

        [TestMethod]
        public void GivenCalling_GetAllCaseSubStatus_WhenCaseSubStatusDonotExists_ThenEmptyResultReturn()
        {
            var dummyCaseSubStatus = new List<SubStatus>();
            innostaxDbMock.Setup(c => c.SubStatus).ReturnsDbSet(dummyCaseSubStatus);
            var actual = sut.GetAllSubStatus();
            Assert.AreEqual(actual.Count(),0);
        }

        [TestMethod]
        public void GivenCalling_GetAllCaseSubStatus_WhenCaseSubStatusExists_ThenMappedResultReturn()
        {
            var dummyCaseSubStatus = GetDummyCaseSubStatus();
            innostaxDbMock.Setup(c => c.SubStatus).ReturnsDbSet(dummyCaseSubStatus);
            var actual = sut.GetAllSubStatus();
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void GivenCalling_GetCaseSubStatusByStatusId_WhenCaseSubStatusDonotExistsForStatusId_ThenEmptyResultReturn()
        {
            var dummyCaseSubStatus = new List<SubStatus>();
            innostaxDbMock.Setup(c => c.SubStatus).ReturnsDbSet(dummyCaseSubStatus);
            var actual = sut.GetSubStatusByStatusId(Guid.NewGuid());
            Assert.AreEqual(actual.Count(), 0);
        }

        [TestMethod]
        public void GivenCalling_GetCaseSubStatusByStatusId_WhenCaseSubStatusExistsForStatusId_ThenMappedResultReturn()
        {
            var dummyCaseSubStatus = GetDummyCaseSubStatus();
            innostaxDbMock.Setup(c => c.SubStatus).ReturnsDbSet(dummyCaseSubStatus);
            var actual = sut.GetSubStatusByStatusId(statusId);
            Assert.IsNotNull(actual);
        }
    }
}
