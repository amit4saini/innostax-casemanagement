﻿(function () {
    'use strict';
    angular.module('case').factory('Cases', ['Authentication','$http', 'toastr', '$uibModal', 'Upload', 'usSpinnerService', 'ErrorHandlerService', 'Enums', '$window', '$location', '$anchorScroll', '$stateParams',
    function (authentication,$http, toastr, $uibModal, Upload, usSpinnerService, ErrorHandlerService, Enums, $window, $location, $anchorScroll, $stateParams) {
        var vm = this;
        vm.userId = authentication.user.UserId;
        vm.defaultPageSize = $window.defaultPageSize;
        vm.fetchAndStoreCasesList = fetchAndStoreCasesList;
        vm.openRequestCaseModal = openRequestCaseModal;
        vm.saveCase = saveCase;
        vm.closeModal = closeModal;
        vm.openCaseDetails = openCaseDetails;
        vm.fetchAndStoreSubStatusList = fetchAndStoreSubStatusList;
        vm.allPages = [];
        vm.cases = [];
        vm.case = {};

        function fetchAndStoreCasesList() {
            $http.get("api/participator/" + vm.userId + "/cases")
                .success(function (response, status) {
                    vm.cases = response;
                })
                .error(function (response, status) {
                    ErrorHandlerService.DisplayErrorMessage(response, status);
                    vm.cases = [];
                });
        }

        function openRequestCaseModal() {
            fetchAndStoreSubStatusList();
            vm.modalInstance = $uibModal.open({
                templateUrl: '/participantapp/modules/case/views/add-case.view.html',
                size: 'lg'
            });
        }

        function closeModal() {
            vm.case = {};
            vm.modalInstance.dismiss('cancel');
        }

        function saveCase(isValid) {
            if (isValid == false) { return; }
            vm.case.client = vm.cases[0].client;
            vm.case.clientContactUserId = null;
            if (vm.case.client) {
                vm.case.clientName = vm.cases[0].client.clientName;
            }
            $http.post('/api/case', vm.case)
                 .success(function (response, status) {
                     vm.closeModal();
                     toastr.success("Request sent successfully.");
                     fetchAndStoreCasesList();
                 }).error(function (response, status) {
                     ErrorHandlerService.DisplayErrorMessage(response, status);
                 });
        }

        function openCaseDetails(selectedCase) {
            $location.path('/participant/cases/' + selectedCase.caseNumber + '/caseDetails');
        }

        function fetchAndStoreSubStatusList() {
            $http.get('/api/subStatus/')
           .success(function (response, status) {
               vm.subStatusList = response;
               for(var eachCaseSubStatus of vm.subStatusList){
                   if (eachCaseSubStatus.subStatus == "Requested") {
                       vm.case.subStatusId = eachCaseSubStatus.id;
                       vm.case.caseSubStatus = eachCaseSubStatus;
                   }
               }
           })
           .error(function (response, status) {
               ErrorHandlerService.DisplayErrorMessage(response, status);
           });
        }

        return vm;
    }
    ]);
})();

