﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseReportAssociationController : ApiController
    {
        private readonly ICaseReportAssociationService _caseReportAssociationService;
        public CaseReportAssociationController(ICaseReportAssociationService caseReportAssociationService)
        { _caseReportAssociationService = caseReportAssociationService; }

        [System.Web.Mvc.HttpPost]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
        public Guid Post(Models.CaseReportAssociation requestData)
        {
            return _caseReportAssociationService.Save(requestData);
        }

    }
}
