﻿'use strict'
describe('DashBoardController', function () {
    var DashBoard;
    var toastr;
    var $httpBackend;
    var ErrorHandlerService;
    var document;
    var Enums;
    var $uibModal;
    var calendarConfig;
    var $global;
    var $location;
    var $stateParams;
    var cbSuccess;
    var cbError;
    var Calendar;
    beforeEach(function () {
        module('dashboard');
        module('dashboard', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $provide.value('$location', $location);
   
            $provide.value('Enums', Enums);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$uibModal', $uibModal);
            calendarConfig = {
                templates:
                    { calendarMonthCell: 'customMonthCell.html' }
            };
            $provide.value('calendarConfig', calendarConfig);
            $provide.value('Calendar', Calendar);
            $global = {
                roleName:"dummy"
            };
            $provide.value('$global', $global);
            $provide.value('$stateParams', $stateParams);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            DashBoard = $injector.get('DashBoard', { Calendar: Calendar, toastr: toastr, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $uibModal: $uibModal, calendarConfig: calendarConfig, $global: $global, $stateParams: $stateParams, $location: $location });
        });
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("getDashboardDetails", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                DashBoard.dashboardDetails = response;
            };
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                DashBoard.dashboardDetails = [];
            }
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/dashboard").respond(200, [{ caseNickName: 'dummy', caseId: 1000, caseNumber: 1 }]);
            DashBoard.getDashboardDetails(cbSuccess, cbError);
            $httpBackend.flush();
            expect(DashBoard.dashboardDetails.length).toEqual(1);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/dashboard").respond(500, "Error Message");
            DashBoard.getDashboardDetails(cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
            expect(DashBoard.dashboardDetails.length).toEqual(0);
        });
    });
});