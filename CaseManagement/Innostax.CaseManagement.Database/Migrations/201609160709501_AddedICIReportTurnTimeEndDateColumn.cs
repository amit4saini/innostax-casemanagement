namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedICIReportTurnTimeEndDateColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "ICIReportTurnTimeEndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "ICIReportTurnTimeEndDate");
        }
    }
}
