﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IClientIndustryService
    {
        List<Innostax.CaseManagement.Models.ClientIndustry> GetAllClientIndustry();
    }

    public class ClientIndustryService : IClientIndustryService
    {
        private IInnostaxDb _innostaxDb;

        public ClientIndustryService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public List<Innostax.CaseManagement.Models.ClientIndustry> GetAllClientIndustry()
        {
            var result = new List<Innostax.CaseManagement.Models.ClientIndustry>();
            var allClientIndustries = _innostaxDb.ClientIndustries.ToList();
            if (allClientIndustries.Count == 0)
            {
                string clientNotFoundError = Common.Constants.ErrorMessages.Common.ClientIndustryError.CLIENT_INDUSTRY_NOT_FOUND;
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(clientNotFoundError)
                };
                throw new HttpResponseException(response);
            }
            result = new List<Innostax.CaseManagement.Models.ClientIndustry>();
            foreach (var clientIndustry in allClientIndustries)
            {
                var industry = Mapper.Map<ClientIndustry>(clientIndustry);
                result.Add(industry);
            }
            return result;
        }
    }
}
