﻿using System;
using System.Collections.Generic;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Models;
using AutoMapper;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IFileService
    {
        List<FileModel> Save(List<FileModel> files);
        void RenameFile(FileModel file);
    }

    public class FileService : IFileService
    {
        public IInnostaxDb _innostaxDb;
        private readonly IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICommonService _commonService;

        public FileService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _commonService = commonService;
        }

        public List<FileModel> Save(List<FileModel> files)
        {
            var result = new List<FileModel>();
            var userId = _userService.UserId;
            if (userId == new Guid())
            {
                string userNotAuthorizeError = Common.Constants.ErrorMessages.Common.FileUploadError.USER_NOT_AUTHORIZE;
                _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizeError);
            }
            var fileList = new List<Innostax.Common.Database.Models.UploadedFile>();
            foreach (var file in files)
            {
                fileList.Add(new Innostax.Common.Database.Models.UploadedFile
                {
                    FileName = file.FileName,
                    FileUrl = file.FileUrl,
                    CreateDateTime = _commonService.GetUtcDateTime(),
                    DeleteDateTime = null,
                    UserId = userId,
                    AzureKey = file.AzureKey,
                    FileId = Guid.Parse(file.Key),
                    FileContent = file.FileContent,
                    FileType = file.FileType
                });
            }          
                _innostaxDb.UploadFiles.AddRange(fileList);
                _innostaxDb.SaveChangesWithErrors();             
            result = new List<FileModel>();

            foreach (var file in fileList)
            {
                var uploadedFile = Mapper.Map<FileModel>(file);
                result.Add(uploadedFile);
            }
            return result;
        }

        public void RenameFile(FileModel file)
        {
            var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId && x.IsDeleted != true);
            if(existingFile != null)
            {
                existingFile.FileName = file.FileName;
                _innostaxDb.SaveChangesWithErrors();
            }
        }
    }
}

