﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class StateServiceTests
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private StateService sut;
        private ErrorHandlerService errorHandlerService;

        //// Arrange
        [TestInitialize]
        public void Setup()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            sut = new StateService(innostaxDbMock.Object, errorHandlerService);           
        }

        private List<State> GetDummyStates()
        {
            var dummyState = new List<State>
            {
                new State
                {
                    StateId = 1,
                    StateName = "Delhi",
                    CountryId = 1
                },
                 new State
                {
                    StateId = 2,
                    StateName = "Gurgaon",
                    CountryId = 1
                }
            };
            return dummyState;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_ListOfStatesBasedOnCountryId_WhenIdIsNotANumberString_ThenThrowBadRequest()
        {
            try
            {
                var dummyState = GetDummyStates();
                innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
                sut.ListOfStatesBasedOnCountryId("x");
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_ListOfStatesBasedOnCountryId_WhenIdIsANumberString_ButStatesDonotExists_ThenThrowNotFound()
        {
            try
            {
                var dummyState = GetDummyStates();
                innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
                sut.ListOfStatesBasedOnCountryId("2");
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        public void GivenCalling_ListOfStatesBasedOnCountryId_WhenIdIsANumberString_ButStatesExists_ThenReturnMappedResult()
        {
            var dummyState = GetDummyStates();
            innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
            var stateList = sut.ListOfStatesBasedOnCountryId("1");
            Assert.IsNotNull(stateList);
        }
    }
}
