﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IICIUserCaseAssociationService
    {
        List<ICIUserCaseAssociation> GetICICaseUsersByCaseId(Guid caseId);
        void Save(ICIUserCaseAssociation iciUserAssociationData);
        List<Case> GetCasesByICIUserId(Guid iciUserId);
    }

    public class ICIUserCaseAssociationService: IICIUserCaseAssociationService
    {
        private IInnostaxDb _innostaxDb;
        private IErrorHandlerService _errorHandlerService;
        private IUserService _userService;
        private ICaseHistoryService _caseHistoryService;
        public ICIUserCaseAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICaseHistoryService caseHistoryService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _caseHistoryService = caseHistoryService;        
        }

        public List<ICIUserCaseAssociation> GetICICaseUsersByCaseId(Guid caseId)
        {
            var result = new List<ICIUserCaseAssociation>();
            var existingICIUserAssociations = _innostaxDb.ICIUserCaseAssociations.Include(x => x.User).Include(x=>x.User.Roles).Where(x => x.CaseId == caseId).ToList();
            foreach (var eachAssociation in existingICIUserAssociations)
            {
                var mappedICIUserCaseAssociation = Mapper.Map<Models.ICIUserCaseAssociation>(eachAssociation);
                var userDetails = _innostaxDb.UserDetails.Include(y=>y.Country).FirstOrDefault(x => x.UserId == eachAssociation.ICIUserId);
                mappedICIUserCaseAssociation.User.UserDetails = Mapper.Map<Models.UserDetail>(userDetails);
                var roleId= eachAssociation.User.Roles.First().RoleId;
                mappedICIUserCaseAssociation.User.Role = Mapper.Map<Role>(_innostaxDb.Roles.FirstOrDefault(x => x.Id == roleId));
                result.Add(mappedICIUserCaseAssociation);
            }            
            return result;
        }

        public void Save(ICIUserCaseAssociation iciCaseUser)
        {
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == iciCaseUser.CaseId && x.IsDeleted == false);       
            if (existingCase == null)
            {
                string caseNotFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(caseNotFoundError);
            }
            var userEmail = iciCaseUser.User.Email;
            var existingICICaseUser = _innostaxDb.ICIUserCaseAssociations.FirstOrDefault(x => x.CaseId == iciCaseUser.CaseId && x.ICIUserId == iciCaseUser.ICIUserId);
            if (iciCaseUser.IsBeingAdded)
            {
                if (existingICICaseUser != null)
                {
                    string iciUserForCaseExistError = Common.Constants.ErrorMessages.Common.ICIUserCaseAssociationError.ICI_USER_ALREADY_EXISTS;
                    _errorHandlerService.ThrowBadRequestError(iciUserForCaseExistError);
                } 
            }
            else
            {
                if (iciCaseUser.User.UserDetails != null)
                {
                    iciCaseUser.User.UserDetails.Country = null;
                    var existingUserDetails = Mapper.Map<Common.Database.Models.UserDetail>(iciCaseUser.User.UserDetails);
                    _innostaxDb.UserDetails.AddOrUpdate(existingUserDetails);
                }
                _userService.SaveUser(iciCaseUser.User);
            }
            iciCaseUser.User = null;
            existingICICaseUser = Mapper.Map<Common.Database.Models.ICIUserCaseAssociation>(iciCaseUser);
            _innostaxDb.ICIUserCaseAssociations.AddOrUpdate(existingICICaseUser);
            _innostaxDb.SaveChangesWithErrors();
            if (iciCaseUser.IsBeingAdded)
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_ICI_USER_ADDED, "Added an ICI user: " + userEmail);
            else
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_ICI_USER_EDITED, "Edited an ICI user: " + userEmail);
        }

        public List<Case> GetCasesByICIUserId(Guid iciUserId)
        {
            var result = new List<Case>();
            var existingICICaseAssociations = _innostaxDb.ICIUserCaseAssociations.Where(x => x.ICIUserId == iciUserId && x.IsActive == true).ToList();
            foreach (var iciCaseAssociation in existingICICaseAssociations)
            {
                var existingCase = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.ClientContactUser).FirstOrDefault(x => x.CaseId == iciCaseAssociation.CaseId && x.IsDeleted != true);
                var mappedCase = Mapper.Map<Models.Case>(existingCase);
                result.Add(mappedCase);
            }
            return result;
        }
    }
}
