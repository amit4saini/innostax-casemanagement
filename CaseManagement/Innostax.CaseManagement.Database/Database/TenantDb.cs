using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Text;
using Innostax.Common.Database.Models;

namespace Innostax.Common.Database.Database
{
    public class TenantDb : DbContext, ITenantDb
    {
        public TenantDb(string connectionString)
            : base(connectionString)
        {

        }

        public virtual DbSet<TenantUser> TenantUsers { get; set; }
        public virtual DbSet<TenantRole> TenantRoles { get; set; }
        public virtual DbSet<TenantUserRole> TenantUserRoles { get; set; }
        public virtual DbSet<TenantUserLogin> TenantUserLogins { get; set; }
        public virtual DbSet<TenantRolePermission> TenantRolePermissions { get; set; } 

        public int SaveChangesWithErrors()
        {
            try
            {
                return SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb, ex
                );
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
    }
}