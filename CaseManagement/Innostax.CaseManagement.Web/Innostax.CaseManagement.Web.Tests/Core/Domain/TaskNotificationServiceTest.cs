﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskNotificationServiceTest
    {
        private TaskNotificationService sut;
        private Mock<InnostaxDb> innostaxDbMock;
        private Mock<IMailSenderService> mailSenderService;
        private Mock<INotificationService> notificationService;
        private Mock<ICommonService> _commonService;
        Guid userId; 
        [TestInitialize]
        public void TestInitialize()
        {
            userId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            mailSenderService = new Mock<IMailSenderService>();
            notificationService = new Mock<INotificationService>();
            _commonService = new Mock<ICommonService>();
            sut = new TaskNotificationService(
                innostaxDbMock.Object,mailSenderService.Object, notificationService.Object, _commonService.Object
                );         
        }
          
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
            {
                new Innostax.Common.Database.Models.UserManagement.User
                {
                    FirstName="Adam",
                    LastName="Gilchrist",
                    Id=userId,
                    Email="sahil.khurana@innostax.com",
                    UserName="sahil.khurana@innostax.com"
                },
                     new Innostax.Common.Database.Models.UserManagement.User
                {
                    FirstName="Sachin",
                    LastName="Tendulkar",
                    Id=Guid.NewGuid(),
                    Email="sahil.khurana@innostax.com",
                    UserName="sahil.khurana@innostax.com"
                },
             };
            return dummyUser;
        }
        private Innostax.Common.Database.Models.UserManagement.User GetSingleDummyUser()
        {
            return new Innostax.Common.Database.Models.UserManagement.User
            {
                FirstName = "Adam",
                LastName = "Gilchrist",
                Id = userId,
                Email = "test@test.com",
                UserName = "test@test.com"
            };                       
        }
        private List<Innostax.Common.Database.Models.Task> GetDummyTask()
        {
            var dummyTask = new List<Innostax.Common.Database.Models.Task>
            {
                new Innostax.Common.Database.Models.Task
                {
                    AssignedTo = userId,
                    CaseId = Guid.NewGuid(),
                    AssignedToUser=GetSingleDummyUser(),
                    IsDeleted = false,
                    Title="test",
                    Status= Innostax.Common.Database.Enums.TaskStatusType.IN_PROGRESS,
                    DueDate=DateTime.Now.AddDays(1),                    
                },
             };
            return dummyTask;
        }
        private List<Innostax.Common.Database.Models.Task> GetDummyCompletedTask()
        {
            var dummyTask = new List<Innostax.Common.Database.Models.Task>
            {
                new Innostax.Common.Database.Models.Task
                {
                    AssignedTo = userId,
                    CaseId = Guid.NewGuid(),
                    AssignedToUser=GetSingleDummyUser(),
                    IsDeleted = false,
                    Title="test",
                    Status= Innostax.Common.Database.Enums.TaskStatusType.COMPLETED,
                    DueDate=DateTime.Now.AddDays(1),
                },
             };
            return dummyTask;
        }
        [TestMethod]
        public void GivenCalling_SendNotification_WhenDataIsValid_ThenNoExceptionOccurs()
        {           
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyTask());
            try
            {
                sut.SendNotification();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_SendNotification_WhenTaskStatusIsCompleted_ThenNoExceptionOccurs()
        {
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyCompletedTask());
            try
            {
                sut.SendNotification();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_SendNotification_WhenNoUserExists_ThenNoExceptionOccurs()
        {
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new List<Innostax.Common.Database.Models.UserManagement.User> {  new Innostax.Common.Database.Models.UserManagement.User() });
            innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(new List<Innostax.Common.Database.Models.Task> {});
            try
            {
                sut.SendNotification();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

    }
}
