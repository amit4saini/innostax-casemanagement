﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Web.Tests.Core.Domain;
using Innostax.Common.Database;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{ 
        [TestClass]
        public class DiscussionMessageTaggedUserServiceTest
        {
            private Mock<InnostaxDb> innostaxDbMock;
            private DiscussionMessageTaggedUserService sut;
            Guid discussionMessageId;
           Guid userId;
            [TestInitialize]
            public void TestInitialize()
            {
                discussionMessageId = Guid.NewGuid();
                innostaxDbMock = new Mock<InnostaxDb>();
                sut = new DiscussionMessageTaggedUserService(
                    innostaxDbMock.Object);
            userId = Guid.NewGuid();
            }

            private List<DiscussionMessageTaggedUser> GetDummyDiscussionMessageTaggedUsers()
            {
                var dummyDiscussionMessageTaggedUsers = new List<DiscussionMessageTaggedUser>
            {
                new DiscussionMessageTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = userId,
                   DiscussionMessageId=discussionMessageId,
                   User=GetDummyUser()
                },
                new DiscussionMessageTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                    DiscussionMessageId=discussionMessageId
                }
            };
                return dummyDiscussionMessageTaggedUsers;
            }

        private Innostax.Common.Database.Models.UserManagement.User GetDummyUser()
        {
            return new Innostax.Common.Database.Models.UserManagement.User
            {
                Id = userId,
            };
        }

        [TestMethod]
            public void GivenCalling_SaveDiscussionMessageTaggers_WhenDiscussionMessageTaggedUserIsNotFound_ThenReturnEmptyResult()
            {
                try
                {
                    var dummyDiscussionMessageTaggedUsers = GetDummyDiscussionMessageTaggedUsers();
                    var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                    innostaxDbMock.Setup(c => c.DiscussionMessageTaggedUsers).ReturnsDbSet(dummyDiscussionMessageTaggedUsers);
                    sut.SaveDiscussionMessageTaggedUsers(Guid.NewGuid(), listOfTaggedUsers);
                }
                catch (Exception ex)
                {
                    Assert.Fail("Expected no exception, but got: " + ex.Message);
                }
            }
            [TestMethod]
            public void GivenCalling_SaveDiscussionMessageTaggers_WhenDiscussionMessageTaggedUserIsFound_ThenReturnEmptyResult()
            {
                try
                {
                    var dummyDiscussionMessageTaggedUsers = GetDummyDiscussionMessageTaggedUsers();
                    var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                    innostaxDbMock.Setup(c => c.DiscussionMessageTaggedUsers).ReturnsDbSet(dummyDiscussionMessageTaggedUsers);
                    sut.SaveDiscussionMessageTaggedUsers(discussionMessageId, listOfTaggedUsers);
                }
                catch (Exception ex)
                {
                    Assert.Fail("Expected no exception, but got: " + ex.Message);
                }
            }

        [TestMethod]
        public void GivenCalling_GetDiscussionMessageTaggedUser_WhenDiscussionMessageTaggedUserIsNotFound_ThenReturnEmptyResult()
        {
            var dummyDiscussionMessageTaggedUsers = GetDummyDiscussionMessageTaggedUsers();
            var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            innostaxDbMock.Setup(c => c.DiscussionMessageTaggedUsers).ReturnsDbSet(new List<DiscussionMessageTaggedUser> { });
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new List<Innostax.Common.Database.Models.UserManagement.User> { });
            var result = sut.GetDiscussionMessageTaggedUsers(discussionMessageId);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void GivenCalling_GetDiscussionMessageTaggedUser_WhenDiscussionMessageTaggedUserIsFound_ThenReturnResult()
        {
            var dummyDiscussionTaggedUsers = GetDummyDiscussionMessageTaggedUsers();
            var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            innostaxDbMock.Setup(c => c.DiscussionMessageTaggedUsers).ReturnsDbSet(GetDummyDiscussionMessageTaggedUsers());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new List<Innostax.Common.Database.Models.UserManagement.User> { GetDummyUser() });
            var result = sut.GetDiscussionMessageTaggedUsers(discussionMessageId);
            Assert.IsNotNull(result);
        }
    }
    }

