﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Core.Services.Implementation;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface INotificationService
    {
        void MarkAllNotificationsReadForUser(Guid id);
        System.Threading.Tasks.Task<List<Notification>> GetAllNotifications(bool isRead);

        //Getting top 5 unread notification
        List<Notification> ViewUnreadNotifications(Guid id);
        List<Guid> SaveNotification(string notificationText, List<Guid> listOfUserId, string url);
        List<string> GetMessageToNotifyUser(Guid userId);
        List<Guid> SaveWebJobNotification(string notificationText, List<Guid> listOfUserId, string url);
    }

    public class NotificationService : INotificationService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;      
        private readonly IAzureFileManager _azureFileManager;
        private readonly ICommonService _commonService; 
          
        public NotificationService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, IAzureFileManager azureFileManager, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _azureFileManager = azureFileManager;
            _commonService = commonService;
        }

        public async System.Threading.Tasks.Task<List<Notification>> GetAllNotifications(bool isRead)
        {
            var result = new List<Notification>();
            var allNotifications = _innostaxDb.Notifications.Include(x => x.CreatedByUser).Where(x => x.UserId == _userService.UserId && x.IsViewed == isRead).OrderByDescending(x => x.CreatedOn).ToList();
            if (allNotifications == null || allNotifications.Count == 0)
            {
                string notificationNotFoundError = Common.Constants.ErrorMessages.NotificationManagement.NotificationError.NOTIFICATION_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(notificationNotFoundError);
            }
            result = new List<Notification>();
            foreach (var notification in allNotifications)
            {
                var notifications = Mapper.Map<Notification>(notification);
                notifications.CreatedByUser = Mapper.Map<User>(notification.CreatedByUser);
                Guid profilePicKey = notifications.CreatedByUser.ProfilePicKey ?? Guid.Empty;
                if (profilePicKey != Guid.Empty)
                    notifications.CreatedByUser.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
                else
                    notifications.CreatedByUser.ProfilePicUrl = Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
                result.Add(notifications);
            }
            return result;
        }

        public void MarkAllNotificationsReadForUser(Guid id)
        {
            var unSeenNotifications = _innostaxDb.Notifications.Where(x => x.UserId == id && x.IsViewed == false).ToList();

            if (unSeenNotifications != null && unSeenNotifications.Count >= 0)
            {
                unSeenNotifications.All(p => { p.IsViewed = true; return true; });              
                    _innostaxDb.SaveChangesWithErrors();        
            }
        }

        public List<Notification> ViewUnreadNotifications(Guid id)
        {
            var result = new List<Notification>();
            var top5Notifications = _innostaxDb.Notifications.OrderByDescending(p => p.CreatedOn).Where(x => x.UserId == id).Take(5).ToList();
            result = new List<Notification>();
            foreach (var notification in top5Notifications)
            {
                var notifications = Mapper.Map<Notification>(notification);
                result.Add(notifications);
            }
            return result;
        }

        public List<Guid> SaveWebJobNotification(string notificationText, List<Guid> listOfUserId, string url)
        {
            var notificationModel = new Notification();
            notificationModel.NotificationText = notificationText;
            notificationModel.ListOfUserId = listOfUserId;
            notificationModel.URL = url;
            var notification = Mapper.Map<Common.Database.Models.Notification>(notificationModel);
            var adminRole = _innostaxDb.Roles.FirstOrDefault(y => y.Name == Roles.DDSupervisor.ToString());
            var adminId = _innostaxDb.UserRoles.FirstOrDefault(y => y.RoleId == adminRole.Id).UserId;
            notification.CreatedBy = adminId;
            notification.CreatedOn = _commonService.GetUtcDateTime();
            notification.IsViewed = false;
            foreach (var userId in notificationModel.ListOfUserId)
            {
                notification.UserId = userId;
                _innostaxDb.Notifications.Add(notification);
                _innostaxDb.SaveChangesWithErrors();
            }
            return notificationModel.ListOfUserId;
        }

        public List<Guid> SaveNotification(string notificationText, List<Guid> listOfUserId, string url)
        {
            var notificationModel = new Notification();       
            notificationModel.NotificationText = notificationText;
            listOfUserId.Remove(_userService.UserId);
            notificationModel.ListOfUserId = listOfUserId;
            notificationModel.URL = url;
            return Save(notificationModel);
        }

        internal List<Guid> Save(Notification model)
        {          
            var notificationModel = Mapper.Map<Common.Database.Models.Notification>(model);                     
            notificationModel.CreatedBy = _userService.UserId;
            notificationModel.CreatedOn = _commonService.GetUtcDateTime();
            notificationModel.IsViewed = false;
            model.ListOfUserId = model.ListOfUserId.Distinct().ToList();
            foreach (var userId in model.ListOfUserId)
            {
                notificationModel.UserId = userId;
                _innostaxDb.Notifications.Add(notificationModel);
                _innostaxDb.SaveChangesWithErrors();
            }                   
            return model.ListOfUserId;
        }

        //TO DO: message to be sent as a list.
        public List<string> GetMessageToNotifyUser(Guid userId)
        {
            var notifications = _innostaxDb.Notifications.Where(x => x.UserId == userId && x.IsViewed == false).ToList();
            var messageToSend = new List<string>();
            if (notifications != null && notifications.Count > 0)
            {
                foreach (var eachNotification in notifications)
                {
                    messageToSend.Add(eachNotification.NotificationText + "#&#" + eachNotification.URL);
                }
            }
            return messageToSend;
        }
    }
}
