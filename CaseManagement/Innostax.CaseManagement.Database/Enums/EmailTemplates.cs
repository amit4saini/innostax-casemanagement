﻿namespace Innostax.Common.Database.Enums
{
    public enum EmailTemplates
    {
        CASE_ADDED,      
        CASE_ASSIGNED,
        DISCUSSION_STARTED,
        ADDED_DISCUSSION_MESSAGE,
        TASK_ASSIGNED,
        PENDING_TASK,
        ERROR_TEMPLATE,
        INVITE_USER,
        FORGOT_PASSWORD,
        TASK_PASSED_DUE_DATE,
        CASE_STATUS_CHANGED,
        TASK_COMMENT
    }
}