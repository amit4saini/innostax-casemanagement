namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addingparticipantCaseAssociationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ParticipantCaseAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.CaseId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParticipantCaseAssociations", "UserId", "dbo.Users");
            DropForeignKey("dbo.ParticipantCaseAssociations", "CaseId", "dbo.Cases");
            DropIndex("dbo.ParticipantCaseAssociations", new[] { "UserId" });
            DropIndex("dbo.ParticipantCaseAssociations", new[] { "CaseId" });
            DropTable("dbo.ParticipantCaseAssociations");
        }
    }
}
