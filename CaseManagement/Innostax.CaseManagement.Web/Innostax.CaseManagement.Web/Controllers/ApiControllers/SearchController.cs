﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Innostax.CaseManagement.Web.Attributes;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class SearchController : ApiController
    {
        private ISearchService _searchService;
        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        [HasPermission(new Common.Database.Enums.Permission[] { Common.Database.Enums.Permission.CASE_READ_ALL, Common.Database.Enums.Permission.CASE_READ_OWN} )]
        public async Task<PageResult<Case>> Get(ODataQueryOptions<Case> options, string keyword)
        {
            return await _searchService.SearchByKeyword(options, Request, keyword);
        }
    }
}
