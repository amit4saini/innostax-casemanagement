﻿using Innostax.CaseManagement.Models.Account;
using System;

namespace Innostax.CaseManagement.Models
{
    public class TimeSheet
    {
        public Guid Id { get; set; }

        public Guid CaseId { get; set; }

        public Guid? DescriptionId { get; set; }

        public TimeSheetDescription Description { get; set; }

        public string CustomDescription { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public Decimal Cost { get; set; }

        public DateTime CreationDateTime { get; set; }

        public Guid? CreatedBy { get; set; }

        public User CreatedByUser { get; set; }

        public Guid? LastEditedBy { get; set; }

        public DateTime? LastEditedDateTime { get; set; }

        public Boolean IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
