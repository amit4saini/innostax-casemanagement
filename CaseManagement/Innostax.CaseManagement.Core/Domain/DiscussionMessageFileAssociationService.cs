﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionMessageFileAssociationService
    {
        void Save(Models.DiscussionMessageFileAssociation requestData);
        void RemoveDiscussionMessageFileAssociation(Guid discussionId);
        List<Models.FileModel> GetDiscussionMessageAssociatedFiles(Guid discussionId);
    }

    public class DiscussionMessageFileAssociationService : IDiscussionMessageFileAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IUserService _userService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly ICommonService _commonService;

        public DiscussionMessageFileAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
        }

        public void Save(Models.DiscussionMessageFileAssociation requestData)
        {
            var existingDiscussionMessage = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).FirstOrDefault(x => x.Id == requestData.DiscussionMessageId && x.IsDeleted != true);
            if (existingDiscussionMessage != null)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, existingDiscussionMessage.DiscussionId);
                var existingDiscussionMessageFileAssociations = _innostaxDb.DiscussionMessageFileAssociations.Include(y=>y.DiscussionMessage).Include(y=>y.DiscussionMessage.Discussion).Include(y=>y.UploadedFile).Where(x => x.DiscussionMessageId == requestData.DiscussionMessageId && x.IsDeleted != true).ToList();
                if (existingDiscussionMessageFileAssociations.Count > 0)
                {
                    foreach (var eachAssociation in existingDiscussionMessageFileAssociations)
                    {
                        if (requestData.Files.FirstOrDefault(x => x.FileId == eachAssociation.FileId) == null)
                        {
                            var fileName = eachAssociation.UploadedFile.FileName;
                            _innostaxDb.DiscussionMessageFileAssociations.Remove(eachAssociation);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_DELETED, fileName, existingDiscussionMessage.Discussion.Title);
                            _caseHistoryService.SaveCaseHistory(existingDiscussionMessage.Discussion.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                        }
                    }
                }

                if (requestData.Files.Count > 0)
                {
                    foreach (var eachFile in requestData.Files)
                    {
                        if (_innostaxDb.DiscussionMessageFileAssociations.FirstOrDefault(x => x.FileId == eachFile.FileId && x.DiscussionMessageId == requestData.DiscussionMessageId && x.IsDeleted != true) == null)
                        {
                            SaveDiscussionMessageAssociatedFiles(eachFile, requestData);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_ADDED, eachFile.FileName, existingDiscussionMessage.Discussion.Title);
                            _caseHistoryService.SaveCaseHistory(existingDiscussionMessage.Discussion.CaseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                        }
                    }
                }           
                    _innostaxDb.SaveChangesWithErrors();        
            }
            else
            {
                var invalidDiscussionMessageIdError = Common.Constants.ErrorMessages.DiscussionManagement.DiscussionMessageError.DISCUSSION_MESSAGE_ID_INVALID;
                _errorHandlerService.ThrowBadRequestError(invalidDiscussionMessageIdError);
            }
        }

        public void RemoveDiscussionMessageFileAssociation(Guid discussionMessageId)
        {
            var existingDiscussionMessage = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).FirstOrDefault(x => x.Id == discussionMessageId && x.IsDeleted == true);
            if (existingDiscussionMessage != null)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_DELETE_ALL, existingDiscussionMessage.DiscussionId);
                var discussionMessageFileAssociations = _innostaxDb.DiscussionMessageFileAssociations.Include(y => y.UploadedFile).Include(y=>y.DiscussionMessage).Include(y=>y.DiscussionMessage.Discussion).Where(x => x.DiscussionMessageId == discussionMessageId && x.IsDeleted != true).ToList();
                foreach (var discussionMessageFileAssociation in discussionMessageFileAssociations)
                {
                    discussionMessageFileAssociation.IsDeleted = true;
                    _innostaxDb.SaveChangesWithErrors();
                    var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_DELETED, discussionMessageFileAssociation.UploadedFile.FileName, existingDiscussionMessage.Discussion.Title);
                    _caseHistoryService.SaveCaseHistory(existingDiscussionMessage.Discussion.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                }                                
            }
            else
            {
                var invalidDiscussionMessageIdError = Common.Constants.ErrorMessages.DiscussionManagement.DiscussionMessageError.DISCUSSION_MESSAGE_ID_INVALID;
                _errorHandlerService.ThrowBadRequestError(invalidDiscussionMessageIdError);
            }
        }

        public List<Models.FileModel> GetDiscussionMessageAssociatedFiles(Guid discussionMessageId)
        {
            var result = new List<Models.FileModel>();
            result = new List<Models.FileModel>();
            var currentDiscussionMessageFileAssociations = _innostaxDb.DiscussionMessageFileAssociations.Include(y=>y.DiscussionMessage).Include(y=>y.DiscussionMessage.Discussion).Where(x => x.DiscussionMessageId == discussionMessageId && x.IsDeleted != true).ToList();
            if(currentDiscussionMessageFileAssociations.Count > 0)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, currentDiscussionMessageFileAssociations.First().DiscussionMessage.DiscussionId);
            }
            foreach (var eachAssociation in currentDiscussionMessageFileAssociations)
            {
                var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == eachAssociation.FileId && x.IsDeleted != true);
                var mappedFile = Mapper.Map<CaseManagement.Models.FileModel>(existingFile);
                mappedFile.FileUrl = existingFile.FileUrl;
                mappedFile.FileName = existingFile.FileName;
                var associatedUser = _userService.GetUser(existingFile.UserId).Data;
                mappedFile.UserName = associatedUser.FirstName + " " + associatedUser.LastName;
                result.Add(mappedFile);
            }
            return result;
        }

        internal void SaveDiscussionMessageAssociatedFiles(FileModel file, Models.DiscussionMessageFileAssociation requestData)
        {
            var result = new Result();
            var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId);
            if (existingFile == null)
            {
                string fileNotFoundError = Common.Constants.ErrorMessages.Common.FileUploadError.NO_FILE_EXISTS;
                _errorHandlerService.ThrowResourceNotFoundError(fileNotFoundError);
            }
            var mappedDiscussionMessageFileAssociation = Mapper.Map<Common.Database.Models.DiscussionMessageFileAssociation>(requestData);
            mappedDiscussionMessageFileAssociation.FileId = file.FileId;
            var newDiscussionMessageFileAssociation = _innostaxDb.DiscussionMessageFileAssociations;
            newDiscussionMessageFileAssociation.Add(mappedDiscussionMessageFileAssociation);     
                _innostaxDb.SaveChangesWithErrors();  
        }
    }
}
