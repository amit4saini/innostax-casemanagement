﻿'use strict';
describe('taskController', function () {
    var tasks;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var Enums;
    var Authentication;
    var CaseTasks;
    beforeEach(function () {
        module('tasks');
        module('tasks', function ($provide) {
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            Enums = {
                SubjectType: {
                    'Company': 0,
                    'Individual': 1,
                    'Other': 2,
                },
            };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5
            };
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('Authentication', Authentication);
            CaseTasks = {
                openTaskCommentsModal: function () { return true; }
            }
            $provide.value('CaseTasks', CaseTasks);
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            tasks = $injector.get('Tasks', { CaseTasks:CaseTasks,toastr: toastr, $uibModal: $uibModal, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, Authentication: Authentication });
        });
        tasks.modalInstance = {
            dismiss: function () { return; }
        };
        tasks.allPages = {
            length: 0
        };
        tasks.case = {
            caseId: 1
        };
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe("get All Users", function () {
        it("Positive Test", function () {
            $httpBackend.expectGET("/api/users?$skip=0").respond(200, { items: [{ id: 1 }, { id: 2 }] });
            tasks.getAllUsers();
            $httpBackend.flush();
            expect(tasks.users.length).toEqual(2);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/users?$skip=0").respond(500, "Error Message");
            tasks.getAllUsers();
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("delete Task", function () {
        it("Positive Test: Get Assigned Tasks:(Positve)", function () {
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "&$filter=IsDeleted eq false";
            $httpBackend.expectDELETE('api/task/' + tasks.taskId + '/true/toggleArchiveStatus').respond(200, { });
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(200, { items: [{ id: 1, dueDate: "2016-06-13" }, { id: 2, dueDate: "2016-06-13" }], count: 2 } );
            tasks.deleteTask(1, true);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task archived");
            expect(tasks.allTasks.length).toEqual(2);
        });
        it("Positive Test: Get Assigned Tasks:(Negative)", function () {
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "&$filter=IsDeleted eq false";
            $httpBackend.expectDELETE('api/task/' + tasks.taskId + '/true/toggleArchiveStatus').respond(200, {});
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/user?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(500, "Error Message");
            tasks.deleteTask(1, true);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task archived");
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
        it("Negative Test", function () {
            tasks.taskId = 1;
            $httpBackend.expectDELETE('api/task/' + tasks.taskId + '/true/toggleArchiveStatus').respond(500, "Error Message");
            tasks.deleteTask(1, true);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("get Assigned Tasks", function () {
        it("Positive Test", function () {
            tasks.currentUser = {
                id: 1
            };
            tasks.defaultPageSize = 5
            var skipCount = 0
            tasks.filter = "";
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(200, { items: [{ id: 1, dueDate: "2016-06-13" }, { id: 2, dueDate: "2016-06-13" }], count: 2 } );
            tasks.getAssignedTasks(0);
            $httpBackend.flush();
            expect(tasks.allTasks.length).toEqual(2);
        });
        it("Negative Test", function () {
            tasks.currentUser = {
                id: 1
            };
            tasks.defaultPageSize = 5
            var skipCount = 0
            tasks.filter = "";
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(500, "Error Message");
            tasks.getAssignedTasks(0);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    describe("Get Current User", function () {
        it("Positive Test : Get Assigned Tasks:(Positive)", function () {
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1000
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "&$filter=IsDeleted eq false";
            $httpBackend.expectGET("/api/user/getCurrentUser").respond(200, { id: "1000" });
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(200, { items: [{ id: 1, dueDate: "2016-06-13" }, { id: 2, dueDate: "2016-06-13" }], count: 2 } );
            tasks.getCurrentUser();
            $httpBackend.flush();
            expect(tasks.currentUser.id).toEqual("1000");
        });
        it("Positive Test : Get Assigned Tasks:(Negative)", function () {
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1000
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "&$filter=IsDeleted eq false";
            $httpBackend.expectGET("/api/user/getCurrentUser").respond(200, { id: "1000" });
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(500, "Error Message");
            tasks.getCurrentUser();
            $httpBackend.flush();
            expect(tasks.currentUser.id).toEqual("1000");
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("/api/user/getCurrentUser").respond(500, "Error Message");
            tasks.getCurrentUser();
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
    
    describe("save tasks", function () {
        it("Positive Test", function () {
            tasks.task = {
                taskId: 1,
                dueDate: "2016-06-13",
                assignedToUserDetails: 1,
                status: 1,
                completionDate: "2016-06-13"
            }
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1000
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "";
            $httpBackend.expectPUT('/api/task/' + tasks.task.taskId, tasks.task).respond(200, {});
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(200, { items: [{ id: 1, dueDate: "2016-06-13" }, { id: 2, dueDate: "2016-06-13" }], count: 2 } );
            tasks.saveTask(1);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task edited");
        });
        it("Positive Test", function () {
            tasks.task = {
                taskId: 1,
                dueDate: "2016-06-13",
                assignedToUserDetails: 1,
                status: 1,
                completionDate: "2016-06-13"
            }
            tasks.taskId = 1;
            tasks.currentUser = {
                id: 1000
            };
            tasks.defaultPageSize = 5
            var skipCount = 0;
            tasks.pageIndex = 0;
            tasks.filter = "";
            $httpBackend.expectPUT('/api/task/' + tasks.task.taskId, tasks.task).respond(200, {});
            $httpBackend.expectGET("api/task/" + tasks.currentUser.id + "/users?$top=" + tasks.defaultPageSize + "&$skip=" + skipCount + tasks.filter + '&$orderby=DueDate,Status,CompletionDate').respond(500, "Error Message");
            tasks.saveTask(1);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Task edited");
        });
        it("Negative Test", function () {
            tasks.task = {
                taskId: 1,
                dueDate: "2016-06-13",
                assignedToUserDetails: 1,
                status: 1,
                completionDate: "2016-06-13"
            }
            $httpBackend.expectPUT('/api/task/' + tasks.task.taskId, tasks.task).respond(500, "Error Message");
            tasks.saveTask(1);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith("Error Message", 500);
        });
    });
});