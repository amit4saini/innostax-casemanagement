﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Core.Domain;
using System;
using Innostax.CaseManagement.Web.Hubs;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseController : ApiController
    {
        private readonly ICaseService _caseService;
        private ICaseFileAssociationService _caseFileAssociationService;
        private INotificationHub _notificationHub;
        private INotificationService _notificationService;
        public CaseController(ICaseService caseService, ICaseFileAssociationService caseFileAssociationService, INotificationHub notificationHub, INotificationService notificationService)
        {
            _caseService = caseService;
            _caseFileAssociationService = caseFileAssociationService;
            _notificationHub = notificationHub;
            _notificationService = notificationService;
        }

        //GET api/<controller>
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Cases")]
        [HasPermission(new Permission[] { Permission.CASE_READ_OWN, Permission.CASE_READ_ALL })]
        public PageResult<Case> Get(ODataQueryOptions<Case> options)
        {          
             return _caseService.GetAllCases(options, Request);                    
        }

        [HttpGet]
        [HasPermission(new Permission[] { Permission.CASE_READ_OWN, Permission.CASE_READ_ALL })]
        [System.Web.Http.Route("api/Search/Cases")]
        public PageResult<Case> SearchCases(ODataQueryOptions<Case> options, string keyword)
        {
            return _caseService.SearchCasesByKeyword(options, Request,keyword);
        }

        // GET api/<controller>/5
        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.CASE_READ_OWN, Permission.CASE_READ_ALL })]
        public Case Get(Guid id)
        {
            return _caseService.GetCaseInfoById(id);
        }

        [HttpDelete]
        [HasPermission(new Permission[] { Permission.CASE_DELETE_ALL, Permission.CASE_DELETE_OWN })]
        public void Delete(Guid id)
        {
            _caseService.ArchiveUnarchiveCase(id, true);
        }

        // POST api/<controller>
        [HasPermission(new Permission[] { Permission.CASE_CREATE,Permission.REQUEST_CASE })]
        [System.Web.Mvc.HttpPost]
        public void Post(Case caseData)
        {
           var listOfUserIdToBeNotified = _caseService.Save(caseData);
            foreach (var userId in listOfUserIdToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        // PUT api/<controller>/5
        [System.Web.Mvc.HttpPut]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_ALL, Permission.CASE_UPDATE_OWN, Permission.VIEW_REPORT_TURN_AROUND_DATE_END_BUTTON })]
        public void Put(Guid id, Case caseData)
        {
             caseData.CaseId = id;
            var listOfUserIdToBeNotified = _caseService.Save(caseData);
            foreach (var userId in listOfUserIdToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [HttpGet]    
        [Route("api/Case/{caseId}/Files")]
        public List<FileModel> GetCaseAssociatedFiles(Guid caseId)
        {
            return _caseFileAssociationService.GetCaseAssociatedFiles(caseId);
        }

        [HttpPost]
        [Route("api/Case/{caseId}/{userId}/User")]
        [HasPermission(new Permission[] { Permission.ASSIGN_TO_USER})]
        public void AssignUserToCase(Guid caseId, Guid userId)
        {
            var result = new Result();
            if (userId != new Guid())
            {
                _caseService.AssignUserToCase(caseId, userId);                
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [HttpGet]
        [Route("api/Case/{caseNumber}/Details")]
        public Case GetCaseDetailsFromCaseNumber(string caseNumber)
        {
            return _caseService.GetCaseDetailsFromCaseNumber(caseNumber);
        }

        [HttpGet]
        [Route("api/Case/{id}/Users")]
        public PageResult<Models.Account.User> GetAllUsersForCase(ODataQueryOptions<Models.Account.User> options,Guid id)
        {
            return _caseService.GetAllUsersForCase(options,Request,id);
        }

        [HttpGet]
        [Route("api/Case/HasOpenTasks/{caseId}")]
        public bool CheckIfCaseHasOpenTasks(Guid caseId)
        {
            return _caseService.CheckIfCaseHasOpenTasks(caseId);
        }
    }
}