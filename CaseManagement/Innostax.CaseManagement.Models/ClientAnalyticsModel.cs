﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
    public class ClientAnalyticsModel
    {
        public string ClientName { get; set; }
        public Case Case { get; set; }
        public Country Country { get; set; }
        public Decimal Margin { get; set; }
    }
}
