namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIndexToCaseNumber : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cases", "CaseNumber", c => c.String(maxLength: 4000));
            CreateIndex("dbo.Cases", "CaseNumber", unique: true, name: "CaseNumberIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Cases", "CaseNumberIndex");
            AlterColumn("dbo.Cases", "CaseNumber", c => c.String());
        }
    }
}
