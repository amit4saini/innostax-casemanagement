namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingCaseTable1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "DateReportReceivedFromICI", c => c.DateTime());
            AddColumn("dbo.Cases", "TurnTimeForICIBusinessDays", c => c.Int());
            AddColumn("dbo.Cases", "AdverseReport", c => c.Int());
            AddColumn("dbo.Cases", "ICIError", c => c.Int());
            AddColumn("dbo.Cases", "ICIErrorDescription", c => c.String());
            AddColumn("dbo.Cases", "IHIError", c => c.Boolean());
            AddColumn("dbo.Cases", "IHIErrorDescription", c => c.String());
            AddColumn("dbo.Cases", "IHIQuality", c => c.Int());
            AddColumn("dbo.Cases", "IHIBonusCase", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "IHIBonusCase");
            DropColumn("dbo.Cases", "IHIQuality");
            DropColumn("dbo.Cases", "IHIErrorDescription");
            DropColumn("dbo.Cases", "IHIError");
            DropColumn("dbo.Cases", "ICIErrorDescription");
            DropColumn("dbo.Cases", "ICIError");
            DropColumn("dbo.Cases", "AdverseReport");
            DropColumn("dbo.Cases", "TurnTimeForICIBusinessDays");
            DropColumn("dbo.Cases", "DateReportReceivedFromICI");
        }
    }
}
