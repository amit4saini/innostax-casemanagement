﻿'use strict';

angular.module('analytics').controller('AnalyticsController', ['Analytics',
    function (analytics) {
        return analytics;
    }
]);
