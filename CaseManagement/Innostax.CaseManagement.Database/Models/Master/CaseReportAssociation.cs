﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innostax.Common.Database.Models.UserManagement;

namespace Innostax.Common.Database.Models
{
    public class CaseReportAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public Guid ReportId { get; set; }

        [ForeignKey("ReportId")]
        public Report Report { get; set; }
        public int? CountryId { get; set; }

        [ForeignKey("CountryId")]
        public Country Country { get; set; }  

        public bool IsPrimaryReport { get; set; }
        public Boolean IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }

        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }
        public bool IsOtherReportType { get; set; }
        public string OtherReportTypeName { get; set; }

        public int? StateId { get; set; }
        [ForeignKey("StateId")]
        public State State { get; set; }
    }
}
