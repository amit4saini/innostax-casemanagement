namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTenantTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tenants",
                c => new
                    {
                        TenantId = c.Guid(nullable: false, identity: true),
                        TenantDomain = c.String(),
                        ConnectionString = c.String(),
                        CreatedByUserId = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastEditedDateTime = c.DateTime(),
                        LastUpdatedByUserId = c.Guid(),
                        Serverip = c.String(),
                        PortNumber = c.String(),
                        Dbname = c.String(),
                        UserId = c.String(),
                        Password = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TenantId)
                .ForeignKey("dbo.Users", t => t.CreatedByUserId)
                .ForeignKey("dbo.Users", t => t.LastUpdatedByUserId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.LastUpdatedByUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tenants", "LastUpdatedByUserId", "dbo.Users");
            DropForeignKey("dbo.Tenants", "CreatedByUserId", "dbo.Users");
            DropIndex("dbo.Tenants", new[] { "LastUpdatedByUserId" });
            DropIndex("dbo.Tenants", new[] { "CreatedByUserId" });
            DropTable("dbo.Tenants");
        }
    }
}
