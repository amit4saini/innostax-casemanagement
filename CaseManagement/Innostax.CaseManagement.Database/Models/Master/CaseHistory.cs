﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    [Table("CaseHistory")]
    public class CaseHistory
    {
        public CaseHistory()
            {
            CreationDatetime = DateTime.Now;
            }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Innostax.Common.Database.Enums.Action Action { get; set; }
        public DateTime? CreationDatetime { get; set; }
        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public string NewValue { get; set; }
    }
}
