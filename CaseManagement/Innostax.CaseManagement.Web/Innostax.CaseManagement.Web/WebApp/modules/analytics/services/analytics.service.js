﻿(function () {
    'use strict';
    angular.module('analytics').factory('Analytics', ['$uibModal', '$http', 'toastr', 'ErrorHandlerService', 'Enums', '$global', '$stateParams', '$location', '$window',
    function ($uibModal, $http, toastr, ErrorHandlerService, Enums, $global, $stateParams, $location, $window) {
        var vm = this;
        
        function init()
        {           
            //getCaseAnalytics(null,null,function (caseAnalytics) {              
            //    if (!caseAnalytics)  //for users who can't view analytics
            //        return;              
            //    fetchCaseAnalytics(caseAnalytics);
            //    fetchTaskAnalytics(caseAnalytics);
            //    fetchExpenseAnalytics(caseAnalytics);
            //    fetchTimesheetAnalytics(caseAnalytics);
            //    fetchReportAnalytics(caseAnalytics);
            //});
            vm.datePickerOptions = { 'maxDate': new Date(), 'showWeeks': false };
            vm.selectedAnalytics = vm.analyticsTypeEnum['Case'];
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Case";           
            getAllAnalyticsReportsForUser();
            vm.endDate = new Date();
            //getAllClients(function (clientIdList)
            //{ getClientAnalytics(clientIdList, function (clientAnalytics) { convertClientAnalytics(clientAnalytics); }); });
            //getUserAnalytics(vm.currentUser.userId, function (userAnalytics) { convertUserAnalytics(userAnalytics); });
        }

        function fetchTaskAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.taskAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.tasks = caseItem.tasks.map(function (taskItem) {
                    //create pascalcase keys of task object to display in pivot-table
                    taskItem.Id = taskItem.taskId;
                    taskItem.CaseNumber = caseItem.caseNumber;
                    taskItem.CaseNickname = caseItem.nickName;
                    taskItem.Client = caseItem.client.clientName;
                    taskItem.Country = caseItem.primaryReportCountryName || "NA";
                    taskItem.Title = taskItem.title;
                    taskItem.Status = Object.keys(vm.taskStatusEnum)[taskItem.status];
                    taskItem.Priority = Object.keys(vm.taskPriorityEnum)[taskItem.priority];
                    taskItem.DueDate = new Date(taskItem.dueDate).toLocaleDateString();
                    taskItem.CompletionDate = (taskItem.completionDate ? new Date(taskItem.completionDate).toLocaleDateString() : "NA");
                    taskItem.AssignedTo = taskItem.assignedToUserDetails.firstName + " " + taskItem.assignedToUserDetails.lastName;
                    taskItem.CreatedBy = taskItem.createdByUserDetails.firstName + " " + taskItem.createdByUserDetails.lastName;
                    taskItem.CreatedDate = new Date(taskItem.createdOn).toLocaleDateString();
                    taskItem.OriginalDueDate = new Date(taskItem.originalDueDate).toLocaleDateString();
                    //delete all camelcase keys of task object
                    var keysList = Object.keys(taskItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete taskItem[keysList[key]];
                    return taskItem;
                });
                vm.taskAnalytics = vm.taskAnalytics.concat(caseItem.tasks);
            });
            vm.taskRows = ["Id", "Title", "CreatedBy", "CreatedDate"];
            vm.taskCols = ["CaseNumber"];
            $window.taskRows = vm.taskRows;
            $window.taskCols = vm.taskCols;
            displayAnalytics(vm.taskAnalytics, vm.taskRows, vm.taskCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Task"]);
        }

        function fetchExpenseAnalytics(caseAnalytics)
        {           
            var analytics = angular.copy(caseAnalytics);
            vm.expenseAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.expenses = caseItem.expenses.map(function (expenseItem) {
                    //create pascalcase keys of expense object to display in pivot-table
                    switch (expenseItem.expensePaymentType) {
                        case vm.expensePaymentTypeEnum.Invoice:
                            expenseItem.expensePaymentType = "Invoice";
                            break;
                        case vm.expensePaymentTypeEnum.CreditCardPayment:
                            expenseItem.expensePaymentType = "Credit Card"; break;
                        case vm.expensePaymentTypeEnum.Account:
                            expenseItem.expensePaymentType = "Account"; break;
                    }
             
                    expenseItem.SubjectName = (caseItem.matterName ?caseItem.matterName : "NA") ;
                    expenseItem.Id = expenseItem.id;                    
                    expenseItem.CaseNumber = caseItem.caseNumber;
                    expenseItem.CaseNickname = caseItem.nickName;
                    expenseItem.Client = caseItem.client.clientName;
                    expenseItem.Country = caseItem.primaryReportCountryName || "NA";
                    expenseItem.Quantity = Math.trunc(expenseItem.quantity * 100) / 100;
                    expenseItem.Cost = Math.trunc(expenseItem.cost * 100) / 100;               
                    expenseItem.CreatedDate = new Date(expenseItem.creationDateTime).toLocaleDateString();
                    expenseItem.CreatedBy = expenseItem.createdByUser.firstName + " " + expenseItem.createdByUser.lastName;
                    expenseItem.VendorName = expenseItem.vendor ? expenseItem.vendor.vendorName : "NA";
                    expenseItem.FinalReportSentToClientDate = (caseItem.finalReportSentToClientDate ? caseItem.finalReportSentToClientDate : "NA");
                    expenseItem.OriginalCurrencyCost = expenseItem.originalCurrencyCost || 0;
                    expenseItem.PaymentType = expenseItem.expensePaymentType;
                    expenseItem.InvoiceApproved = expenseItem.invoiceApproved;
                    expenseItem.ICIAssignedUser = (caseItem.iciAssignedUser ? caseItem.iciAssignedUser.firstName + " " + caseItem.iciAssignedUser.lastName : "NA");
                    //delete all camelcase keys of expense object
                    var keysList = Object.keys(expenseItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete expenseItem[keysList[key]];
                    return expenseItem;
                })
                vm.expenseAnalytics = vm.expenseAnalytics.concat(caseItem.expenses);
            });
            vm.expenseRows = ["Id", "Quantity", "Cost", "CreatedBy", "CreatedDate"];
            vm.expenseCols = ["CaseNumber"];
            $window.expenseRows = vm.expenseRows;
            $window.expenseCols = vm.expenseCols;
            displayAnalytics(vm.expenseAnalytics, vm.expenseRows, vm.expenseCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Expense"]);
        }

        function fetchTimesheetAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.timesheetAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.timesheets = caseItem.timesheets.map(function (timesheetItem) {
                    //create pascalcase keys of timesheet object to display in pivot-table
                    timesheetItem.Id = timesheetItem.id;
                    timesheetItem.CaseNumber = caseItem.caseNumber;
                    timesheetItem.CaseNickname = caseItem.nickName;
                    timesheetItem.Client = caseItem.client.clientName;
                    timesheetItem.Country = caseItem.primaryReportCountryName || "NA";                 
                    timesheetItem.Description = timesheetItem.description.description;
                    timesheetItem.endTime = (timesheetItem.endTime ? new Date(timesheetItem.endTime).getHours() + new Date(timesheetItem.endTime).getMinutes() / 60.00 : null);
                    timesheetItem.startTime = new Date(timesheetItem.startTime).getHours() + new Date(timesheetItem.startTime).getMinutes() / 60.00;
                    timesheetItem.Hours = Math.trunc((timesheetItem.endTime ? timesheetItem.endTime - timesheetItem.startTime : 0) * 100) / 100;
                    timesheetItem.Cost = Math.trunc(timesheetItem.cost * 100) / 100;
                    timesheetItem.CreatedDate = new Date(timesheetItem.creationDateTime).toLocaleDateString();
                    timesheetItem.CreatedBy = timesheetItem.createdByUser.firstName + " " + timesheetItem.createdByUser.lastName;
                    timesheetItem.TimesheetUserName = timesheetItem.user.firstName + " " + timesheetItem.user.lastName;
                    timesheetItem.TimesheetUser = timesheetItem.user.role.name;
                    //delete all camelcase keys of expense object
                    var keysList = Object.keys(timesheetItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete timesheetItem[keysList[key]];
                    return timesheetItem;
                });
                vm.timesheetAnalytics = vm.timesheetAnalytics.concat(caseItem.timesheets);
            });
            vm.timesheetRows = ["Id", "Hours", "Cost", "CreatedBy", "CreatedDate"];
            vm.timesheetCols = ["CaseNumber"];
            $window.timesheetRows = vm.timesheetRows;
            $window.timesheetCols = vm.timesheetCols;
            displayAnalytics(vm.timesheetAnalytics, vm.timesheetRows, vm.timesheetCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Timesheet"]);
        }

        function fetchReportAnalytics(caseAnalytics) {
            var analytics = angular.copy(caseAnalytics);
            vm.reportAnalytics = [];
            analytics.forEach(function (caseItem) {
                caseItem.reports = caseItem.reports.map(function (reportItem) {
                    //create pascalcase keys of timesheet object to display in pivot-table                  
                    reportItem.CaseNumber = caseItem.caseNumber;
                    reportItem.CaseNickname = caseItem.nickName;
                    reportItem.Client = caseItem.client.clientName;
                    reportItem.Country = caseItem.primaryReportCountryName || "NA";                
                    reportItem.ReportType = reportItem.reportType;
                    //delete all camelcase keys of expense object
                    var keysList = Object.keys(reportItem);
                    for (var key in keysList)
                        if (keysList[key][0] == keysList[key][0].toLowerCase())
                            delete reportItem[keysList[key]];
                    return reportItem;
                });
                vm.reportAnalytics = vm.reportAnalytics.concat(caseItem.reports);
            });
            vm.reportRows = ["ReportType"];
            vm.reportCols = ["CaseNumber"];
            $window.reportRows = vm.reportRows;
            $window.reportCols = vm.reportCols;
            displayAnalytics(vm.reportAnalytics, vm.reportRows, vm.reportCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Report"]);
        }

        function stringify(listName, list)
        {
            var stringResult = "";
            for(var index in list)
            {
                stringResult += ((index == 0) ? "?" : "&") + listName + "=" + list[index];
            }
            return stringResult;
        }

        function getAllClients(cbSuccess)
        {
            var options = '?$skip=0&$filter=IsDeleted eq false';
            getExistingClients(options, function (response, status) {
                vm.clientIdList = response.items.map(function (item) { return item.clientId; });
                cbSuccess(vm.clientIdList);
            },
            function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getExistingClients(options, cbSuccess, cbError)
        {
            $http.get("api/clients" + options)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getCaseAnalytics()
        {
            if (vm.startDate == undefined) {
                vm.showValidationMessage = true;
                return;
            }         
            var startDate = moment.utc(vm.startDate).format("YYYY-MM-DD");
            var endDate = moment.utc(vm.endDate).format("YYYY-MM-DD");
            $window.analyticsStartDate = startDate;
            $window.analyticsEndDate = endDate;
            getCaseAnalyticsForSelectedCases(startDate, endDate, function (response, status) {
                var caseAnalytics = response.items;
                if (!caseAnalytics)  //for users who can't view analytics
                    return;
                fetchCaseAnalytics(caseAnalytics);
                fetchTaskAnalytics(caseAnalytics);
                fetchExpenseAnalytics(caseAnalytics);
                fetchTimesheetAnalytics(caseAnalytics);
                fetchReportAnalytics(caseAnalytics);          
            },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            getClientAnalyticsForSelectedClients(startDate, endDate, function (response, status) {                
                    var clientAnalytics = response.items;
                    if (!clientAnalytics)
                        return;
                    fetchClientAnalytics(clientAnalytics);
                },
                 function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
            }          

        function getCaseAnalyticsForSelectedCases(startDate, endDate, cbSuccess, cbError)
        {
            $http.get("api/Analytics/Cases?startDate=" + startDate + "&endDate=" + endDate)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getClientAnalytics(clientIdList, cbSuccess)
        {
            getClientAnalyticsForSelectedClients( function (response, status) { cbSuccess(response.items); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getClientAnalyticsForSelectedClients(startDate, endDate, cbSuccess, cbError)
        {
            $http.get("api/Analytics/Clients?startDate=" + startDate + "&endDate=" + endDate)
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function getUserAnalytics(userId, cbSuccess)
        {
            getUserAnalyticsByUserId(userId, function (response, status) { cbSuccess(response); },
                function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); });
        }

        function getUserAnalyticsByUserId(userId, cbSuccess, cbError)
        {
            $http.get("api/User/" + userId + "/Analytics")       
                .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }
        function fetchClientAnalytics(clientAnalytics)
        {  
            var analytics = angular.copy(clientAnalytics);
            vm.clientAnalytics = analytics.map(function (clientItem) {
                clientItem.Name = clientItem.clientName;
                clientItem.SalesRepresentative = (clientItem.salesRepresentative ? clientItem.salesRepresentative.firstName + " " + clientItem.salesRepresentative.lastName : "NA");
                clientItem.LatestCaseOrderDate = (clientItem.latestCaseOrderDate ? clientItem.latestCaseOrderDate : "NA");
                //delete all camelcase keys of case object
                var keysList = Object.keys(clientItem);
                for (var key in keysList)
                    if (keysList[key][0] == keysList[key][0].toLowerCase())
                        delete clientItem[keysList[key]];
                return clientItem;
            });
            $window.clientAnalytics = vm.clientAnalytics;
            $window.analyticsTypeEnum = vm.analyticsTypeEnum;
            vm.clientRows = ["Name"];
            vm.clientCols = ["SalesRepresentative"];
            $window.clientRows = vm.clientRows;
            $window.clientCols = vm.clientCols;
            displayAnalytics(vm.clientAnalytics, vm.clientRows, vm.clientCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Client"]);
        }

        function fetchCaseAnalytics(caseAnalytics)
        {
            var analytics = angular.copy(caseAnalytics);
            vm.caseAnalytics = analytics.map(function (caseItem) {                
                //create pascalcase keys of case object to display in pivot-table
                caseItem.Client = caseItem.client.clientName;
                caseItem.Country = caseItem.primaryReportCountryName || "NA";
                caseItem.CaseNumber = caseItem.caseNumber;
                caseItem.Speed = Object.keys(vm.speedEnum)[caseItem.speed];
                caseItem.Nickname = caseItem.nickName;
                caseItem.Margin = Math.trunc(caseItem.margin * 100) / 100;
                caseItem.IsNFC = caseItem.isNFCCase;
                caseItem.CaseStatus = (caseItem.caseStatus ? caseItem.caseStatus.status : "NA");
                caseItem.ActualRetail = Math.trunc((caseItem.actualRetail || 0) * 100) / 100;
                caseItem.CreatedBy = caseItem.createdByUser.firstName + " " + caseItem.createdByUser.lastName;
                caseItem.CreatedDate = new Date(caseItem.creationDateTime).toLocaleDateString();
                caseItem.ClosedBy = (caseItem.closedByUser ? caseItem.closedByUser.firstName + " " + caseItem.closedByUser.lastName : "NA");
                caseItem.ClosedDate =(caseItem.closingDateTime ? new Date(caseItem.closingDateTime).toLocaleDateString(): "NA");
                caseItem.CaseAge = Math.trunc((caseItem.caseAge || 0) * 100) / 100;
                caseItem.FinalReportSentToClient = caseItem.finalReportSentToClient;
                caseItem.FinalReportSentToClientDate = (caseItem.finalReportSentToClientDate ? caseItem.finalReportSentToClientDate : "NA");
                caseItem.AccountCode = (caseItem.businessUnit ? caseItem.businessUnit.accountCode : "NA");                
                caseItem.InformationPending = caseItem.informationPending;              
                caseItem.Reports = (caseItem.reports.length != 0 ? caseItem.reports.map(function (item) { if (item.isPrimary) return item.reportType; }) : "NA");
                caseItem.InHouseTurnTime = (caseItem.inHouseTurnTime ? caseItem.inHouseTurnTime : "NA");
                caseItem.SalesRepresentative = (caseItem.client.salesRepresentative ? caseItem.client.salesRepresentative.firstName + " " + caseItem.client.salesRepresentative.lastName : "NA");
                caseItem.ICIErrorType = (caseItem.iciError ? Object.keys(vm.iciErrorTypeEnum)[caseItem.iciError] : "NA");
                caseItem.ICIAssignedUser = (caseItem.iciAssignedUser ? caseItem.iciAssignedUser.firstName + " " + caseItem.iciAssignedUser.lastName : "NA");
                caseItem.ReportToClientAheadOfTime = (caseItem.reportToClientAheadOfTime ? caseItem.reportToClientAheadOfTime : "NA");
                caseItem.CaseCancelled = caseItem.caseCancelled;
                caseItem.ClientCancelled = (caseItem.clientCancelled ? caseItem.clientCancelled : "NA");
                caseItem.BusinessUnit = (caseItem.businessUnit ? caseItem.businessUnit.businessUnit : "NA");
                caseItem.SubjectName = (caseItem.matterName ? caseItem.matterName : "NA");
                caseItem.ContactName = (caseItem.clientContactUser ? caseItem.clientContactUser.contactName : "NA");
                caseItem.NFCBudget = (caseItem.nfcBudget ? caseItem.nfcBudget : "NA");
                caseItem.FinalReportSentToSalesRep = caseItem.finalReportSentToSalesRep;
                caseItem.FinalReportSentToSalesRepDate = (caseItem.finalReportSentToSalesRepDate ? caseItem.finalReportSentToSalesRepDate : "NA");
                caseItem.IHIQuality = (caseItem.ihiQuality? Object.keys(vm.ihiQualityEnum)[caseItem.ihiQuality]:"NA");
                caseItem.IHIAssignedUser = (caseItem.ihiAssignedUser ? caseItem.ihiAssignedUser.firstName + " " + caseItem.ihiAssignedUser.lastName : "NA");
                caseItem.IHIBonusCase = (caseItem.ihiBonusCase ? caseItem.ihiBonusCase : "NA");
                caseItem.AdverseReport = (caseItem.adverseReport ? Object.keys(vm.adverseReportEnum)[caseItem.adverseReport] : "NA");
                caseItem.CaseReviewedDate = (caseItem.caseReviewedDate ? caseItem.caseReviewedDate : "NA");
                caseItem.CaseReviewedByUser = (caseItem.reviewedByUser ? caseItem.reviewedByUser.firstName + " " + caseItem.reviewedByUser.lastName : "NA");
                caseItem.TurnTimeForICIBusinessDays = (caseItem.turnTimeForICIBusinessDays ? caseItem.turnTimeForICIBusinessDays : "NA");
                caseItem.CaseDueDate = (caseItem.caseDueDate ? caseItem.caseDueDate : "NA");   
                caseItem.DDSupervisorWhoApprovedCase = (caseItem.ddsupervisorWhoApprovedCaseByUser ? caseItem.ddsupervisorWhoApprovedCaseByUser.firstName + " " + caseItem.ddsupervisorWhoApprovedCaseByUser.lastName : "NA");
                caseItem.DDSupervisorApprovedCaseDate = (caseItem.ddsupervisorApprovedCaseDate ? caseItem.ddsupervisorApprovedCaseDate : "NA");
                //delete all camelcase keys of case object
                var keysList = Object.keys(caseItem);
                for (var key in keysList)
                    if (keysList[key][0] == keysList[key][0].toLowerCase())
                        delete caseItem[keysList[key]];
                return caseItem;
            });
            $window.caseAnalytics = vm.caseAnalytics;
            $window.analyticsTypeEnum = vm.analyticsTypeEnum;
            vm.caseRows = ["CaseNumber", "Nickname", "CreatedBy", "CreatedDate"];
            vm.caseCols = ["Client"];
            $window.caseRows = vm.caseRows;
            $window.caseCols = vm.caseCols;
            displayAnalytics(vm.caseAnalytics, vm.caseRows, vm.caseCols, vm.analyticsTypeEnum, vm.analyticsTypeEnum["Case"]);
        }

        function convertClientAnalytics(clientAnalytics)
        {
            vm.clientAnalytics = clientAnalytics.map(function (item) {
                item.ClientName = item.clientName;
                item.Country = item.country != null ? item.country.countryName : "NA";
                item.ClientContactUser = item.case.clientContactUser != null ? item.case.clientContactUser.contactName : "NA";
                item.AssignedToUser = item.case.assignedToUser != null ? item.case.assignedToUser.firstName + " " + item.case.assignedToUser.lastName : "NA";
                item.CaseNumber = item.case.caseNumber;
                item.CaseNickName = item.case.nickName;
                item.Margin = item.margin;
                delete item['clientName'];
                delete item['margin'];
                delete item['country'];
                delete item['case'];
                return item;
            });
            displayClientAnalytics(vm.clientAnalytics);
        }

        function convertUserAnalytics(userAnalytics)
        {
            var item = userAnalytics;
            vm.userAnalytics = [];
            var userAnalyticsModel = {
                UserName : item.userName,
                CasesCreated : item.totalCasesCreatedByAUser,
                DelayedTasks : item.listOfTaskDelayed.length,
                Timesheets : item.listOfTimesheet.length,
                Expenses : item.listOfCaseExpense.length,
            }
            vm.userAnalytics.push(userAnalyticsModel);
            displayUserAnalytics(vm.userAnalytics);
        }

        function getAllAnalyticsReportsForUser() {
            getAnalyticsReportsForCurrentUser(function (response, status) {               
                vm.newReport = { reportName: "New Report", id: null };
                vm.caseAnalyticsReports = [];
                vm.taskAnalyticsReports = [];
                vm.expenseAnalyticsReports = [];
                vm.timesheetAnalyticsReports = [];
                vm.clientAnalyticsReports = [];
                vm.allAnalyticsReports = response;
                vm.caseAnalyticsReports.push(vm.newReport);
                vm.taskAnalyticsReports.push(vm.newReport);
                vm.reportAnalyticsReports.push(vm.newReport);
                vm.expenseAnalyticsReports.push(vm.newReport);
                vm.timesheetAnalyticsReports.push(vm.newReport);
                vm.clientAnalyticsReports.push(vm.newReport);
                vm.isReportEditable = false;
                vm.selectedCaseAnalyticsReport = vm.selectedTaskAnalyticsReport = vm.selectedExpenseAnalyticsReport = vm.selectedTimesheetAnalyticsReport = vm.selectedReportAnalyticsReport = vm.selectedClientAnalyticsReport = vm.newReport;
                for (var count = 0; count < vm.allAnalyticsReports.length; count++) {                    
                    switch (vm.allAnalyticsReports[count].analyticsType) {
                        case "Case": vm.caseAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedCaseAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                        case "Task": vm.taskAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedTaskAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                        case "Expense": vm.expenseAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedExpenseAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                        case "Timesheet": vm.timesheetAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedTimesheetAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                        case "Report": vm.reportAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedReportAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                        case "Client": vm.clientAnalyticsReports.push(vm.allAnalyticsReports[count]);
                            if ($window.analyticsReportName == vm.allAnalyticsReports[count].reportName)
                                vm.selectedClientAnalyticsReport = vm.allAnalyticsReports[count];
                            break;
                    }
                }               
            }, function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);              
                vm.caseAnalyticsReports = [];
                vm.taskAnalyticsReports = [];
                vm.expenseAnalyticsReports = [];
                vm.timesheetAnalyticsReports = [];
                vm.reportAnalyticsReports = [];
                vm.clientAnalyticsReports = [];
            });
        }

        function getAnalyticsReportsForCurrentUser(cbSuccess, cbError) {
            $http.get("api/Analytics/" + vm.currentUser.userId + "/Reports")
            .success(function (response, status) { cbSuccess(response, status); })
            .error(function (response, status) { cbError(response, status); });
        }

        function onAnalyticsReportSelected(selectedItem) {
            vm.selectedAnalyticsReport = selectedItem;
            $window.selectedAnalyticsReportId = vm.selectedAnalyticsReport.id;
            if (vm.selectedAnalyticsReport.id != null) {
                $window.restoreAnalyticsReportConfiguration();
                vm.isReportEditable = true;
            }
            else {               
                vm.isReportEditable = false;               
                $window.restoreInitialConfiguration();
            }
        }
        
        function saveCurrentReport() {
            if (!$window.selectedAnalyticsReportId)
                $window.selectedAnalyticsReportId = null;
            switch ($window.activeAnalyticsTab) {
                case "Case": vm.selectedAnalyticsReport = vm.selectedCaseAnalyticsReport;
                    break;
                case "Task": vm.selectedAnalyticsReport = vm.selectedTaskAnalyticsReport;
                    break;
                case "Expense": vm.selectedAnalyticsReport = vm.selectedExpenseAnalyticsReport;
                    break;
                case "Timesheet": vm.selectedAnalyticsReport = vm.selectedTimesheetAnalyticsReport;
                    break;
                case "Report": vm.selectedAnalyticsReport = vm.selectedReportAnalyticsReport;
                    break;
                case "Client": vm.selectedAnalyticsReport = vm.selectedClientAnalyticsReport;
                    break;
            }
            if (vm.selectedAnalyticsReport.id == null){
                openSaveNewReportNameModal();
                closeModal();
            }              
            else {
                $window.analyticsReportName = vm.selectedAnalyticsReport.reportName;
                $window.selectedAnalyticsReportId = vm.selectedAnalyticsReport.id;
                var saveConfigResult = $window.saveAnalyticsReportConfiguration();               
            }                    
        }

        function openSaveNewReportNameModal() {
            vm.newReportName = "";
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/analytics/views/add-analytics-report-modal-view.html',
                size: 'md'
            });
        }

        function saveAnalyticsNewReport() {
            if (vm.newReportName == "") {
                toastr.error("Please enter a valid report name");
                return;
            }           
            $window.analyticsReportName = vm.newReportName;           
            $window.saveAnalyticsReportConfiguration();         
            closeModal();
        }

        function closeModal() {
            vm.modalInstance.dismiss('cancel');
        }

        function restoreConfiguration() {           
            if (!$window.selectedAnalyticsReportId) {
                toastr.error("Please select an existing report to restore");
                return;
            }         
            $window.restoreAnalyticsReportConfiguration();
        }

        function editCurrentReport() {
            switch (vm.activeAnalyticsTab) {
                case "Case": vm.newReportName = vm.selectedCaseAnalyticsReport.reportName;
                    break;
                case "Task": vm.newReportName = vm.selectedTaskAnalyticsReport.reportName;
                    break;
                case "Expense": vm.newReportName = vm.selectedExpenseAnalyticsReport.reportName;
                    break;
                case "Timesheet": vm.newReportName = vm.selectedTimesheetAnalyticsReport.reportName;
                    break;
                case "Report": vm.newReportName = vm.selectedReportAnalyticsReport.reportName;
                    break;
                case "Client": vm.newReportName = vm.selectedClientAnalyticsReport.reportName;
                    break;
            }            
            vm.modalInstance = $uibModal.open({
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/webapp/modules/analytics/views/add-analytics-report-modal-view.html',
                size: 'md'
            });
        }        

        function viewCaseAnalytics() {
            if (!vm.selectedCaseAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Case;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Case";
        }

        function viewTaskAnalytics() {
            if (!vm.selectedTaskAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Task;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Task";
        }

        function viewExpenseAnalytics() {
            if (!vm.selectedExpenseAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Expense;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Expense";
        }

        function viewTimesheetAnalytics() {
            if (!vm.selectedTimesheetAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Timesheet;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Timesheet";
        }
        function viewReportAnalytics() {
            if (!vm.selectedReportAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Report;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Report";
        }
        function viewClientAnalytics() {
            if (!vm.selectedClientAnalyticsReport.id)
                vm.isReportEditable = false;
            else
                vm.isReportEditable = true;
            vm.selectedAnalytics = vm.analyticsTypeEnum.Client;
            vm.activeAnalyticsTab = $window.activeAnalyticsTab = "Client";
        }
        vm.adverseReportEnum = Enums.AdverseReportType;
        vm.speedEnum = Enums.Speed;
        vm.iciErrorTypeEnum=Enums.ICIErrorType;
        vm.ihiQualityEnum=Enums.IHIQuality;
        vm.taskStatusEnum = Enums.TaskStatus;
        vm.taskPriorityEnum = Enums.TaskPriority;
        vm.analyticsTypeEnum = Enums.AnalyticsType;
        vm.expensePaymentTypeEnum = Enums.ExpensePaymentType;
        vm.currentUser = $global.user;
        vm.init = init;
        vm.getCaseAnalytics = getCaseAnalytics;
        vm.getClientAnalytics = getClientAnalytics;
        vm.getUserAnalytics = getUserAnalytics;
        vm.getCaseAnalyticsForSelectedCases = getCaseAnalyticsForSelectedCases;
        vm.getClientAnalyticsForSelectedClients = getClientAnalyticsForSelectedClients;
        vm.getUserAnalyticsByUserId = getUserAnalyticsByUserId;
        vm.getAllClients = getAllClients;
        vm.getExistingCases = $global.getExistingCases;
        vm.fetchCaseAnalytics = fetchCaseAnalytics;
        vm.fetchTaskAnalytics = fetchTaskAnalytics;
        vm.fetchExpenseAnalytics = fetchExpenseAnalytics;
        vm.fetchTimesheetAnalytics = fetchTimesheetAnalytics;
        vm.fetchClientAnalytics = fetchClientAnalytics;
        vm.fetchReportAnalytics = fetchReportAnalytics;
        vm.convertClientAnalytics = convertClientAnalytics;
        vm.convertUserAnalytics = convertUserAnalytics;
        vm.displayAnalytics = displayAnalytics;
        vm.getAllAnalyticsReportsForUser = getAllAnalyticsReportsForUser;
        vm.onAnalyticsReportSelected = onAnalyticsReportSelected;
        vm.saveCurrentReport = saveCurrentReport;
        vm.openSaveNewReportNameModal = openSaveNewReportNameModal;
        vm.saveAnalyticsNewReport = saveAnalyticsNewReport;
        vm.closeModal = closeModal;
        vm.restoreConfiguration = restoreConfiguration;
        vm.editCurrentReport = editCurrentReport;
        vm.viewTaskAnalytics = viewTaskAnalytics;
        vm.viewCaseAnalytics = viewCaseAnalytics;
        vm.viewExpenseAnalytics = viewExpenseAnalytics;
        vm.viewTimesheetAnalytics = viewTimesheetAnalytics;
        vm.viewClientAnalytics = viewClientAnalytics;
        vm.viewReportAnalytics = viewReportAnalytics;
        vm.allAnalyticsReports = [];
        vm.caseAnalyticsReports = [];
        vm.taskAnalyticsReports = [];
        vm.expenseAnalyticsReports = [];
        vm.timesheetAnalyticsReports = [];
        vm.reportAnalyticsReports = [];
        vm.clientAnalyticsReports = [];
        vm.isReportEditable = false;
        vm.showValidationMessage = false;
        return vm;
    }
    ]);
})();
