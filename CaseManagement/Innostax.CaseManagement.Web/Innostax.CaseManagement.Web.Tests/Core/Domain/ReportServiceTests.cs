﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ReportServiceTests
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ReportService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> _userService;
        private Mock<ICommonService> _commonService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            _userService = new Mock<IUserService>();
            _commonService = new Mock<ICommonService>();
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            sut = new ReportService(
                innostaxDbMock.Object,
                errorHandlerService,_commonService.Object,_userService.Object
                );
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);              

        }
        private List<Report> GetDummyReport()
        {
            var dummyReport = new List<Report>
            {
                new Report
                {
                    ReportId=Guid.NewGuid()
                },
                new Report
                {
                    ReportId=Guid.NewGuid()
                },
            };
            return dummyReport;
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetReports_WhenNoReportIsPresent_ThenThrowNotFoundRequest()
        {
            try
            {
                var dummyReport = new List<Report>();
                innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(dummyReport);
                var actual = sut.GetReports();
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetReports_WhenReportsArePresent_ThenMappedResultReturn()
        {
            var dummyReport = GetDummyReport();
            innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(dummyReport);
            var actual = sut.GetReports();
            Assert.IsNotNull(actual);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetReportById_WhenReportIdIsNotPresent_ThenThrowNotFoundRequest()
        {
            try
            {
                var dummyReport = GetDummyReport();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(dummyReport);
                var actual = sut.GetReportById(notPresent);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetReportById_WhenReportIdIsPresent_ThenMappedResultReturn()
        {
            var dummyReport = GetDummyReport();
            var presentReportId = Guid.NewGuid();
            var dummyCase = new Report
            {
                ReportId = presentReportId
            };

            dummyReport.Add(dummyCase);
            innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(dummyReport);
            var actual = sut.GetReportById(presentReportId);
            Assert.IsNotNull(actual);
        }
    }
}