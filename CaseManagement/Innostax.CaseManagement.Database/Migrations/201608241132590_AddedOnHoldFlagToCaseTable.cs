namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOnHoldFlagToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "OnHold", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "OnHold");
        }
    }
}
