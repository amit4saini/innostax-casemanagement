﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IClientContactUserService
    {
        List<ClientContactUser> GetClientAssociatedContactUsers(Guid clientId);
        void Save(ClientContactUser[] clientContactUserData, Guid clientId);
    }

    public class ClientContactUserService : IClientContactUserService
    {
        private IInnostaxDb _innostaxDb;
        private IErrorHandlerService _errorHandlerService;
        private IContactUserBusinessUnitService _contactUserBusinessUnitService;

        public ClientContactUserService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IContactUserBusinessUnitService contactUserBusinessUnitService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _contactUserBusinessUnitService = contactUserBusinessUnitService;
        }

        public List<ClientContactUser> GetClientAssociatedContactUsers(Guid clientId)
        {
            var result = new List<ClientContactUser>();
            var existingClientContactUsers = _innostaxDb.ClientContactUsers.Where(x => x.ClientId == clientId && x.IsDeleted == false)
                .Include(x => x.Country)
                .Include(x => x.State).ToList();
            if(existingClientContactUsers != null)
            {
                result = new List<ClientContactUser>();
                existingClientContactUsers.ForEach(x => result.Add(Mapper.Map<ClientContactUser>(x)));
            }
            foreach (var clientContactUser in result)
            {
                var contactUserBusinessUnit = _contactUserBusinessUnitService.GetContactUserBusinessUnitDetails(clientContactUser.Id);
                clientContactUser.ContactUserBusinessUnits = contactUserBusinessUnit;
            }
            return result=result.OrderByDescending(x=>x.CreationDateTime).ToList();
        }

        public void Save(ClientContactUser[] clientContactUserData, Guid clientId)
        {
            foreach (var contactUser in clientContactUserData)
                {
                    var mappedContactUser = Mapper.Map<Common.Database.Models.ClientContactUser>(contactUser);
                    mappedContactUser.Id = contactUser.Id;
                    mappedContactUser.Country = null;
                    mappedContactUser.State = null;      
                    mappedContactUser.ClientId = clientId;
                if (mappedContactUser.Id == new Guid())
                {
                    mappedContactUser.CreationDateTime = DateTime.Now;
                }               
                    _innostaxDb.ClientContactUsers.AddOrUpdate(mappedContactUser);
                    _innostaxDb.SaveChangesWithErrors();
                    if (contactUser.ContactUserBusinessUnits != null)
                    {
                        contactUser.ContactUserBusinessUnits.Select(c => { c.ClientContactUserId = mappedContactUser.Id; return c; }).ToList();
                    }                                   
                    _contactUserBusinessUnitService.Save(contactUser.ContactUserBusinessUnits,mappedContactUser.Id);
                }
        }
    }
}
