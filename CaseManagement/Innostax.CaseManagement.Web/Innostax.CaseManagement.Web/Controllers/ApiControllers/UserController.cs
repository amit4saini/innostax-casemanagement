﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Models.Account;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAzureFileManager _azureFileManager;
        public UserController(IUserService userService, IAzureFileManager azureFileManager)
        {
            _userService = userService;
            _azureFileManager = azureFileManager;
        }

        // [Queryable]
         [System.Web.Http.Route("api/Users")]
        [HasPermission(new Permission[] { Permission.USER_READ_ALL,Permission.NFCUSER_READ_ALL, Permission.USER_GET_ALL })]
        public PageResult<User> Get(ODataQueryOptions<User> options)
        {
            return _userService.GetAllUsers(options, Request);
        }

        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.USER_READ_ALL })]
        [System.Web.Http.Route("api/User/{id}")]
        public Result<User> GetUser(Guid id)
        {
            return _userService.GetUser(id);
        }

        [HasPermission(new Permission[] { Permission.USER_CREATE })]
        public User Post(User userData)
        {
            return _userService.SaveUser(userData);
        }

        [System.Web.Http.Route("api/User/{id}")]
        //[HasPermission(new Permission[] { Permission.USER_UPDATE })]
        public User Put(Guid id, User userData)
        {
            userData.Id = id;
            return _userService.SaveUser(userData);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/GetRoles")]
        [HasPermission(new Permission[] { Permission.USER_READ_ALL,Permission.NFCUSER_READ_ALL ,Permission.GET_ALL_ROLES })]
        public PageResult<Role> GetRoles(ODataQueryOptions<Role> options)
        {
            return _userService.GetAllRoles(options, Request);
        }

        [System.Web.Mvc.HttpDelete]
        [System.Web.Http.Route("api/User/{id}")]
        [HasPermission(new Permission[] { Permission.USER_DELETE })]
        public Result Delete(Guid id)
        {
            return _userService.Delete(id);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/User/GetCurrentUser")]
        public async Task<User> GetCurrentUser()
        {
            var result = new User();
            result = _userService.GetCurrentUser();
            result.ProfilePicUrl = string.Empty;
            Guid profilePicKey = result.ProfilePicKey ?? Guid.Empty;
            if (profilePicKey != Guid.Empty)
                result.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
            else
                result.ProfilePicUrl = Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
            return result;
        }
        
        [HttpGet]
        [Route("api/Search/Users")]
        public PageResult<User> SearchUsers(ODataQueryOptions<Models.Account.User> options, string keyword)
        {
            return _userService.SearchUsersByKeyword(options, Request, keyword);
        }
    }
}