﻿using System.Web.Http.OData;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionPageResult
    {
        public PageResult<Discussion> Result { get; set; }
        public int TotalDiscussionCount { get; set; }
    }
}
