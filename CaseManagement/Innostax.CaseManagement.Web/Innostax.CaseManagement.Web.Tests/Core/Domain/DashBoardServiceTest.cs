﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Models;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DashBoardServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private Mock<ICommonService> _commonService;
        private Mock<IUserService> _userService;
        private DashBoardService sut;


        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            _commonService = new Mock<ICommonService>();
            _userService = new Mock<IUserService>();
            sut = new DashBoardService(innostaxDbMock.Object, _commonService.Object,_userService.Object);          
        }

        [TestMethod]
        public void GivenCalling_GetDashBoardRelatedData_WhenCasesArePresentThanResultCountIsNotZero()
        {
            _commonService.Setup(x => x.GetAuthorizedCasesForUser()).Returns(new List<Case> { new Case { CaseId = Guid.NewGuid() ,SubStatus=new SubStatus { Name="Requested"} } }.AsQueryable());
            var result = sut.GetDashBoardRelatedData();
            Assert.AreEqual(result.NewCasesCount,1);
        }

        [TestMethod]
        public void GivenCalling_GetDashBoardRelatedData_WhenCasesAreNotPresentThanResultCountIsZero()
        {
            _commonService.Setup(x => x.GetAuthorizedCasesForUser()).Returns(new List<Case> { }.AsQueryable());
            var result = sut.GetDashBoardRelatedData();
            Assert.AreEqual(result.ListOfRecentCases.Count,0);
        }
    }
}
