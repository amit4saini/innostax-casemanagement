namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOriginalDueDateColumnToTaskTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "OriginalDueDate", c => c.DateTime(nullable: false));            
        }
        
        public override void Down()
        {           
            DropColumn("dbo.Tasks", "OriginalDueDate");
        }
    }
}
