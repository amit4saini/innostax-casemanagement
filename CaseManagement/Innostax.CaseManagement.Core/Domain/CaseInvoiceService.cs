﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseInvoiceService
    {   
        List<TimeSheet> GetTimesheetsExpensesForACase(Guid caseId);
    }

    public class CaseInvoiceService : ICaseInvoiceService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private IErrorHandlerService _errorHandlerService;
        private ICommonService _commonService;
        private ICaseHistoryService _caseHistoryService;

        public CaseInvoiceService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, ICommonService commonService, ICaseHistoryService caseHistoryService)
        {
            _errorHandlerService = errorHandlerService;
            _innostaxDb = innostaxDb;
            _userService = userService;
            _commonService = commonService;
            _caseHistoryService = caseHistoryService;
        }

        public List<TimeSheet> GetTimesheetsExpensesForACase(Guid caseId)
        {
            var result = new List<TimeSheet>();
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_READ_ALL, caseId);                          
                var caseAssociatedTimeSheets = _innostaxDb.TimeSheets.Where(x => x.CaseId == caseId && x.IsDeleted == false);
                foreach (var existingTimesheet in caseAssociatedTimeSheets)
                {                        
                    var totalTimeSpent = existingTimesheet.EndTime.Value.Subtract(existingTimesheet.StartTime);
                    double totalHoursSpent = totalTimeSpent.TotalHours;
                    var timesheetTotalCost = existingTimesheet.Cost * System.Convert.ToDecimal(totalHoursSpent);  
                    var mappedTimesheet = Mapper.Map<TimeSheet>(existingTimesheet);
                    mappedTimesheet.Cost = timesheetTotalCost;                   
                    result.Add(mappedTimesheet);                  
                }            
            return result;
        }     
    }
}

