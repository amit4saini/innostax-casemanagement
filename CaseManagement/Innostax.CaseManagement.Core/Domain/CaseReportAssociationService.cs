﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using System;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseReportAssociationService
    {
        Guid Save(CaseReportAssociation requestData);
        void DeleteCaseReportAssociation(Guid caseId, bool isArchive);
    }

    public class CaseReportAssociationService : ICaseReportAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private ICaseHistoryService _caseHistoryService;

        public CaseReportAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICommonService commonService, ICaseHistoryService caseHistoryService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _commonService = commonService;
            _caseHistoryService = caseHistoryService;
        }

        public Guid Save(CaseReportAssociation requestData)
        {
            var result = new Guid();
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_UPDATE_ALL, requestData.CaseId);            
                var primaryCaseReportAssociationId = new Guid();
                var primaryReportId = new Guid();
                result = new Guid();
                var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == requestData.CaseId);
                if (existingCase != null)
                {
                    var allCaseReportAssociations = _innostaxDb.CaseReportAssociations;
                    var existingCaseReportAssociations = allCaseReportAssociations.Where(x => x.CaseId == requestData.CaseId);
                    if (existingCaseReportAssociations != null)
                    {
                        foreach (var eachAssociation in existingCaseReportAssociations)
                        {
                        if (requestData.Reports.FirstOrDefault(x => x.ReportId == eachAssociation.ReportId && x.CountryId == eachAssociation.CountryId) == null)
                        {
                            eachAssociation.IsDeleted = true;
                            eachAssociation.DeletedOn = _commonService.GetUtcDateTime();
                            eachAssociation.DeletedBy = _userService.UserId;
                        }
                        }
                    }
                    if (requestData.Reports.Count > 0)
                    {
                        foreach (var report in requestData.Reports)
                        {
                            if (report != null)
                            {
                            if (_innostaxDb.CaseReportAssociations.FirstOrDefault(x => x.ReportId == report.ReportId && x.CaseId == requestData.CaseId && x.CountryId == report.CountryId && x.StateId == report.StateId && x.IsDeleted != true) == null)
                            {
                                SaveCaseAssociatedReports(report, requestData);
                                if (report.IsPrimary)
                                {
                                    primaryReportId = report.ReportId;
                                }
                                _caseHistoryService.SaveCaseHistory(requestData.CaseId, Common.Database.Enums.Action.CASE_REPORT_ADDED, null);
                            }
                            }
                        }
                    }                
                        _innostaxDb.SaveChangesWithErrors();             
                }
                else
                {
                    var caseReportAssociationError = Common.Constants.ErrorMessages.CaseManagement.CaseReportAssociation.CASE_REPORT_ASSOCIATION_ERROR;
                    _errorHandlerService.ThrowBadRequestError(caseReportAssociationError);
                }
                var primaryCaseReportAssociation = _innostaxDb.CaseReportAssociations.FirstOrDefault(x => x.ReportId == primaryReportId && x.CaseId == requestData.CaseId);
                if (primaryCaseReportAssociation != null)
                {
                    primaryCaseReportAssociationId = primaryCaseReportAssociation.Id;
                }
                result = primaryCaseReportAssociationId;            
            return result;
        }

        public void DeleteCaseReportAssociation(Guid caseId, bool isArchive)
        {
            var existingCaseReportAssociations = _innostaxDb.CaseReportAssociations.Where(x => x.CaseId == caseId);
            if (existingCaseReportAssociations != null)
            {
                foreach (var eachAssociation in existingCaseReportAssociations)
                {
                    eachAssociation.IsDeleted = isArchive;
                }
            }        
                _innostaxDb.SaveChangesWithErrors();
        }

        internal void SaveCaseAssociatedReports(Models.Report report, CaseReportAssociation requestData)
        {
            var result = new Result();
            var existingReport = _innostaxDb.Reports.FirstOrDefault(x => x.ReportId == report.ReportId);
            if (existingReport == null)
            {
                string reportNotFoundError = Common.Constants.ErrorMessages.Common.ReportError.REPORT_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(reportNotFoundError);
            }
            var mappedCaseReportAssociation = Mapper.Map<Innostax.Common.Database.Models.CaseReportAssociation>(requestData);
            mappedCaseReportAssociation.ReportId = report.ReportId;
            if (report.CountryId != 0)
                mappedCaseReportAssociation.CountryId = report.CountryId;
            if (report.StateId != 0)
            {
                mappedCaseReportAssociation.StateId = report.StateId;
            }
            mappedCaseReportAssociation.IsPrimaryReport = report.IsPrimary;
            mappedCaseReportAssociation.IsOtherReportType = report.IsOtherReportType;
            mappedCaseReportAssociation.OtherReportTypeName = report.OtherReportTypeName;
            _innostaxDb.CaseReportAssociations.Add(mappedCaseReportAssociation);
        }
    }
}
