﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoMapper;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
   public class TaskFileAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ITaskFileAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> userService;
        private Guid taskId;
        private Guid fileId;
        private Guid taskFileAssociationId;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICommonService> _commonService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            userService = new Mock<IUserService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _commonService = new Mock<ICommonService>();          
            taskId=System.Guid.NewGuid();
            fileId =System.Guid.NewGuid();
            taskFileAssociationId= System.Guid.NewGuid(); 
        sut = new TaskFileAssociationService(innostaxDbMock.Object, errorHandlerService,_caseHistoryService.Object, _commonService.Object);

        }

        private Models.Task GetDummyTask()
        {

            var dummyTask = new Models.Task
            {
                TaskId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA14866D0F"), 
                TaskFiles=new List<Models.FileModel> { }              
            };
            return dummyTask;
        }
        private List<TaskFileAssociation> GetDummyDatabaseModelOfTaskFileAssociation()
        {

            var dummyAssociation = new List<TaskFileAssociation>
            {
                new TaskFileAssociation
                {
                TaskId = taskId,
                IsDeleted=false,
                FileId=fileId,
                Id=taskFileAssociationId,
                UploadedFile=new UploadedFile { FileName="test"} ,
                Task=new Innostax.Common.Database.Models.Task { Title="test"}
                }          
            };
            return dummyAssociation;
        }
        private List<UploadedFile> GetDummyDatabaseModelOfUploadedFile()
        {

            var dummyUploadedFile = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileName="test",
                    FileUrl="test",         
                FileId=fileId ,
                FileType="test"        
                }
            };
            return dummyUploadedFile;
        }

        private List<Innostax.Common.Database.Models.Task> GetDummyInvalidDatabaseModelOfTask()
        {
            var dummyTask = new List<Innostax.Common.Database.Models.Task>
            {
                new Innostax.Common.Database.Models.Task
            {
                TaskId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                },
            new Innostax.Common.Database.Models.Task
            {
                TaskId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA18866D0F")
            } };
            return dummyTask;
        }
        private List<Innostax.Common.Database.Models.Task> GetDummyDatabaseModelOfTask()
        {
            var dummyTask = new List<Innostax.Common.Database.Models.Task>
            {
                new Innostax.Common.Database.Models.Task
            {
                TaskId = taskId
                },
            new Innostax.Common.Database.Models.Task
            {
                TaskId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA14866D0F")
            } };
            return dummyTask;
        }

        public List<Models.FileModel> GetDummyListOfFiles()
        {
            var dummyListOfFiles = new List<Models.FileModel>
            {
                  new Models.FileModel { FileId=Guid.NewGuid()}
            };
            return dummyListOfFiles;
        }

        public List<UploadedFile> GetListOfFiles()
        {
            var dummyListOfFiles = new List<UploadedFile>
            {             
                  new UploadedFile { FileId=Guid.NewGuid()}  
            };
            return dummyListOfFiles;
        }
       
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenTaskTableDoesNotContainsSpecifiedTaskId_ThenThrowsBadRequest()
        {
            try
            {        
                innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyInvalidDatabaseModelOfTask());
                sut.Save(GetDummyTask());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        public void GivenCallingSave_WhenTaskTableContainsSpecifiedTaskId_ThenResultReturns()
        {            
                innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(GetDummyDatabaseModelOfTaskFileAssociation());
                innostaxDbMock.Setup(c => c.Tasks).ReturnsDbSet(GetDummyDatabaseModelOfTask());
              sut.Save(GetDummyTask());                  
        }

        [TestMethod]     
        public void GivenCallingGetTaskAssociatedFiles_WhenItContainsFile_ThenMappedResultReturns()
        {
             innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(GetDummyDatabaseModelOfTaskFileAssociation());
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyDatabaseModelOfUploadedFile());
                sut.GetTaskAssociatedFiles(taskId);        
        }

        [TestMethod]
        public void GivenCalling_DeleteTaskFileAssociation_WhenFunctionExecuteSuccessfully_ThenMappedResultReturn()
        {        
            innostaxDbMock.Setup(c => c.TaskFileAssociations).ReturnsDbSet(GetDummyDatabaseModelOfTaskFileAssociation());
            sut.RemoveTaskFileAssociation(taskId, null);         
        }
    }
}
