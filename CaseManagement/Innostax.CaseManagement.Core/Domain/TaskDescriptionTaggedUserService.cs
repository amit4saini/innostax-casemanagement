﻿using AutoMapper;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskDescriptionTaggedUserService
    {
        void SaveTaskTaggedUsers(Guid taskId, List<Guid> taggedUserIdsList);
        List<Models.Account.User> GetTaskDescriptionTaggedUsers(Guid taskId);
        void DeleteTaskTaggedUsers(Guid taskId);
    }

    public class TaskDescriptionTaggedUserService : ITaskDescriptionTaggedUserService
    {
        private IInnostaxDb _innostaxDb;

        public TaskDescriptionTaggedUserService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public void SaveTaskTaggedUsers(Guid taskId, List<Guid> taggedUserIdsList)
        {
            var existingtaggedUsersList = _innostaxDb.TaskDescriptionTaggedUsers.Where(x => x.TaskId == taskId).ToList();
            if (existingtaggedUsersList != null)
            {
                existingtaggedUsersList.ForEach(x => _innostaxDb.TaskDescriptionTaggedUsers.Remove(x));
            }
            foreach (var eachTaggedUser in taggedUserIdsList.Distinct())
            {
                var newTaskDescritionTagUser = new TaskDescriptionTaggedUser
                {
                    TaskId = taskId,
                    UserId = eachTaggedUser
                };
                _innostaxDb.TaskDescriptionTaggedUsers.Add(newTaskDescritionTagUser);
            }
            _innostaxDb.SaveChangesWithErrors();
        }

        public List<Models.Account.User> GetTaskDescriptionTaggedUsers(Guid taskId)
        {
            var existingTaggedUsersList = _innostaxDb.TaskDescriptionTaggedUsers.Include(y => y.User).Where(x => x.TaskId == taskId).Select(y => y.User).ToList();
            var usersList = new List<Models.Account.User>();
            usersList = Mapper.Map<List<Models.Account.User>>(existingTaggedUsersList);
            return usersList;
        }

        public void DeleteTaskTaggedUsers(Guid taskId)
        {
            var existingTaggedUsersList = _innostaxDb.TaskDescriptionTaggedUsers.Where(x => x.TaskId == taskId).ToList();
            existingTaggedUsersList.ForEach(x => _innostaxDb.TaskDescriptionTaggedUsers.Remove(x));
        }
    }
}

