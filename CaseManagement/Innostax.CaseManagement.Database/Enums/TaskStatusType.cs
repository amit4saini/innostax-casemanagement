﻿namespace Innostax.Common.Database.Enums
{
    public enum TaskStatusType
    {
        NOT_STARTED,
        IN_PROGRESS,
        COMPLETED
    }
}
