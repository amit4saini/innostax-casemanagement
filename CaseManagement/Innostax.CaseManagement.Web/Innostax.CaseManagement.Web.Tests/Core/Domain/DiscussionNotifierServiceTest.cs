﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionNotifierServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ErrorHandlerService errorHandlerService;
        private DiscussionNotifierService sut;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            sut = new DiscussionNotifierService(innostaxDbMock.Object, errorHandlerService);
        }

        
    }
}
