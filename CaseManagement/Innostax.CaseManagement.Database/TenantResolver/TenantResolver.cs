﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models.Master;

namespace Innostax.Common.Database.TenantResolver
{
    public interface ITenantResolver
    {
        Task<object> Resolve(string identifier);
    }

    public class TenantResolver : ITenantResolver
    {
        private IInnostaxDb _innostaxDb;

        public TenantResolver(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public async Task<object> Resolve(string identifier)
        {
            var tenant = _innostaxDb.Tenants.FirstOrDefault(x => x.TenantDomain == identifier);
            if (tenant == null)
            {
                return null;
            }
            var connectionString = GetConnectionStringByIdentifier(tenant);
            var tenantDbContext = new TenantDb(connectionString);
            return tenantDbContext;
        }
        private string GetConnectionStringByIdentifier(Tenant tenant)
        {

            var connectionString = @"Server=" + tenant.Serverip + ";Initial Catalog=" + tenant.Dbname + ";User ID =" + (tenant.UserId != null ? tenant.UserId : "") + ";Password =" + (tenant.Password != null ? tenant.Password : "") + ";Integrated Security=True;MultipleActiveResultSets=True; Connection Timeout=30";
            return connectionString;
        }
    }
}
