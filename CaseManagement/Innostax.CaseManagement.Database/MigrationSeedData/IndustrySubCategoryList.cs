﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class IndustrySubCategoryList
    {
        public List<string> IndustrySubCategory()
        {
            var industrySubCategories = new List<string>{
                "Healthcare/Pharmaceutical",
                "Electronics/Software/Telecommunications",
                "Transportation",
                "Food/Drink",
                "Raw Materials",
                "Paper Products",
                "Apparel/Textiles",
                "Chemicals/Oil/Gas",
                "Metals",
                "Machinery/Tools",
                "Shipping/Logistics",
                "Home Goods",
                "Construction/Building Materials",
                "Military/Security",
                "Bank/Finance/Insurance",
                "Other"
                 };
            return industrySubCategories;
        }
    }
}
