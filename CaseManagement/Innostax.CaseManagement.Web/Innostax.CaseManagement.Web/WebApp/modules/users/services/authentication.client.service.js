'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window',
  function ($window) {
      var auth = { user: {} };
      getAuthenticationObject();

      function toCamelCase(str)
      {
          return str.charAt(0).toLowerCase() + str.slice(1);
      }

      function getAuthenticationObject()
      {
          var currentUser = angular.copy($window.user);
          auth = { user: convertKeysToCamelCase(currentUser) };
      }

      function convertKeysToCamelCase(currentObject)
      {
          if (currentObject == null) { return currentObject; }
          if (!Array.isArray(currentObject) &&  typeof currentObject !== 'object') { return currentObject; }
          if (Array.isArray(currentObject))
          {
              var keysList = currentObject;
              for (var index = 0; index < keysList.length; index++) {
                  currentObject[index] = convertKeysToCamelCase(currentObject[index]);
              }
          }
          else
          {
              var keysList = Object.keys(currentObject);
              for (var index = 0; index < keysList.length; index++) {
                  var camelCaseKey = toCamelCase(keysList[index]);
                  currentObject[camelCaseKey] = currentObject[keysList[index]];
                  delete currentObject[keysList[index]];
                  currentObject[camelCaseKey] = convertKeysToCamelCase(currentObject[camelCaseKey]);
              }
          }
          return currentObject;
      }

      return auth;
  }
]);
