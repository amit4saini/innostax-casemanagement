﻿using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{

    public interface IRegionService
    {
        List<Innostax.CaseManagement.Models.Region> GetAllRegions();
    }

    public class RegionService : IRegionService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public RegionService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Innostax.CaseManagement.Models.Region> GetAllRegions()
        {

            var result = new List<Innostax.CaseManagement.Models.Region>();
            var regionList = _innostaxDb.Regions.ToList();
            if (regionList.Count == 0)
            {
                string regionNotFoundError = Common.Constants.ErrorMessages.Common.RegionError.REGION_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(regionNotFoundError);
            }
            result = new List<Innostax.CaseManagement.Models.Region>();
            foreach (var eachregion in regionList)
            {
                var region = Mapper.Map<Innostax.CaseManagement.Models.Region>(eachregion);
                result.Add(region);
            }
            return result;
        }

    }
}
