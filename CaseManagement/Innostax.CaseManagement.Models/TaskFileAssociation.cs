﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class TaskFileAssociation
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; } 
        public Task Task { get; set; }
        public Guid FileId { get; set; }
        public FileModel UploadedFile { get; set; }
        public bool IsDeleted { get; set; }
    }
}
