namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Added_Vendor_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorId = c.Guid(nullable: false, identity: true),
                        VendorName = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VendorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Vendors");
        }
    }
}
