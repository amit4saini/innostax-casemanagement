﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
