﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class DiscountList
    {
        public List<double> Discount()
        {
            var discounts = new List<double>{
                      5,
                      10,
                      15,
                      20,
                 };
            return discounts;
        }
    }
}
