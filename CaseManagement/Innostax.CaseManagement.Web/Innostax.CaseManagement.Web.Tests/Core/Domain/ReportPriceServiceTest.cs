﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Moq;
using AutoMapper;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Innostax.CaseManagement.Core.Domain;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ReportPriceServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ReportPriceService sut;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            sut = new ReportPriceService(innostaxDbMock.Object);          
        }
        
        public List<CaseReportAssociation> getDummyCaseReportAssociations()
        {
            var dummyAssociations = new List<CaseReportAssociation>()
            {
                new CaseReportAssociation
                {
                    Id=new Guid(),
                    CaseId = new Guid("BA4F42FF-7222-E611-AF2A-408D5C634209"),
                    ReportId=new Guid("3312B2C1-6822-E611-AF2A-408D5C634209"),
                    CountryId=1
                }
            };
            return dummyAssociations;
        }

        public List<ReportPrice> getDummyReportPrices()
        {
            var dummyReportPrices = new List<ReportPrice>()
            {
                new ReportPrice
                {
                    Id=new Guid(),
                    ReportId=new Guid("3312B2C1-6822-E611-AF2A-408D5C634209"),
                    CountryId=1,
                    Price=Convert.ToDecimal(55.00)
                }
            };
            return dummyReportPrices;
        }

        [TestMethod]
        public void GivenCalling_GetReportPricesByCaseId_WhenCaseReportAssociationsArePresent()
        {
            var dummyAssociations= getDummyCaseReportAssociations();
            var dummyReportPrices = getDummyReportPrices();
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(dummyAssociations);
            innostaxDbMock.Setup(c => c.ReportPrices).ReturnsDbSet(dummyReportPrices);
            var returnedReportPrices=Mapper.Map<List<ReportPrice>>(sut.GetReportPricesByCaseId(new Guid("BA4F42FF-7222-E611-AF2A-408D5C634209")));
            Assert.AreEqual(returnedReportPrices.Count,1);
        }

        [TestMethod]
        public void GivenCalling_GetReportPricesByCaseId_WhenNoAssociationsIsPresent()
        {
            var dummyAssociations = new List<CaseReportAssociation>();
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(dummyAssociations);
            var returnedReportPrices = sut.GetReportPricesByCaseId(new Guid("BA4F42FF-7222-E611-AF2A-408D5C634209"));
            Assert.AreEqual(returnedReportPrices.Count,0);
        }


    }
}
