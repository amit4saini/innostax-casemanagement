namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingCaseTable2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "CaseApprovedDate", c => c.DateTime());
            AddColumn("dbo.Cases", "CaseApprovedBy", c => c.Guid());
            AddColumn("dbo.Cases", "CaseReviewedDate", c => c.DateTime());
            CreateIndex("dbo.Cases", "CaseApprovedBy");
            AddForeignKey("dbo.Cases", "CaseApprovedBy", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "CaseApprovedBy", "dbo.Users");
            DropIndex("dbo.Cases", new[] { "CaseApprovedBy" });
            DropColumn("dbo.Cases", "CaseReviewedDate");
            DropColumn("dbo.Cases", "CaseApprovedBy");
            DropColumn("dbo.Cases", "CaseApprovedDate");
        }
    }
}
