﻿using Innostax.CaseManagement.Core.Domain;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class IndustrySubCategoryController : ApiController
    {
        private readonly IIndustrySubCategoryService _industrySubCategory;
        public IndustrySubCategoryController(IIndustrySubCategoryService industrySubCategory)
        {
            _industrySubCategory = industrySubCategory;
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/IndustrySubCategories")]
        public List<Models.IndustrySubCategory> Get()
        {
            return _industrySubCategory.GetIndustrySubCategory();
        }
    }
}
