﻿using System.Collections.Generic;
using System.Linq;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITimeSheetDescriptionService
    {
        List<TimeSheetDescription> GetAllTimeSheetDescriptions();
    }

    public class TimeSheetDescriptionService:ITimeSheetDescriptionService
    {
        private IInnostaxDb _innostaxDb;
        private ErrorHandlerService _errorHandlerService;

        public TimeSheetDescriptionService(IInnostaxDb innostaxDb, ErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<TimeSheetDescription> GetAllTimeSheetDescriptions()
        {
            var result = new List<TimeSheetDescription>();
            var timeSheetDescriptions = _innostaxDb.TimeSheetDescriptions.ToList();
            if (timeSheetDescriptions != null)
            {
                result = new List<TimeSheetDescription>();
                timeSheetDescriptions.ForEach(x => result.Add(Mapper.Map<TimeSheetDescription>(x)));
            }
            return result;
        }
    }
}
