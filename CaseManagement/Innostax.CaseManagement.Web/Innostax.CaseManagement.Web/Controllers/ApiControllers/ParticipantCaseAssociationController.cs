﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class ParticipantCaseAssociationController : ApiController
    {
        private readonly IParticipantCaseAssociationService _participantCaseAssociationService;
        public ParticipantCaseAssociationController(IParticipantCaseAssociationService participantCaseAssociationService)
        {
            _participantCaseAssociationService = participantCaseAssociationService;
        }

        [HttpGet]
        [Route("api/Participator/{id}")]
        public List<ParticipantCaseAssociation> Get(Guid id)
        {
            return _participantCaseAssociationService.GetParticipantCaseAssociationByCaseId(id);
        }

        [HttpGet]
        [Route("api/Participator/{id}/Cases")]
        public List<Case> GetCasesByParticipantId(Guid id)
        {
            return _participantCaseAssociationService.GetCasesByParticipantId(id);
        }

        [HttpPost]
        [Route("api/Participator")]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
        public void Post(ParticipantCaseAssociation participantCaseAssociationData)
        {
            _participantCaseAssociationService.Save(participantCaseAssociationData);
        }
        
        [HttpPut]
        [Route("api/Participator/{id}")]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
        public void Put(Guid id, ParticipantCaseAssociation participantCaseAssociationData)
        {
            participantCaseAssociationData.Id = id;
            _participantCaseAssociationService.Save(participantCaseAssociationData);
        }

    }
}
