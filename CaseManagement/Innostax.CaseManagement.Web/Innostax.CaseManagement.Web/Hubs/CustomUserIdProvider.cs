﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Microsoft.AspNet.SignalR;

namespace Innostax.CaseManagement.Web.Hubs
{
    public class CustomUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            var errorHandler = new ErrorHandlerService();
            var innostaxDb = new InnostaxDb();
            var _userService = new UserService(innostaxDb, errorHandler);
            return _userService.UserId.ToString();
        }
    }
}