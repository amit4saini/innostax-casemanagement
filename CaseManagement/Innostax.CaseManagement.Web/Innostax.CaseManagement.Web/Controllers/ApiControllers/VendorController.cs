﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class VendorController : ApiController
    {
        private readonly IVendorService _vendorService;
        public VendorController(IVendorService vendorService)
        {
            _vendorService = vendorService;
        }

        [HttpPost]
        public void Save(Vendor vendorData)
        {
            _vendorService.Save(vendorData);
        }

        [HttpPut]
        public void Put(Guid id,Vendor vendorData)
        {
            vendorData.VendorId = id;
            _vendorService.Save(vendorData);
        }

        [HttpGet]
        public List<Vendor> GetAllVendors()
        {
           return _vendorService.GetAllVendors();
        }
    }
}
