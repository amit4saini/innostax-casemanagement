﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class RegionController : ApiController
    {
        private readonly IRegionService _regionService;
        public RegionController(IRegionService regionService)
        {
            _regionService = regionService;
        }

        // GET api/<controller>
        [System.Web.Http.Route("api/Regions")]
        public List<Innostax.CaseManagement.Models.Region> Get()
        {
            var getAllRegionsResult = _regionService.GetAllRegions();
            return getAllRegionsResult;
        }

    }
}