﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Core.Services.Implementation;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
   public class NotificationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private NotificationService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandlerService;
        private Mock<IAzureFileManager> _azureFileManager;
        private Mock<ICommonService> _commonService;
        Guid userId;

        [TestInitialize]
        public void TestInitialize()
        {
            userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandlerService = new ErrorHandlerService();
            _azureFileManager = new Mock<IAzureFileManager>();
            _commonService = new Mock<ICommonService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);          
            sut = new NotificationService(innostaxDbMock.Object,_userService.Object, _errorHandlerService,_azureFileManager.Object, _commonService.Object);           
        }
        private List<Notification> GetDummyNotifications()
        {

            var dummyNotifications = new List<Notification>
            {
                 new Notification()
                {
                    Id = Guid.NewGuid(),
                    UserId=new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F"),
                    IsViewed =false,
                    CreatedBy = userId,
                    CreatedByUser = GetADummyUser()
                },
                new Notification()
                {
                    Id = Guid.NewGuid(),
                    UserId=new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F"),
                    IsViewed=false,
                    CreatedBy = userId,
                    CreatedByUser = GetADummyUser()
                },
                new Notification()
                {
                    Id = Guid.NewGuid(),
                    UserId=new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F"),
                    IsViewed=true,
                    CreatedBy = userId,
                    CreatedByUser = GetADummyUser()
                }
            };
            return dummyNotifications;
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId }
                            };
            return dummyUser;
        }
        private Innostax.Common.Database.Models.UserManagement.User GetADummyUser()
        {
            var dummyUser = new Innostax.Common.Database.Models.UserManagement.User
                            {
                                 Id = userId , 
                                 ProfilePicKey=Guid.NewGuid(),                                                           
                            };
            return dummyUser;
        }
        [TestMethod]    
        public void GivenCalling_GetAllNotification_WhenNoNotificationIsNotPresent_ThenThrowException()
        {            
                var dummyNotifications = new List<Notification>();
                innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
                innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
                var actual = sut.GetAllNotifications(true);
                Assert.AreEqual(actual.Status, System.Threading.Tasks.TaskStatus.Faulted);                  
        }
        [TestMethod]
        public void GivenCalling_GetAllNotification_WhenNotificationIsPresent_ThenReturnMappedResult()
        {
            var dummyNotifications = GetDummyNotifications();
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
            var actual = sut.GetAllNotifications(true);
            Assert.IsNotNull(actual);
        }
        [TestMethod]   
        public void GivenCalling_GetAllUnreadNotification_WhenNoNotificationIsNotPresent_ThenThrowException()
        {            
                var dummyNotifications = new List<Notification>();
                innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
                var actual = sut.GetAllNotifications(false);
                Assert.AreEqual(actual.Status, System.Threading.Tasks.TaskStatus.Faulted);                    
        }
        [TestMethod]
        public void GivenCalling_GetAllUnreadNotification_WhenNotificationIsPresent_ThenReturnMappedResult()
        {
            var dummyNotifications = GetDummyNotifications();
            innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
            var actual = sut.GetAllNotifications(false);
            Assert.IsNotNull(actual);
        }
        [TestMethod]
        public void GivenCalling_MarkAllNotificationsReadForUser_WhenNotificationWithIsViewedTruePresent_ThenReturnMappedResult()
        {
            var dummyNotifications = GetDummyNotifications();
            innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
            try
            {
                sut.MarkAllNotificationsReadForUser(new System.Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F"));
            }
            catch (Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }
        //[TestMethod]
        //public void GivenCalling_Save_WhenNotificationIsPresent_ThenReturnMappedResult()
        //{
        //    var dummyNotifications = GetDummyNotifications();
        //    var notification = new CaseManagement.Models.Notification
        //    {
        //        Id = Guid.NewGuid(),
        //        NotificationText = "hello",
        //        ListOfUserId =new List<Guid> { Guid.Parse("AF7A5C9B-7E00-E611-8845-08002700BC7E") }
        //    };
        //    innostaxDbMock.Setup(c => c.Notifications).ReturnsDbSet(dummyNotifications);
        //    try
        //    {
        //        sut.Save(notification);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //    }
        //}
    }
}
