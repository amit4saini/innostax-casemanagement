namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "InHouseTurnTime", c => c.Int());
            AddColumn("dbo.Cases", "NumberOfDaysLate", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "NumberOfDaysLate");
            DropColumn("dbo.Cases", "InHouseTurnTime");
        }
    }
}
