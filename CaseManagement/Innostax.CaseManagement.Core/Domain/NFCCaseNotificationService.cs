﻿using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AutoMapper;
using System.Data.Entity;

namespace Innostax.CaseManagement.Core.Domain
{

   public class NFCCaseNotificationService
    {
        private IInnostaxDb _innostaxDb; 
        private INotificationService _notificationService;
        private ICommonService _commonService;

        public NFCCaseNotificationService(IInnostaxDb innostaxDb, INotificationService notificationService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;      
            _notificationService = notificationService;
            _commonService = commonService;
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(baseDirectory, @"..\..\..\"));
            string connectionStringPath = Path.Combine(newPath, "Kreller.CaseManagement.Web", "App_Data");
            AppDomain.CurrentDomain.SetData("DataDirectory", connectionStringPath);
        }
        public List<Models.Case> DueNFCCaseDetails()
        {   
            var dueNFCCases = _innostaxDb.Cases.Include(y=>y.Client).Where(x => x.IsDeleted == false && x.IsNFCCase==true && (System.Data.Entity.DbFunctions.DiffDays(DateTime.UtcNow, x.NFCDueDate) == 0)).ToList();
            var mappedCases = Mapper.Map<List<Models.Case>>(dueNFCCases);
            return mappedCases;
        }

        public void SendNotification()
        {
            var dueNFCCases = DueNFCCaseDetails();
            foreach (var dueCase in dueNFCCases)
            {
                var notificationText = string.Empty;
                var URL = _commonService.GetCaseRelatedUrl(Common.Database.Enums.NotificationUrl.CASE, dueCase.CaseNumber, dueCase.CaseId);              
                    notificationText = Common.Constants.NotificationMessages.CaseNotification.NFC_CASE_DUE;
                    notificationText = string.Format(notificationText, dueCase.CaseNumber, dueCase.NickName);
                    _notificationService.SaveWebJobNotification(notificationText, new List<Guid> { dueCase.Client.SalesRepresentativeId.Value }, URL);                      
            };
        }
    }
}
