﻿using Innostax.CaseManagement.Models.Account;
using System;
using System.Collections.Generic;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Models
{
    public class Task
    {
        public System.Guid TaskId { get; set; }
        public Guid CaseId { get; set; }
        public String CaseNumber { get; set; }
        public Case Case { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TaskStatusType Status { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public Guid AssignedTo { get; set; }
        public User AssignedToUserDetails { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public User CreatedByUserDetails { get; set; }
        public bool IsDeleted { get; set; }
        public List<FileModel> TaskFiles { get; set; }
        public TaskPriority Priority { get; set; }
        public DateTime OriginalDueDate { get; set; }
    }
}
