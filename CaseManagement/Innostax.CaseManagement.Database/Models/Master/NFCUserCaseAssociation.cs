﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Database.Models
{
    public class NFCUserCaseAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }

        public Guid NFCUserId { get; set; }
        [ForeignKey("NFCUserId")]
        public UserManagement.User User { get; set; }
        public bool IsActive { get; set; }
    }
}
