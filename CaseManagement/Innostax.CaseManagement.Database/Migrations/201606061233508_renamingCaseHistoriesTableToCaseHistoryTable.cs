namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class renamingCaseHistoriesTableToCaseHistoryTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CaseHistories", newName: "CaseHistory");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.CaseHistory", newName: "CaseHistories");
        }
    }
}
