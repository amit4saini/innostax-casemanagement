﻿using Innostax.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class DiscussionNotifier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid DiscussionId { get; set; }
        [ForeignKey("DiscussionId")]
        public Discussion Discussion { get; set; }

        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        public bool IsRead { get; set; }
    }
}
