﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http;
using Innostax.Common.Infrastructure.Utility;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;
using System.Web.Configuration;
using System.Data;
using System.Data.Entity;
using Innostax.CaseManagement.Models.EmailModels;
using Innostax.Common.Database.Enums;
using System.Text.RegularExpressions;
using System.Globalization;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskService
    {       
        void ArchiveUnarchiveTask(Guid taskId, bool isArchive);
        void DeleteCaseAssociatedTasks(Guid caseId, bool isArchive);
        TaskPageResult GetAllTasksByCaseId(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, Guid caseId);
        PageResult<Task> GetAllTasksForAUser(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, Guid userId);
        Task GetTaskByTaskId(Guid taskId);
        List<Task> GetTasksByCaseIdAndUserId(CasesAndUsersIdModel ids);
        List<Guid> Save(Models.Task task);
        TaskPageResult SearchTasksByKeyword(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, string keyword, Guid? id);
        List<Task> GetRecentTasksForAUser(Guid userId);
    }

    public class TaskService : ITaskService
    {
        public IInnostaxDb _innostaxDb;
        public IUserService _userService;
        public ICommonUtils _commonUtils;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ITaskFileAssociationService _taskFileAssociationService;
        private ICommonService _commonService;
        private ICaseHistoryService _caseHistoryService;
        private IMailSenderService _mailSenderService;
        private ITaskActivityService _taskActivityService;
        private INotificationService _notificationService;
        private ITaskDescriptionTaggedUserService _taskDescriptionTaggedUserService;
        private IReportService _reportService;

        public TaskService(IInnostaxDb innostaxDb, IUserService userService,
            ICommonUtils commonUtils, IErrorHandlerService errorHandlerService, 
            ITaskFileAssociationService taskFileAssociationService, ICommonService commonService, 
            ICaseHistoryService caseHistoryService, IMailSenderService mailSenderService,
            ITaskActivityService taskActivityService, INotificationService notificationService,
            ITaskDescriptionTaggedUserService taskDescriptionTaggedUserService,
            IReportService reportService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _commonUtils = commonUtils;
            _errorHandlerService = errorHandlerService;
            _taskFileAssociationService = taskFileAssociationService;
            _commonService = commonService;
            _caseHistoryService = caseHistoryService;
            _mailSenderService = mailSenderService;
            _taskActivityService = taskActivityService;
            _notificationService = notificationService;
            _taskDescriptionTaggedUserService = taskDescriptionTaggedUserService;
            _reportService = reportService;
        }

        public List<Guid> Save(Models.Task task)
        {
            var listOfUserToBeNotified = new List<Guid>();          
            Common.Database.Enums.Action currentAction = new Common.Database.Enums.Action();
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_UPDATE_ALL, task.CaseId);            
                var isNewTask = false;
                var isStatusChanged = false;
                var isAssignedToUser = false;
                var existingTaskCreatedByUser = new Guid();
                DateTime existingTaskCreatedOn = _commonService.GetUtcDateTime();
                var existingTask = _innostaxDb.Tasks.FirstOrDefault(x => x.TaskId == task.TaskId && x.IsDeleted == false);
                if (existingTask == null)
                {
                    currentAction = Common.Database.Enums.Action.CASE_TASK_ADDED;
                    existingTask = new Common.Database.Models.Task();
                    isNewTask = true;
                }
                else
                {
                _commonService.ValidateAccess(Validate.TASK, Permission.CASE_UPDATE_ALL, existingTask.TaskId);
                if (task.Status != existingTask.Status)
                    {
                        isStatusChanged = true;
                    }
                    if (task.AssignedTo != existingTask.AssignedTo)
                    { isAssignedToUser = true; }
                    currentAction = Common.Database.Enums.Action.CASE_TASK_EDITED;
                    existingTaskCreatedByUser = existingTask.CreatedBy;
                    existingTaskCreatedOn = existingTask.CreatedOn;
                    if(existingTask.AssignedTo != task.AssignedTo)
                    {
                        var newValue = task.AssignedToUserDetails.FirstName + " " + task.AssignedToUserDetails.LastName;
                        _taskActivityService.SaveTaskActivity(task.TaskId, TaskActivityAction.TASK_ASSIGNED_TO, newValue);

                    }
                    if (existingTask.DueDate != task.DueDate)
                    {
                        _taskActivityService.SaveTaskActivity(task.TaskId, TaskActivityAction.TASK_DUEDATE_CHANGED, task.DueDate.ToString());
                    }
                    if (existingTask.Status != task.Status)
                    {
                        _taskActivityService.SaveTaskActivity(task.TaskId, TaskActivityAction.TASK_STATUS_CHANGED, task.Status.ToString());
                    }
                }
                existingTask = Mapper.Map<Common.Database.Models.Task>(task);
                if (isNewTask)
                {
                    existingTask.CreatedBy = _userService.UserId;
                    existingTask.CreatedOn = _commonService.GetUtcDateTime();
                    existingTask.Status = Common.Database.Enums.TaskStatusType.NOT_STARTED;
                    existingTask.OriginalDueDate = task.DueDate;
                }
                else
                {
                    existingTask.CreatedBy = existingTaskCreatedByUser;
                    existingTask.CreatedOn = existingTaskCreatedOn;
                }
                existingTask.EditedBy = _userService.UserId;
                existingTask.EditedOn = _commonUtils.CurrentDateTime();
                _innostaxDb.Tasks.AddOrUpdate(existingTask);                
                    _innostaxDb.SaveChangesWithErrors();
                if (task.Description != null)
                {
                    var regex = new Regex((@"@\[(.*?)\]"));
                    var matchesFound = regex.Matches(task.Description);
                    List<User> usersList = new List<User>();                  
                    foreach (Match eachMatch in matchesFound)
                    {
                        string userEmail = string.Empty;
                        userEmail = eachMatch.Value;
                        userEmail = userEmail.Replace("@[~", string.Empty).Replace("]", string.Empty);
                        var user = _userService.GetUserByEmail(userEmail);
                        if (user != null)
                        {
                            usersList.Add(user);
                        }
                    }                                                        
                    if (usersList.Count != 0)
                    {
                        _taskDescriptionTaggedUserService.SaveTaskTaggedUsers(existingTask.TaskId, usersList.Select(x => x.Id).ToList());
                    }
                }
                TaskFileAssociation associationDetails = new TaskFileAssociation();             
                if (isStatusChanged)
                {
                    listOfUserToBeNotified = SendNotification(existingTask, true, isNewTask,isAssignedToUser);
                }
                else
                {
                    if (isNewTask)
                    {
                        listOfUserToBeNotified = SendNotification(existingTask, false, isNewTask,isAssignedToUser);
                    }
                    else
                    {
                        listOfUserToBeNotified = SendNotification(existingTask, false, isNewTask,isAssignedToUser);
                    }
                }              
                if (task.TaskFiles.Count > 0 && isNewTask)
                {
                    task.TaskId = existingTask.TaskId;
                    _taskFileAssociationService.Save(task);
                }
                _caseHistoryService.SaveCaseHistory(existingTask.CaseId, currentAction, existingTask.Title);
                if (isNewTask)
                {                    
                    _taskActivityService.SaveTaskActivity(existingTask.TaskId, TaskActivityAction.TASK_CREATED, null);
                }                                       
            return listOfUserToBeNotified;
        }

        private List<Guid> SendNotification(Common.Database.Models.Task assignedTask, bool isStatusChanged,bool isNewTask,bool isAssignedToUser)
        {
            var notificationModel = new Notification();
            var notificationText = string.Empty;
            var listOfUserId = new List<Guid>();
            var assignedTaskDetails = _innostaxDb.Tasks.Include(x => x.Case).Include(x => x.CreatedByUser).Include(x => x.AssignedToUser).Include(x => x.EditedByUser).FirstOrDefault(x => x.TaskId == assignedTask.TaskId);
            var userName = string.Empty;
            string taskStatusText;
            switch (assignedTaskDetails.Status)
            {
                case TaskStatusType.NOT_STARTED: taskStatusText = "Not Started"; break;
                case TaskStatusType.IN_PROGRESS: taskStatusText = "In Progress"; break;
                case TaskStatusType.COMPLETED: taskStatusText = "Completed"; break;
                default:  taskStatusText = ""; break;
            }
            if (isStatusChanged)
            {
                notificationText = Common.Constants.NotificationMessages.TaskNotification.TASK_STATUS_CHANGED;
                userName = string.Concat(assignedTaskDetails.EditedByUser.FirstName, " ", assignedTaskDetails.EditedByUser.LastName);
                notificationText = string.Format(notificationText, userName, assignedTaskDetails.Title, taskStatusText, assignedTaskDetails.EditedOn);
                listOfUserId.Add(assignedTaskDetails.CreatedByUser.Id);
            }
            else if (isNewTask || isAssignedToUser)
            {
                notificationText = Common.Constants.NotificationMessages.TaskNotification.TASK_ASSIGNED_TO_USER;
                userName = string.Concat(assignedTaskDetails.CreatedByUser.FirstName, " ", assignedTaskDetails.CreatedByUser.LastName);
                notificationText = string.Format(notificationText, userName, assignedTaskDetails.Title, assignedTaskDetails.Case.CaseNumber, assignedTaskDetails.Case.NickName);
                listOfUserId.Add(assignedTaskDetails.AssignedToUser.Id);
            }
            else
            {
                notificationText = Common.Constants.NotificationMessages.TaskNotification.TASK_EDITED;
                userName = string.Concat(assignedTaskDetails.CreatedByUser.FirstName, " ", assignedTaskDetails.CreatedByUser.LastName);
                notificationText = string.Format(notificationText, userName, assignedTaskDetails.Title, assignedTaskDetails.Case.CaseNumber, assignedTaskDetails.Case.NickName);
                listOfUserId.Add(assignedTaskDetails.AssignedToUser.Id);
            }
            listOfUserId.AddRange(_taskDescriptionTaggedUserService.GetTaskDescriptionTaggedUsers(assignedTask.TaskId).Select(user => user.Id).ToList());
            listOfUserId = listOfUserId.Distinct().ToList();
            if (assignedTaskDetails.Case.ReviewedBy.HasValue)
            {
                listOfUserId.Add(assignedTaskDetails.Case.ReviewedBy.Value);
            }
            var URL = _commonService.GetCaseRelatedUrl(NotificationUrl.TASK, assignedTaskDetails.Case.CaseNumber, assignedTaskDetails.TaskId);         
            return _notificationService.SaveNotification(notificationText,listOfUserId,URL);
        }

        private void SendAssignedTaskEmailNotification(Common.Database.Models.Task assignedTask)
        {
            var assignedTaskEmailModel = new AssignTaskEmailModel();
            var assignedTaskDetails = _innostaxDb.Tasks.Include(x => x.Case).Include(x => x.CreatedByUser).Include(x => x.AssignedToUser).FirstOrDefault(x => x.TaskId == assignedTask.TaskId);
            assignedTaskEmailModel.CaseNumber = assignedTaskDetails.Case.CaseNumber;
            assignedTaskEmailModel.TaskTitle = assignedTaskDetails.Title;
            assignedTaskEmailModel.TaskDescription = assignedTaskDetails.Description;
            assignedTaskEmailModel.AssigneeName = assignedTaskDetails.CreatedByUser.FirstName + " " + assignedTaskDetails.CreatedByUser.LastName;
            assignedTaskEmailModel.AssignedUserMail = assignedTaskDetails.AssignedToUser.Email;
            assignedTaskEmailModel.AssignedUserName = assignedTaskDetails.AssignedToUser.FirstName + " " + assignedTaskDetails.AssignedToUser.LastName;
            _mailSenderService.SendAssignedTaskDetails(assignedTaskEmailModel);
        }

        public TaskPageResult GetAllTasksByCaseId(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, Guid caseId)
        {
            var result = new List<Task>();
            _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_READ_ALL, caseId);
            var allTasks = _commonService.GetAuthorizedTasks(caseId).Include(x=>x.Case).Include(x=>x.Case.Client).Where(x=>x.IsDeleted == false).ToList();
            foreach (var eachTask in allTasks)
            {
                var mappedTask = Mapper.Map<Task>(eachTask);
                var existingTaskFiles = _taskFileAssociationService.GetTaskAssociatedFiles(mappedTask.TaskId);
                var assignedToUserDetails = _innostaxDb.Users.FirstOrDefault(x => x.Id == eachTask.AssignedTo);
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(assignedToUserDetails);
                mappedTask.TaskFiles = existingTaskFiles;
                mappedTask.Case.PrimaryReportCountryName = _commonService.GetCaseCountryName(mappedTask.Case);
                result.Add(mappedTask);
            }
            var selectedTasksCount = result.Count;
            if (options.Filter != null)
                selectedTasksCount = (options.Filter.ApplyTo(result.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Task>).ToList().Count;
            IQueryable results = options.ApplyTo(result.AsQueryable());
            var pageResult = new TaskPageResult();
            pageResult.Result = new PageResult<Models.Task>(
            results as IEnumerable<Models.Task>, request.ODataProperties().NextLink,
            selectedTasksCount);
            pageResult.AllTasksCount = allTasks.Count;
            return pageResult;
        }

        public PageResult<Task> GetAllTasksForAUser(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, Guid userId)
        {
            var allTasks = _innostaxDb.Tasks.Where(x => x.AssignedTo == userId).Include(x => x.AssignedToUser).Include(x => x.Case).Include(x => x.Case.Client).ToList();           
            var mappedTasks = new List<Task>();
            foreach (var eachTask in allTasks)
            {
                var mappedTask = Mapper.Map<Task>(eachTask);
                if (_userService.GetUser(userId).Data.Role.Name == Roles.Client_Participant.ToString())
                {
                    var participantCaseAssociation=_innostaxDb.ParticipantCaseAssociation.FirstOrDefault(x => x.CaseId == eachTask.CaseId);
                    if (participantCaseAssociation !=null) 
                    {
                        if(participantCaseAssociation.IsActive == false)
                        continue;
                    }
              }
                var associatedCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == eachTask.CaseId);
                if (associatedCase != null)
                {
                    mappedTask.CaseNumber = associatedCase.CaseNumber;
                    var caseCountryName = _commonService.GetCaseCountryName(Mapper.Map<Case>(associatedCase));
                    mappedTask.Case.PrimaryReportCountryName = caseCountryName;
                }
                var existingTaskFiles = _taskFileAssociationService.GetTaskAssociatedFiles(mappedTask.TaskId);
                mappedTask.TaskFiles = existingTaskFiles;
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(eachTask.AssignedToUser);
                mappedTasks.Add(mappedTask);
            }
            int selectedTasksCount = mappedTasks.Count;
            if (options.Filter != null)
                selectedTasksCount = (options.Filter.ApplyTo(mappedTasks.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Task>).ToList().Count;
            IQueryable results = options.ApplyTo(mappedTasks.AsQueryable());
            return new PageResult<Models.Task>(
            results as IEnumerable<Models.Task>, request.ODataProperties().NextLink,
            selectedTasksCount);
        }

        public void ArchiveUnarchiveTask(Guid taskId, bool isArchive)
        {
            var existingTask = _innostaxDb.Tasks.FirstOrDefault(x => x.TaskId == taskId);
            if (existingTask != null)
            {
                  _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_DELETE_ALL, existingTask.CaseId);                
                    existingTask.IsDeleted = isArchive;
                    existingTask.DeletedBy = _userService.UserId;
                    existingTask.DeletedOn = _commonService.GetUtcDateTime();
                    _taskFileAssociationService.RemoveTaskFileAssociation(taskId, null);
                    _innostaxDb.SaveChangesWithErrors();
                    if (isArchive)
                    {
                        _caseHistoryService.SaveCaseHistory(existingTask.CaseId, Common.Database.Enums.Action.CASE_TASK_ARCHIVED, existingTask.Title);
                    }
                    else
                    {
                        _caseHistoryService.SaveCaseHistory(existingTask.CaseId, Common.Database.Enums.Action.CASE_TASK_UNARCHIVED, existingTask.Title);
                    }
                }            
        }
		
        public void DeleteCaseAssociatedTasks(Guid caseId, bool isArchive)
        {
            var existingTasks = _innostaxDb.Tasks.Where(x => x.CaseId == caseId).ToList();
            if(existingTasks.Count > 0)
            {
                _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_DELETE_ALL, caseId);
            }
            foreach (var task in existingTasks)
            {
                task.IsDeleted = isArchive;
                task.DeletedBy = _userService.UserId;
                task.DeletedOn = _commonService.GetUtcDateTime();
                _taskFileAssociationService.RemoveTaskFileAssociation(task.TaskId, null);
                _innostaxDb.SaveChangesWithErrors();
                if (isArchive)
                {
                    _caseHistoryService.SaveCaseHistory(task.CaseId, Common.Database.Enums.Action.CASE_TASK_ARCHIVED, task.Title);
                }
                else
                {
                    _caseHistoryService.SaveCaseHistory(task.CaseId, Common.Database.Enums.Action.CASE_TASK_UNARCHIVED, task.Title);
                }
            }
		}
        
        public Task GetTaskByTaskId(Guid taskId)
        {
            var mappedTask = new Task();
            mappedTask = Mapper.Map<Task>(_innostaxDb.Tasks.Include(x => x.Case).Include(x => x.Case.Client).FirstOrDefault(x => x.TaskId == taskId));
            if(mappedTask == null)
            {
                _errorHandlerService.ThrowResourceNotFoundError(Innostax.Common.Constants.ErrorMessages.Common.TaskErrors.TASK_NOT_FOUND);
            }
            else
            {
                _commonService.ValidateAccess(Validate.TASK,Permission.CASE_READ_ALL, mappedTask.TaskId);
                var associatedCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == mappedTask.CaseId);
                if (associatedCase != null)
                {
                    mappedTask.CaseNumber = associatedCase.CaseNumber;
                    mappedTask.Case.Reports = _reportService.GetCaseAssociatedReports(associatedCase.CaseId).ToArray();
                    if (mappedTask.Case.Reports.Length != 0)
                    {
                        var primaryReport = mappedTask.Case.Reports.FirstOrDefault(x => x.IsPrimary == true);
                        if (primaryReport != null)
                        {
                            mappedTask.Case.PrimaryReportType = primaryReport.ReportType;
                            mappedTask.Case.PrimaryReportCountryName = primaryReport.CountryName;
                        }
                    }
                    
                }
                var existingTaskFiles = _taskFileAssociationService.GetTaskAssociatedFiles(mappedTask.TaskId);
                mappedTask.TaskFiles = existingTaskFiles;               
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == mappedTask.AssignedTo));
                mappedTask.CreatedByUserDetails = Mapper.Map<Models.Account.User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == mappedTask.CreatedBy));
            }
            return mappedTask;
        }

        public List<Task> GetTasksByCaseIdAndUserId(CasesAndUsersIdModel caseIdAndUserIdList)
        {
            var mappedTasks = new List<Task>();
            if(caseIdAndUserIdList == null)
            {
                caseIdAndUserIdList = new CasesAndUsersIdModel();
            }
            if(caseIdAndUserIdList.CasesIdList == null)
            {
                caseIdAndUserIdList.CasesIdList = new Guid[0];
            }
            if (caseIdAndUserIdList.UsersIdList == null)
            {
                caseIdAndUserIdList.UsersIdList = new Guid[0];
            }

            var allSearchedTasks = _innostaxDb.Tasks.Where(x => x.IsDeleted == false && (caseIdAndUserIdList.CasesIdList.Contains(x.CaseId) || caseIdAndUserIdList.UsersIdList.Contains(x.AssignedTo))).Include(x=>x.AssignedToUser).Include(x => x.Case.Client).Include(x => x.Case).ToList();
            if(allSearchedTasks.Count > 0)
            {
                _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_READ_ALL, allSearchedTasks.First().CaseId);
            }
            foreach (var eachTask in allSearchedTasks)
            {
                var mappedTask = Mapper.Map<Task>(eachTask);
                var associatedCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == eachTask.CaseId);
                if (associatedCase != null)
                    mappedTask.CaseNumber = associatedCase.CaseNumber;
                var existingTaskFiles = _taskFileAssociationService.GetTaskAssociatedFiles(mappedTask.TaskId);
                mappedTask.TaskFiles = existingTaskFiles;
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(eachTask.AssignedToUser);
                mappedTask.Case.PrimaryReportCountryName = _commonService.GetCaseCountryName(mappedTask.Case);
                mappedTasks.Add(mappedTask);
            }
            mappedTasks = mappedTasks.OrderBy(x => x.Status).ThenBy(x => x.DueDate).ThenBy(x => x.CompletionDate).ToList();
            return mappedTasks;
        }

        public TaskPageResult SearchTasksByKeyword(ODataQueryOptions<Models.Task> options, HttpRequestMessage request, string keyword, Guid? id)
        {
            IQueryable<Common.Database.Models.Task> taskSearchResult;
            var defaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) || (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            var tasks = _commonService.GetAuthorizedTasks(null).Where(x => x.IsDeleted != true);
            taskSearchResult = tasks.Include(y => y.Case).Include(y => y.Case.Client).Include(y => y.Case.Client.SalesRepresentative).Include(y => y.Case.ClientContactUser).Include(y => y.AssignedToUser).Where(x => ((x.Description ?? "").Contains(keyword) || (x.Title ?? "").Contains(keyword)) && x.IsDeleted == false && x.Case.IsDeleted == false && (id != null ? (x.CaseId == id || x.AssignedTo == id && x.AssignedToUser.IsDeleted == false) : true));
            var taskCommentSearchResult = SearchTaskCommentsByKeyword(keyword, id);
            var allSearchedTasks = taskSearchResult.Union(taskCommentSearchResult).ToList();
            var mappedTasks = new List<Task>();
            foreach(var eachTask in allSearchedTasks)
            {
                var mappedTask = Mapper.Map<Task>(eachTask);
                if (eachTask.Case != null)
                    mappedTask.CaseNumber = eachTask.Case.CaseNumber;
                var existingTaskFiles = _taskFileAssociationService.GetTaskAssociatedFiles(mappedTask.TaskId);
                mappedTask.TaskFiles = existingTaskFiles;
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(eachTask.AssignedToUser);
                mappedTask.Case.PrimaryReportCountryName = _commonService.GetCaseCountryName(mappedTask.Case);
                mappedTasks.Add(mappedTask);
            }
            int selectedTasksCount = mappedTasks.Count;
            if (options.Filter != null)
                selectedTasksCount = (options.Filter.ApplyTo(mappedTasks.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Task>).ToList().Count;
            IQueryable results = options.ApplyTo(mappedTasks.AsQueryable());
            var pageResult = new TaskPageResult();
            pageResult.Result = new PageResult<Models.Task>(
            results as IEnumerable<Models.Task>, request.ODataProperties().NextLink,
            selectedTasksCount);
            pageResult.AllTasksCount = _innostaxDb.Tasks.Where(x => x.IsDeleted == false && x.Case.IsDeleted == false && (x.CaseId == id || x.AssignedTo == id && x.AssignedToUser.IsDeleted == false)).ToList().Count;
            return pageResult;
        }

        internal IQueryable<Innostax.Common.Database.Models.Task> SearchTaskCommentsByKeyword(string keyword, Guid? id)
        {
            var tasks = _commonService.GetAuthorizedTasks(null).Where(x => x.IsDeleted != true);
            return _innostaxDb.TaskComments.Include(x => x.Task).Include(x => x.Task.Case).Include(x => x.Task.Case.Client).Include(x => x.Task.Case.Client.SalesRepresentative).Include(x => x.Task.Case.ClientContactUser).Include(x => x.Task.AssignedToUser).Where(x => tasks.Contains(x.Task) && ((x.Comment ?? "").Contains(keyword) && x.IsDeleted == false && x.Task.IsDeleted == false && x.Task.Case.IsDeleted == false) && (id != null ? (x.Task.CaseId == id || x.Task.AssignedTo == id && x.Task.AssignedToUser.IsDeleted == false) : true)).Select(x => x.Task);
        }

        public List<Task> GetRecentTasksForAUser(Guid userId)
        {
            var lastOneWeek = DateTime.Now.AddDays(-7);
            var nextThreeWeeks = DateTime.Now.AddDays(21);
            var recentTasks = _innostaxDb.Tasks.Where(x => x.AssignedTo == userId && x.DueDate >= lastOneWeek && x.DueDate <= nextThreeWeeks && x.IsDeleted == false).Include(x => x.AssignedToUser).Include(x => x.Case).Include(x => x.Case.Client).ToList();           
            var mappedTasks = new List<Task>();
            foreach (var eachTask in recentTasks)
            {
                var mappedTask = Mapper.Map<Task>(eachTask);
                if (_userService.GetUser(userId).Data.Role.Name == Roles.Client_Participant.ToString())
                {
                    var participantCaseAssociation = _innostaxDb.ParticipantCaseAssociation.FirstOrDefault(x => x.CaseId == eachTask.CaseId);
                    if (participantCaseAssociation != null)
                    {
                        if (participantCaseAssociation.IsActive == false)
                            continue;
                    }
                }
                var associatedCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == eachTask.CaseId);
                if (associatedCase != null)
                {
                    mappedTask.CaseNumber = associatedCase.CaseNumber;
                    var caseCountryName = _commonService.GetCaseCountryName(Mapper.Map<Case>(associatedCase));
                    mappedTask.Case.PrimaryReportCountryName = caseCountryName;
                }              
                mappedTask.AssignedToUserDetails = Mapper.Map<Models.Account.User>(eachTask.AssignedToUser);
                mappedTasks.Add(mappedTask);
            }            
            return mappedTasks;
        }
    }
}
