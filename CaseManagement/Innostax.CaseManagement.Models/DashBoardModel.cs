﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
    public class DashBoardModel
    {
        public int NFCCasesCount { get; set; }
        public int NewCasesCount { get; set; }
        public int ClosedCasesCount { get; set; }
        public List<Case> ListOfRecentCases { get; set; }
    }
}
