'use strict';

// Setting up route
angular.module('core').factory('angular-spinner-factory', ['$q', '$window', '$rootScope', function ($q, $window, $rootScope) {
    return {
        request: function (config) {
            $rootScope.spinner = { active: true };
            return config || $q.when(config);

        },
        response: function (response) {
            $rootScope.spinner = { active: false };
            return response || $q.when(response);

        },
        responseError: function (response) {
            $rootScope.spinner = { active: false };
            return $q.reject(response);
        }
    };
}
]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$routeProvider',
  function ($stateProvider, $urlRouterProvider, $httpProvider, $routeProvider) {

     $httpProvider.interceptors.push('angular-spinner-factory');
     $httpProvider.interceptors.push('httpInterceptor');
    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise('not-found');

    // Home state routing
      $stateProvider
        .state('homes', {
            url: '/participant',
            templateUrl: '/participantApp/modules/case/views/cases-listing.view.html'
        })
      .state('not-found', {
          url: '/not-found',
          templateUrl: '/participantApp/modules/core/views/404.client.view.html'
      });
    $routeProvider
     .when("LogOff", {
         templateUrl: "LogOff",
     });
  }
]);
angular.module('core').factory('httpInterceptor', ['$q', 'usSpinnerService', function ($q, usSpinnerService) {

    var numLoadings = 0;

    return {
        request: function (config) {

            numLoadings++;

            // Show loader
            usSpinnerService.spin('spinner');
            return config || $q.when(config);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
                usSpinnerService.stop('spinner');
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if (!(--numLoadings)) {
                // Hide loader
                usSpinnerService.stop('spinner');
            }

            return $q.reject(response);
        }
    };
}]);
