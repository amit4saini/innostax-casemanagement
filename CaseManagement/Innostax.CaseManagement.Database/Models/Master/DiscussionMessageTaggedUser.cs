﻿using Innostax.Common.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Database
{
    public class DiscussionMessageTaggedUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public Models.UserManagement.User User { get; set; }
        public Guid DiscussionMessageId { get; set; }
        [ForeignKey("DiscussionMessageId")]
        public DiscussionMessage DiscussionMessage { get; set; }

    }
}
