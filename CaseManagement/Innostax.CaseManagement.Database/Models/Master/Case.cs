﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Innostax.Common.Database.Enums;
using Innostax.Common.Database.Models.UserManagement;

namespace Innostax.Common.Database.Models
{
    public class Case
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CaseId { get; set; }
        public Guid ClientId { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }

        public Guid? SubStatusId { get; set; }
        [ForeignKey("SubStatusId")]
        public SubStatus SubStatus { get; set; }
        public string ReportSubject { get; set; }
        public string NickName { get; set; }
        public string ClientPO { get; set; }

        public int? SubjectType { get; set; }

        public int NumberOfPrincipals { get; set; }

        public bool IsSpecificPrincipal { get; set; }

        public bool ConsentReceived { get; set; }

        public bool AgreementReceived { get; set; }

        public bool FCRACertificationEmailReceivedFromClient { get; set; }

        public int? StateOfEmploymentId { get; set; }

        [ForeignKey("StateOfEmploymentId")]
        public State StateOfEmployment { get; set; }

        public int? StateOfResidenceId { get; set; }

        [ForeignKey("StateOfResidenceId")]
        public State StateOfResidence { get; set; }

        public bool SalaryGreaterThan75k { get; set; }

        public bool References { get; set; }

        public bool Reputation { get; set; }

        public bool Rush { get; set; }

        public bool CreditReport { get; set; }

        public decimal ActualRetail { get; set; }

        public decimal Discount { get; set; }

        public bool IsProBonoGratis { get; set; }

        public string WhyProBonoGratis { get; set; }

        public string ClientProvidedInformation { get; set; }
        public bool ClientProvidedAttachments { get; set; }

        public string AdditionalSalesRepNotes { get; set; }
        public DateTime CreationDateTime { get; set; }

        public Guid CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public DateTime LastEditedDateTime { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public Guid? DeletedBy { get; set; }

        [ForeignKey("DeletedBy")]
        public User DeleteByUser { get; set; }

        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        [Index("CaseNumberIndex", IsUnique = true)]
        public string CaseNumber { get; set; }
        public string HoldReason { get; set; } 
        public DateTime? CasePlacedOnHoldDate { get; set; }
        public int? CaseAge { get; set; }
        public bool? ClientCancelled { get; set; }
        public DateTime? DDSupervisorApprovedCaseDate { get; set; }

        public Guid? DDSupervisorWhoApprovedCase { get; set; }

        [ForeignKey("DDSupervisorWhoApprovedCase")]
        public User DDSupervisorWhoApprovedCaseByUser { get; set; }      
        public DateTime? IHIDueDate { get; set; }
        public bool? ICIAssigned { get; set; }
        public DateTime? ICIDueDate { get; set; }
        public DateTime? FinalReportSentToSalesRepDate { get; set; }
        public DateTime? FinalReportSentToClientDate { get; set; }
        public Guid? AssignedTo { get; set; }
        [ForeignKey("AssignedTo")]
        public User AssignedToUser { get; set; }
        public bool IsNFCCase { get; set; }
        public bool? CaseAcceptedByNFC { get; set; }
        public DateTime? CaseAcceptedByNFCDate { get; set; }
        public DateTime? NFCDueDate { get; set; }
        public decimal? NFCBudget { get; set; }
        public bool? ReportReceivedFromNFC { get; set; }
        public DateTime? DateReportReceivedFromNFC { get; set; }
        public bool? InvoiceReceivedFromNFC { get; set; }
        public DateTime? DateInvoiceReceivedFromNFC { get; set; }
        public Guid? ClientContactUserId { get; set; }
        [ForeignKey("ClientContactUserId")]
        public ClientContactUser ClientContactUser { get; set; }
        public Speed Speed { get; set; }
        public Guid? BusinessUnitId { get; set; }
        [ForeignKey("BusinessUnitId")]
        public ContactUserBusinessUnit BusinessUnit { get; set; }
        public Guid? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public CaseStatus CaseStatus { get; set; }
        public Guid? ReviewedBy { get; set; }
        [ForeignKey("ReviewedBy")]
        public User ReviewedByUser { get; set; }
        public string MatterName { get; set; }
        public bool IsEligibleForInvoice { get; set; }
        public DateTime? DateReportReceivedFromICI { get; set; }
        public int? TurnTimeForICIBusinessDays { get; set; }     
        public AdverseReportType? AdverseReport { get; set; }
        public ICIErrorType? ICIError { get; set; }
        public string ICIErrorDescription { get; set; }
        public bool? IHIError { get; set; }
        public string IHIErrorDescription { get; set; }
        public IHIQuality? IHIQuality { get; set; }
        public string IHIQualityDescription { get; set; }
        public bool? IHIBonusCase { get; set; }
        public bool InQC { get; set; }
        public bool OnHold { get; set; }
        public int? InHouseTurnTime { get; set; }
        public int? NumberOfDaysLate { get; set; }   
        public DateTime? CaseReviewedDate { get; set; }
        public Guid? IHIAssignedUserId { get; set; }
        [ForeignKey("IHIAssignedUserId")]
        public User IHIAssignedUser { get; set; }
        public int? TotalInvestigativePeriod { get; set; }
        public int? TotalICIInvestigativePeriod { get; set; }
        public int? IHIAheadOfTime { get; set; }
        public int? ReportToClientAheadOfTime { get; set; }
        public int? ICICorpRegistrationTurnTime { get; set; }
        public DateTime? CaseCancelledDate { get; set; }
        public string CancellationReason { get; set; }
        public DateTime? EstimatedCompletionDate { get; set; }
        public DateTime? CaseAssignedToNFCDate { get; set; }
        public Guid? UserWhoAssignedToNFC { get; set; }
        [ForeignKey("UserWhoAssignedToNFC")]
        public User UserWhoAssignedToNFCUser { get; set; }
        public DateTime? NFCToKrellerAssignedDate { get; set; }
        public Guid? NFCToKrellerAssignedByUserId { get; set; }
        [ForeignKey("NFCToKrellerAssignedByUserId")]
        public User NFCToKrellerAssignedByUser { get; set; }
        public DateTime? CaseAssignedToICIDate { get; set; }
        public Guid? CaseAssignedToICIByUserId { get; set; }
        [ForeignKey("CaseAssignedToICIByUserId")]
        public User CaseAssignedToICIByUser { get; set; }
        public DateTime? CaseAssignedToIHIDate { get; set; }
        public DateTime? ICIConfirmedReceiptDate { get; set; }
        public Guid? ICIConfirmationMarkedByUserId { get; set; }
        [ForeignKey("ICIConfirmationMarkedByUserId")]
        public User ICIConfirmationMarkedByUser { get; set; }
        public DateTime? InitialContactWithReferencesDate { get; set; }
        public Guid? UserIdWhoMadeInitialContactWithReferences { get; set; }
        [ForeignKey("UserIdWhoMadeInitialContactWithReferences")]
        public User UserWhoMadeInitialContactWithReferences { get; set; }
        public DateTime? RegistrationDetailsReceivedDate { get; set; }
        public DateTime? DateReportDueFromICI { get; set; }
        public Guid? UserIdWhoMarkedReportFromICIAsReceived { get; set; }
        [ForeignKey("UserIdWhoMarkedReportFromICIAsReceived")]
        public User UserWhoMarkedReportFromICIAsReceived { get; set; }
        public DateTime? IHIReviewOfICIReportCompletedDate { get; set; }
        public Guid? UserIdWhoCompletedReviewOfICIReport { get; set; }
        [ForeignKey("UserIdWhoCompletedReviewOfICIReport")]
        public User UserWhoCompletedReviewOfICIReport { get; set; }
        public DateTime? DateIHISubmittedReportForReview { get; set; }
        public Guid? IHIUserIdWhoSubmittedReport { get; set; }
        [ForeignKey("IHIUserIdWhoSubmittedReport")]
        public User IHIUserWhoSubmittedReport { get; set; }
        public DateTime? DDSupervisorBeganQCReviewDate { get; set; }
        public Guid? UserIdWhoConductedQCReview { get; set; }
        [ForeignKey("UserIdWhoConductedQCReview")]
        public User UserWhoConductedQCReview { get; set; }
        public DateTime? DDSupervisorApprovedQCReviewDate { get; set; }
        public Guid? UserIdWhoApprovedQCReviewOfReport { get; set; }
        [ForeignKey("UserIdWhoApprovedQCReviewOfReport")]
        public User UserWhoApprovedQCReviewOfReport { get; set; }
        public Guid? UserIdWhoSentFinalReportToSalesRep { get; set; }
        [ForeignKey("UserIdWhoSentFinalReportToSalesRep")]
        public User UserWhoSentFinalReportToSalesRep { get; set; }
        public Guid? UserIdWhoSentFinalReportToClient { get; set; }
        [ForeignKey("UserIdWhoSentFinalReportToClient")]
        public User UserWhoSentFinalReportToClient { get; set; }
        public Guid? UserIdWhoPlacedCaseOnHold { get; set; }
        [ForeignKey("UserIdWhoPlacedCaseOnHold")]
        public User UserWhoPlacedCaseOnHold { get; set; }
        public DateTime? DateCaseRemovedFromHold { get; set; }
        public Guid? UserIdWhoRemovedCaseFromHold { get; set; }
        [ForeignKey("UserIdWhoRemovedCaseFromHold")]
        public User UserWhoRemovedCaseFromHold { get; set; }
        public DateTime? DateCaseClosed { get; set; }
        public Guid? UserIdWhoClosedCase { get; set; }
        [ForeignKey("UserIdWhoClosedCase")]
        public User UserWhoClosedCase { get; set; }  
        public DateTime? CaseDueDate { get; set; }
        public bool CaseCancelled { get; set; }
        [DefaultValue(true)]
        public bool InformationPending { get; set; }
        public bool UpdateRequested { get; set; }
        public Guid? CaseAssignedToIHIByUserId { get; set; }
        [ForeignKey("CaseAssignedToIHIByUserId")]
        public User CaseAssignedToIHIByUser { get; set; }
        public Guid? ICIAssignedUserId { get; set; }
        [ForeignKey("ICIAssignedUserId")]
        public User ICIAssignedUser { get; set; }
        public bool FinalReportSentToClient { get; set; }
        public bool FinalReportSentToSalesRep { get; set; }
        public Guid? DDSupervisorId { get; set; }
        [ForeignKey("DDSupervisorId")]
        public User DDSupervisorUserDetails { get; set; }
        public Guid? UserIdWhoCancelledCase { get; set; }
        [ForeignKey("UserIdWhoCancelledCase")]
        public User UserWhoCancelledCase { get; set; }
        public DateTime? ICIReportTurnTimeEndDate { get; set; }
    }
}
