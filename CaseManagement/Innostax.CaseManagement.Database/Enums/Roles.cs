﻿namespace Innostax.Common.Database.Enums
{
    public enum Roles
    {
        DDSupervisor,
        Supervisor,
        User,
        In_Country_Investigator,
        Sales_Rep,
        Client_Participant,
        Investigator,
        NFC,
        Billing,
        Client  
    }
}
