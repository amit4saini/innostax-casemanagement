namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addingUserDetailsAndICIUserAssociationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ICIUserCaseAssociations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        ICIUserId = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.ICIUserId)
                .Index(t => t.CaseId)
                .Index(t => t.ICIUserId);
            
            CreateTable(
                "dbo.UserDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        CompanyName = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CountryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDetails", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserDetails", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.ICIUserCaseAssociations", "ICIUserId", "dbo.Users");
            DropForeignKey("dbo.ICIUserCaseAssociations", "CaseId", "dbo.Cases");
            DropIndex("dbo.UserDetails", new[] { "CountryId" });
            DropIndex("dbo.UserDetails", new[] { "UserId" });
            DropIndex("dbo.ICIUserCaseAssociations", new[] { "ICIUserId" });
            DropIndex("dbo.ICIUserCaseAssociations", new[] { "CaseId" });
            DropTable("dbo.UserDetails");
            DropTable("dbo.ICIUserCaseAssociations");
        }
    }
}
