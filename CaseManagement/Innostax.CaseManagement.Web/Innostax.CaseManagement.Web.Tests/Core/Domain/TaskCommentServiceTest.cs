﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Moq;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Core.Domain;
using AutoMapper;
using Innostax.CaseManagement.Core.Services.Implementation;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskCommentServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandlerService;
        private Mock<ITaskService> _taskService;
        private TaskCommentService sut;
        private Mock<IAzureFileManager> _azureFileManager;
        private Mock<ICommonService> _commonService;
        private Mock<ITaskCommentFileAssociationService> _taskCommentFileAsociationService;
        private Mock<IMailSenderService> _mailSenderService;
        private Mock<ITaskCommentTaggedUserService> _taskCommentTaggedUser;
        private Mock<INotificationService> _notificationService;

        [TestInitialize]
        public void Initialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandlerService = new ErrorHandlerService();
            _taskService = new Mock<ITaskService>();
            _azureFileManager = new Mock<IAzureFileManager>();
            _commonService = new Mock<ICommonService>();
            _mailSenderService = new Mock<IMailSenderService>();
            _taskCommentTaggedUser = new Mock<ITaskCommentTaggedUserService>();
            _notificationService = new Mock<INotificationService>();
            

            _taskCommentFileAsociationService = new Mock<ITaskCommentFileAssociationService>();
            sut = new TaskCommentService(innostaxDbMock.Object, _taskService.Object, _userService.Object, _errorHandlerService,_azureFileManager.Object, _taskCommentFileAsociationService.Object,_commonService.Object,_mailSenderService.Object, _taskCommentTaggedUser.Object, _notificationService.Object);           
        }

        private List<TaskComment> getDummyTaskComments()
        {
            var dummyComments = new List<TaskComment>() {
                new TaskComment {
                    Id=new Guid(),
                    TaskId= new Guid("53A2053F-D523-E611-B573-408D5C634209"),
                    Comment="First Comment",
                    IsDeleted=false,
                    CreatedByUser=new Innostax.Common.Database.Models.UserManagement.User { Id = new Guid() },
                    Task = new Task() {TaskId= new Guid("12996776-D623-E611-B573-408D5C634209") },
                    DeletedByUser = new Innostax.Common.Database.Models.UserManagement.User {DeletedBy = new Guid() },
                },
                new TaskComment
                {
                    Id=new Guid("12996776-D623-E611-B573-408D5C634209"),
                    TaskId=new Guid("53A2053F-D523-E611-B573-408D5C634209"),
                    Comment="Second Comment",
                    IsDeleted=false,
                    CreatedByUser=new Innostax.Common.Database.Models.UserManagement.User{ Id = new Guid() },
                    Task = new Task() {TaskId= new Guid("12996776-D623-E611-B573-408D5C634209")},
                    DeletedByUser = new Innostax.Common.Database.Models.UserManagement.User {DeletedBy = new Guid() }
                }
            };
            return dummyComments;
        }

        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetCommenntsForTask_WhenCommentsArePresent()
        {
            var dummyComments = getDummyTaskComments();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(dummyComments);
            var actualComments = await sut.GetAllCommentsForTask(new Guid("53A2053F-D523-E611-B573-408D5C634209"));
            Assert.AreEqual(actualComments.Count, 2);
        }
        
        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetCommentsForTask_WhenNoCommentIsPresent()
        {
            var dummyComments = new List<TaskComment>();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(dummyComments);
            var actualComments = await sut.GetAllCommentsForTask(new Guid());
            Assert.AreEqual(actualComments.Count, 0);
        }
        
        [TestMethod]
        public void GivenCalling_DeleteTaskComment_WhenTaskCommmentWithRequestedIdIsPresent()
        {
            var dummyComments = getDummyTaskComments();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(dummyComments);
            try
            {
                sut.Delete(new Guid("12996776-D623-E611-B573-408D5C634209"));
            }
            catch(Exception e)
            {
                Assert.Fail("Expected no exception but got" + e.Message);
            }
        } 
    }
}
