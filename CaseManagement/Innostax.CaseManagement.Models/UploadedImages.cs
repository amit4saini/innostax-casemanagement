﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
   public class UploadedImages
    {
        public string ImageUrl { get; set; }
        public string ImageName { get; set; }
        public string CreatedBy { get; set; }
    }
}
