﻿'use strict';

// Setting up route
angular.module('client').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise('not-found');
    // Home state routing
    $stateProvider
        .state('clients', {
            url: '/case-management/clients',
            templateUrl: '/webapp/modules/clients/views/clients-list.view.html?ver=1234'
        })
        .state('client-form', {
            url: '/case-management/clients/:clientId',
            templateUrl:'/webapp/modules/clients/views/client-form.view.html'
        });
      
  }
]);
