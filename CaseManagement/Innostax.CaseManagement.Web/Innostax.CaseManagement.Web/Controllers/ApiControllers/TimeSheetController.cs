﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class TimeSheetController : ApiController
    {
        private readonly ITimeSheetService _timeSheetService;
        public TimeSheetController(ITimeSheetService timeSheetService)
        {
            _timeSheetService = timeSheetService;
        }

        [HttpGet]
        [HasPermission(new Permission[] { Permission.CASE_ALL_TIMESHEET,Permission.CASE_OWN_TIMESHEET })]
        [System.Web.Http.Route("api/Case/{caseId}/Timesheets")]
        public List<TimeSheet> Get(Guid caseId)
        {
            return _timeSheetService.GetTimeSheetsByCaseId(caseId);
        }

        [HttpPost]
        [HasPermission(new Permission[] { Permission.ADD_TIMESHEET })]
        public void Post(TimeSheet timeSheetData)
        {
            _timeSheetService.Save(timeSheetData);
        }

        [HttpDelete]
        [HasPermission(new Permission[] { Permission.CAN_DELETE_TIMESHEET })]
        public void Delete(Guid id)
        {
            _timeSheetService.Delete(id);
        }

        [HttpPut]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_ALL, Permission.ADD_TIMESHEET })]
        public void Put(Guid id, TimeSheet timeSheetData)
        {
            timeSheetData.Id = id;
            _timeSheetService.Save(timeSheetData);
        }
    }
}
