﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ClientContactUserServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ClientContactUserService sut;
        private ErrorHandlerService _errorHandlerService;
        private Mock<IContactUserBusinessUnitService> _contactUserBusinessUnitService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            _contactUserBusinessUnitService = new Mock<IContactUserBusinessUnitService>();
            _errorHandlerService = new ErrorHandlerService();
            sut = new ClientContactUserService(innostaxDbMock.Object, _errorHandlerService,_contactUserBusinessUnitService.Object);           
        }

        [TestMethod]
        public void GivenCalling_GetClientAssociatedContactUsers_WhenDataIsPresent_MappedResultReturn()
        {
            var clientId = Guid.NewGuid();
            var clientContactUsers = new List<Innostax.Common.Database.Models.ClientContactUser>
                                    {   new Innostax.Common.Database.Models.ClientContactUser { ClientId = clientId, ContactName = "TestUser1" },
                                        new Innostax.Common.Database.Models.ClientContactUser { ClientId = clientId, ContactName = "TestUser2" }
                                    };
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(clientContactUsers);
            var actual = sut.GetClientAssociatedContactUsers(clientId);
            Assert.AreEqual(actual.Count, 2);
        }

        [TestMethod]
        public void GivenCalling_GetClientAssociatedContactUsers_WhenDataIsNull_MappedResultReturn()
        {
            var clientId = Guid.NewGuid();
            var clientContactUsers = new List<Innostax.Common.Database.Models.ClientContactUser> ();
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(clientContactUsers);
            var actual = sut.GetClientAssociatedContactUsers(clientId);
            Assert.AreEqual(actual.Count, 0);
        }
    }
}
