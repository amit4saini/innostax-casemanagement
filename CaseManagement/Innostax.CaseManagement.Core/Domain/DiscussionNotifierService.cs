﻿using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionNotifierService
    {
        void SaveUsersForADiscussion(Guid discussionId, List<Guid> users);
        List<Guid> GetNotifiersListForDiscussion(Guid id);
    }

    public class DiscussionNotifierService : IDiscussionNotifierService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errrorHandlerService;

        public DiscussionNotifierService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errrorHandlerService = errorHandlerService;
        }

        public void SaveUsersForADiscussion(Guid discussionId, List<Guid> users)
        {
            var existingUserForDiscussion = _innostaxDb.DiscussionNotifiers.Where(x => x.DiscussionId == discussionId).ToList();
            _innostaxDb.DiscussionNotifiers.RemoveRange(existingUserForDiscussion);
            users.ForEach(x => _innostaxDb.DiscussionNotifiers.Add(new Common.Database.Models.DiscussionNotifier { DiscussionId = discussionId, UserId = x }));        
                _innostaxDb.SaveChangesWithErrors();
        }

        public List<Guid> GetNotifiersListForDiscussion(Guid id)
        {
            var userIdsList = new List<Guid>();
            userIdsList = _innostaxDb.DiscussionNotifiers.Where(x => x.DiscussionId == id).Select(x => x.UserId).ToList();
            return userIdsList;
        }
    }
}
