﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using Moq;
using System.Web.Http;
using System.Net;
using AutoMapper;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscountServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscountService sut;
        private Mock<IDiscountService> discountService;
        private ErrorHandlerService errorHandlerService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            discountService = new Mock<IDiscountService>();
            errorHandlerService = new ErrorHandlerService();
            sut = new DiscountService(innostaxDbMock.Object, errorHandlerService);
        }
        private List<Discount> GetDummyDiscounts()
        {
            var dummyRegions = new List<Discount>
            {
                new Discount
                {
                    DiscountId = new Guid(),
                    DiscountAmount = 5
                },
                 new Discount
                {
                    DiscountId = new Guid(),
                    DiscountAmount = 15
                }
            };
            return dummyRegions;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingGetDiscounts_NoDiscountFound_ThenThrowsNotFound()
        {
            try
            {
                var dummyDiscounts = new List<Discount>();
                innostaxDbMock.Setup(c => c.Discounts).ReturnsDbSet(dummyDiscounts);
                sut.GetDiscounts();
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCallingGetDiscount_WhenDiscountFound()
        {
            var dummyDiscounts = GetDummyDiscounts();
            innostaxDbMock.Setup(c => c.Discounts).ReturnsDbSet(dummyDiscounts);
            var actual = sut.GetDiscounts();
            Assert.IsNotNull(actual);
        }
    }
}
