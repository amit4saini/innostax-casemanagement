﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskCommentTaggedUserServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private TaskCommentTaggedUserService sut;
        Guid taskCommentId;

        [TestInitialize]
        public void TestInitialize()
        {
            taskCommentId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            sut = new TaskCommentTaggedUserService(
                innostaxDbMock.Object);
        }
        private List<TaskCommentTaggedUser> GetDummyTaskCommentTaggedUsers()
        {
            var dummyTaskCommentTaggedUsers = new List<TaskCommentTaggedUser>
            {
                new TaskCommentTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                    TaskCommentId=taskCommentId
                },
                new TaskCommentTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                    TaskCommentId=taskCommentId
                }
            };
            return dummyTaskCommentTaggedUsers;
        }
        [TestMethod]
        public void GivenCalling_SaveTaskCommentTaggers_WhenTaskCommentTaggedUserIsNotFound_ThenReturnEmptyResult()
        {
            try
            {
                var dummyTaskCommentTaggedUsers = GetDummyTaskCommentTaggedUsers();
                var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                innostaxDbMock.Setup(c => c.TaskCommentTaggedUsers).ReturnsDbSet(dummyTaskCommentTaggedUsers);
                sut.SaveTaskCommentTaggedUsers(Guid.NewGuid(), listOfTaggedUsers);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_SaveTaskCommentTaggers_WhenTaskCommentTaggedUserIsFound_ThenReturnEmptyResult()
        {
            try
            {
                var dummyTaskCommentTaggedUsers = GetDummyTaskCommentTaggedUsers();
                var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                innostaxDbMock.Setup(c => c.TaskCommentTaggedUsers).ReturnsDbSet(dummyTaskCommentTaggedUsers);
                sut.SaveTaskCommentTaggedUsers(taskCommentId, listOfTaggedUsers);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
