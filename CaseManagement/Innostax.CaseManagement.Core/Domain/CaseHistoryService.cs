﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData.Extensions;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseHistoryService
    {
        void SaveCaseHistory(Guid caseId, Common.Database.Enums.Action action,string newValue);
        Task<PageResult<CaseHistory>> GetHistoryDetailsForACase(ODataQueryOptions<CaseHistory> options, HttpRequestMessage request, Guid caseId, HistoryFilter filter);
        List<CaseHistory> GetCaseHistoryDetailsForAUser(Guid caseId, Guid userId);
    }

    public class CaseHistoryService : ICaseHistoryService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private ICommonService _commonService;
        private readonly IAzureFileManager _azureFileManager;
        private readonly IErrorHandlerService _errorHandlerService;
        public CaseHistoryService(IInnostaxDb innostaxDb, IUserService userService, ICommonService commonService, IAzureFileManager azureFileManager,IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _commonService = commonService;
            _azureFileManager = azureFileManager;
            _errorHandlerService = errorHandlerService;
        }

        public async Task<PageResult<CaseHistory>> GetHistoryDetailsForACase(ODataQueryOptions<CaseHistory> options, HttpRequestMessage request,Guid caseId, HistoryFilter filter)
        {
            var result = new List<CaseHistory>();
            _commonService.ValidateAccess(Validate.CASE, Permission.CASE_READ_ALL, caseId);           
                var caseHistoryList = new List<Common.Database.Models.CaseHistory>();
                var action = GetCaseHistoryEnumFromFilterAttribute(filter);
                if (_userService.UserPermissions.Contains(Permission.CASE_HISTORY_READ_ALL))
                    if (action.Count != 0)
                    {
                        caseHistoryList = _innostaxDb.CaseHistory.Where(x => x.CaseId == caseId && action.Contains(x.Action)).OrderByDescending(x => x.CreationDatetime).ToList();
                    }
                    else
                    {
                        caseHistoryList = _innostaxDb.CaseHistory.Where(x => x.CaseId == caseId).OrderByDescending(x => x.CreationDatetime).ToList();
                    }
                else if (_userService.UserPermissions.Contains(Permission.CASE_HISTORY_READ_OWN))
                    if (action.Count != 0)
                    {
                        caseHistoryList = _innostaxDb.CaseHistory.Where(x => x.CaseId == caseId && x.UserId == _userService.UserId && action.Contains(x.Action)).OrderByDescending(x => x.CreationDatetime).ToList();
                    }
                    else
                    {
                        caseHistoryList = _innostaxDb.CaseHistory.Where(x => x.CaseId == caseId && x.UserId == _userService.UserId).OrderByDescending(x => x.CreationDatetime).ToList();
                    }
                if (caseHistoryList != null && caseHistoryList.Count > 0)
                {
                    result = new List<CaseHistory>();
                    var mappedCaseHistory = new CaseHistory();
                    Dictionary<Guid, string> profilePicKeyUrlList = new Dictionary<Guid, string>();
                    foreach (var caseHistory in caseHistoryList)
                    {
                        mappedCaseHistory = Mapper.Map<CaseHistory>(caseHistory);
                        var caseRelatedUser = _innostaxDb.Users.FirstOrDefault(x => x.Id == caseHistory.UserId);
                        mappedCaseHistory.User = Mapper.Map<Models.Account.User>(caseRelatedUser);
                        Guid profilePicKey = mappedCaseHistory.User.ProfilePicKey ?? Guid.Empty;
                        if (profilePicKey != Guid.Empty)
                        {
                            if(profilePicKeyUrlList.ContainsKey(profilePicKey))
                            {
                                mappedCaseHistory.User.ProfilePicUrl = profilePicKeyUrlList[profilePicKey];
                            }
                            else
                            {
                                mappedCaseHistory.User.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
                                profilePicKeyUrlList.Add(profilePicKey, mappedCaseHistory.User.ProfilePicUrl);
                            }
                        }  
                        else
                            mappedCaseHistory.User.ProfilePicUrl= Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
                        result.Add(mappedCaseHistory);
                    }
                }            
            IQueryable results = options.ApplyTo(result.AsQueryable());
            return new PageResult<CaseHistory>(
            results as IEnumerable<CaseHistory>, request.ODataProperties().NextLink,
            result.Count()); ;
        }

        internal List<Innostax.Common.Database.Enums.Action> GetCaseHistoryEnumFromFilterAttribute(HistoryFilter filter)
        {
            var action = new List<Innostax.Common.Database.Enums.Action>();
            switch (filter)
            {
                case HistoryFilter.CASE_STATUS: action = new List<Common.Database.Enums.Action> { Common.Database.Enums.Action.CASE_STATUS_CHANGED };break;
                case HistoryFilter.TASK: action = new List<Common.Database.Enums.Action> { Common.Database.Enums.Action.CASE_TASK_ADDED, Common.Database.Enums.Action.CASE_TASK_ARCHIVED, Common.Database.Enums.Action.CASE_TASK_UNARCHIVED, Common.Database.Enums.Action.CASE_TASK_EDITED }; break;
                case HistoryFilter.EXPENSES: action = new List<Common.Database.Enums.Action> { Common.Database.Enums.Action.CASE_EXPENSE_ADDED, Common.Database.Enums.Action.CASE_EXPENSE_DELETED, Common.Database.Enums.Action.CASE_EXPENSE_EDITED, Common.Database.Enums.Action.CASE_EXPENSE_APPROVED }; break;
                case HistoryFilter.DISCUSSION: action = new List<Common.Database.Enums.Action> { Common.Database.Enums.Action.CASE_DISCUSSION_UNARCHIVED,Common.Database.Enums.Action.CASE_DISCUSSION_ARCHIVED,Common.Database.Enums.Action.CASE_DISCUSSION_ADDED,Common.Database.Enums.Action.CASE_DISCUSSION_DELETED,Common.Database.Enums.Action.CASE_DISCUSSION_EDITED,
                                                                                            Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_ADDED,Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_ARCHIVED,Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_DELETED,Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_UNARCHIVED,Common.Database.Enums.Action.CASE_DISCUSSION_MESSSAGE_EDITED}; break;
                case HistoryFilter.FILES: action = new List<Common.Database.Enums.Action> { Common.Database.Enums.Action.FILE_ADDED, Common.Database.Enums.Action.FILE_DELETED }; break;
                case HistoryFilter.ALL: action = new List<Common.Database.Enums.Action> { }; break;
                default: new List<Common.Database.Enums.Action> { };break;
            }
            return action;
        }

        public List<CaseHistory> GetCaseHistoryDetailsForAUser(Guid caseId, Guid userId)
        {
            var result = new List<CaseHistory>();
            _commonService.ValidateAccess(Validate.CASE, Permission.CASE_READ_ALL, caseId);
            {
                var caseHistoryList = new List<Innostax.Common.Database.Models.CaseHistory>();

                if (_userService.UserPermissions.Contains(Permission.CASE_HISTORY_READ_ALL))
                    caseHistoryList = _innostaxDb.CaseHistory.Where(x => x.CaseId == caseId && x.UserId == userId).ToList();
                
                if (caseHistoryList != null && caseHistoryList.Count > 0)
                {
                    result = new List<CaseHistory>();
                    caseHistoryList.ForEach(x => result.Add(Mapper.Map<CaseHistory>(x)));
                }
            }
            return result;
        }

        public void SaveCaseHistory(Guid caseId, Common.Database.Enums.Action action,string newValue)
        {
            Common.Database.Enums.Action mappedAction = (Common.Database.Enums.Action)action;
            var caseHistoryData = new Innostax.Common.Database.Models.CaseHistory
            {
                CaseId = caseId,
                Action = mappedAction,
                CreationDatetime = _commonService.GetUtcDateTime(),
                UserId =_userService.UserId,
                NewValue = newValue
            };             
            _innostaxDb.CaseHistory.Add(caseHistoryData);
            _innostaxDb.SaveChangesWithErrors();
        }
    }
}
