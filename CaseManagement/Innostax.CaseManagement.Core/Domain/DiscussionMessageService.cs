﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using System.Data.Entity;
using Innostax.CaseManagement.Core.Services.Implementation;
using Innostax.Common.Database.Enums;
using System.Net;
using System.Web.Http;
using System.Text.RegularExpressions;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionMessageService
    {
        Task<PageResult<DiscussionMessage>> GetAllDiscussionMessagesByDiscussionId(ODataQueryOptions<DiscussionMessage> options, HttpRequestMessage request, Guid id);
        List<Guid> Save(DiscussionMessage discussionMessage);
        void ArchiveUnarchiveDiscussionMessage(Guid id, bool toBeArchived);
        DiscussionMessage GetRecentDiscussionMessageByDiscussionId(Guid discussionId);
    }

    public class DiscussionMessageService : IDiscussionMessageService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IUserService _userService;
        private readonly IDiscussionMessageFileAssociationService _discussionMessageFileAssociationService;
        private readonly IAzureFileManager _azureFileManager;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly IMailSenderService _mailSenderService;
        private readonly IDiscussionNotifierService _discussionNotifierService;
        private readonly ICaseService _caseService;
        private readonly INotificationService _notificationService;
        private readonly ICommonService _commonService;
        private readonly IDiscussionMessageTaggedUserService _discussionMessageTaggedUserService;

        public DiscussionMessageService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService,IDiscussionMessageFileAssociationService discussionMessageFileAssociationService, IAzureFileManager azureFileManager, ICaseHistoryService caseHistoryService,IMailSenderService mailSenderService, IDiscussionNotifierService discussionNotifierService,ICaseService caseService,INotificationService notificationService,ICommonService commonService, IDiscussionMessageTaggedUserService discussionMessageTaggedUserService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;      
            _discussionMessageFileAssociationService = discussionMessageFileAssociationService;
            _azureFileManager = azureFileManager;
            _caseHistoryService = caseHistoryService;
            _mailSenderService = mailSenderService;
            _discussionNotifierService = discussionNotifierService;
            _notificationService = notificationService;
            _caseService = caseService;
            _discussionMessageTaggedUserService = discussionMessageTaggedUserService;          
            _commonService = commonService;          
        } 
           
        public async Task<PageResult<DiscussionMessage>> GetAllDiscussionMessagesByDiscussionId(ODataQueryOptions<DiscussionMessage> options, HttpRequestMessage request, Guid discussionId)
        {
            var result = new List<DiscussionMessage>();
            ODataQuerySettings settings = new ODataQuerySettings(){};
            var allDiscussionMessages = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).Where(x => x.DiscussionId == discussionId && x.IsDeleted == false).Include(x => x.CreatedByUser).ToList();
            if(allDiscussionMessages.Count > 0)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, discussionId);
            }
            result = new List<Models.DiscussionMessage>();
            Dictionary<Guid, string> profilePicKeyUrlList = new Dictionary<Guid, string>();
            foreach (var eachDiscussionMessage in allDiscussionMessages)
            {
                var existingDiscussionMessageFile = _discussionMessageFileAssociationService.GetDiscussionMessageAssociatedFiles(eachDiscussionMessage.Id);
                var mappedDiscussion = Mapper.Map<Models.DiscussionMessage>(eachDiscussionMessage);
                mappedDiscussion.Files = existingDiscussionMessageFile.ToArray();
                mappedDiscussion.Images = new List<UploadedImages>();
                var listOfImageFileType = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".tiff", ".rgb", ".svg", ".pgm", ".ppm", ".pnm" };
                foreach(var eachFile in mappedDiscussion.Files)
                {
                    var eachImage = new UploadedImages();
                    var picUrl = string.Empty;
                    if(listOfImageFileType.Contains(eachFile.FileType.ToLower()))
                    {
                        picUrl = await _azureFileManager.DownloadFile(eachFile.FileId, DownloadFileType.ProfilePic);
                        eachImage.ImageUrl = picUrl;
                        eachImage.ImageName = eachFile.FileName;
                        eachImage.CreatedBy = eachFile.UserName;
                        mappedDiscussion.Images.Add(eachImage);
                    }
                }
                mappedDiscussion.CreatedByUser.ProfilePicUrl = string.Empty;
                Guid profilePicKey = mappedDiscussion.CreatedByUser.ProfilePicKey ?? Guid.Empty;
                if (profilePicKey != Guid.Empty)
                {
                    if(profilePicKeyUrlList.ContainsKey(profilePicKey))
                    {
                        mappedDiscussion.CreatedByUser.ProfilePicUrl = profilePicKeyUrlList[profilePicKey];
                    }
                    else
                    {
                        mappedDiscussion.CreatedByUser.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
                        profilePicKeyUrlList.Add(profilePicKey, mappedDiscussion.CreatedByUser.ProfilePicUrl);
                    }
                }   
                else
                    mappedDiscussion.CreatedByUser.ProfilePicUrl = Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
                result.Add(mappedDiscussion);
            }
            IQueryable results = options.ApplyTo(result.AsQueryable(), settings);
            return new PageResult<DiscussionMessage>(
            results as IEnumerable<DiscussionMessage>, request.ODataProperties().NextLink,
            result.Count());
        } 

        public List<Guid> Save(DiscussionMessage discussionMessage)
        {
            var isNewDiscussionMessage = false;
            var listOfUserToBeNotified = new List<Guid>();
            var existingDiscussionMessage = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).FirstOrDefault(x => x.Id == discussionMessage.Id && x.IsDeleted == false);
            if (existingDiscussionMessage == null)
            {
                existingDiscussionMessage = new Common.Database.Models.DiscussionMessage();
                isNewDiscussionMessage = true;
            }
            else
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_UPDATE_ALL, discussionMessage.DiscussionId);
                if (existingDiscussionMessage.CreatedBy != _userService.UserId)
                {
                    string userNotAuthorized = Common.Constants.ErrorMessages.DiscussionManagement.DiscussionMessageError.NOT_AUTHORIZED;
                    _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorized);
                }
            }
            existingDiscussionMessage = Mapper.Map<Innostax.Common.Database.Models.DiscussionMessage>(discussionMessage);
            if (isNewDiscussionMessage)
            {
                existingDiscussionMessage.CreatedBy = _userService.UserId;
                existingDiscussionMessage.CreationDateTime = _commonService.GetUtcDateTime();
            }
            existingDiscussionMessage.EditedBy = _userService.UserId;
            existingDiscussionMessage.LastEditedDateTime = _commonService.GetUtcDateTime();
            var associatedDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionMessage.DiscussionId);
            if (associatedDiscussion != null)
                associatedDiscussion.LastEditedDateTime = _commonService.GetUtcDateTime();
            _innostaxDb.DiscussionMessages.AddOrUpdate(existingDiscussionMessage);    
                _innostaxDb.SaveChangesWithErrors();
            if (discussionMessage.Message != null)
            {
                var regex = new Regex((@"@\[(.*?)\]"));
                var matchesFound = regex.Matches(discussionMessage.Message);
                List<User> usersList = new List<User>();
                foreach (Match eachMatch in matchesFound)
                {
                    string userEmail = string.Empty;
                    userEmail = eachMatch.Value;
                    userEmail = userEmail.Replace("@[~", string.Empty).Replace("]", string.Empty);
                    var user = _userService.GetUserByEmail(userEmail);
                    if (user != null)
                    {
                        usersList.Add(user);
                    }
                }                                               
                if (usersList.Count != 0)
                {
                    _discussionMessageTaggedUserService.SaveDiscussionMessageTaggedUsers(existingDiscussionMessage.Id, usersList.Select(x => x.Id).ToList());
                }
            }            
            var discussionMessageFileAssociation = new DiscussionMessageFileAssociation();
            discussionMessageFileAssociation.Files = discussionMessage.Files.ToList();
            discussionMessageFileAssociation.DiscussionMessageId = existingDiscussionMessage.Id;
            _discussionMessageFileAssociationService.Save(discussionMessageFileAssociation);        
            if (isNewDiscussionMessage)
                listOfUserToBeNotified = SendNotification(Mapper.Map<DiscussionMessage>(existingDiscussionMessage));
            var caseId = GetCaseIdForDiscussionMessage(existingDiscussionMessage.DiscussionId);
            if (caseId != new Guid())
            {
                if (isNewDiscussionMessage)
                    _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_ADDED,null);
                else
                    _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_DISCUSSION_MESSSAGE_EDITED,null);
            }
            return listOfUserToBeNotified;
        }
       
        //private void SendNewDiscussionMessageEmail(DiscussionMessage discussionMessage)
        //{
        //    var newDiscussionMessageEmailModel = new NewDiscussionMessageEmailModel();
        //    var addedByUser = _userService.GetUser(discussionMessage.CreatedBy).Data;
        //    newDiscussionMessageEmailModel.AddedByUser = addedByUser.FirstName + " " + addedByUser.LastName;
        //    var associatedDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionMessage.DiscussionId);
        //    newDiscussionMessageEmailModel.DiscussionTitle = associatedDiscussion.Title;
        //    var associatedCase = _caseService.GetCaseInfoById(associatedDiscussion.CaseId);
        //    newDiscussionMessageEmailModel.CaseNumber = associatedCase.CaseNumber;
        //    newDiscussionMessageEmailModel.Message = discussionMessage.Message;
        //    newDiscussionMessageEmailModel.RecipientMails = new List<string>();
        //    newDiscussionMessageEmailModel.RecipientNames = new List<string>();
        //    var recipients = _discussionNotifierService.GetNotifiersListForDiscussion(associatedDiscussion.Id);
        //    if (recipients.Count == 0) { return; }
        //    foreach(var recipient in recipients)
        //    {
        //        var eachRecipient = _userService.GetUser(recipient).Data;
        //        newDiscussionMessageEmailModel.RecipientMails.Add(eachRecipient.Email);
        //        newDiscussionMessageEmailModel.RecipientNames.Add(eachRecipient.FirstName + " " + eachRecipient.LastName);
        //    }
        //    _mailSenderService.SendNewDiscussionMessageDetails(newDiscussionMessageEmailModel);
        //}

        private List<Guid> SendNotification(DiscussionMessage discussionMessage)
        {                            
            var associatedDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionMessage.DiscussionId);
            var associatedCase = _caseService.GetCaseInfoById(associatedDiscussion.CaseId);           
            var notificationText = Common.Constants.NotificationMessages.DiscussionMessageNotification.NEW_DISCUSSION_MESSAGE_POSTED;
            var addedByUser = _userService.GetUser(discussionMessage.CreatedBy).Data;
            var userName = string.Concat(addedByUser.FirstName, " ", addedByUser.LastName);
            notificationText = string.Format(notificationText, userName, associatedDiscussion.Title, associatedCase.CaseNumber, associatedCase.NickName);
            var recipients = _discussionNotifierService.GetNotifiersListForDiscussion(associatedDiscussion.Id);
            recipients.AddRange(_discussionMessageTaggedUserService.GetDiscussionMessageTaggedUsers(discussionMessage.Id).Select(user => user.Id).ToList());
            recipients = recipients.Distinct().ToList();          
            if (recipients.Count == 0) { return new List<Guid> { }; }     
            var URL= _commonService.GetCaseRelatedUrl(NotificationUrl.DISCUSSIONMESSAGE, associatedCase.CaseNumber, null);         
            return _notificationService.SaveNotification(notificationText, recipients, URL);
        }
        
        public void ArchiveUnarchiveDiscussionMessage(Guid id, bool toBeArchived)
        {
            var existingDiscussionMessage = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).FirstOrDefault(x => x.Id == id && x.IsDeleted != true);
            if (existingDiscussionMessage == null)
                _errorHandlerService.ThrowBadRequestError(Common.Constants.ErrorMessages.DiscussionManagement.DiscussionMessageError.DISCUSSION_MESSAGE_ID_INVALID);
            else
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_DELETE_ALL, existingDiscussionMessage.DiscussionId);
                existingDiscussionMessage.IsDeleted = toBeArchived;
                if(toBeArchived)
                {
                    existingDiscussionMessage.DeletedBy = _userService.UserId;
                    existingDiscussionMessage.DeletedOn = _commonService.GetUtcDateTime();
                }           
                    _innostaxDb.SaveChangesWithErrors();       
                var caseId = GetCaseIdForDiscussionMessage(existingDiscussionMessage.DiscussionId);
                if (caseId != null)
                {
                    if (toBeArchived)
                    {
                        _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_ARCHIVED,null);
                        _discussionMessageFileAssociationService.RemoveDiscussionMessageFileAssociation(id);
                       _discussionMessageTaggedUserService.DeleteDiscussionMessageTaggedUsers(existingDiscussionMessage.Id);
                    }
                    else
                    {
                        _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_DISCUSSION_MESSAGE_UNARCHIVED,null);
                    }
                }
            }
        }

        public DiscussionMessage GetRecentDiscussionMessageByDiscussionId(Guid discussionId)
        {
            var result = new DiscussionMessage();
            var recentDiscussionMessage = _innostaxDb.DiscussionMessages.Include(y=>y.Discussion).OrderByDescending(x => x.LastEditedDateTime).FirstOrDefault(x => x.DiscussionId == discussionId && x.IsDeleted == false);
            if (recentDiscussionMessage != null)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, discussionId);
                result = Mapper.Map<Models.DiscussionMessage>(recentDiscussionMessage);
            }
            return result;
        }

        internal Guid GetCaseIdForDiscussionMessage(Guid discussionId)
        {
            var discussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionId);
            if (discussion != null)
            {
                return discussion.CaseId;
            }
            return new Guid();        
        }
    }
}
