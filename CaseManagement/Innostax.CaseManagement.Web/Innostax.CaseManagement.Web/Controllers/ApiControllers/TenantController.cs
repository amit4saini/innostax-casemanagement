﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.DependencyResolution;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class TenantController : ApiController
    {
        private readonly ITenantService _tenantService;    
        public TenantController(ITenantService tenantService)
        {       
            _tenantService = tenantService;
        }

        [HttpGet]
        [Route("api/Tenants")]
        public PageResult<Models.Tenant> GetAllTenants(ODataQueryOptions<Models.Tenant> options)
        {                            
            return _tenantService.GetAllTenants(options, Request);
        }

        [HttpGet]
        [Route("api/Tenant/{tenantId}")]
        public Models.Tenant Get(Guid tenantId)
        {
            return _tenantService.GetTenantDetailsByTenantId(tenantId);
        }

        public void Post(Models.Tenant tenantDetails)
        {
            _tenantService.Save(tenantDetails);
        }

        [Route("api/Tenant/{tenantId}")]
        public void Put(Guid tenantId, Models.Tenant tenantDetails)
        {
            tenantDetails.TenantId = tenantId;
            _tenantService.Save(tenantDetails);
        }

        [Route("api/Tenant/{tenantId}")]
        public void Delete(Guid tenantId)
        {
            _tenantService.Delete(tenantId);
        }
    }
}
