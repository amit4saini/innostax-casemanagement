﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class IndustryList
    {
        public List<string> Industry()
        {
            var Industries = new List<string>{
                "Distributor",
                "Government/Political Entity",
                "Manufacturer",
                "Services",
                "Supplier",
                "Trade"
                 };
            return Industries;
        }
    }
}
