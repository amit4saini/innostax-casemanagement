namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addingTaskMessageFileAssociationTableToDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskCommentFileAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TaskCommentId = c.Guid(nullable: false),
                        FileId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskComments", t => t.TaskCommentId)
                .ForeignKey("dbo.UploadedFiles", t => t.FileId)
                .Index(t => t.TaskCommentId)
                .Index(t => t.FileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskCommentFileAssociations", "FileId", "dbo.UploadedFiles");
            DropForeignKey("dbo.TaskCommentFileAssociations", "TaskCommentId", "dbo.TaskComments");
            DropIndex("dbo.TaskCommentFileAssociations", new[] { "FileId" });
            DropIndex("dbo.TaskCommentFileAssociations", new[] { "TaskCommentId" });
            DropTable("dbo.TaskCommentFileAssociations");
        }
    }
}
