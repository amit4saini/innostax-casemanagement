﻿using System;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Models
{
    public class TaskActivity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Account.User User { get; set; }
        public TaskActivityAction Action { get; set; }
        public DateTime? CreationDatetime { get; set; }
        public Guid TaskId { get; set; }
        public string NewValue { get; set; }
    }
}
