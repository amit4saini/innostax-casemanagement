﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class TaskCommentTaggedUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public UserManagement.User User { get; set; }
        public Guid TaskCommentId { get; set; }
        [ForeignKey("TaskCommentId")]
        public TaskComment TaskComment { get; set; }
    }
}
