﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Query;
using System.Web.Http.OData;
using System.Net.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseHistoryServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CaseHistoryService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandler;
        private Mock<ICommonService> _commonService;
        private List<Permission> userPermission;
        private Mock<IAzureFileManager> _azureFileManager;
        ODataQueryOptions<Models.CaseHistory> options;
        Guid userId1;
        Guid userId2;
        [TestInitialize]
        public void TestInitialize()
        {
            userId1 = Guid.NewGuid();
            userId2 = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandler = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            _azureFileManager = new Mock<IAzureFileManager>();
            sut = new CaseHistoryService(innostaxDbMock.Object, _userService.Object, _commonService.Object,_azureFileManager.Object,_errorHandler);          
            userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_HISTORY_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_HISTORY_READ_OWN, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
           // _commonService.Setup(x => x.ValidateCaseAccess(It.IsAny<Guid>())).Returns(true);
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.CaseHistory>("CaseHistory");
            options = new ODataQueryOptions<Models.CaseHistory>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.CaseHistory)), new HttpRequestMessage());
        }

        private List<Innostax.Common.Database.Models.CaseHistory> GetCaseHistory()
        {
            return new List<Innostax.Common.Database.Models.CaseHistory>
            {
                new Innostax.Common.Database.Models.CaseHistory {Action = Innostax.Common.Database.Enums.Action.CASE_ADDED, CaseId = new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), UserId =userId1 },
                new Innostax.Common.Database.Models.CaseHistory {Action = Innostax.Common.Database.Enums.Action.CASE_ASSIGNED, CaseId = new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), UserId = userId2 },

            };
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1 },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId2 }
                            };
            return dummyUser;
        }

        [TestMethod]
        public void Given_Calling_GetHistoryDetailsForACase_WhenCaseHistoryIsPresent_And_HavePermissions_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(GetCaseHistory());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            var actual = sut.GetHistoryDetailsForACase(options,new HttpRequestMessage(),new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"),new HistoryFilter());
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task Given_Calling_GetHistoryDetailsForACase_WhenCaseHistoryIsNotPresent_And_HavePermissions_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(new List<Innostax.Common.Database.Models.CaseHistory> { });
            var actual = await sut.GetHistoryDetailsForACase(options, new HttpRequestMessage(), new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), new HistoryFilter());
            Assert.AreEqual(actual.Count, 0);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task Given_Calling_GetHistoryDetailsForACase_WhenPermissionsAreNotEnough_MappedResultReturn()
        {
            var dummyCaseHistories = GetCaseHistory();
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(dummyCaseHistories);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            userPermission = new List<Permission> { };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            var actual = await sut.GetHistoryDetailsForACase(options, new HttpRequestMessage(), new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), new HistoryFilter());
            Assert.AreEqual(actual.Count, 0);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task Given_Calling_GetHistoryDetailsForACase_WhenPermissionIsToReadOwnCaseHistory_MappedResultReturn()
        {
            var dummyCaseHistories = GetCaseHistory();
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(dummyCaseHistories);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            userPermission = new List<Permission> {Permission.CASE_HISTORY_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _userService.SetupGet(x => x.UserId).Returns(userId1);
            var actual = await sut.GetHistoryDetailsForACase(options, new HttpRequestMessage(), new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), new HistoryFilter());
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.Count,0);
        }

        [TestMethod]
        public void Given_Calling_GetCaseHistoryDetailsForAUser_WhenCaseHistoryForThatUserIsPresent_And_HavePermissions_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(GetCaseHistory());
            var actual = sut.GetCaseHistoryDetailsForAUser(new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), userId1);
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.Count, 1);
        }

        [TestMethod]
        public void Given_Calling_GetCaseHistoryDetailsForAUser_WhenCaseHistoryForThatUserIsPresent_But_DoesNotHavePermissions_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(GetCaseHistory());
            userPermission = new List<Permission> { };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            var actual = sut.GetCaseHistoryDetailsForAUser(new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), userId1);
            Assert.AreEqual(actual.Count, 0);
        }

        [TestMethod]
        public void Given_Calling_GetCaseHistoryDetailsForAUser_WhenCaseHistoryForThatUserIsNotPresent_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.CaseHistory).ReturnsDbSet(new List<Innostax.Common.Database.Models.CaseHistory> { });
            var actual = sut.GetCaseHistoryDetailsForAUser(new Guid("829FF9B7-AC15-E611-8B84-FCAA14866CAF"), new Guid("7A62F038-AC15-E611-8B84-FCAA14866CAF"));
            Assert.AreEqual(actual.Count, 0);
        }


    }
}
