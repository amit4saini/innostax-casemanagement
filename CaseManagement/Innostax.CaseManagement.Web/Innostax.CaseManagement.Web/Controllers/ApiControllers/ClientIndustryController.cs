﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class ClientIndustryController : ApiController
    {
        private readonly IClientIndustryService _clientService;
        public ClientIndustryController(IClientIndustryService clientService)
        {
            _clientService = clientService;
        }

        [System.Web.Mvc.HttpGet]
        [Route("api/ClientIndustries")]
        public List<ClientIndustry> Get()
        {
            return _clientService.GetAllClientIndustry();
        }
    }
}
