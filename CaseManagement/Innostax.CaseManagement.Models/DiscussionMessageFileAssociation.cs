﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionMessageFileAssociation
    {
        public Guid DiscussionMessageId { get; set; }
        public List<FileModel> Files { get; set; }
    }
}
