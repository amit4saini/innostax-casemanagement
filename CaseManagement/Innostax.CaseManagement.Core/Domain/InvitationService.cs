﻿using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Models.Account;
using Innostax.CaseManagement.Models.EmailModels;
using Innostax.Common.Database.Database;
using System.Linq;
using System;
using AutoMapper;
using System.Data.Entity.Migrations;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IInvitationService
    {
        void SendInvitationToUser(User user, string link);
        void CreateUserAssociation(InvitationModel invitation);
    }

    public class InvitationService : IInvitationService
    {
        public IInnostaxDb _innostaxDb;
        private IMailSenderService _mailSenderService;
        private IUserService _userService;
        private IICIUserCaseAssociationService _iciUserCaseAssociationService;
        private IParticipantCaseAssociationService _participantCaseAssociationService;
        private INFCUserCaseAssociationService _nfcCaseAssociationService;

        public InvitationService(IInnostaxDb innostaxDb, IMailSenderService mailSenderService,
            IUserService userService, 
            IICIUserCaseAssociationService iciUserCaseAssociationService, 
            IParticipantCaseAssociationService participantCaseAssociationService,
            INFCUserCaseAssociationService nfcCaseAssociationService)
        {
            _innostaxDb = innostaxDb;
            _mailSenderService = mailSenderService;
            _userService = userService;
            _iciUserCaseAssociationService = iciUserCaseAssociationService;
            _participantCaseAssociationService = participantCaseAssociationService;
            _nfcCaseAssociationService = nfcCaseAssociationService;
        }

        public void SendInvitationToUser(User user,string link)
        {
            var inviteUserEmailModel = new InviteUserEmailModel();
            inviteUserEmailModel.InviteeName = user.FirstName + " " + user.LastName;
            inviteUserEmailModel.InviteeEmail = user.Email;
            inviteUserEmailModel.link = link;
            var inviterDetails = _innostaxDb.Users.FirstOrDefault(x => x.Id == _userService.UserId);
            inviteUserEmailModel.InviterName = inviterDetails.FirstName + " " + inviterDetails.LastName;
            _mailSenderService.SendInviteUserDetails(inviteUserEmailModel);
        }

        public void CreateUserAssociation(InvitationModel invitation)
        {
            if (invitation.User.Role.Name == Roles.Client_Participant.ToString() && invitation.ParticipantUser.CaseId != Guid.Empty)
            {
                invitation.ParticipantUser.UserId = invitation.User.Id;
                _participantCaseAssociationService.Save(invitation.ParticipantUser);
            }
            else if (invitation.User.Role.Name == Roles.In_Country_Investigator.ToString() && invitation.ICICaseUser.CaseId != Guid.Empty)
            {
                invitation.ICICaseUser.ICIUserId = invitation.User.Id;
                if(invitation.ICICaseUser.User.UserDetails != null)
                {
                    invitation.ICICaseUser.User.UserDetails.Country = null;
                    invitation.ICICaseUser.User.UserDetails.UserId = invitation.User.Id;
                    var newUserDetail = Mapper.Map<Common.Database.Models.UserDetail>(invitation.ICICaseUser.User.UserDetails);
                    _innostaxDb.UserDetails.AddOrUpdate(newUserDetail);
                    _innostaxDb.SaveChangesWithErrors();
                }
                _iciUserCaseAssociationService.Save(invitation.ICICaseUser);
            }
            else if (invitation.User.Role.Name == Roles.NFC.ToString() && invitation.NFCCaseUser.CaseId != Guid.Empty)
            {
                invitation.NFCCaseUser.NFCUserId = invitation.User.Id;
                if (invitation.NFCCaseUser.User.UserDetails != null)
                {
                    invitation.NFCCaseUser.User.UserDetails.Country = null;
                    invitation.NFCCaseUser.User.UserDetails.UserId = invitation.User.Id;
                    var newUserDetail = Mapper.Map<Common.Database.Models.UserDetail>(invitation.NFCCaseUser.User.UserDetails);
                    _innostaxDb.UserDetails.AddOrUpdate(newUserDetail);
                    _innostaxDb.SaveChangesWithErrors();
                }
                _nfcCaseAssociationService.Save(invitation.NFCCaseUser);
            }
        }
    }
}
