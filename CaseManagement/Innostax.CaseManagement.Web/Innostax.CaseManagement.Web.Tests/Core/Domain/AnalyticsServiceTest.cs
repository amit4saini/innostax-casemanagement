﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData;
using System.Linq;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
   public class AnalyticsServiceTest
    {

        private Mock<InnostaxDb> innostaxDbMock;
        private AnalyticsService sut;
        private Mock<IUserService> _userService;  
        private Mock<ICaseService> _caseService;
        private Mock<ITaskService> _taskService;
        private Mock<ICommonService> _commonService;
        Guid caseId1 = Guid.NewGuid();
        Guid caseId2 = Guid.NewGuid();
        Guid clientId1 = Guid.NewGuid();
        Guid clientId2 = Guid.NewGuid();
        Guid userId1;
        Guid userId2;
        Guid reportId1;
        List<Guid> listOfCaseId;
        List<Guid> listOfClientId;
        Guid caseExpenseId;
        ODataQueryOptions<Models.Case> caseAnalyticsOptions;
        ODataQueryOptions<Models.Client> clientAnalyticsOptions;
        IEnumerable<Models.Task> TaskReturn()
        {
            yield return new Models.Task() { CaseId = caseId1  };
            yield return new Models.Task() { CaseId = caseId2 };
            yield return new Models.Task() { CaseId = caseId1 };
        }
        IEnumerable<Models.Task> EmptyEnumerableReturn()
        {       
                yield return new Models.Task() { };                      
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            userId1 = Guid.NewGuid();
            userId2 = Guid.NewGuid();
            reportId1 = Guid.NewGuid();
            caseExpenseId = System.Guid.NewGuid();
            listOfCaseId = new List<Guid> { caseId1, caseId2 };
            listOfClientId = new List<Guid> { clientId1, clientId2 };
            innostaxDbMock = new Mock<InnostaxDb>();
            _caseService = new Mock<ICaseService>();
            _userService = new Mock<IUserService>();              
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _taskService = new Mock<ITaskService>();
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Task>("Task");
            ODataQueryOptions<Models.Task> taskOptions = new ODataQueryOptions<Models.Task>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Task)), new HttpRequestMessage());
            _taskService.Setup(x => x.GetAllTasksByCaseId(It.IsAny<ODataQueryOptions<Models.Task>>(),It.IsAny<HttpRequestMessage>(),It.IsAny<Guid>())).Returns(new Models.TaskPageResult() { Result = new PageResult<Models.Task>(TaskReturn(), new Uri("http://www.google.com"), 12) });    
            ODataModelBuilder caseAnalyticsModelBuilder = new ODataConventionModelBuilder();
            caseAnalyticsModelBuilder.EntitySet<Models.Case>("Case");
            caseAnalyticsOptions = new ODataQueryOptions<Models.Case>(new ODataQueryContext(caseAnalyticsModelBuilder.GetEdmModel(), typeof(Models.Case)), new HttpRequestMessage());
            ODataModelBuilder clientAnalyticsModelBuilder = new ODataConventionModelBuilder();
            clientAnalyticsModelBuilder.EntitySet<Models.ClientAnalyticsModel>("ClientAnalyticsModel");
            clientAnalyticsOptions = new ODataQueryOptions<Models.Client>(new ODataQueryContext(clientAnalyticsModelBuilder.GetEdmModel(), typeof(Models.ClientAnalyticsModel)), new HttpRequestMessage());
            _commonService = new Mock<ICommonService>();
            sut = new AnalyticsService(
                innostaxDbMock.Object,
                 _userService.Object, _caseService.Object, _taskService.Object,_commonService.Object
                );           
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _caseService.Setup(x => x.CalculateCaseMargin(It.IsAny<Models.Case>())).Returns(new Models.Case { Margin = 20 });
            _userService.Setup(x => x.GetUser(It.IsAny<Guid>())).Returns(new Models.Result<Models.Account.User> { Data = new Models.Account.User { UserName = "test" } });
        }
       
        private Industry GetDummyIndustry()
        {
            var dummyIndustry = new Industry { Id = 1, Name = "DummyIndustry" };
            return dummyIndustry;
        }

        private IndustrySubCategory GetDummyIndustrySubCategory()
        {
            var dummyIndustrySubCategory = new IndustrySubCategory { Id = 1, Name = "DummyIndustrySubCategory" };
            return dummyIndustrySubCategory;
        }

        private Client GetADummyClient()
        {
            var dummyClient = new Client { ClientName = "test" };
            return dummyClient;
        }

        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=caseId1,             
                    ClientId=clientId1,
                    CreatedBy=userId1,
                    Client = GetADummyClient(),
                    IsDeleted=false,                
                    AdditionalSalesRepNotes= "notes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } },
                    AssignedToUser = new Innostax.Common.Database.Models.UserManagement.User {Id= userId1 }
                },
                new Case
                {
                    CaseId=Guid.NewGuid(),                   
                    ClientId=clientId2,
                    CreatedBy=userId1,
                    Client = GetADummyClient(),
                    AdditionalSalesRepNotes= "AdditinalSalesRepNotes",
                    ClientPO="clientPO",
                    ClientProvidedInformation = "hgdjmd",
                    HoldReason="holdReason",
                    NickName="matter",
                    StateOfEmployment = new State {Country = new Country {CountryName= "india" } },
                    IsDeleted=false,
                    AssignedToUser = new Innostax.Common.Database.Models.UserManagement.User {Id = userId1 }
                }
            };
            return dummyCase;
        }

        private List<Client> GetDummyClients()
        {

            var dummyClients = new List<Client>
            {
                new Client
                {
                    ClientId = clientId1,
                    SalesRepresentativeId = userId1,
                    ClientName = "abcd",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "first", LastName = "last" },
                    Notes = "notes",
                    Industry = GetDummyIndustry(),
                    IndustrySubCategory = GetDummyIndustrySubCategory(),
                    SalesRepresentativeEmail= "abc@afg.com",
                    IsDeleted =false ,
                },
                new Client
                {
                ClientId = clientId2,
                SalesRepresentativeId = userId1,
                ClientName = "abcdbf",
                SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User { FirstName = "first", LastName = "last" },
                Notes = "Mynotes",
                Industry = GetDummyIndustry(),
                IndustrySubCategory = GetDummyIndustrySubCategory(),
                SalesRepresentativeEmail= "asfbc@afg.com",
                IsDeleted =false ,
                }
            };
            return dummyClients;
        }

        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1, FirstName="first", LastName="last" },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1, FirstName="1st", LastName="lst" }
                            };
            return dummyUser;
        }


        private List<CaseReportAssociation> GetDummyCaseReportAssociation()
        {

            var dummyAssociation = new List<CaseReportAssociation>
            {
                new CaseReportAssociation {
                CaseId = caseId1,
                ReportId = reportId1,
                IsDeleted = false,
                IsPrimaryReport=true,
                Country = new Country {CountryId = 1 }
                }

            };
            return dummyAssociation;
        }

        private List<Task> GetDummyTasks()
        {
            var dummyTask = new List<Task>
            {
                new Task {TaskId = Guid.NewGuid(),Title= "task",IsDeleted=false,CaseId=caseId1,AssignedTo=userId1,DueDate=DateTime.Now.AddDays(1)}
            };
            return dummyTask;
        }

        private List<Task> GetDummyDelayedTasks()
        {
            var dummyTask = new List<Task>
            {
                new Task {TaskId = Guid.NewGuid(),Title= "task",IsDeleted=false,CaseId=caseId1,DueDate=DateTime.Now.AddDays(-1),AssignedTo=userId1}
            };
            return dummyTask;
        }

        private List<TimeSheet> GetDummyTimeSheets()
        {
            var dummyTimeSheet = new List<TimeSheet>
            {
                new TimeSheet {IsDeleted=false,CaseId=caseId1,CreatedBy=userId1,UserId=userId1}
            };
            return dummyTimeSheet;
        }

        private List<CaseExpense> GetDummyCaseExpense()
        {
            var dummyCaseExpense = new List<CaseExpense>
            {
                new CaseExpense {IsDeleted=false,CreatedBy=userId1,CaseId=caseId1}
            };
            return dummyCaseExpense;
        }

        [TestMethod]
        public void GivenCalling_GetCaseAnalytics_WhenCaseIsFound_ThenResultIsNotNull()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.CaseReportAssociations).ReturnsDbSet(GetDummyCaseReportAssociation());
          var result =  sut.GetCaseAnalytics(caseAnalyticsOptions,new HttpRequestMessage(),null,null);
            Assert.IsNotNull(result.Items);
        }


        [TestMethod]
        public void GivenCalling_GetCaseAnalytics_WhenCaseIsNotFound_ThenResultCountIsZero()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(new List<Case> { });
            _taskService.Setup(x => x.GetAllTasksByCaseId(It.IsAny<ODataQueryOptions<Models.Task>>(), It.IsAny<HttpRequestMessage>(), It.IsAny<Guid>())).Returns(new Models.TaskPageResult() { Result = new PageResult<Models.Task>(TaskReturn(), new Uri("http://www.google.com"), 12) });
            innostaxDbMock.Setup(x => x.CaseReportAssociations).ReturnsDbSet(new List<CaseReportAssociation> { });
            var result = sut.GetCaseAnalytics(caseAnalyticsOptions, new HttpRequestMessage(),null,null);
            Assert.AreEqual(result.Items.AsEnumerable().ToList().Count,0);
            
        }


        [TestMethod]
        public void GivenCalling_GetClientAnalytics_WhenClientIsFound_ThenResultIsNotNull()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(x => x.CaseReportAssociations).ReturnsDbSet(GetDummyCaseReportAssociation());
            var result = sut.GetClientAnalytics(clientAnalyticsOptions, new HttpRequestMessage(),null,null);
            Assert.IsNotNull(result.Items);
        }


        [TestMethod]
        public void GivenCalling_GetClientAnalytics_WhenClientIsNotFound_ThenResultCountIsZero()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(x => x.CaseReportAssociations).ReturnsDbSet(GetDummyCaseReportAssociation());
            var result = sut.GetClientAnalytics(clientAnalyticsOptions, new HttpRequestMessage(), null,null);
            Assert.AreEqual(result.Items.AsEnumerable().ToList().Count, 0);
        }

        [TestMethod]
        public void GivenCalling_GetUserAnalytics_WhenUserIdIsFound_ThenResultIsNotNull()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(userId1,null);
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void GivenCalling_GetUserAnalytics_WhenUserIdIsNotFound_ThenResultIsNull()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(Guid.NewGuid(),null);
            Assert.AreEqual(result.ListOfCaseExpense.Count(),0);
        }

        [TestMethod]
        public void GivenCalling_GetUserAnalytics_WhenUserDoesNotHaveTaskDelayed_ThenCountOfTaskDelayedIsZero()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(userId1, null);
            Assert.AreEqual(result.ListOfTaskDelayed.Count(), 0);
        }

        [TestMethod]
        public void GivenCalling_GetUserAnalytics_WhenUserHaveTaskDelayed_ThenCountOfTaskDelayedIsNotZero()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyDelayedTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(userId1, null);
            Assert.AreEqual(result.ListOfTaskDelayed.Count(), 1);
        }

        [TestMethod]
        public void GivenCalling_GetUserAnalyticsForACase_WhenCaseIdIsFound()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(userId1, caseId1);
            Assert.AreEqual(result.ListOfCaseExpense.Count(), 1);
        }


        [TestMethod]
        public void GivenCalling_GetUserAnalyticsForACase_WhenCaseIdIsNotFound()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(x => x.Tasks).ReturnsDbSet(GetDummyTasks());
            innostaxDbMock.Setup(x => x.TimeSheets).ReturnsDbSet(GetDummyTimeSheets());
            innostaxDbMock.Setup(x => x.CaseExpenses).ReturnsDbSet(GetDummyCaseExpense());
            var result = sut.GetUserAnalytics(userId2, caseId2);
            Assert.AreEqual(result.ListOfCaseExpense.Count(), 0);
        }
    }
}
