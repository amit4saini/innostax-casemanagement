﻿using System;
using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Database;
using AutoMapper;
using System.Data.Entity;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IReportService
    {
        List<Models.Report> GetReports();
        Models.Report GetReportById(Guid id);
        List<Models.Report> GetCaseAssociatedReports(Guid caseId);
    }

    public class ReportService : IReportService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;

        public ReportService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, ICommonService commonService, IUserService userService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _commonService = commonService;
            _userService = userService;
        }

        public List<Models.Report> GetReports()
        {
            var result = new List<Models.Report>();
            var allExistingReports = _innostaxDb.Reports.ToList();
            if (allExistingReports == null || allExistingReports.Count <= 0)
            {
                string reportNotFoundError = Common.Constants.ErrorMessages.Common.ReportError.REPORT_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(reportNotFoundError);
            }
            else
            {
                result = new List<Models.Report>();
                foreach (var existingReport in allExistingReports)
                {
                    result.Add(Mapper.Map<Models.Report>(existingReport));
                }
            }
            return result;
        }

        public Models.Report GetReportById(Guid id)
        {
            var result = new Models.Report();
            var existingReport = _innostaxDb.Reports.FirstOrDefault(x => x.ReportId == id);
            if (existingReport == null)
            {
                string reportNotFoundError = Common.Constants.ErrorMessages.Common.ReportError.REPORT_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(reportNotFoundError);
            }
            else
            {
                result = new Models.Report();
                result = Mapper.Map<Models.Report>(existingReport);
            }
            return result;
        }

        public List<Models.Report> GetCaseAssociatedReports(Guid caseId)
        {
            var result = new List<Models.Report>();
            _commonService.ValidateAccess(Validate.CASE, Permission.CASE_READ_ALL, caseId);
            var allCaseAssociatedReports = _innostaxDb.CaseReportAssociations.Include(x => x.Report).Include(x => x.Country).Include(x => x.State)
                    .Where(x => x.CaseId == caseId && x.IsDeleted == false).ToList();
                result = new List<Models.Report>();
                if (allCaseAssociatedReports.Count > 0)
                {
                    foreach (var report in allCaseAssociatedReports)
                    {
                        var mappedReport = Mapper.Map<Models.Report>(report.Report);
                        if (report.Country == null)
                        {
                            mappedReport.CountryId = 0;
                            mappedReport.CountryName = null;
                        }
                        else
                        {
                            mappedReport.CountryId = report.Country.CountryId;
                            mappedReport.CountryName = report.Country.CountryName;
                        }
                    if (report.State != null)
                    {
                        mappedReport.StateId = report.State.StateId;
                        mappedReport.StateName = report.State.StateName;
                    }
                        mappedReport.IsPrimary = report.IsPrimaryReport;
                        mappedReport.OtherReportTypeName = report.OtherReportTypeName;
                        mappedReport.IsOtherReportType = report.IsOtherReportType;
                        result.Add(mappedReport);
                    }
                }            
            return result;
        }
    }
}
