﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseFileAssociationService
    {
        void Save(Case caseData);
        void ArchiveCaseFileAssociation(Guid caseId, bool isArchive);
        List<Models.FileModel> GetCaseAssociatedFiles(Guid caseId);
    }

    public class CaseFileAssociationService : ICaseFileAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;

        public CaseFileAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, ICaseHistoryService caseHistoryService, IUserService userService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _caseHistoryService = caseHistoryService;
            _userService = userService;
            _commonService = commonService;
        }

        public void Save(Case caseData)
        {
            _commonService.ValidateAccess(Common.Database.Enums.Validate.CASE,Common.Database.Enums.Permission.CASE_UPDATE_ALL, caseData.CaseId);
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseData.CaseId);
            if (existingCase != null)
            {
                var existingCaseFileAssociations = _innostaxDb.CaseFileAssociations.Include(y=>y.UploadedFile).Where(x => x.CaseId == caseData.CaseId && x.IsDeleted != true).ToList();
                if (existingCaseFileAssociations.Count > 0)
                {
                    RemoveCaseFileAssociation(existingCaseFileAssociations, caseData);
                }

                if (caseData.Files.Count > 0)
                {
                    foreach (var eachFile in caseData.Files)
                    {
                        if (_innostaxDb.CaseFileAssociations.FirstOrDefault(x => x.FileId == eachFile.FileId && x.CaseId == caseData.CaseId && x.IsDeleted != true) == null)
                        {
                            SaveCaseAssociatedFiles(eachFile, caseData);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.CASE_FILE_ADDED, eachFile.FileName);
                            _caseHistoryService.SaveCaseHistory(caseData.CaseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                        }
                    }
                }             
                    _innostaxDb.SaveChangesWithErrors();           
            }
            else
            {
                var invalidCaseIdError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(invalidCaseIdError);
            }
        }

        private void RemoveCaseFileAssociation(List<Common.Database.Models.CaseFileAssociation> listOfCaseFileAssociation,Case caseData)
        {
            foreach (var eachAssociation in listOfCaseFileAssociation)
            {
                if (caseData.Files.FirstOrDefault(x => x.FileId == eachAssociation.FileId) == null)
                {
                    var fileName = eachAssociation.UploadedFile.FileName;
                    _innostaxDb.CaseFileAssociations.Remove(eachAssociation);
                    var historyMessage = string.Format(Common.Constants.CaseHistory.CASE_FILE_DELETED, fileName);
                    _caseHistoryService.SaveCaseHistory(eachAssociation.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                    _innostaxDb.SaveChangesWithErrors();
                }
            }
        }      

        public void ArchiveCaseFileAssociation(Guid caseId,bool isArchive)
        {
            _commonService.ValidateAccess(Common.Database.Enums.Validate.CASE,Common.Database.Enums.Permission.CASE_DELETE_ALL, caseId);
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId);
            if (existingCase != null)
            {
                var caseFileAssociations = _innostaxDb.CaseFileAssociations.Include(y=>y.UploadedFile).Where(x => x.CaseId == caseId && x.IsDeleted != true).ToList();
                foreach (var caseFileAssociation in caseFileAssociations)
                {
                    caseFileAssociation.IsDeleted = isArchive;
                    _innostaxDb.SaveChangesWithErrors();
                    if (isArchive)
                    {
                        var historyMessage = string.Format(Common.Constants.CaseHistory.CASE_FILE_DELETED, caseFileAssociation.UploadedFile.FileName);
                        _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.FILE_DELETED,historyMessage);
                    }
                    else
                    {
                        var historyMessage = string.Format(Common.Constants.CaseHistory.CASE_FILE_ADDED, caseFileAssociation.UploadedFile.FileName);
                        _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                    }
                }             
            }
            else
            {
                var invalidCaseIdError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(invalidCaseIdError);
            }
        }

        public List<Models.FileModel> GetCaseAssociatedFiles(Guid caseId)
        {
            var result = new List<Models.FileModel>();
            result = new List<Models.FileModel>();
            _commonService.ValidateAccess(Common.Database.Enums.Validate.CASE,Common.Database.Enums.Permission.CASE_READ_ALL, caseId);
            var currentCaseFileAssociations = _innostaxDb.CaseFileAssociations.Where(x => x.CaseId == caseId && x.IsDeleted != true).ToList();
            foreach (var eachAssociation in currentCaseFileAssociations)
            {
                var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == eachAssociation.FileId);
                var mappedFile = Mapper.Map<CaseManagement.Models.FileModel>(existingFile);
                mappedFile.FileUrl = existingFile.FileUrl;
                var associatedUser = _userService.GetUser(existingFile.UserId).Data;
                mappedFile.UserName = associatedUser.FirstName + " " + associatedUser.LastName;
                mappedFile.FileName = existingFile.FileName;
                result.Add(mappedFile);
            }
            return result.OrderByDescending(x=>x.CreateDateTime).ToList();
        }

        internal void SaveCaseAssociatedFiles(FileModel file, Case caseData)
        {
            var result = new Result();
            var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId);
            if (existingFile == null)
            {
                string fileNotFoundError = Common.Constants.ErrorMessages.Common.FileUploadError.NO_FILE_EXISTS;
                _errorHandlerService.ThrowResourceNotFoundError(fileNotFoundError);
            }
            var mappedCaseFileAssociation = new Common.Database.Models.CaseFileAssociation();
            mappedCaseFileAssociation.CaseId = caseData.CaseId;
            mappedCaseFileAssociation.FileId = file.FileId;
            var newCaseFileAssociation = _innostaxDb.CaseFileAssociations;
            newCaseFileAssociation.Add(mappedCaseFileAssociation);        
                _innostaxDb.SaveChangesWithErrors();      
        }
    }
}
