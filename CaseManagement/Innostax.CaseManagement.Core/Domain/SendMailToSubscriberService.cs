﻿using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
namespace Innostax.CaseManagement.Core.Domain
{
    public interface ISendMailToSubscriberService
    {      
        void SendMail(Guid caseId, string status);
    }

   public class SendMailToSubscriberService: ISendMailToSubscriberService
    {
        private IMailSenderService _mailSenderService;
        private IInnostaxDb _innostaxDb;
        public SendMailToSubscriberService(IMailSenderService mailSenderService, IInnostaxDb innostaxDb)
        {
            _mailSenderService = mailSenderService;
            _innostaxDb = innostaxDb;
        }

        internal List<string> GetEmailIdOfSubscriber(Guid caseId)
        {
            var listOfUserAssociatedWithCase = _innostaxDb.Cases.Include(y=>y.CreatedByUser).FirstOrDefault(x => x.CaseId == caseId && x.IsDeleted != true);
            if (listOfUserAssociatedWithCase == null)
            {
                return new List<string> { };
            }
            List<string> users=new List<string>();
            users.Add(listOfUserAssociatedWithCase.CreatedByUser.Email);   
            return users;
        }

        public string GetCaseNumberOfCase(Guid caseId)
        {
            var caseDetail = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId && x.IsDeleted != true);
            if (caseDetail != null)
            {
                return caseDetail.CaseNumber;
            }

            return "";
        }

        public void SendMail(Guid caseId, string status)
        {
            var userEmailId = GetEmailIdOfSubscriber(caseId);
            var caseNumber = GetCaseNumberOfCase(caseId);
            _mailSenderService.SendNotificationToSubscriber(caseNumber,status,userEmailId);
        }
    }
}
