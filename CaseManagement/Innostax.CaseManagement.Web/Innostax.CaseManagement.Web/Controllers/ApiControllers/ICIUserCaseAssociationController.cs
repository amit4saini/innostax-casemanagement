﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class ICIUserCaseAssociationController : ApiController
    {
        private readonly IICIUserCaseAssociationService _iciUserCaseAssociationService;
        public ICIUserCaseAssociationController(IICIUserCaseAssociationService iciUserCaseAssociationService)
        {
            _iciUserCaseAssociationService = iciUserCaseAssociationService;
        }

        [HttpGet]
        public List<ICIUserCaseAssociation> Get(Guid id)
        {
            return _iciUserCaseAssociationService.GetICICaseUsersByCaseId(id);
        }

        [HttpGet]
        [Route("api/ICIUserCaseAssociation/{id}/Cases")]
        public List<Case> GetCasesByICIUserId(Guid id)
        {
            return _iciUserCaseAssociationService.GetCasesByICIUserId(id);
        }

        [HttpPost]   
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]
        public void Post(ICIUserCaseAssociation iciUserCaseAssociationData)
        {
            _iciUserCaseAssociationService.Save(iciUserCaseAssociationData);
        }

        [HttpPut]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_OWN, Permission.CASE_UPDATE_ALL })]  
        public void Put(int id, ICIUserCaseAssociation iciUserCaseAssociationData)
        {
            iciUserCaseAssociationData.Id = id;
            _iciUserCaseAssociationService.Save(iciUserCaseAssociationData);
        }
    }
}
