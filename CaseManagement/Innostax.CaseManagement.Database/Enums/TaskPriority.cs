namespace Innostax.Common.Database.Enums
{
    public enum TaskPriority
    {
        REGULAR,
        LOW,
        HIGH
    }
}
