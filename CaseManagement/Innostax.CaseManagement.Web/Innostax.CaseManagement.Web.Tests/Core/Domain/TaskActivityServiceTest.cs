﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskActivityServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private TaskActivityService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService errorHandlerService;
        private Mock<ICommonService> _commonService;
        Guid userId;
        Guid taskId;

        [TestInitialize]
        public void TestInitialize()
        {
            userId = Guid.NewGuid();
            taskId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _commonService = new Mock<ICommonService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            errorHandlerService = new ErrorHandlerService();
            sut = new TaskActivityService(innostaxDbMock.Object, _userService.Object, errorHandlerService, _commonService.Object);           
        }
        private List<TaskActivity> GetDummyTaskActivity()
        {
            var dummyTaskActivity = new List<TaskActivity>
            {
                new TaskActivity
                {
                    Id=Guid.NewGuid(),
                    UserId=userId,
                    CreationDatetime=new DateTime(),
                    Action = 0,
                    TaskId=taskId
                },
                new TaskActivity
                {
                     Id=Guid.NewGuid(),
                    UserId=userId,
                    CreationDatetime=new DateTime(),
                    Action = 0,
                    TaskId=taskId
                }
            };
            return dummyTaskActivity;
        }
        [TestMethod]
        public void GivenCalling_SaveTaskActivity_WhenSavingTheTaskActivity()
        {
            try
            {
                var dummyTaskActivities = new List<TaskActivity>();
                innostaxDbMock.Setup(c => c.TaskActivities).ReturnsDbSet(dummyTaskActivities);
                sut.SaveTaskActivity(taskId, Innostax.Common.Database.Enums.TaskActivityAction.TASK_ASSIGNED_TO,Guid.NewGuid().ToString());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetAllTaskActivityForATAsk_WhenTasksAreNotPresentForTaskId_ThenReturnEmptyList()
        {
            var dummyTaskActivities = GetDummyTaskActivity();
            innostaxDbMock.Setup(c => c.TaskActivities).ReturnsDbSet(dummyTaskActivities);
            var actual = sut.GetTaskActivityById(Guid.NewGuid());
            Assert.AreEqual(actual.Count, 0);
        }

        [TestMethod]
        public void GivenCalling_GetAllTaskActivityForATAsk_WhenTasksArePresentForTaskId_ThenReturnEmptyList()
        {
            var dummyTaskActivities = GetDummyTaskActivity();
            innostaxDbMock.Setup(c => c.TaskActivities).ReturnsDbSet(dummyTaskActivities);
            var actual = sut.GetTaskActivityById(taskId);
            Assert.AreEqual(actual.Count, 2);
        }
    }
}
