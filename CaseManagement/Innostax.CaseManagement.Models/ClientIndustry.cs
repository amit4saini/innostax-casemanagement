﻿namespace Innostax.CaseManagement.Models
{
    public class ClientIndustry
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
