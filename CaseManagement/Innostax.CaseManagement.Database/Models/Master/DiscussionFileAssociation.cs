﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class DiscussionFileAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid DiscussionId { get; set; }
        [ForeignKey("DiscussionId")]
        public Discussion Discussion { get; set; }
        public Guid FileId { get; set; }

        [ForeignKey("FileId")]
        public UploadedFile UploadedFile { get; set; }
        public bool IsDeleted { get; set; }
    }
}
