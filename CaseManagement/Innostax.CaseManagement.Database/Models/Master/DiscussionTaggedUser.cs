﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Database.Models
{
    public class DiscussionTaggedUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public UserManagement.User User { get; set; }
        public Guid DiscussionId { get; set; }
        [ForeignKey("DiscussionId")]
        public Discussion Discussion { get; set; }
    }
}
