﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskCommentFileAssociationService
    {
        void Save(Models.TaskCommentFileAssociation requestData);
        void RemoveTaskCommentFileAssociation(Guid taskCommentId);
        List<Models.FileModel> GetTaskCommentAssociatedFiles(Guid taskCommentId);
    }

    public class TaskCommentFileAssociationService : ITaskCommentFileAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IUserService _userService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly ICommonService _commonService;
        public TaskCommentFileAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
        }

        public void Save(Models.TaskCommentFileAssociation requestData)
        {
            var existingTaskComment = _innostaxDb.TaskComments.Include(y=>y.Task).FirstOrDefault(x => x.Id == requestData.TaskCommentId && x.IsDeleted != true);
            if (existingTaskComment != null)
            {
                _commonService.ValidateAccess(Validate.TASK,Permission.CASE_UPDATE_ALL, existingTaskComment.TaskId);
                var existingTaskCommentFileAssociations = _innostaxDb.TaskCommentFileAssociations.Include(y=>y.TaskComment).Include(y=>y.TaskComment.Task).Include(y=>y.UploadedFile).Where(x => x.TaskCommentId == requestData.TaskCommentId && x.IsDeleted != true).ToList();
                if (existingTaskCommentFileAssociations.Count > 0)
                {
                    foreach (var eachAssociation in existingTaskCommentFileAssociations)
                    {
                        if (requestData.Files.FirstOrDefault(x => x.FileId == eachAssociation.FileId) == null)
                        {
                            var fileName = eachAssociation.UploadedFile.FileName;
                            _innostaxDb.TaskCommentFileAssociations.Remove(eachAssociation);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_DELETED, fileName, existingTaskComment.Task.Title);
                            _caseHistoryService.SaveCaseHistory(existingTaskComment.Task.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                        }
                    }
                }

                if (requestData.Files.Count > 0)
                {
                    foreach (var eachFile in requestData.Files)
                    {
                        if (_innostaxDb.TaskCommentFileAssociations.FirstOrDefault(x => x.FileId == eachFile.FileId && x.TaskCommentId == requestData.TaskCommentId) == null)
                        {
                            SaveTaskCommentAssociatedFiles(eachFile, requestData);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_ADDED, eachFile.FileName, existingTaskComment.Task.Title);
                            _caseHistoryService.SaveCaseHistory(existingTaskComment.Task.CaseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                        }
                    }
                }
                _innostaxDb.SaveChangesWithErrors();
            }
            else
            {
                var invalidTaskCommentIdError = Common.Constants.ErrorMessages.Common.TaskCommentErrors.TASK_COMMENT_NOT_PRESENT;
                _errorHandlerService.ThrowBadRequestError(invalidTaskCommentIdError);
            }
            return;
        }

        public void RemoveTaskCommentFileAssociation(Guid taskCommentId)
        {
            var existingTaskComment = _innostaxDb.TaskComments.Include(y=>y.Task).FirstOrDefault(x => x.Id == taskCommentId && x.IsDeleted != true);
            if (existingTaskComment != null)
            {
                _commonService.ValidateAccess(Validate.TASK,Permission.CASE_DELETE_ALL, existingTaskComment.TaskId);
                var taskCommentFileAssociations = _innostaxDb.TaskCommentFileAssociations.Include(y=>y.TaskComment).Include(y=>y.UploadedFile).Where(x => x.TaskCommentId == taskCommentId && x.IsDeleted != true).ToList();
                foreach (var taskCommentFileAssociation in taskCommentFileAssociations)
                {
                    taskCommentFileAssociation.IsDeleted = true;
                    _innostaxDb.SaveChangesWithErrors();
                    var historyMessage = string.Format(Common.Constants.CaseHistory.TASK_FILE_DELETED, taskCommentFileAssociation.UploadedFile.FileName, existingTaskComment.Task.Title);
                    _caseHistoryService.SaveCaseHistory(existingTaskComment.Task.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                }              
            }
            else
            {
                var invalidTaskCommentIdError = Common.Constants.ErrorMessages.Common.TaskCommentErrors.TASK_COMMENT_NOT_PRESENT;
                _errorHandlerService.ThrowBadRequestError(invalidTaskCommentIdError);
            }
            return;
        }

        public List<Models.FileModel> GetTaskCommentAssociatedFiles(Guid taskCommentId)
        {
            var result = new List<Models.FileModel>();
            var currentTaskCommentFileAssociations = _innostaxDb.TaskCommentFileAssociations.Include(y=>y.TaskComment).Include(y=>y.TaskComment.Task).Where(x => x.TaskCommentId == taskCommentId && x.IsDeleted != true).ToList();
            if(currentTaskCommentFileAssociations.Count > 0)
            {
                _commonService.ValidateAccess(Validate.TASK, Permission.CASE_READ_ALL, currentTaskCommentFileAssociations.First().TaskComment.TaskId);
            }
            foreach (var eachAssociation in currentTaskCommentFileAssociations)
            {
                var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == eachAssociation.FileId);
                var mappedFile = Mapper.Map<CaseManagement.Models.FileModel>(existingFile);
                mappedFile.FileUrl = existingFile.FileUrl;
                mappedFile.FileName = existingFile.FileName;
                var associatedUser = _userService.GetUser(existingFile.UserId).Data;
                mappedFile.UserName = associatedUser.FirstName + " " + associatedUser.LastName;
                result.Add(mappedFile);
            }
            return result;
        }

        internal void SaveTaskCommentAssociatedFiles(FileModel file, Models.TaskCommentFileAssociation requestData)
        {
            var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId);
            if (existingFile == null)
            {
                string fileNotFoundError = Common.Constants.ErrorMessages.Common.FileUploadError.NO_FILE_EXISTS;
                _errorHandlerService.ThrowResourceNotFoundError(fileNotFoundError);
            }
            var mappedTaskCommentFileAssociation = Mapper.Map<Common.Database.Models.TaskCommentFileAssociation>(requestData);
            mappedTaskCommentFileAssociation.FileId = file.FileId;
            var newTaskCommentFileAssociation = _innostaxDb.TaskCommentFileAssociations;
            newTaskCommentFileAssociation.Add(mappedTaskCommentFileAssociation);
            _innostaxDb.SaveChangesWithErrors();
        }
    }
}
