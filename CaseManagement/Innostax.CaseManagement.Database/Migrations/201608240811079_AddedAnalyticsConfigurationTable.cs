namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAnalyticsConfigurationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnalyticsConfigurations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AnalyticsType = c.String(),
                        Configuration = c.String(),
                        ReportName = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnalyticsConfigurations", "UserId", "dbo.Users");
            DropIndex("dbo.AnalyticsConfigurations", new[] { "UserId" });
            DropTable("dbo.AnalyticsConfigurations");
        }
    }
}
