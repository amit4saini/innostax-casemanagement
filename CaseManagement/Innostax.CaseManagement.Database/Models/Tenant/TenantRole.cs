﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.Common.Database.Models
{
    public class TenantRole: IdentityRole<Guid, TenantUserRole>
    {
        public TenantRole() { }
        public TenantRole(string name) { Name = name; }

        public IList<TenantRolePermission> RolePermissions { get; set; }
    }
}
