﻿namespace Innostax.Common.Database.Enums
{
    public enum HistoryFilter
    {
        TASK,
        EXPENSES,
        CASE_STATUS,
        DISCUSSION,
        ALL,
        FILES
    }
}
