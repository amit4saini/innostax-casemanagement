﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class ReportPrice
    {
        public Guid Id;
        public Guid ReportId;
        public int CountryId;
        public decimal Price;
    }
}
