﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionMessageTaggedUser
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid DiscussionMessageId { get; set; }
    }
}
