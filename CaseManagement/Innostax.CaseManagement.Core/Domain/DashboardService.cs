﻿using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using AutoMapper;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDashBoardService
    {
        DashBoardModel GetDashBoardRelatedData();
    }
    public class DashBoardService: IDashBoardService
    {
        private IInnostaxDb _innostaxDb;
        private ICommonService _commonService;
        private IUserService _userService;
        public DashBoardService(IInnostaxDb innostaxDb, ICommonService commonService, IUserService userService)
        {
            _innostaxDb = innostaxDb;
            _commonService = commonService;
            _userService = userService;
        }

        public DashBoardModel GetDashBoardRelatedData()
        {
            var dashBoardRelatedData = new DashBoardModel();
            var listOfAuthorizedCases = _commonService.GetAuthorizedCasesForUser();           
            dashBoardRelatedData.ClosedCasesCount = listOfAuthorizedCases.Where(x => x.CaseStatus.Status == "CLOSED" && x.IsDeleted != true).Count();
            dashBoardRelatedData.NewCasesCount = listOfAuthorizedCases.Where(x => x.CaseStatus.Status == "NEW" && x.IsDeleted != true).Count();
            dashBoardRelatedData.NFCCasesCount = listOfAuthorizedCases.Where(x => x.IsNFCCase == true && x.IsDeleted != true).Count();
            if (_userService.GetCurrentUser().Role.Name == Roles.Sales_Rep.ToString())
            {
                dashBoardRelatedData.ListOfRecentCases = Mapper.Map<List<Models.Case>>(listOfAuthorizedCases.Where(x => x.IsDeleted != true && x.SubStatus.CaseStatus.Status == "NEW").OrderByDescending(x => x.LastEditedDateTime).Take(10).ToList());
            }
            else
            {
                dashBoardRelatedData.ListOfRecentCases = Mapper.Map<List<Models.Case>>(listOfAuthorizedCases.Where(x => x.IsDeleted != true).OrderByDescending(x => x.LastEditedDateTime).Take(10).ToList());
            }
            return dashBoardRelatedData;
        }
    }
}
