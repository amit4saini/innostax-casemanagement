﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class ICIUserCaseAssociation
    {
        public int Id { get; set; }
        public Guid CaseId { get; set; }
        public Case Case { get; set; }
        public Guid ICIUserId { get; set; }      
        public bool IsActive { get; set; }    
        public Account.User User { get; set; } 
        public bool IsBeingAdded { get; set; }
    }
}
