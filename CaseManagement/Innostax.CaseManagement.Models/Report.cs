﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class Report
    {
        public Guid ReportId { get; set; }
        public string ReportType { get; set; }
        public string Description { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }    
        public bool IsPrimary { get; set; }
        public bool IsOtherReportType { get; set; }
        public string OtherReportTypeName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
    }
}
