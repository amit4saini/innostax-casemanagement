﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoMapper;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseReportAssociationTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CaseReportAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> userService;
        private Mock<ICaseHistoryService> caseHistoryService;
        private Mock<ICommonService> commonService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            userService = new Mock<IUserService>();
            caseHistoryService = new Mock<ICaseHistoryService>();
            commonService = new Mock<ICommonService>();
            sut = new CaseReportAssociationService(innostaxDbMock.Object, errorHandlerService,userService.Object, commonService.Object,caseHistoryService.Object);
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            //commonService.Setup(x => x.ValidateCaseAccess(It.IsAny<Guid>())).Returns(true);

        }

        private Models.CaseReportAssociation GetDummyCaseReportAssociation()
        {

            var dummyAssociation = new Models.CaseReportAssociation
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA14866D0F"),
                Reports = GetDomainModelDummyReports()
            };
            return dummyAssociation;
        }
        private Models.CaseReportAssociation GetInvalidDummyCaseReportAssociation()
        {

            var dummyAssociation = new Models.CaseReportAssociation
            {
                CaseId = new Guid("D2BF5024-DBF9-E511-AA98-FCAA14866D0F"),
                Reports = GetDomainModelDummyReports()
            };
            return dummyAssociation;
        }

        public List<Report> GetDummyReport()
        {
            var dummyReport = new List<Report>
            {
                new Report
                {
                    ReportId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new Report
                {
                    ReportId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyReport;
        }

        public List<Models.Report> GetDomainModelDummyReports()
        {
            var dummyReport = new List<Models.Report>
            {
                new Models.Report
                {
                    ReportId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new Models.Report
                {
                    ReportId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyReport;
        }
        public List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                     CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA14866D0F")
                },
                new Case
                {
                   CaseId = new Guid("02561DAC-DAF0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyCase;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenReportTableIsEmpty_ThenThrowsNotFoundRequest()
        {
            try
            {
                var dummyReport = new List<Report>();
                innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(dummyReport);
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
                innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(new CaseReportAssociation[0]);
                sut.Save(GetDummyCaseReportAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenCaseTableIsEmpty_ThenThrowsBadRequest()
        {
            try
            {
                var dummyCase = new List<Case>();
                innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(GetDummyReport());
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCase);
                sut.Save(GetDummyCaseReportAssociation());
            }
            catch (HttpResponseException e)
            {     
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenCaseIdOrReportIdIsNotValid_ThenThrowsBadRequest()
        {
            try
            {
                innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(GetDummyReport());
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
                sut.Save(GetInvalidDummyCaseReportAssociation());
            }
            catch (HttpResponseException e)
            {                
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]  
        public void GivenCallingSave_WhenDataIsValid_ThenMappedResultReturn()
        {
            var dummyCaseReportAssociation = new List<CaseReportAssociation>();
            innostaxDbMock.Setup(c => c.Reports).ReturnsDbSet(GetDummyReport());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(dummyCaseReportAssociation);
            try
            {
                var result = sut.Save(GetDummyCaseReportAssociation());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_DeleteCaseReportAssociation_WhenFunctionExecuteSuccessfully_ThenMappedResultReturn()
        {
            var dummyCaseReportAssociation = new List<CaseReportAssociation>();
            var dummyCases = GetDummyCase();
            var presentCaseId = Guid.NewGuid();
            var dummyCase = new Case
            {
                CaseId = presentCaseId
            };
            dummyCases.Add(dummyCase);
            innostaxDbMock.Setup(c => c.CaseReportAssociations).ReturnsDbSet(dummyCaseReportAssociation);
            try
            {
                sut.DeleteCaseReportAssociation(presentCaseId,true);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }   
}
