﻿using System;
using System.Collections.Generic;
using Innostax.Common.Database.Models;

namespace Innostax.Common.Database.MigrationSeedData
{
   public class VendorList
    {
       public List<Vendor> GetVendorList()
       {
           var vendorList = new List<Vendor> {
                new Vendor {VendorName="Ahluwalia Holdings Ltd. (PremierShield-non Indian Cases)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Barada Associates, Inc. (Consumer Report/In Depth References)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Cal Search" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Compass Driving Records (Datalink Services, Inc.)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Consolidated Services International (CSI)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Corporate Research & Investigations LLC (CRI)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Delaware Department of State: Division of Corporations" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="D-Quest, Inc." ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Dye & Durham" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Florida Department of Law Enforcement" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="G.A. Public Record Services" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="German Zuniga" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Hernan Gonzalez" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Human Resource Profile, Inc. (NY Criminal Check)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Kreller Business Credit Report (Kim)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Lamb's Security Solutions, Inc." ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Los Angeles County Superior Court" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="National Student Clearinghouse" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Nexis" ,Cost=15,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="NFC Global" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="PACER (Administrative Office of the U.S. Courts)" ,Cost=11,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Pangaea WordComm, LLC (Conversa Translation)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Performance Risk Management" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="PPSAPRO" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="Premier Shield Pvt (India)" ,Cost=0,CreatedOn=DateTime.UtcNow},
                new Vendor {VendorName="RA Consultants LTD" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Sure Step Investigations" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Texas Department of Public Safety" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Texas Secretary of State" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="The Work Number (Equifax Verification Services" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="TLO (TransUnion)" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Tracers Information Specialists, Inc." ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Velaton Limited (VLASTA)" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Whitehall Bureau of Canada Limited" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="World Compliance (LexisNexis)" ,Cost=1,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Youngtex International, Inc." ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Soquij (Quebec Court Records)" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="ICRIS (Hong Kong Registry)" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Office of the Superintendent of Bankruptcy Canada" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="I-Docket" ,Cost=0,CreatedOn=DateTime.UtcNow},
               new Vendor {VendorName="Higher Education Degree Datacheck-HEDD (UK Education)" ,Cost=0,CreatedOn=DateTime.UtcNow},
            };
           return vendorList;
       }
    }
}
