﻿using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Core.Services.Implementation;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System.IO;
using System.Web;
using System.Globalization;

namespace Innostax.CaseManagement.Web.Controllers
{
    [System.Web.Http.Authorize]
    public class FileController : ApiController
    {
        private IAzureFileManager _azureFileManager;
        public IFileService _fileUploadService;
        private IErrorHandlerService _errorHandlerService;
        public FileController(IAzureFileManager azureFileManager, IFileService fileUploadService, IErrorHandlerService errorHandlerService)
        {
            _azureFileManager = azureFileManager;
            _fileUploadService = fileUploadService;
            _errorHandlerService = errorHandlerService;
        }

        [System.Web.Mvc.HttpPost]
        public async Task<List<FileModel>> Post()
        {
            var result = new List<FileModel>();
            var uploadedFiles = await _azureFileManager.Add(Request);
            var saveFileResult = _fileUploadService.Save(uploadedFiles.ToList());
            result = new List<FileModel>();
            result = saveFileResult;
            return result;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/File/{id}/{type}")]
        public async Task<string> Get(Guid id,DownloadFileType type)
        {
            return await _azureFileManager.DownloadFile(id,type);
        }

        [HttpPut]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_ALL, Permission.CASE_UPDATE_OWN })]
        public void RenameFile(Guid fileId,FileModel file)
        {
            _fileUploadService.RenameFile(file);
        }

        [HttpGet]
        public void DownloadExcelfile(string fileName)
        {
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PivotDataExcel", fileName);
            var file = new System.IO.FileInfo(fullPath);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString(CultureInfo.InvariantCulture));
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }     
    }
}
