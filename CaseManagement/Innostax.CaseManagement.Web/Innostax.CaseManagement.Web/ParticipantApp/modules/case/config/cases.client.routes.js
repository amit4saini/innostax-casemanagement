﻿'use strict';

// Setting up route
angular.module('case').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
          .state('caseDetails', {
              url: '/participant/cases/:caseNumber/caseDetails',
              templateUrl: '/participantApp/modules/case/views/cases-details.view.html'
          });
  }
]);
