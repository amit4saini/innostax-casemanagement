﻿namespace Innostax.Common.Database.Enums
{
    public enum IHIQuality
    {
        Excellent,
        Average,
        Not_To_Standard,
        NA
    }
}
