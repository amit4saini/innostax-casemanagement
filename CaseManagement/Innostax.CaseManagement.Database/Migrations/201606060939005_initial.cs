namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaseExpenses",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        ExpenseName = c.String(),
                        Note = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpenseId = c.Int(nullable: false),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        LastEditedBy = c.Guid(),
                        LastEditedDateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.LastEditedBy)
                .ForeignKey("dbo.StandardExpenses", t => t.ExpenseId)
                .Index(t => t.CaseId)
                .Index(t => t.ExpenseId)
                .Index(t => t.CreatedBy)
                .Index(t => t.LastEditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        CaseId = c.Guid(nullable: false, identity: true),
                        ClientId = c.Guid(nullable: false),
                        SubStatusId = c.Guid(),
                        ReportSubject = c.String(),
                        MatterName = c.String(),
                        ClientPO = c.String(),
                        SubjectType = c.Int(),
                        NumberOfPrincipals = c.Int(nullable: false),
                        IsSpecificPrincipal = c.Boolean(nullable: false),
                        ConsentReceived = c.Boolean(nullable: false),
                        AgreementReceived = c.Boolean(nullable: false),
                        FCRACertificationEmailReceivedFromClient = c.Boolean(nullable: false),
                        StateOfEmploymentId = c.Int(),
                        StateOfResidenceId = c.Int(),
                        SalaryGreaterThan75k = c.Boolean(nullable: false),
                        References = c.Boolean(nullable: false),
                        Reputation = c.Boolean(nullable: false),
                        Rush = c.Boolean(nullable: false),
                        CreditReport = c.Boolean(nullable: false),
                        MinimumSuggestedRetail = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualRetail = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsProBonoGratis = c.Boolean(nullable: false),
                        WhyProBonoGratis = c.String(),
                        ClientProvidedInformation = c.String(),
                        ClientProvidedAttachments = c.Boolean(nullable: false),
                        AdditionalSalesRepNotes = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.Guid(),
                        CaseNumber = c.String(),
                        CaseReferenceName = c.String(),
                        HoldReasons = c.String(),
                        DaysOnHold = c.Int(),
                        CasePlacedOnHoldDate = c.DateTime(),
                        CaseAge = c.Int(),
                        ClientCancelled = c.Boolean(),
                        DDSupervisorApprovedCaseDate = c.DateTime(),
                        DDSupervisorWhoApprovedCase = c.Guid(),
                        IHIAssigned = c.Boolean(),
                        IHIDueDate = c.DateTime(),
                        ICIAssigned = c.Boolean(),
                        ICIDueDate = c.DateTime(),
                        FinalReportSentSalesRepDate = c.DateTime(),
                        FinalReportSentClientDate = c.DateTime(),
                        AssignedTo = c.Guid(),
                        IsNFCCase = c.Boolean(nullable: false),
                        CaseAcceptedByNFC = c.Boolean(),
                        NFCDueDate = c.DateTime(),
                        NFCBudget = c.Decimal(precision: 18, scale: 2),
                        ReportReceivedFromNFC = c.Boolean(),
                        InvoiceReceivedFromNFC = c.Boolean(),
                        DateInvoiceReceivedFromNFC = c.DateTime(),
                        ClientContactUserId = c.Guid(),
                        Speed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.AssignedTo)
                .ForeignKey("dbo.CaseSubStatus", t => t.SubStatusId)
                .ForeignKey("dbo.Clients", t => t.ClientId)
                .ForeignKey("dbo.ClientContactUsers", t => t.ClientContactUserId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DDSupervisorWhoApprovedCase)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.States", t => t.StateOfEmploymentId)
                .ForeignKey("dbo.States", t => t.StateOfResidenceId)
                .Index(t => t.ClientId)
                .Index(t => t.SubStatusId)
                .Index(t => t.StateOfEmploymentId)
                .Index(t => t.StateOfResidenceId)
                .Index(t => t.CreatedBy)
                .Index(t => t.DeletedBy)
                .Index(t => t.DDSupervisorWhoApprovedCase)
                .Index(t => t.AssignedTo)
                .Index(t => t.ClientContactUserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        CreationDateTime = c.DateTime(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                        ProfilePicKey = c.Guid(),
                        DeletedBy = c.Guid(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        UserClaimId = c.Guid(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.UserClaimId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        UserLoginId = c.Guid(nullable: false, identity: true),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.UserLoginId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserRoleId = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.UserRoleId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.CaseSubStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        StatusId = c.Guid(nullable: false),
                        SubStatus = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaseStatus", t => t.StatusId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.CaseStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Guid(nullable: false, identity: true),
                        ClientName = c.String(),
                        Notes = c.String(),
                        IndustryId = c.Int(),
                        IndustrySubCategoryId = c.Int(),
                        SalesRepresentative = c.Guid(),
                        SalesRepresentativeEmail = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.Guid(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ClientId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Industries", t => t.IndustryId)
                .ForeignKey("dbo.IndustrySubCategories", t => t.IndustrySubCategoryId)
                .ForeignKey("dbo.Users", t => t.SalesRepresentative)
                .Index(t => t.IndustryId)
                .Index(t => t.IndustrySubCategoryId)
                .Index(t => t.SalesRepresentative)
                .Index(t => t.DeletedBy)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.Industries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IndustrySubCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ClientContactUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ClientId = c.Guid(nullable: false),
                        CountryId = c.Int(),
                        StateId = c.Int(),
                        Street = c.String(),
                        City = c.String(),
                        ZipCode = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                        AccountCode = c.String(),
                        ContactName = c.String(),
                        PositionHeldbyContact = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.States", t => t.StateId)
                .Index(t => t.ClientId)
                .Index(t => t.CountryId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                        RegionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CountryId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        RegionId = c.Int(nullable: false, identity: true),
                        RegionName = c.String(),
                    })
                .PrimaryKey(t => t.RegionId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        StateId = c.Int(nullable: false, identity: true),
                        StateName = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StateId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.StandardExpenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceName = c.String(),
                        Currency = c.String(),
                        Amount = c.Double(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        EditedOn = c.DateTime(nullable: false),
                        EditedBy = c.Guid(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.EditedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.EditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.CaseFileAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        FileId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.UploadedFiles", t => t.FileId)
                .Index(t => t.CaseId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.UploadedFiles",
                c => new
                    {
                        FileId = c.Guid(nullable: false),
                        FileName = c.String(),
                        FileUrl = c.String(),
                        FileContent = c.String(),
                        FileType = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                        AzureKey = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteDateTime = c.DateTime(),
                        DeletedByUser = c.String(),
                    })
                .PrimaryKey(t => t.FileId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CaseHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Action = c.Int(nullable: false),
                        CreationDatetime = c.DateTime(),
                        CaseId = c.Guid(nullable: false),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .Index(t => t.CaseId);
            
            CreateTable(
                "dbo.CasePrincipals",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        PrincipalName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .Index(t => t.CaseId);
            
            CreateTable(
                "dbo.CaseReportAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        ReportId = c.Guid(nullable: false),
                        CountryId = c.Int(),
                        IsPrimaryReport = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                        IsOtherReportType = c.Boolean(nullable: false),
                        OtherReportTypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Reports", t => t.ReportId)
                .Index(t => t.CaseId)
                .Index(t => t.ReportId)
                .Index(t => t.CountryId)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ReportId = c.Guid(nullable: false, identity: true),
                        ReportType = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ReportId);
            
            CreateTable(
                "dbo.ClientIndustries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactUserBusinessUnits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ClientContactUserId = c.Guid(nullable: false),
                        BusinessUnit = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClientContactUsers", t => t.ClientContactUserId)
                .Index(t => t.ClientContactUserId);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        DiscountId = c.Guid(nullable: false, identity: true),
                        DiscountAmount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.DiscountId);
            
            CreateTable(
                "dbo.DiscussionFileAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DiscussionId = c.Guid(nullable: false),
                        FileId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Discussions", t => t.DiscussionId)
                .ForeignKey("dbo.UploadedFiles", t => t.FileId)
                .Index(t => t.DiscussionId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Message = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CaseId = c.Guid(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                        EditedBy = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsArchive = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.EditedBy)
                .Index(t => t.CaseId)
                .Index(t => t.CreatedBy)
                .Index(t => t.EditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.DiscussionMessageFileAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DiscussionMessageId = c.Guid(nullable: false),
                        FileId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DiscussionMessages", t => t.DiscussionMessageId)
                .ForeignKey("dbo.UploadedFiles", t => t.FileId)
                .Index(t => t.DiscussionMessageId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.DiscussionMessages",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DiscussionId = c.Guid(nullable: false),
                        Message = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                        EditedBy = c.Guid(),
                        IsArchive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsNotes = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Discussions", t => t.DiscussionId)
                .ForeignKey("dbo.Users", t => t.EditedBy)
                .Index(t => t.DiscussionId)
                .Index(t => t.CreatedBy)
                .Index(t => t.EditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.DiscussionNotifiers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DiscussionId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Discussions", t => t.DiscussionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.DiscussionId)
                .Index(t => t.UserId);          
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        NoteId = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        LastEditedBy = c.Guid(),
                        LastEditedDateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.NoteId)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.LastEditedBy)
                .Index(t => t.CaseId)
                .Index(t => t.CreatedBy)
                .Index(t => t.LastEditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        NotificationText = c.String(),
                        IsViewed = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.ReportPrices",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ReportId = c.Guid(nullable: false),
                        CountryId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Reports", t => t.ReportId)
                .Index(t => t.ReportId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.RolePermissions",
                c => new
                    {
                        RolePermissionId = c.Guid(nullable: false, identity: true),
                        RoleId = c.Guid(nullable: false),
                        Permission = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RolePermissionId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskActivities",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Action = c.Int(nullable: false),
                        CreationDatetime = c.DateTime(),
                        TaskId = c.Guid(nullable: false),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        TaskId = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        CompletionDate = c.DateTime(),
                        AssignedTo = c.Guid(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        EditedBy = c.Guid(nullable: false),
                        EditedOn = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("dbo.Users", t => t.AssignedTo)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.EditedBy)
                .Index(t => t.CaseId)
                .Index(t => t.AssignedTo)
                .Index(t => t.CreatedBy)
                .Index(t => t.EditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.TaskComments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TaskId = c.Guid(nullable: false),
                        Comment = c.String(),
                        CreatedBy = c.Guid(nullable: false),
                        CreationDateTime = c.DateTime(nullable: false),
                        LastEditedBy = c.Guid(nullable: false),
                        LastEditedDateTime = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.Guid(),
                        DeletionDateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.LastEditedBy)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .Index(t => t.TaskId)
                .Index(t => t.CreatedBy)
                .Index(t => t.LastEditedBy)
                .Index(t => t.DeletedBy);
            
            CreateTable(
                "dbo.TaskCommentTaggedUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        TaskCommentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskComments", t => t.TaskCommentId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TaskCommentId);
            
            CreateTable(
                "dbo.TaskFileAssociations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TaskId = c.Guid(nullable: false),
                        FileId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .ForeignKey("dbo.UploadedFiles", t => t.FileId)
                .Index(t => t.TaskId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.TimeSheetDescriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TimeSheets",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Description = c.String(),
                        CustomDescription = c.String(),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        LastEditedBy = c.Guid(),
                        LastEditedDateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.CreatedBy)
                .ForeignKey("dbo.Users", t => t.DeletedBy)
                .ForeignKey("dbo.Users", t => t.LastEditedBy)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.CaseId)
                .Index(t => t.UserId)
                .Index(t => t.CreatedBy)
                .Index(t => t.LastEditedBy)
                .Index(t => t.DeletedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimeSheets", "UserId", "dbo.Users");
            DropForeignKey("dbo.TimeSheets", "LastEditedBy", "dbo.Users");
            DropForeignKey("dbo.TimeSheets", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.TimeSheets", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.TimeSheets", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.TaskFileAssociations", "FileId", "dbo.UploadedFiles");
            DropForeignKey("dbo.TaskFileAssociations", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskCommentTaggedUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.TaskCommentTaggedUsers", "TaskCommentId", "dbo.TaskComments");
            DropForeignKey("dbo.TaskComments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.TaskComments", "LastEditedBy", "dbo.Users");
            DropForeignKey("dbo.TaskComments", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.TaskComments", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.TaskActivities", "UserId", "dbo.Users");
            DropForeignKey("dbo.TaskActivities", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "EditedBy", "dbo.Users");
            DropForeignKey("dbo.Tasks", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Tasks", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Tasks", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.Tasks", "AssignedTo", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RolePermissions", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.ReportPrices", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.ReportPrices", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Notifications", "UserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "LastEditedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.DiscussionNotifiers", "UserId", "dbo.Users");
            DropForeignKey("dbo.DiscussionNotifiers", "DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.DiscussionMessageFileAssociations", "FileId", "dbo.UploadedFiles");
            DropForeignKey("dbo.DiscussionMessageFileAssociations", "DiscussionMessageId", "dbo.DiscussionMessages");
            DropForeignKey("dbo.DiscussionMessages", "EditedBy", "dbo.Users");
            DropForeignKey("dbo.DiscussionMessages", "DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.DiscussionMessages", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.DiscussionMessages", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.DiscussionFileAssociations", "FileId", "dbo.UploadedFiles");
            DropForeignKey("dbo.DiscussionFileAssociations", "DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Discussions", "EditedBy", "dbo.Users");
            DropForeignKey("dbo.Discussions", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Discussions", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Discussions", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.ContactUserBusinessUnits", "ClientContactUserId", "dbo.ClientContactUsers");
            DropForeignKey("dbo.CaseReportAssociations", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.CaseReportAssociations", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.CaseReportAssociations", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CaseReportAssociations", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.CasePrincipals", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.CaseHistories", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.CaseFileAssociations", "FileId", "dbo.UploadedFiles");
            DropForeignKey("dbo.UploadedFiles", "UserId", "dbo.Users");
            DropForeignKey("dbo.CaseFileAssociations", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.CaseExpenses", "ExpenseId", "dbo.StandardExpenses");
            DropForeignKey("dbo.StandardExpenses", "EditedBy", "dbo.Users");
            DropForeignKey("dbo.StandardExpenses", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.StandardExpenses", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.CaseExpenses", "LastEditedBy", "dbo.Users");
            DropForeignKey("dbo.CaseExpenses", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.CaseExpenses", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.CaseExpenses", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.Cases", "StateOfResidenceId", "dbo.States");
            DropForeignKey("dbo.Cases", "StateOfEmploymentId", "dbo.States");
            DropForeignKey("dbo.Cases", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Cases", "DDSupervisorWhoApprovedCase", "dbo.Users");
            DropForeignKey("dbo.Cases", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Cases", "ClientContactUserId", "dbo.ClientContactUsers");
            DropForeignKey("dbo.ClientContactUsers", "StateId", "dbo.States");
            DropForeignKey("dbo.States", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.ClientContactUsers", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Countries", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.ClientContactUsers", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Cases", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Clients", "SalesRepresentative", "dbo.Users");
            DropForeignKey("dbo.Clients", "IndustrySubCategoryId", "dbo.IndustrySubCategories");
            DropForeignKey("dbo.Clients", "IndustryId", "dbo.Industries");
            DropForeignKey("dbo.Clients", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Clients", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Cases", "SubStatusId", "dbo.CaseSubStatus");
            DropForeignKey("dbo.CaseSubStatus", "StatusId", "dbo.CaseStatus");
            DropForeignKey("dbo.Cases", "AssignedTo", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropIndex("dbo.TimeSheets", new[] { "DeletedBy" });
            DropIndex("dbo.TimeSheets", new[] { "LastEditedBy" });
            DropIndex("dbo.TimeSheets", new[] { "CreatedBy" });
            DropIndex("dbo.TimeSheets", new[] { "UserId" });
            DropIndex("dbo.TimeSheets", new[] { "CaseId" });
            DropIndex("dbo.TaskFileAssociations", new[] { "FileId" });
            DropIndex("dbo.TaskFileAssociations", new[] { "TaskId" });
            DropIndex("dbo.TaskCommentTaggedUsers", new[] { "TaskCommentId" });
            DropIndex("dbo.TaskCommentTaggedUsers", new[] { "UserId" });
            DropIndex("dbo.TaskComments", new[] { "DeletedBy" });
            DropIndex("dbo.TaskComments", new[] { "LastEditedBy" });
            DropIndex("dbo.TaskComments", new[] { "CreatedBy" });
            DropIndex("dbo.TaskComments", new[] { "TaskId" });
            DropIndex("dbo.Tasks", new[] { "DeletedBy" });
            DropIndex("dbo.Tasks", new[] { "EditedBy" });
            DropIndex("dbo.Tasks", new[] { "CreatedBy" });
            DropIndex("dbo.Tasks", new[] { "AssignedTo" });
            DropIndex("dbo.Tasks", new[] { "CaseId" });
            DropIndex("dbo.TaskActivities", new[] { "TaskId" });
            DropIndex("dbo.TaskActivities", new[] { "UserId" });
            DropIndex("dbo.RolePermissions", new[] { "RoleId" });
            DropIndex("dbo.ReportPrices", new[] { "CountryId" });
            DropIndex("dbo.ReportPrices", new[] { "ReportId" });
            DropIndex("dbo.Notifications", new[] { "CreatedBy" });
            DropIndex("dbo.Notifications", new[] { "UserId" });
            DropIndex("dbo.Notes", new[] { "DeletedBy" });
            DropIndex("dbo.Notes", new[] { "LastEditedBy" });
            DropIndex("dbo.Notes", new[] { "CreatedBy" });
            DropIndex("dbo.Notes", new[] { "CaseId" });
            DropIndex("dbo.DiscussionNotifiers", new[] { "UserId" });
            DropIndex("dbo.DiscussionNotifiers", new[] { "DiscussionId" });
            DropIndex("dbo.DiscussionMessages", new[] { "DeletedBy" });
            DropIndex("dbo.DiscussionMessages", new[] { "EditedBy" });
            DropIndex("dbo.DiscussionMessages", new[] { "CreatedBy" });
            DropIndex("dbo.DiscussionMessages", new[] { "DiscussionId" });
            DropIndex("dbo.DiscussionMessageFileAssociations", new[] { "FileId" });
            DropIndex("dbo.DiscussionMessageFileAssociations", new[] { "DiscussionMessageId" });
            DropIndex("dbo.Discussions", new[] { "DeletedBy" });
            DropIndex("dbo.Discussions", new[] { "EditedBy" });
            DropIndex("dbo.Discussions", new[] { "CreatedBy" });
            DropIndex("dbo.Discussions", new[] { "CaseId" });
            DropIndex("dbo.DiscussionFileAssociations", new[] { "FileId" });
            DropIndex("dbo.DiscussionFileAssociations", new[] { "DiscussionId" });
            DropIndex("dbo.ContactUserBusinessUnits", new[] { "ClientContactUserId" });
            DropIndex("dbo.CaseReportAssociations", new[] { "DeletedBy" });
            DropIndex("dbo.CaseReportAssociations", new[] { "CountryId" });
            DropIndex("dbo.CaseReportAssociations", new[] { "ReportId" });
            DropIndex("dbo.CaseReportAssociations", new[] { "CaseId" });
            DropIndex("dbo.CasePrincipals", new[] { "CaseId" });
            DropIndex("dbo.CaseHistories", new[] { "CaseId" });
            DropIndex("dbo.UploadedFiles", new[] { "UserId" });
            DropIndex("dbo.CaseFileAssociations", new[] { "FileId" });
            DropIndex("dbo.CaseFileAssociations", new[] { "CaseId" });
            DropIndex("dbo.StandardExpenses", new[] { "DeletedBy" });
            DropIndex("dbo.StandardExpenses", new[] { "EditedBy" });
            DropIndex("dbo.StandardExpenses", new[] { "CreatedBy" });
            DropIndex("dbo.States", new[] { "CountryId" });
            DropIndex("dbo.Countries", new[] { "RegionId" });
            DropIndex("dbo.ClientContactUsers", new[] { "StateId" });
            DropIndex("dbo.ClientContactUsers", new[] { "CountryId" });
            DropIndex("dbo.ClientContactUsers", new[] { "ClientId" });
            DropIndex("dbo.Clients", new[] { "CreatedBy" });
            DropIndex("dbo.Clients", new[] { "DeletedBy" });
            DropIndex("dbo.Clients", new[] { "SalesRepresentative" });
            DropIndex("dbo.Clients", new[] { "IndustrySubCategoryId" });
            DropIndex("dbo.Clients", new[] { "IndustryId" });
            DropIndex("dbo.CaseSubStatus", new[] { "StatusId" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "DeletedBy" });
            DropIndex("dbo.Cases", new[] { "ClientContactUserId" });
            DropIndex("dbo.Cases", new[] { "AssignedTo" });
            DropIndex("dbo.Cases", new[] { "DDSupervisorWhoApprovedCase" });
            DropIndex("dbo.Cases", new[] { "DeletedBy" });
            DropIndex("dbo.Cases", new[] { "CreatedBy" });
            DropIndex("dbo.Cases", new[] { "StateOfResidenceId" });
            DropIndex("dbo.Cases", new[] { "StateOfEmploymentId" });
            DropIndex("dbo.Cases", new[] { "SubStatusId" });
            DropIndex("dbo.Cases", new[] { "ClientId" });
            DropIndex("dbo.CaseExpenses", new[] { "DeletedBy" });
            DropIndex("dbo.CaseExpenses", new[] { "LastEditedBy" });
            DropIndex("dbo.CaseExpenses", new[] { "CreatedBy" });
            DropIndex("dbo.CaseExpenses", new[] { "ExpenseId" });
            DropIndex("dbo.CaseExpenses", new[] { "CaseId" });
            DropTable("dbo.TimeSheets");
            DropTable("dbo.TimeSheetDescriptions");
            DropTable("dbo.TaskFileAssociations");
            DropTable("dbo.TaskCommentTaggedUsers");
            DropTable("dbo.TaskComments");
            DropTable("dbo.Tasks");
            DropTable("dbo.TaskActivities");
            DropTable("dbo.Roles");
            DropTable("dbo.RolePermissions");
            DropTable("dbo.ReportPrices");
            DropTable("dbo.Notifications");
            DropTable("dbo.Notes");       
            DropTable("dbo.DiscussionNotifiers");
            DropTable("dbo.DiscussionMessages");
            DropTable("dbo.DiscussionMessageFileAssociations");
            DropTable("dbo.Discussions");
            DropTable("dbo.DiscussionFileAssociations");
            DropTable("dbo.Discounts");
            DropTable("dbo.ContactUserBusinessUnits");
            DropTable("dbo.ClientIndustries");
            DropTable("dbo.Reports");
            DropTable("dbo.CaseReportAssociations");
            DropTable("dbo.CasePrincipals");
            DropTable("dbo.CaseHistories");
            DropTable("dbo.UploadedFiles");
            DropTable("dbo.CaseFileAssociations");
            DropTable("dbo.StandardExpenses");
            DropTable("dbo.States");
            DropTable("dbo.Regions");
            DropTable("dbo.Countries");
            DropTable("dbo.ClientContactUsers");
            DropTable("dbo.IndustrySubCategories");
            DropTable("dbo.Industries");
            DropTable("dbo.Clients");
            DropTable("dbo.CaseStatus");
            DropTable("dbo.CaseSubStatus");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.Cases");
            DropTable("dbo.CaseExpenses");
        }
    }
}
