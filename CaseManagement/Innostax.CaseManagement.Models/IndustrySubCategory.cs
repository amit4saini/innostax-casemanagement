﻿namespace Innostax.CaseManagement.Models
{
    public class IndustrySubCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
