namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "ReviewedBy", c => c.Guid());
            CreateIndex("dbo.Cases", "ReviewedBy");
            AddForeignKey("dbo.Cases", "ReviewedBy", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "ReviewedBy", "dbo.Users");
            DropIndex("dbo.Cases", new[] { "ReviewedBy" });
            DropColumn("dbo.Cases", "ReviewedBy");
        }
    }
}
