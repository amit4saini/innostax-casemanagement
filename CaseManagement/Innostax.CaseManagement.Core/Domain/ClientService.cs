﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Migrations;
using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System.Net.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;
using System.Web.Configuration;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IClientService
    {
        Innostax.CaseManagement.Models.Client GetClientDetailsFromClientId(Guid clientId);
        PageResult<Client> GetAllClientsDetails(ODataQueryOptions<Client> options, HttpRequestMessage request);
        void Delete(Guid clientId);
        Client Save(Client clientData);
        Guid ValidateClientId(string id);
        PageResult<Client> GetClientsBasedOnClientName(HttpRequestMessage request,string clientName);
        List<Client> GetAllClientsForCases();
        PageResult<Case> GetAllCasesForAClient(ODataQueryOptions<Case> options, HttpRequestMessage request,Guid clientId);
        PageResult<Client> SearchClientsByKeyword(ODataQueryOptions<Client> options, HttpRequestMessage request, string keyword);
    }

    public class ClientService : IClientService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private readonly IReportService _reportService;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IClientContactUserService _clientContactUserService;
        private readonly ICommonService _commonService;

        public ClientService(IInnostaxDb innostaxDb, IUserService userService, IReportService reportService, IErrorHandlerService errorHandlerService, IClientContactUserService clientContactUserService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _reportService = reportService;
            _errorHandlerService = errorHandlerService;
            _clientContactUserService = clientContactUserService;
            _commonService = commonService;
        }

        public Innostax.CaseManagement.Models.Client GetClientDetailsFromClientId(Guid clientId)
        {
            var result = new Innostax.CaseManagement.Models.Client();
            var existingClient = _innostaxDb.Clients.Include(x => x.Industry)
            .Include(x => x.IndustrySubCategory).FirstOrDefault(x => x.ClientId == clientId && x.IsDeleted == false);    
            if (existingClient != null)
            {
                var getExistingClientContactUsers = _clientContactUserService.GetClientAssociatedContactUsers(clientId);
                var mappedContactUsersList = new List<ClientContactUser>();
                if (getExistingClientContactUsers.Count > 0)
                {
                    foreach (var eachContactUser in getExistingClientContactUsers)
                    {
                        var caseAssociatedClientContactUser = _innostaxDb.Cases.FirstOrDefault(x => x.ClientContactUserId == eachContactUser.Id && x.IsDeleted == false);
                        var mappedClientContactUser = Mapper.Map<ClientContactUser>(eachContactUser);
                        mappedContactUsersList.Add(mappedClientContactUser);
                        if (caseAssociatedClientContactUser != null)
                        {
                            mappedClientContactUser.IsCaseAssociatedClientContactUser = true;
                        }
                    }
                }                                         
                var client = Mapper.Map<Client>(existingClient);
                client.SalesRepresentative = Mapper.Map<User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == client.SalesRepresentativeId));
                client.ClientContactUsers = mappedContactUsersList.ToArray();               
                result = client;
            }
            return result;
        }

        public PageResult<Client> GetAllClientsDetails(ODataQueryOptions<Client> options, HttpRequestMessage request) 
        {
            var result = new List<Innostax.CaseManagement.Models.Client>();                         
            var  defaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) || (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            ODataQuerySettings settings = new ODataQuerySettings()
                {
                    PageSize = Convert.ToInt32(defaultPageSize)
                };                    
            var allExistingClients = _innostaxDb.Clients.Where(client => client.IsDeleted == false)
                .Include(client => client.Industry).
                Include(client => client.IndustrySubCategory).ToList();
            result = new List<Innostax.CaseManagement.Models.Client>();
            foreach (var existingClient in allExistingClients)
            {
                var client = Mapper.Map<Client>(existingClient);

                var existingCasesForClient = _innostaxDb.Cases.Where(x => x.ClientId == client.ClientId).OrderByDescending(x => x.CreationDateTime);
                if (existingCasesForClient.Count() > 0)
                {
                    client.LatestCaseOrderDate = existingCasesForClient.First().CreationDateTime;
                }
    
                client.SalesRepresentative = Mapper.Map<User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == client.SalesRepresentativeId));
                client.ClientContactUsers = _clientContactUserService.GetClientAssociatedContactUsers(client.ClientId).ToArray();
                result.Add(client);
            }
            IQueryable results = options.ApplyTo(result.AsQueryable(), settings);
            return new PageResult<Client>(
            results as IEnumerable<Client>, request.ODataProperties().NextLink,
            result.Count());
        }

        public List<Client> GetAllClientsForCases()
        {
            var result = new List<Innostax.CaseManagement.Models.Client>();
            var allExistingClients = _innostaxDb.Clients.Where(client => client.IsDeleted == false)
            .Include(client => client.Industry).
             Include(client => client.IndustrySubCategory).ToList();
            result = new List<Innostax.CaseManagement.Models.Client>();
            foreach (var existingClient in allExistingClients)
            {
                var client = Mapper.Map<Client>(existingClient);
                client.SalesRepresentative = Mapper.Map<User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == client.SalesRepresentativeId));
                client.ClientContactUsers = _clientContactUserService.GetClientAssociatedContactUsers(client.ClientId).ToArray();
                var existingCasesForClient = _innostaxDb.Cases.Where(x => x.ClientId == client.ClientId).OrderByDescending(x => x.CreationDateTime);
                if (existingCasesForClient.Count() > 0)
                {
                    client.LatestCaseOrderDate = existingCasesForClient.First().CreationDateTime;
                }
                result.Add(client);
            }
            return result;
        }

        public PageResult<Case> GetAllCasesForAClient(ODataQueryOptions<Case> options, HttpRequestMessage request, Guid clientId)
        {           
            var defaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) || (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            ODataQuerySettings settings = new ODataQuerySettings()
            {
                PageSize = Convert.ToInt32(defaultPageSize)
            };
            var allCases = _userService.UserPermissions.Contains(Permission.CLIENT_READ_ALL) ? _innostaxDb.Cases.Include(x => x.ClientContactUser).Include(x => x.BusinessUnit).Where(x => x.IsDeleted == false && x.ClientId == clientId).ToList() : new List<Common.Database.Models.Case>();
            var mappedCaseList = Mapper.Map<List<Innostax.CaseManagement.Models.Case>>(allCases);
            foreach (var eachCase in mappedCaseList)
            {
                eachCase.Reports = _reportService.GetCaseAssociatedReports(eachCase.CaseId).ToArray();
                if (eachCase.Reports.Length != 0)
                {
                    var primaryReport = eachCase.Reports.FirstOrDefault(x => x.IsPrimary == true);
                    if (primaryReport != null)
                    {
                        eachCase.PrimaryReportType = primaryReport.ReportType;
                        eachCase.PrimaryReportCountryName = primaryReport.CountryName;
                    }
                }
            }
           
            IQueryable result = options.ApplyTo(mappedCaseList.AsQueryable(), settings);
            return new PageResult<Case>(
            result as IEnumerable<Case>, request.ODataProperties().NextLink,
            mappedCaseList.Count());
        }

        public Guid ValidateClientId(string id)
        {
            Guid result = new Guid();
            Guid receivedClientId;
            if (!Guid.TryParse(id, out receivedClientId))
            {
                string clientIdNotValidError = Common.Constants.ErrorMessages.ClientManagement.ClientUpdationErrors.CLIENTID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(clientIdNotValidError);
            }
            result = receivedClientId;
            return result;
        }

        public void Delete(Guid clientId)
        {
            var clientToBeDeleted = _innostaxDb.Clients.FirstOrDefault(client => client.ClientId == clientId);
            if (clientToBeDeleted == null)
            {
                string clientNotFoundError = Common.Constants.ErrorMessages.ClientManagement.ClientUpdationErrors.CLIENT_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(clientNotFoundError);
            }
            var caseAssociatedClient = _innostaxDb.Cases.Include(x => x.ClientContactUser).Where(x => x.ClientId == clientId).ToList();
            if (caseAssociatedClient.Count > 0)
            {
                foreach (var eachCase in caseAssociatedClient)
                {
                    if (eachCase.IsDeleted == false)
                    {                       
                        _errorHandlerService.ThrowBadRequestError(Common.Constants.ErrorMessages.ClientManagement.ClientUpdationErrors.CASE_ASSOCIATED_CLIENT_DELETE);
                    }
                }
            }
            clientToBeDeleted.IsDeleted = true;
            clientToBeDeleted.DeletedBy = _userService.UserId;       
                _innostaxDb.SaveChangesWithErrors();
                
            var existingClientContactUser = _innostaxDb.ClientContactUsers.Where(x => x.ClientId == clientId && x.IsDeleted == false).ToList();
            if (existingClientContactUser.Count > 0)
            {
                foreach (var eachContactUser in existingClientContactUser)
                {
                    eachContactUser.IsDeleted = true;
                }
            }         
                _innostaxDb.SaveChangesWithErrors();
        }

        public Client Save(Client clientData)
        {
            var result = new Models.Client();
            if (clientData == null)
            {
                clientData = new Client();
                clientData.ClientId = new Guid();
            }
            var existingClient = _innostaxDb.Clients.FirstOrDefault(x => (x.ClientId == clientData.ClientId && x.IsDeleted == false));
            //Workaround code for automapper guid mapper bugs
            var existingClientId = Guid.Empty;
            var existingClientCreatedBy = Guid.Empty;
            var currentUser = _userService.UserId;
            if (existingClient == null)
            {
                existingClient = new Innostax.Common.Database.Models.Client();
                existingClient.ClientId = Guid.NewGuid();
                existingClient.CreationDateTime = _commonService.GetUtcDateTime();
            }
            existingClientCreatedBy = existingClient.CreatedBy == Guid.Empty ? currentUser : existingClient.CreatedBy;
            existingClientId = existingClient.ClientId;
            existingClient = Mapper.Map<Innostax.Common.Database.Models.Client>(clientData);
            existingClient.ClientName = clientData.ClientName;

            //Workaround code for automapper guid mapper bugs
            existingClient.ClientId = existingClientId;
            existingClient.CreatedBy = existingClientCreatedBy;
            existingClient.LastEditedDateTime = _commonService.GetUtcDateTime();

            _innostaxDb.Clients.AddOrUpdate(existingClient);         
                _innostaxDb.SaveChangesWithErrors();        
            _clientContactUserService.Save(clientData.ClientContactUsers, existingClient.ClientId);           
            result = new Models.Client();
            result = Mapper.Map<Models.Client>(existingClient);
            return result;
        }

        public PageResult<Client> GetClientsBasedOnClientName(HttpRequestMessage request, string clientName)
        {
            var result = new List<Innostax.CaseManagement.Models.Client>();
            var existingClients = _innostaxDb.Clients.Where(x => x.ClientName.StartsWith(clientName) && x.IsDeleted == false).ToList();
            if (existingClients == null || existingClients.Count <= 0)
            {
                string clientNameNotFoundError = Common.Constants.ErrorMessages.ClientManagement.ClientUpdationErrors.CLIENT_NAME_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(clientNameNotFoundError + clientName);
            }
            result = new List<Innostax.CaseManagement.Models.Client>();
            foreach (var eachClient in existingClients)
            {
                result.Add(Mapper.Map<Models.Client>(eachClient));
            }           
            return new PageResult<Client>(
            result as IEnumerable<Client>, request.ODataProperties().NextLink,
            result.Count());
        }

        public PageResult<Client> SearchClientsByKeyword(ODataQueryOptions<Client> options, HttpRequestMessage request, string keyword)
        {
            var clientSearchResult = _innostaxDb.Clients.Include(y => y.SalesRepresentative).Include(y => y.Industry).Include(y => y.IndustrySubCategory).Where(x => ((x.SalesRepresentative.FirstName ?? "").Contains(keyword) || (x.SalesRepresentative.LastName ?? "").Contains(keyword) || (x.ClientName ?? "").Contains(keyword) || (x.Notes ?? "").Contains(keyword) || (x.Industry.Name ?? "").Contains(keyword) || (x.IndustrySubCategory.Name ?? "").Contains(keyword) || (x.SalesRepresentativeEmail ?? "").Contains(keyword)) && x.IsDeleted == false);         
            var contactUserSearchResult = SearchContactUsersByKeyword(keyword);
            var businessUnitSearchResult = SearchBusinessUnitsByKeyword(keyword);
            var mappedSearchResult = Mapper.Map<List<Client>>(clientSearchResult.Union(contactUserSearchResult).Union(businessUnitSearchResult).ToList());
            foreach (var client in mappedSearchResult)
            {
                var existingCasesForClient = _innostaxDb.Cases.Where(x => x.ClientId == client.ClientId).OrderByDescending(x => x.CreationDateTime);
                if (existingCasesForClient.Count() > 0)
                {
                    client.LatestCaseOrderDate = existingCasesForClient.First().CreationDateTime;
                }

                client.SalesRepresentative = Mapper.Map<User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == client.SalesRepresentativeId));
                client.ClientContactUsers = _clientContactUserService.GetClientAssociatedContactUsers(client.ClientId).ToArray();
            }
            IQueryable results = options.ApplyTo(mappedSearchResult.AsQueryable());
            return new PageResult<Client>(
            results as IEnumerable<Client>, request.ODataProperties().NextLink,
            mappedSearchResult.Count());
        }

        internal IQueryable<Innostax.Common.Database.Models.Client> SearchContactUsersByKeyword(string keyword)
        {
            return _innostaxDb.ClientContactUsers.Include(y => y.Client).Where(x => ((x.PositionHeldbyContact ?? "").Contains(keyword) || (x.Street ?? "").Contains(keyword) || (x.City ?? "").Contains(keyword) || (x.ZipCode ?? "").Contains(keyword) || (x.PhoneNumber ?? "").Contains(keyword) || (x.EmailAddress ?? "").Contains(keyword) || (x.Country.CountryName ?? "").Contains(keyword) || (x.State.StateName ?? "").Contains(keyword) || (x.ContactName ?? "").Contains(keyword)) && x.IsDeleted == false && x.Client.IsDeleted == false).Select(y => y.Client);
        }

        internal IQueryable<Innostax.Common.Database.Models.Client> SearchBusinessUnitsByKeyword(string keyword)
        {
            return _innostaxDb.ContactUserBusinessUnits.Include(y=>y.ClientContactUser).Include(y => y.ClientContactUser.Client).Where(x => ((x.BusinessUnit ?? "").Contains(keyword) || (x.AccountCode ?? "").Contains(keyword)) && x.ClientContactUser.IsDeleted == false && x.ClientContactUser.Client.IsDeleted == false).Select(y => y.ClientContactUser.Client);
        }
    }
}
