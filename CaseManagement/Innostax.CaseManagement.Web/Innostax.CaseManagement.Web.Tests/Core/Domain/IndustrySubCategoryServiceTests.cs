﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class IndustrySubCategoryServiceTests
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private IndustrySubCategoryService sut;
        private Mock<IErrorHandlerService> errorHandlerService;
        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new Mock<IErrorHandlerService>();
            sut = new IndustrySubCategoryService(innostaxDbMock.Object, errorHandlerService.Object);           
        }
        private List<IndustrySubCategory> GetDummySubCategory()
        {
            var dummySubCategories = new List<IndustrySubCategory>
            {
                new IndustrySubCategory
                {
                    Id=1,
                    Name="abc",
                },
                new IndustrySubCategory
                {
                    Id=2,
                    Name="xyz",
                }
            };
            return dummySubCategories;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetSubCategoryOfIndustry_WhenNoSubCategoryOfIndustryExists_ThenThrowBadRequest()
        {
            try
            {
                var dummySubCategories = new List<IndustrySubCategory>();
                innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(dummySubCategories);
                sut.GetIndustrySubCategory();
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetSubCategoryOfIndustry_WhenIndustryExists_ThenMappedResultReturn()
        {
            var dummySubCategories = GetDummySubCategory();
            innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(dummySubCategories);
            var actual = sut.GetIndustrySubCategory();
            Assert.IsNotNull(actual);
        }
    }
}