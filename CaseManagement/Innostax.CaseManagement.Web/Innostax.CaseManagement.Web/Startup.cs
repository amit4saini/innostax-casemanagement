﻿using Innostax.CaseManagement.Web.Hubs;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartupAttribute(typeof(Innostax.CaseManagement.Web.Startup))]
namespace Innostax.CaseManagement.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var idProvider = new CustomUserIdProvider();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);
            app.MapSignalR();
            app.Use(typeof(Innostax.CaseManagement.Web.App_Start.MultiTenantMiddleware));

        }
    }
}
