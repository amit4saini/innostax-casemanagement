namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCaseAcceptedByNFCDateColumnInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "CaseAcceptedByNFCDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "CaseAcceptedByNFCDate");
        }
    }
}
