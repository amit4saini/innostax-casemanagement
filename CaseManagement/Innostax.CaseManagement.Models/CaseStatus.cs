﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class CaseStatus
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
    }
}
