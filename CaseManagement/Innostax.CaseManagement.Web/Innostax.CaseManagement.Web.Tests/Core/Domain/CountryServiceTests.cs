﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CountryServiceTests
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CountryService sut;
        private ErrorHandlerService errorHandlerService;
        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            sut = new CountryService(innostaxDbMock.Object, errorHandlerService);           
        }
        private List<Country> GetDummyCountries()
        {
            var dummyCountries = new List<Country>
            {
                new Country
                {
                    CountryId=1,
                    CountryName="India",
                    RegionId=1
                },
                new Country
                {
                    CountryId=2,
                    CountryName="Usa",
                    RegionId=1
                }
            };
            return dummyCountries;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetAllCountries_WhenNoCountryExists_ThenThrowNotFound()
        {
            try
            {
                var dummyCountries = new List<Country> { };
                innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummyCountries);
                sut.GetAllCountries();
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetAllCountries_WhenCountryExists_ThenMappedResultReturn()
        {
            var dummyCountries = GetDummyCountries();
            innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummyCountries);
            var countryList = sut.GetAllCountries();
            Assert.IsNotNull(countryList);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetAllCountriesByRegion_WhenNoCountryExistsForARegion_ThenThrowNotFound()
        {
            try
            {
                var dummyCountries = new List<Country> { };
                var regionId = "2";
                innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummyCountries);
                sut.GetAllCountriesByRegion(regionId);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetAllCountriesByRegion_WhenCountryExists_ThenMappedResultReturn()
        {
            var dummyCountries = GetDummyCountries();
            var regionId = "1";
            innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummyCountries);
            var countryList = sut.GetAllCountriesByRegion(regionId);
            Assert.IsNotNull(countryList);
        }
    }
}

