﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Models
{
    public class CaseAnalyticsModel
    {
        public Guid CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string NickName { get; set; }
        public decimal Margin { get; set; }
        public bool IsNFC { get; set; }
        public DateTime CreationDateTime { get; set; }
        public Country Country { get; set; }
        public Speed CaseRush { get; set; }
    }
}
