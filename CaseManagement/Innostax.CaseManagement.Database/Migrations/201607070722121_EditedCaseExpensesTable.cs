namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedCaseExpensesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaseExpenses", "VendorId", c => c.Guid());
            AddColumn("dbo.CaseExpenses", "VendorName", c => c.String());
            CreateIndex("dbo.CaseExpenses", "VendorId");
            AddForeignKey("dbo.CaseExpenses", "VendorId", "dbo.Vendors", "VendorId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaseExpenses", "VendorId", "dbo.Vendors");
            DropIndex("dbo.CaseExpenses", new[] { "VendorId" });
            DropColumn("dbo.CaseExpenses", "VendorName");
            DropColumn("dbo.CaseExpenses", "VendorId");
        }
    }
}
