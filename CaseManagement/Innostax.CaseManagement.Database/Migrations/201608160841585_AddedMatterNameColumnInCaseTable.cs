namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatterNameColumnInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "MatterName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "MatterName");
        }
    }
}
