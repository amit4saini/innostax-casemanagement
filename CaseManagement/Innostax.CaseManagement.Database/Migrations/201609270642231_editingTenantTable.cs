namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingTenantTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tenants", "FirstName", c => c.String());
            AddColumn("dbo.Tenants", "LastName", c => c.String());
            AddColumn("dbo.Tenants", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tenants", "Email");
            DropColumn("dbo.Tenants", "LastName");
            DropColumn("dbo.Tenants", "FirstName");
        }
    }
}
