﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface INFCUserCaseAssociationService
    {
        List<NFCUserCaseAssociation> GetNFCUserCaseAssociationsByCaseId(Guid caseId);
        void Save(NFCUserCaseAssociation NFCUserAssociationData);
        List<Case> GetCasesByNFCUserId(Guid NFCUserId);
    }

    public class NFCUserCaseAssociationService : INFCUserCaseAssociationService
    {
        private IInnostaxDb _innostaxDb;
        private IErrorHandlerService _errorHandlerService;
        private IUserService _userService;
        private ICaseHistoryService _caseHistoryService;
        public NFCUserCaseAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICaseHistoryService caseHistoryService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _caseHistoryService = caseHistoryService;
        }

        public List<NFCUserCaseAssociation> GetNFCUserCaseAssociationsByCaseId(Guid caseId)
        {
            var result = new List<NFCUserCaseAssociation>();
            var existingNFCUserCaseAssociations = _innostaxDb.NFCUserCaseAssociations.Include(x => x.User).Include(x => x.User.Roles).Where(x => x.CaseId == caseId).ToList();
            foreach (var eachAssociation in existingNFCUserCaseAssociations)
            {
                var mappedNFCUserCaseAssociation = Mapper.Map<Models.NFCUserCaseAssociation>(eachAssociation);
                var userDetails = _innostaxDb.UserDetails.Include(y => y.Country).FirstOrDefault(x => x.UserId == eachAssociation.NFCUserId);
                mappedNFCUserCaseAssociation.User.UserDetails = Mapper.Map<Models.UserDetail>(userDetails);
                var roleId = eachAssociation.User.Roles.First().RoleId;
                mappedNFCUserCaseAssociation.User.Role = Mapper.Map<Role>(_innostaxDb.Roles.FirstOrDefault(x => x.Id == roleId));
                result.Add(mappedNFCUserCaseAssociation);
            }
            return result;
        }

        public void Save(NFCUserCaseAssociation nfcUserCaseAssociation)
        {
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == nfcUserCaseAssociation.CaseId && x.IsDeleted == false);
            if (existingCase == null)
            {
                string caseNotFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(caseNotFoundError);
            }
            var userEmail = nfcUserCaseAssociation.User.Email;
            var existingNFCUserCaseAssociation = _innostaxDb.NFCUserCaseAssociations.FirstOrDefault(x => x.CaseId == nfcUserCaseAssociation.CaseId && x.NFCUserId == nfcUserCaseAssociation.NFCUserId);
            if (nfcUserCaseAssociation.IsBeingAdded)
            {
                if (existingNFCUserCaseAssociation != null)
                {
                    string NFCUserForCaseExistError = Common.Constants.ErrorMessages.Common.NFCUserCaseAssociationError.NFC_USER_ALREADY_EXISTS;
                    _errorHandlerService.ThrowBadRequestError(NFCUserForCaseExistError);
                }
            }
            else
            {
                if (nfcUserCaseAssociation.User.UserDetails != null)
                {
                    nfcUserCaseAssociation.User.UserDetails.Country = null;
                    var existingUserDetails = Mapper.Map<Common.Database.Models.UserDetail>(nfcUserCaseAssociation.User.UserDetails);
                    _innostaxDb.UserDetails.AddOrUpdate(existingUserDetails);
                }
                _userService.SaveUser(nfcUserCaseAssociation.User);
            }
            nfcUserCaseAssociation.User = null;
            existingNFCUserCaseAssociation = Mapper.Map<Common.Database.Models.NFCUserCaseAssociation>(nfcUserCaseAssociation);
            _innostaxDb.NFCUserCaseAssociations.AddOrUpdate(existingNFCUserCaseAssociation);
            _innostaxDb.SaveChangesWithErrors();
            if (nfcUserCaseAssociation.IsBeingAdded)
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_NFC_USER_ADDED, "Added an NFC user: " + userEmail);
            else
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_NFC_USER_EDITED, "Edited an NFC user: " + userEmail);
        }

        public List<Case> GetCasesByNFCUserId(Guid NFCUserId)
        {
            var result = new List<Case>();
            var existingNFCUserCaseAssociations = _innostaxDb.NFCUserCaseAssociations.Where(x => x.NFCUserId == NFCUserId && x.IsActive == true).ToList();
            foreach (var existingNFCUserCaseAssociation in existingNFCUserCaseAssociations)
            {
                var existingCase = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.ClientContactUser).FirstOrDefault(x => x.CaseId == existingNFCUserCaseAssociation.CaseId && x.IsDeleted != true);
                var mappedCase = Mapper.Map<Models.Case>(existingCase);
                result.Add(mappedCase);
            }
            return result;
        }
    }
}
