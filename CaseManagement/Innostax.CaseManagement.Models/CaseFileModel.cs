﻿using Innostax.Common.Database.Enums;
using System;

namespace Innostax.CaseManagement.Models
{
    public class CaseFileModel
    {
        public Guid FileId { get; set; }
        public string FileName { get; set; }
        public CaseFileType FileType { get; set; }
        public DateTime FileUploadedTime { get; set; }
        public string FileUploadedBy { get; set; }
        public Guid? DiscussionId { get; set; }
        public string DiscussionName { get; set; }
        public Guid? TaskId { get; set; }
        public string TaskName { get; set; }
        public int DiscussionMessageCount { get; set; }       
    }
}
