﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TimeSheetServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private TimeSheetService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandlerService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICommonService> _commonService;
        Guid timeSheetId;
        Guid clientId1;
        Guid clientId2;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            timeSheetId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _errorHandlerService = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            sut = new TimeSheetService(
                innostaxDbMock.Object,
                 _userService.Object, _errorHandlerService, _caseHistoryService.Object,
                 _commonService.Object
                );          
            _caseHistoryService.Setup(x => x.SaveCaseHistory(It.IsAny<Guid>(), It.IsAny<Innostax.Common.Database.Enums.Action>(),null));
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId1, ClientName = "DummyClient1" },
                    ClientId=clientId1,
                    IsDeleted=false
                },
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId2, ClientName = "DummyClient2" },
                    ClientId=clientId2,
                    IsDeleted=false
                }
            };
            return dummyCase;
        }
        private Models.TimeSheet GetDummyTimeSheet()
        {
            var timeSheet = new Models.TimeSheet
            {
                Id = Guid.NewGuid(),
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
                StartTime = Convert.ToDateTime("2016-05-12 10:56:50.230"),
                EndTime = Convert.ToDateTime("2016-05-12 11:56:50.230"),
                Cost = 17,
                UserId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F")
            };
            return timeSheet;
        }
        private List<TimeSheet> GetDummyDatabaseTimeSheet()
        {
            var dummyTimeSheet = new List<TimeSheet>
            {
             new TimeSheet
            {
                Id = Guid.NewGuid(),
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
                StartTime = Convert.ToDateTime("2016-05-12 10:56:50.230"),
                EndTime = Convert.ToDateTime("2016-05-12 11:56:50.230"),
                Cost = 17,
                UserId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F")
            },
             new TimeSheet
            {
                Id = timeSheetId,
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
               StartTime = Convert.ToDateTime("2016-05-12 10:56:50.230"),
                EndTime = Convert.ToDateTime("2016-05-12 11:56:50.230"),
                Cost = 17,
                UserId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F")
            } };
            return dummyTimeSheet;
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_Save_WhenCaseIdIsNotPresent_ThenThrowNotFoundException()
        {
            try
            {
                var dummyCases = GetDummyCase();
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
                sut.Save(GetDummyTimeSheet());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        public void GivenCalling_Delete_WhenDataIsValid_MappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(GetDummyDatabaseTimeSheet());
            try
            {
                sut.Delete(timeSheetId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
