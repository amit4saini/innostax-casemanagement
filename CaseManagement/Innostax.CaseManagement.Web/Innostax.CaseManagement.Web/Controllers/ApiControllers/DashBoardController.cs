﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class DashBoardController : ApiController
    {
        private readonly IDashBoardService _dashBoardService;
        public DashBoardController(IDashBoardService dashBoardService)
        {
            _dashBoardService = dashBoardService;
        }

        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.VIEW_DASHBOARD})]
        public DashBoardModel Get()
        {
           return _dashBoardService.GetDashBoardRelatedData();
        }
    }
}
