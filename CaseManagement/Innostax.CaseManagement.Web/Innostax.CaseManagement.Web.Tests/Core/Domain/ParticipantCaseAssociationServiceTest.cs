﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ParticipantCaseAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private IParticipantCaseAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> userService;
        private Mock<ICaseHistoryService> caseHistoryService;
        private Guid fileId;      
        private Guid caseId;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            userService = new Mock<IUserService>();     
            caseId = new Guid();
            caseHistoryService = new Mock<ICaseHistoryService>();          
            fileId = System.Guid.NewGuid();
            sut = new ParticipantCaseAssociationService(innostaxDbMock.Object, caseHistoryService.Object, errorHandlerService, userService.Object);
        }

        private Models.ParticipantCaseAssociation GetDummyParticipantCaseAssociation()
        {

            var dummyAssociation = new Models.ParticipantCaseAssociation
            {
                Id = Guid.NewGuid(),
                CaseId = caseId,
                IsActive = false,
                UserId = Guid.NewGuid(),
                User= GetDummyModelOfUser()
            };
            return dummyAssociation;
        }
        private User GetDummyDatabaseModelOfUser()
        {

            var dummyAssociation =
                new User
                {
                Id=Guid.NewGuid(),
                Email="test@test" ,                                        
            };
            return dummyAssociation;
        }
        private Innostax.CaseManagement.Models.Account.User GetDummyModelOfUser()
        {

            var dummyAssociation =
                new Models.Account.User
                {
                    Id = Guid.NewGuid(),
                    Email = "test@test",
                };
            return dummyAssociation;
        }

        private List<ParticipantCaseAssociation> GetDummyDatabaseModelOfCaseParticipantAssociation()
        {

            var dummyAssociation = new List<ParticipantCaseAssociation>
            {
                new ParticipantCaseAssociation
                {
                Id=Guid.NewGuid(),
                CaseId=caseId,
                IsActive=false,
                UserId=Guid.NewGuid()
                },
                  new ParticipantCaseAssociation
                {
                Id=Guid.NewGuid(),
                CaseId=caseId,
                IsActive=false,
                UserId=Guid.NewGuid()
                }
            };
            return dummyAssociation;
        }
        private List<UploadedFile> GetDummyDatabaseModelOfUploadedFile()
        {

            var dummyUploadedFile = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileName="test",
                    FileUrl="test",
                IsDeleted=false,
                FileId=fileId ,
                FileType="test"
                }
            };
            return dummyUploadedFile;
        }

        private List<Innostax.Common.Database.Models.Case> GetDummyInvalidDatabaseModelOfCase()
        {
            var dummyCase = new List<Innostax.Common.Database.Models.Case>
            {
                new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                },
                 new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                }, };
            return dummyCase;
        }
        private List<Innostax.Common.Database.Models.Case> GetDummyValidDatabaseModelOfCase()
        {
            var dummyCase = new List<Innostax.Common.Database.Models.Case>
            {
                new Innostax.Common.Database.Models.Case
            {
                CaseId = caseId
                },
                 new Innostax.Common.Database.Models.Case
            {
                CaseId = new Guid("D2BF5024-DBF0-E511-AA98-FCAA16866D0F")
                }, };
            return dummyCase;
        }      


        public List<Guid> GetListOfFileId()
        {
            var dummyListOfFileId = new List<Guid>
            {
                  fileId
                ,
               new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
            };
            return dummyListOfFileId;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenCaseTableDoesNotContainsSpecifiedCaseId_ThenThrowsBadRequest()
        {
            try
            {
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyInvalidDatabaseModelOfCase());
                sut.Save(GetDummyParticipantCaseAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        //[TestMethod]
        //public void GivenCallingSave_WhenCaseTableContainsSpecifiedCaseId_ThenResultReturns()
        //{
        //    innostaxDbMock.Setup(c => c.ParticipantCaseAssociation).ReturnsDbSet(GetDummyDatabaseModelOfCaseParticipantAssociation());
        //    innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
        //    sut.Save(GetDummyParticipantCaseAssociation());
        //}

        [TestMethod]
        public void GivenCallingGetParticipantCaseAssociationByCaseId_WhenItContainsParticipant_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.ParticipantCaseAssociation).ReturnsDbSet(GetDummyDatabaseModelOfCaseParticipantAssociation());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            sut.GetParticipantCaseAssociationByCaseId(caseId);
        }
        [TestMethod]
        public void GivenCallingGetParticipantCaseAssociationByCaseId_WhenItDoesNotContainsParticipant_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.ParticipantCaseAssociation).ReturnsDbSet(GetDummyDatabaseModelOfCaseParticipantAssociation());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyValidDatabaseModelOfCase());
            sut.GetParticipantCaseAssociationByCaseId(caseId);
        }

    }
}
