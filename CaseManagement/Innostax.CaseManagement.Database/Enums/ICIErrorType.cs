﻿
namespace Innostax.Common.Database.Enums
{
   public enum ICIErrorType
    {
        Significant,
        Minor_Typo,
        None,
        NA                
    }
}
