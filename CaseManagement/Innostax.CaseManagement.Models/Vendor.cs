﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class Vendor
    {
        public Guid VendorId { get; set; }
        public string VendorName { get; set; }
        public decimal Cost { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
