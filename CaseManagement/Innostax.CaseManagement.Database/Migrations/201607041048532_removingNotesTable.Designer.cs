// <auto-generated />
namespace Innostax.Common.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class removingNotesTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(removingNotesTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201607041048532_removingNotesTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
