﻿using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseSubStatusService
    {
        void ChangeCaseSubStatus(SubStatus subStatus, Guid caseId);               
    }
    public class CaseSubStatusService: ICaseSubStatusService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly ICommonService _commonService;
        public CaseSubStatusService(IInnostaxDb innostaxDb,IErrorHandlerService errorHandlerService, ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
            
        }
        public void ChangeCaseSubStatus(SubStatus subStatus, Guid caseId)
        {
            _commonService.ValidateAccess(Common.Database.Enums.Validate.CASE,Common.Database.Enums.Permission.CASE_UPDATE_ALL, caseId);
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId);
            if (existingCase != null)
            {
                existingCase.SubStatusId = subStatus.Id;
                _innostaxDb.SaveChangesWithErrors();
            }
            else
            {
                var caseNotFoundError = Innostax.Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(caseNotFoundError); 
            }
            _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_SUBSTATUS_CHANGED, subStatus.Name);
        }
    }
}
