﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Innostax.Common.Database.TenantResolver
{
    public interface ITenantIdentificationStrategy
    {
        string Invoke(IOwinRequest request);
    }

    public class TenantIdentificationStrategy : ITenantIdentificationStrategy
    {
        public string Invoke(IOwinRequest request)
        {
            return GetSubDomain(request.Uri);
        }

        private string GetSubDomain(Uri url)
        {

            if (url.HostNameType == UriHostNameType.Dns)
            {

                string host = url.Host;

                var nodes = host.Split('.');
                int startNode = 0;
                if (nodes[0] == "www") startNode = 1;

                return nodes[startNode];

            }

            return null;
        }
    }
}
