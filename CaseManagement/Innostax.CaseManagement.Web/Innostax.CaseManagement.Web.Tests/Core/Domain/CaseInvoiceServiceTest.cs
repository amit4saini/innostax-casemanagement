﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseInvoiceServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ICaseInvoiceService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> _userService;
        private Mock<ICaseHistoryService> caseHistoryService;
        private Mock<ICommonService> commonService;
        private Guid taskId;
        private Guid fileId;
        private Guid taskFileAssociationId;
        private Guid caseId; 
        private Guid userId;
        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            _userService = new Mock<IUserService>();
            commonService = new Mock<ICommonService>();        
            caseId = new Guid();
            userId = new Guid();
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            caseHistoryService = new Mock<ICaseHistoryService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _userService.Setup(x => x.GetUser(It.IsAny<Guid>())).Returns(new Models.Result<User> { Data = new User { FirstName = "sdfdf", LastName = "dsdfdf" } });           
            taskId = System.Guid.NewGuid();           
            fileId = System.Guid.NewGuid();
            taskFileAssociationId = System.Guid.NewGuid();
            sut = new CaseInvoiceService(innostaxDbMock.Object, _userService.Object, errorHandlerService, commonService.Object, caseHistoryService.Object);
        }
        private List<TimeSheet> GetDummyDatabaseTimeSheet()
        {
            var dummyTimeSheet = new List<TimeSheet>
            {
             new TimeSheet
            {
                Id = Guid.NewGuid(),
                CaseId = caseId,
                StartTime = Convert.ToDateTime("2016-05-12 10:56:50.230"),
                EndTime = Convert.ToDateTime("2016-05-12 11:56:50.230"),
                Cost = 17,
                UserId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F")
            },
             new TimeSheet
            {
                Id = Guid.NewGuid(),
                CaseId = caseId,
                StartTime = Convert.ToDateTime("2016-05-12 10:56:50.230"),
                EndTime = Convert.ToDateTime("2016-05-12 11:56:50.230"),
                Cost = 17,
                UserId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F")
            } };
            return dummyTimeSheet;
        }
        [TestMethod]
        public void GivenCallingGetCaseInvoiceByCaseId_WhenItContainsTimeSheet_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(GetDummyDatabaseTimeSheet());
           var result=sut.GetTimesheetsExpensesForACase(caseId);
            Assert.AreEqual(result.Count, 2);
        }
        [TestMethod]
        public void GivenCallingGetCaseInvoiceByCaseId_WhenItDoesNotContainsTimeSheet_ThenMappedResultReturns()
        {
            innostaxDbMock.Setup(c => c.TimeSheets).ReturnsDbSet(new List<TimeSheet> { });
            var result = sut.GetTimesheetsExpensesForACase(caseId);
            Assert.AreEqual(result.Count, 0);
        }

    }
}
