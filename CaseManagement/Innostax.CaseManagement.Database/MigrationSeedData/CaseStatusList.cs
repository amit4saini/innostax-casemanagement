﻿using Innostax.Common.Database.Models;
using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class CaseStatusList
    {
        public List<CaseStatus> CaseStatus()
        {
            var caseStatus = new List<CaseStatus> {
            new CaseStatus {Status="START" },
            new CaseStatus {Status="CAPTURE" },
            new CaseStatus {Status="UNPROCESSED" },
            new CaseStatus {Status="IN_PROCESS" },
            new CaseStatus {Status="REPORT_TO_SALES" },
            new CaseStatus {Status="CLOSED" },
            new CaseStatus {Status ="ON_HOLD" },
            new CaseStatus {Status="CANCEL" },
            new CaseStatus {Status="INFORMATION_PENDING" },
            new CaseStatus {Status="CLIENT_REQUESTED_UPDATE" },
            new CaseStatus {Status="QC_REVIEW" },
            new CaseStatus {Status="REPORT_TO_CLIENT" },
            new CaseStatus {Status ="APPROVED" },
            new CaseStatus {Status ="SENT_FOR_APPROVAL" },
            new CaseStatus {Status ="READY_FOR_INVOICE" },
            new CaseStatus {Status="NEW" },
            new CaseStatus {Status="PROCESSING" },
            new CaseStatus {Status="REQUEST_FOR_CLOSING" },
            new CaseStatus {Status="REQUESTED" },
            new CaseStatus {Status="SENT_FOR_REVIEW" },
            };
            return caseStatus;
        }
    }
}
