using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Models
{
    public class AnalyticsConfiguration
    {       
        public Guid Id { get; set; }
        public string AnalyticsType { get; set; }
        public string Configuration { get; set; }
        public DateTime CreationDateTime { get; set; }
        public Guid UserId { get; set; }               
        public User User { get; set; }
        public string ReportName { get; set; }
    }
}