﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;
using System.Data.Entity;
using System.Net;
using System.Web.Http;
using System.Text.RegularExpressions;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionService
    {
        List<Guid> Save(Discussion discussionData);
        Task<DiscussionPageResult> GetAllDiscussionsByUserIdOrCaseId(ODataQueryOptions<Discussion> options, HttpRequestMessage request, Guid id);
        Discussion GetDiscussionByDiscussionId(Guid discussionId);
        void ArchiveUnarchiveDiscussion(Guid id, bool toBeArchived);
        Task<DiscussionPageResult> SearchDiscussionsByKeyword(ODataQueryOptions<Discussion> options, HttpRequestMessage request, Guid? caseId, string keyword);
    } 

    public class DiscussionService : IDiscussionService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;
        private IDiscussionNotifierService _discussionNotifierService;
        private readonly IDiscussionFileAssociationService _discussionFileAssociationService;
        private readonly IDiscussionMessageService _discussionMessageService;
        private readonly IDiscussionMessageFileAssociationService _discussionMessageFileAssociationService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly IAzureFileManager _azureFileManager;	
        private readonly ICaseService _caseService;
        private readonly INotificationService _notificationService;
        private readonly ICommonService _commonService;
        private readonly IDiscussionTaggedUserService _discussionTaggedUserService;

        public DiscussionService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, 
            IDiscussionNotifierService discussionNotifierService, 
            IDiscussionFileAssociationService discussionFileAssociationService, 
            IDiscussionMessageService discussionMessageService,
            IDiscussionMessageFileAssociationService discussionMessageFileAssociationService,
            IAzureFileManager azureFileManager,ICaseHistoryService caseHistoryService,
            ICaseService caseService,
            INotificationService notificationService, ICommonService commonService, IDiscussionTaggedUserService discussionTaggedUserService)
        {			       
            _innostaxDb = innostaxDb;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _discussionNotifierService = discussionNotifierService;
            _discussionFileAssociationService = discussionFileAssociationService;
            _discussionMessageService = discussionMessageService;
            _discussionMessageFileAssociationService = discussionMessageFileAssociationService;
            _azureFileManager = azureFileManager;
            _caseHistoryService = caseHistoryService;
            _caseService = caseService;
            _notificationService = notificationService;
            _commonService = commonService;
            _discussionTaggedUserService = discussionTaggedUserService;
        }

        public List<Guid> Save(Discussion discussionData)
        {
            bool isNewDiscussion = false;
            var existingDiscussion = _innostaxDb.Discussions.Include(y=>y.Case).Include(y=>y.CreatedByUser).FirstOrDefault(x => x.Id == discussionData.Id && x.IsDeleted == false);     
            var currentUserId = _userService.UserId;
            if (existingDiscussion == null)
            {
                existingDiscussion = new Innostax.Common.Database.Models.Discussion();
                isNewDiscussion = true;
            }
            _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_UPDATE_ALL, discussionData.CaseId);
            existingDiscussion = Mapper.Map<Innostax.Common.Database.Models.Discussion>(discussionData);
            if (isNewDiscussion)
            {
                existingDiscussion.CreatedBy = currentUserId;
                existingDiscussion.CreationDateTime = _commonService.GetUtcDateTime();
            }
            existingDiscussion.EditedBy = currentUserId;
            existingDiscussion.LastEditedDateTime = _commonService.GetUtcDateTime();
            _innostaxDb.Discussions.AddOrUpdate(existingDiscussion);       
                _innostaxDb.SaveChangesWithErrors();                  
            if (isNewDiscussion)
            {
                _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.CASE_DISCUSSION_ADDED, existingDiscussion.Title);
            }
            else
            {
                _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.CASE_DISCUSSION_EDITED, existingDiscussion.Title);
            }
            var notifiersList = new List<Guid>(discussionData.UserIdList.Concat(discussionData.ParticipantIdList));
            _discussionNotifierService.SaveUsersForADiscussion(existingDiscussion.Id, notifiersList);
            if (discussionData.Message != null)
            {
                var regex = new Regex((@"@\[(.*?)\]"));
                var matchesFound = regex.Matches(discussionData.Message);
                List<User> usersList = new List<User>();
                foreach (Match eachMatch in matchesFound)
                {
                    string userEmail = string.Empty;
                    userEmail = eachMatch.Value;
                    userEmail = userEmail.Replace("@[~", string.Empty).Replace("]", string.Empty);
                    var user = _userService.GetUserByEmail(userEmail);
                    if (user != null)
                    {
                        usersList.Add(user);
                    }
                }                                  
                if (usersList.Count != 0)
                {                
                    _discussionTaggedUserService.SaveDiscussionTaggedUsers(existingDiscussion.Id, usersList.Select(x => x.Id).ToList());
                }
            }
            var discussionFileAssociation = new DiscussionFileAssociation();
            discussionFileAssociation.Files = discussionData.DiscussionFiles.ToList();
            discussionFileAssociation.DiscussionId = existingDiscussion.Id;
            _discussionFileAssociationService.Save(discussionFileAssociation);
            if (isNewDiscussion)
            {
                var mappedDiscussion = Mapper.Map<Discussion>(existingDiscussion);
                mappedDiscussion.UserIdList = discussionData.UserIdList;
                mappedDiscussion.ParticipantIdList = discussionData.ParticipantIdList;
                var caseDetail= _caseService.GetCaseInfoById(discussionData.CaseId);
                mappedDiscussion.Case = caseDetail;
                var createdByUserDetail=_userService.GetUser(_userService.UserId);
                mappedDiscussion.CreatedByUser = createdByUserDetail.Data;
                return SendNotification(mappedDiscussion);
            }
            return new List<Guid>();
        }
    
        private List<Guid> SendNotification(Discussion discussion)
        {
            var listOfUserId = new List<Guid>();
            listOfUserId = discussion.UserIdList;
            foreach (var participantId in discussion.ParticipantIdList)
            {
                listOfUserId.Add(participantId);
            }
            listOfUserId.AddRange(_discussionTaggedUserService.GetDiscussionTaggedUsers(discussion.Id).Select(user => user.Id).ToList());
            listOfUserId = listOfUserId.Distinct().ToList();
            var userName =string.Concat(discussion.CreatedByUser.FirstName , " " , discussion.CreatedByUser.LastName);                   
            var notificationText = Common.Constants.NotificationMessages.DiscussionNotification.NEW_DISCUSSION_ADDED;
            notificationText = string.Format(notificationText, userName, discussion.Title, discussion.Case.CaseNumber, discussion.Case.NickName);
            var URL = _commonService.GetCaseRelatedUrl(NotificationUrl.DISCUSSION, discussion.Case.CaseNumber, discussion.Id);       
            return _notificationService.SaveNotification(notificationText,listOfUserId,URL);
        }     

        //private void SendNewDiscussionEmail(Discussion discussion)
        //{
        //    var newDiscussionEmailModel = new NewDiscussionEmailModel();
        //    newDiscussionEmailModel.DiscussionTitle = discussion.Title;
        //    var discussionAssociatedCase = _caseService.GetCaseInfoById(discussion.CaseId);
        //    newDiscussionEmailModel.CaseNumber = discussionAssociatedCase.CaseNumber;
        //    var startedByUser = _userService.GetUser(discussion.CreatedBy).Data;
        //    newDiscussionEmailModel.StartedByUser = startedByUser.FirstName + " " + startedByUser.LastName;
        //    newDiscussionEmailModel.RecipientMails = new List<string>();
        //    newDiscussionEmailModel.RecipientNames = new List<string>();
        //    if (discussion.UserIdList.Count == 0) { return; }
        //    foreach (var userId in discussion.UserIdList)
        //    {
        //        var recipient = _userService.GetUser(userId).Data;
        //        newDiscussionEmailModel.RecipientMails.Add(recipient.Email);
        //        newDiscussionEmailModel.RecipientNames.Add(recipient.FirstName + " " + recipient.LastName);
        //    }
        //    foreach (var participantId in discussion.ParticipantIdList)
        //    {
        //        var recipient = _userService.GetUser(participantId).Data;
        //        newDiscussionEmailModel.RecipientMails.Add(recipient.Email);
        //        newDiscussionEmailModel.RecipientNames.Add(recipient.FirstName + " " + recipient.LastName);
        //    }
        //    _mailSenderService.SendNewDiscussionDetails(newDiscussionEmailModel);
        //}

        public async Task<DiscussionPageResult> GetAllDiscussionsByUserIdOrCaseId(ODataQueryOptions<Discussion> options, HttpRequestMessage request, Guid id)
        {
            var allDiscussions = new List<Common.Database.Models.Discussion>();
            if (id == new Guid())
            {
                allDiscussions = _innostaxDb.DiscussionNotifiers.Include(y => y.Discussion).Where(x => x.UserId == _userService.UserId && x.Discussion.IsDeleted != true).Select(x => x.Discussion).ToList();
            }
            else
            {
                allDiscussions = _commonService.GetAuthorizedDiscussions(id).Where(x=>x.IsDeleted == false).ToList();
            }
            return await GetDiscussionsData(options, request, allDiscussions);
        }

        public Discussion GetDiscussionByDiscussionId(Guid discussionId)
        {
            var discussion = new Discussion();
            discussion = Mapper.Map<Discussion>(_innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionId && x.IsDeleted != true));
            if (discussion==null)
            {
                _errorHandlerService.ThrowResourceNotFoundError("Discussion with requested Id not found");
            }
            _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, discussionId);
            var existingFiles = _discussionFileAssociationService.GetDiscussionAssociatedFiles(discussionId);
            discussion.DiscussionFiles = existingFiles.ToArray();
            discussion.UserIdList = new List<Guid>();
            discussion.ParticipantIdList = new List<Guid>();
            var allUserIds= _discussionNotifierService.GetNotifiersListForDiscussion(discussionId);
            foreach(var eachUserId in allUserIds)
            {
                var roleId = _innostaxDb.UserRoles.FirstOrDefault(x => x.UserId == eachUserId).RoleId;
                var roleName = _innostaxDb.Roles.FirstOrDefault(x => x.Id == roleId).Name;
                if(roleName != Roles.Client_Participant.ToString())
                {
                    discussion.UserIdList.Add(eachUserId);
                }
                else
                {
                    discussion.ParticipantIdList.Add(eachUserId);
                }
            }
            return discussion;
        }

        public void ArchiveUnarchiveDiscussion(Guid id, bool toBeArchived)
        {
            var existingDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == id && x.IsDeleted != true);
            if (existingDiscussion == null)
                _errorHandlerService.ThrowBadRequestError(Common.Constants.ErrorMessages.DiscussionManagement.DiscussionMessageError.DISCUSSION_MESSAGE_ID_INVALID);
            else
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_DELETE_ALL, id); ;
                existingDiscussion.IsDeleted = toBeArchived;
                if (toBeArchived)
                {
                    _discussionFileAssociationService.RemoveDiscussionFileAssociation(id);
                    existingDiscussion.DeletedBy = _userService.UserId;                 
                    existingDiscussion.DeletedOn = _commonService.GetUtcDateTime();
                    _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.CASE_DISCUSSION_ARCHIVED, existingDiscussion.Title);
                }
                else
                {
                    _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.CASE_DISCUSSION_UNARCHIVED, existingDiscussion.Title);
                }          
                _innostaxDb.SaveChangesWithErrors();           
            }      
        }

        public async Task<DiscussionPageResult> SearchDiscussionsByKeyword(ODataQueryOptions<Discussion> options, HttpRequestMessage request, Guid? caseId, string keyword)
        {
            var allDiscussions = new List<Common.Database.Models.Discussion>();
            var authorizedDiscussions = _commonService.GetAuthorizedDiscussions(null).Where(x=>x.IsDeleted != true);
            allDiscussions = authorizedDiscussions.Include(x => x.Case).Include(x => x.Case.Client).Include(x => x.Case.Client.SalesRepresentative).Include(x => x.Case.ClientContactUser).Where(x => ((x.Title ?? "").Contains(keyword) || (x.Message ?? "").Contains(keyword)) && x.IsDeleted == false && x.Case.IsDeleted == false && (caseId != null ? x.CaseId == caseId : true)).Union(_innostaxDb.DiscussionMessages.Include(x => x.Discussion).Include(x => x.Discussion.Case).Include(x => x.Discussion.Case.Client).Include(x => x.Discussion.Case.Client.SalesRepresentative).Include(x => x.Discussion.Case.ClientContactUser).Where(x => authorizedDiscussions.Contains(x.Discussion) && ((x.Message ?? "").Contains(keyword) && x.IsDeleted == false && x.Discussion.IsDeleted == false && x.Discussion.Case.IsDeleted == false && (caseId != null ? x.Discussion.CaseId == caseId : true))).Select(x => x.Discussion)).ToList();
            if (caseId == null)
            {
                return new DiscussionPageResult { Result = new PageResult<Models.Discussion>(Mapper.Map<List<Discussion>>(allDiscussions).AsEnumerable(), request.ODataProperties().NextLink, allDiscussions.Count) };
            }
            return await GetDiscussionsData(options, request, allDiscussions);
        }

        internal async Task<DiscussionPageResult> GetDiscussionsData(ODataQueryOptions<Discussion> options, HttpRequestMessage request, List<Innostax.Common.Database.Models.Discussion> discussionsList)
        {
            var result = new List<Discussion>();
            Dictionary<Guid, string> profilePicKeyUrlList = new Dictionary<Guid, string>();
            foreach (var eachDiscussion in discussionsList)
            {
                if (_userService.GetCurrentUser().Role.Name == Roles.Client_Participant.ToString())
                {
                    var participantCaseAssociation = _innostaxDb.ParticipantCaseAssociation.FirstOrDefault(x => x.CaseId == eachDiscussion.CaseId);
                    if (participantCaseAssociation != null)
                    {
                        if (participantCaseAssociation.IsActive == false)
                            continue;
                    }
                }
                var eachUser = _innostaxDb.Users.FirstOrDefault(x => x.Id == eachDiscussion.CreatedBy);
                if (eachUser == null)
                {
                    string userNotFoundError = Common.Constants.ErrorMessages.UserManagement.UserError.USER_NOT_FOUND;
                    _errorHandlerService.ThrowResourceNotFoundError(userNotFoundError);
                }
                var existingDiscussionRelatedFiles = _discussionFileAssociationService.GetDiscussionAssociatedFiles(eachDiscussion.Id);
                var recentMessage = _discussionMessageService.GetRecentDiscussionMessageByDiscussionId(eachDiscussion.Id);
                var discussionMessages = _innostaxDb.DiscussionMessages.Where(x => x.DiscussionId == eachDiscussion.Id && x.IsDeleted == false).ToList();
                var mappedDiscussion = Mapper.Map<Models.Discussion>(eachDiscussion);
                mappedDiscussion.DiscussionFiles = existingDiscussionRelatedFiles.ToArray();
                mappedDiscussion.DiscussionAndDiscussionMessagesFiles = existingDiscussionRelatedFiles;
                foreach (var eachDiscussionMessage in discussionMessages)
                {
                    var existingDiscussionMessageRelatedFiles = _discussionMessageFileAssociationService.GetDiscussionMessageAssociatedFiles(eachDiscussionMessage.Id);
                    if (existingDiscussionMessageRelatedFiles != null)
                    {
                        foreach (var eachDiscussionMessageRelatedFiles in existingDiscussionMessageRelatedFiles)
                        {
                            mappedDiscussion.DiscussionAndDiscussionMessagesFiles.Add(eachDiscussionMessageRelatedFiles);
                        }
                    }
                }
                mappedDiscussion.ParticipantIdList = new List<Guid>();
                mappedDiscussion.UserIdList = new List<Guid>();
                var allUserIds = _discussionNotifierService.GetNotifiersListForDiscussion(mappedDiscussion.Id).ToList();
                foreach (var eachUserId in allUserIds)
                {
                    var roleId = _innostaxDb.UserRoles.FirstOrDefault(x => x.UserId == eachUserId).RoleId;
                    var roleName = _innostaxDb.Roles.FirstOrDefault(x => x.Id == roleId).Name;
                    if (roleName != Roles.Client_Participant.ToString())
                    {
                        mappedDiscussion.UserIdList.Add(eachUserId);
                    }
                    else
                    {
                        mappedDiscussion.ParticipantIdList.Add(eachUserId);
                    }
                }
                mappedDiscussion.User = Mapper.Map<Models.Account.User>(eachUser);
                mappedDiscussion.User.ProfilePicUrl = string.Empty;
                Guid profilePicKey = mappedDiscussion.User.ProfilePicKey ?? Guid.Empty;
                if (profilePicKey != Guid.Empty)
                {
                    if (profilePicKeyUrlList.ContainsKey(profilePicKey))
                    {
                        mappedDiscussion.User.ProfilePicUrl = profilePicKeyUrlList[profilePicKey];
                    }
                    else
                    {
                        mappedDiscussion.User.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
                        profilePicKeyUrlList.Add(profilePicKey, mappedDiscussion.User.ProfilePicUrl);
                    }
                }
                else
                    mappedDiscussion.User.ProfilePicUrl = Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
                if (recentMessage != null)
                    mappedDiscussion.RecentMessage = recentMessage.Message;
                else
                    mappedDiscussion.RecentMessage = "";
                mappedDiscussion.MessageCount = discussionMessages.Count;
                var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == eachDiscussion.CaseId);
                if (existingCase != null)
                {
                    mappedDiscussion.CaseNumber = existingCase.CaseNumber;
                }              
                result.Add(mappedDiscussion);
            }
            var discussionPageResult = new DiscussionPageResult();
            discussionPageResult.TotalDiscussionCount = result.Count;
            int resultCount = discussionPageResult.TotalDiscussionCount;
            if (options.Filter != null)
                resultCount = (options.Filter.ApplyTo(result.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Discussion>).ToList().Count;
            IQueryable results = options.ApplyTo(result.AsQueryable());
            discussionPageResult.Result = new PageResult<Discussion>(
            results as IEnumerable<Discussion>, request.ODataProperties().NextLink,
            resultCount);
            return discussionPageResult;
        }
    }
}
