﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class StateController : ApiController
    {
        private readonly IStateService _stateService;
        public StateController(IStateService stateService)
        {
            _stateService = stateService;
        }

        // GET api/<controller>/5    
        [Route("api/Country/{id}/States")]
        public List<Innostax.CaseManagement.Models.State> Get(string id)
        {
            var listOfStatesBasedOnCountryIdResult = _stateService.ListOfStatesBasedOnCountryId(id);
            return listOfStatesBasedOnCountryIdResult;
        }
    }
}