﻿using Innostax.CaseManagement.Core.Domain;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class IndustryController : ApiController
    {
        private readonly IIndustryService _industryService;
        public IndustryController(IIndustryService industryService)
        {
            _industryService = industryService;
        }

        [System.Web.Http.Route("api/Industries")]
        public List<Models.Industry> Get()
        {
            return _industryService.GetAllIndustries();
        }
       
        [System.Web.Mvc.HttpGet]      
        [System.Web.Http.Route("api/Industry/{industryId}")]
        public Models.Industry Get(string industryId)
        {
            return _industryService.GetIndustry(industryId);
        }

    }
}