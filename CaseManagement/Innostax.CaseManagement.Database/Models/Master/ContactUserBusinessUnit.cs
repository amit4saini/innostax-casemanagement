﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class ContactUserBusinessUnit
    {      
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid ClientContactUserId { get; set; }
        [ForeignKey("ClientContactUserId")]
        public ClientContactUser ClientContactUser { get; set; }
        public string BusinessUnit { get; set; }
        public string AccountCode { get; set; }
        public DateTime? CreationDateTime { get; set; }
    }
}
