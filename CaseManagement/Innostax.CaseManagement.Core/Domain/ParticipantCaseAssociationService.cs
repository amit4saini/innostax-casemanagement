﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IParticipantCaseAssociationService
    {
        void Save(ParticipantCaseAssociation participantCaseAssociationData);
        List<ParticipantCaseAssociation> GetParticipantCaseAssociationByCaseId(Guid caseId);
        List<Case> GetCasesByParticipantId(Guid participantId);
    }

    public class ParticipantCaseAssociationService : IParticipantCaseAssociationService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private ICaseHistoryService _caseHistoryService;
        private IErrorHandlerService _errorHandlerService;

        public ParticipantCaseAssociationService(IInnostaxDb innostaxDb, ICaseHistoryService caseHistoryService, IErrorHandlerService errorHandlerService, IUserService userService)
        {
            _innostaxDb = innostaxDb;
            _caseHistoryService = caseHistoryService;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
        }

        public List<ParticipantCaseAssociation> GetParticipantCaseAssociationByCaseId(Guid caseId)
        {
            return Mapper.Map<List<ParticipantCaseAssociation>>(_innostaxDb.ParticipantCaseAssociation.Include(x => x.User).Where(x => x.CaseId == caseId).ToList());
        }

        public void Save(ParticipantCaseAssociation participantUser)
        {
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == participantUser.CaseId && x.IsDeleted == false);
            if (existingCase == null)
            {
                string caseNotFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(caseNotFoundError);
            }
            var userEmail = participantUser.User.Email;
            var existingParticipantUser = _innostaxDb.ParticipantCaseAssociation.FirstOrDefault(x => x.CaseId == participantUser.CaseId && x.UserId == participantUser.UserId);
            if (participantUser.IsBeingAdded) {
                
                if (existingParticipantUser != null)
                {
                    string participantForCaseExistError = Common.Constants.ErrorMessages.Common.ParticipantCaseAssociationError.PARTICIPANT_EXISTS_FOR_CASE;
                    _errorHandlerService.ThrowBadRequestError(participantForCaseExistError);
                }
            }
            else
            {
                _userService.SaveUser(participantUser.User);
            }
            participantUser.User = null;
            existingParticipantUser = Mapper.Map<Common.Database.Models.ParticipantCaseAssociation>(participantUser);
            _innostaxDb.ParticipantCaseAssociation.AddOrUpdate(existingParticipantUser);
            _innostaxDb.SaveChangesWithErrors();
            if (participantUser.IsBeingAdded)
                    _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_PARTICIPANT_ADDED, "Added a participant: " + userEmail);
                else
                    _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_PARTICIPANT_EDITED, "Edited a participant: " + userEmail);            
        }

        public List<Case> GetCasesByParticipantId(Guid participantId)
        {
            var result = new List<Case>();
            var existingParticipantCaseAssociations =_innostaxDb.ParticipantCaseAssociation.Where(x => x.UserId == participantId && x.IsActive == true).ToList();
            foreach (var participantCaseAssociation in existingParticipantCaseAssociations)
            {
                var existingCase = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.ClientContactUser).FirstOrDefault(x => x.CaseId == participantCaseAssociation.CaseId);
                var mappedCase = Mapper.Map<Models.Case>(existingCase);
                result.Add(mappedCase);
            }
            return result;
        }
    }
}
