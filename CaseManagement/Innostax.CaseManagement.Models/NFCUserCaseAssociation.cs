﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innostax.CaseManagement.Models
{
    public class NFCUserCaseAssociation
    {
        public int Id { get; set; }
        public Guid CaseId { get; set; }
        public Case Case { get; set; }
        public Guid NFCUserId { get; set; }
        public bool IsActive { get; set; }
        public Account.User User { get; set; }
        public bool IsBeingAdded { get; set; }
    }
}
