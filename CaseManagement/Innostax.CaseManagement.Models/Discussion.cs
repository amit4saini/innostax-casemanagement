﻿using Innostax.CaseManagement.Models.Account;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class Discussion
    {             
        public Guid Id { get; set; }      
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime CreationDateTime { get; set; }
        public FileModel[] DiscussionFiles { get; set; }
        public Guid CaseId { get; set; }
        public string CaseNumber { get; set; }
        public Case Case{get;set;}
        public Guid CreatedBy { get; set; }  
        public User User { get; set; }
        public DateTime LastEditedDateTime { get; set; }
        public Guid EditedBy { get; set; }               
        public bool IsDeleted { get; set; }
        public Guid? DeletedBy { get; set; }  
        public DateTime? DeletedOn { get; set; } 
        public List<Guid> UserIdList { get; set; }
        public List<Guid> ParticipantIdList { get; set; } 
        public bool IsArchive { get; set; }
        public string RecentMessage { get; set; }
        public int MessageCount { get; set; }
        public bool IsNotes { get; set; }
        public List<FileModel> DiscussionAndDiscussionMessagesFiles { get; set; }
        public User CreatedByUser { get; set; }
        public List<Guid> taggedUserIdList { get; set; }
    }
}
