﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class DueTaskDetails
    {
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string TaskName { get; set; }
        public DateTime DueDate { get; set; }
        public string UserEmailAddress { get; set; }
        public bool HasPassedDueDate { get; set; }
        public Case Case { get; set; }
        public Guid TaskId { get; set; }
    }
}
