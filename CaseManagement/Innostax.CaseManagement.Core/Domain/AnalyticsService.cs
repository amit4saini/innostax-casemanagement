﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Models.Account;
using System.Web.Configuration;
using Innostax.Common.Database.Database;
using AutoMapper;
using System.Web.Http.OData.Extensions;
using System.Data.Entity;
using System.Web.Http.OData.Builder;
using System.Data;
using System.Data.Entity.Migrations;
using System.IO;
using System.ComponentModel;
using System.Web;
using System.Net;
using System.Net.Http.Headers;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IAnalyticsService
    {
        PageResult<Case> GetCaseAnalytics(ODataQueryOptions<Case> options, HttpRequestMessage request, DateTime? startDate, DateTime? endDate);
        UserAnayticsModel GetUserAnalytics(Guid userId, Guid? caseId);      
        void SaveAnalyticsReport(AnalyticsConfiguration configuration);
        AnalyticsConfiguration GetAnalyticsReportById(Guid userId, Guid reportId);
        List<AnalyticsConfiguration> GetAllAnalyticsReportsForUser(Guid userId);
        string ExportCurrentAnalyticsToExcel(string[] currentColumns, string analyticsType, DateTime? startDate, DateTime? endDate);
         PageResult<Client> GetClientAnalytics(ODataQueryOptions<Client> options, HttpRequestMessage request, DateTime? startDate, DateTime? endDate);
    }

    public class AnalyticsService : IAnalyticsService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IUserService _userService;
        private readonly ICaseService _caseService;
        private readonly ITaskService _taskService;
        private readonly ICommonService _commonService;       

        public AnalyticsService(IInnostaxDb innostaxDb, IUserService userService, ICaseService caseService, ITaskService taskService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
            _caseService = caseService;
            _taskService = taskService;
            _commonService = commonService;         
        }

        public PageResult<Case> GetCaseAnalytics(ODataQueryOptions<Case> options, HttpRequestMessage request,DateTime? startDate,DateTime? endDate)
        {
            Dictionary<Guid, List<Models.Task>> listOfTasks=new Dictionary<Guid, List<Models.Task>>();
            Dictionary<Guid, List<CaseExpense>> listOfExpenses = new Dictionary<Guid, List<CaseExpense>>();
            Dictionary<Guid, List<TimeSheet>> listOfTimesheets = new Dictionary<Guid, List<TimeSheet>>();
            Dictionary<Guid, User> listOfCaseClosingUsers = new Dictionary<Guid, User>();
            var mappedTasks = Mapper.Map<List<Models.Task>>(_innostaxDb.Tasks.Include(x => x.AssignedToUser).Include(x => x.CreatedByUser).Where(x => x.IsDeleted == false).ToList());
            var mappedExpenses = Mapper.Map <List<CaseExpense>>(_innostaxDb.CaseExpenses.Include(x=>x.Vendor).Include(x =>x.CreatedByUser).Where(x => x.IsDeleted == false).ToList());
            var listOfTimesheet = _innostaxDb.TimeSheets.Include(x => x.CreatedByUser).Include(x => x.Description).Include(x => x.User).Include(x => x.User.Roles).Where(x => x.IsDeleted == false).ToList();
            var mappedTimesheets = Mapper.Map<List<TimeSheet>>(listOfTimesheet);
            foreach (var timesheet in listOfTimesheet)
            {                
                    var timeSheetId = timesheet.Id;
                    var roles = timesheet.User.Roles;
                    if (roles.Count != 0)
                    {
                        var roleId = roles.FirstOrDefault().RoleId;
                        var role = _innostaxDb.Roles.FirstOrDefault(x => x.Id == roleId);
                        mappedTimesheets.FirstOrDefault(x => x.Id == timesheet.Id).User.Role = Mapper.Map<Models.Role>(role);
                    }                           
            }
            var caseReportAssociations = _innostaxDb.CaseReportAssociations.Include(y => y.Country).Include(x=>x.Report).Where(x=>x.IsDeleted != true).ToList();
            var caseHistoryForClosedCases = _innostaxDb.CaseHistory.Where(x => x.NewValue == "Closed").ToList();
            var mappedCaseReportAssociation = Mapper.Map<List<CaseReportAssociation>>(caseReportAssociations);
            var mappedUsers = Mapper.Map<List<User>>(_innostaxDb.Users.ToList());
            caseHistoryForClosedCases.ForEach(x => {
                listOfCaseClosingUsers[x.CaseId] = mappedUsers.FirstOrDefault(y => y.Id == x.UserId);
            });
            mappedTasks.ForEach(x => {
                if (!listOfTasks.Keys.Contains(x.CaseId))
                    listOfTasks[x.CaseId] = new List<Models.Task>();
                listOfTasks[x.CaseId].Add(x);
            });
            mappedExpenses.ForEach(x => {
                if (!listOfExpenses.Keys.Contains(x.CaseId))
                    listOfExpenses[x.CaseId] = new List<CaseExpense>();
                listOfExpenses[x.CaseId].Add(x);
            });
            mappedTimesheets.ForEach(x => {
                if (!listOfTimesheets.Keys.Contains(x.CaseId))
                    listOfTimesheets[x.CaseId] = new List<TimeSheet>();
                listOfTimesheets[x.CaseId].Add(x);
            });
            var existingCases = new List<Common.Database.Models.Case>();
            if (endDate.HasValue && startDate.HasValue)
            {
                existingCases = _innostaxDb.Cases.Include(x => x.Client).Include(x=>x.ICIAssignedUser).Include(x => x.Client.SalesRepresentative).Include(x => x.CreatedByUser).Include(x=>x.ClientContactUser).Include(x=>x.BusinessUnit).Include(x => x.CaseStatus).Include(x=>x.IHIAssignedUser).Include(x=>x.ReviewedByUser).Include(x=>x.DDSupervisorWhoApprovedCaseByUser).Where(x => x.IsDeleted == false && DbFunctions.TruncateTime(x.CreationDateTime) >= startDate.Value && DbFunctions.TruncateTime(x.CreationDateTime) <= DbFunctions.TruncateTime(endDate.Value)).ToList();
            }
            else if (startDate.HasValue)
                {
                    existingCases = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.ICIAssignedUser).Include(x => x.Client.SalesRepresentative).Include(x => x.CreatedByUser).Include(x => x.ClientContactUser).Include(x => x.BusinessUnit).Include(x => x.CaseStatus).Include(x => x.IHIAssignedUser).Include(x => x.ReviewedByUser).Include(x => x.DDSupervisorWhoApprovedCaseByUser).Where(x => x.IsDeleted == false && DbFunctions.TruncateTime(x.CreationDateTime) >= DbFunctions.TruncateTime(startDate.Value)).ToList();
                }
            else
            {
                existingCases = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.ICIAssignedUser).Include(x => x.Client.SalesRepresentative).Include(x => x.CreatedByUser).Include(x => x.ClientContactUser).Include(x => x.BusinessUnit).Include(x => x.CaseStatus).Include(x => x.IHIAssignedUser).Include(x => x.ReviewedByUser).Include(x => x.DDSupervisorWhoApprovedCaseByUser).Where(x => x.IsDeleted == false).ToList();
            }      
           var mappedCases = Mapper.Map<List<Case>>(existingCases);
            foreach(var eachCase in mappedCases)
            {
                var caseReportAssociation = caseReportAssociations.FirstOrDefault(x => x.CaseId == eachCase.CaseId && x.IsPrimaryReport == true);
                var caseHistoryForEachCase = caseHistoryForClosedCases.FirstOrDefault(x => x.CaseId == eachCase.CaseId);
                if (caseReportAssociation != null && caseReportAssociation.Country != null)
                {
                    eachCase.PrimaryReportCountryName =caseReportAssociation.Country.CountryName;
                }
                eachCase.Margin = _caseService.CalculateCaseMargin(eachCase).Margin;
                eachCase.TurnTimeForICIBusinessDays = _caseService.CalculateICITurnTime(eachCase).TurnTimeForICIBusinessDays;
                eachCase.ReportToClientAheadOfTime = _caseService.CalculateReportToClientAheadOfTime(eachCase).ReportToClientAheadOfTime;
                if (caseHistoryForEachCase != null)
                {
                    eachCase.ClosingDateTime = caseHistoryForEachCase.CreationDatetime;
                    eachCase.UserWhoClosedCase = listOfCaseClosingUsers[eachCase.CaseId];
                }
                eachCase.Tasks = listOfTasks.Keys.Contains(eachCase.CaseId) ? listOfTasks[eachCase.CaseId] : new List<Models.Task>();
                eachCase.Expenses = listOfExpenses.Keys.Contains(eachCase.CaseId) ? listOfExpenses[eachCase.CaseId] : new List<CaseExpense>();
                eachCase.Timesheets = listOfTimesheets.Keys.Contains(eachCase.CaseId) ? listOfTimesheets[eachCase.CaseId] : new List<TimeSheet>();
                var reports = new List<Report>();
                var caseAssociatedReports= caseReportAssociations.Where(x => x.CaseId == eachCase.CaseId).ToList();
                foreach (var report in caseAssociatedReports)
                {
                    var mappedReport = Mapper.Map<Report>(report.Report);
                    mappedReport.IsPrimary = report.IsPrimaryReport;
                    reports.Add(mappedReport);
                }
                eachCase.Reports = reports.ToArray();
            }
            IQueryable results = options.ApplyTo(mappedCases.AsQueryable());
            var resultCount = mappedCases.Count;
            if (options.Filter != null)
            {
                resultCount = (options.Filter.ApplyTo(mappedCases.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Case>).ToList().Count;
            }
            return new PageResult<Case>(
            results as IEnumerable<Case>, request.ODataProperties().NextLink,
            resultCount);
        }

        public PageResult<Client> GetClientAnalytics(ODataQueryOptions<Client> options, HttpRequestMessage request, DateTime? startDate, DateTime? endDate)
        {
            var allClients = new List<Common.Database.Models.Client>();
            if (startDate.HasValue && endDate.HasValue)
            {
                allClients = _innostaxDb.Clients.Include(y => y.SalesRepresentative).Where(x => DbFunctions.TruncateTime(x.CreationDateTime) >= startDate.Value && DbFunctions.TruncateTime(x.CreationDateTime) <= DbFunctions.TruncateTime(endDate.Value) && x.IsDeleted == false).ToList();
            }
            else if (startDate.HasValue)
            {
                allClients = _innostaxDb.Clients.Include(y => y.SalesRepresentative).Where(x => DbFunctions.TruncateTime(x.CreationDateTime) >= startDate.Value && x.IsDeleted == false).ToList();
            }
            else
            {
                allClients = _innostaxDb.Clients.Include(y => y.SalesRepresentative).Where(x => x.IsDeleted == false).ToList();
            }
            var mappedClient = Mapper.Map<List<Models.Client>>(allClients);        
            IQueryable results = options.ApplyTo(mappedClient.AsQueryable());
            var resultCount = 0;
            foreach (var eachClient in mappedClient)
            {
                var existingCasesForClient = _innostaxDb.Cases.Where(x => x.ClientId == eachClient.ClientId).OrderByDescending(x => x.CreationDateTime);
                if (existingCasesForClient.Count() > 0)
                {
                    eachClient.LatestCaseOrderDate = existingCasesForClient.First().CreationDateTime;
                }
            }
            if (options.Filter != null)
            {
                resultCount = (options.Filter.ApplyTo(allClients.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Client>).ToList().Count;
            }
            return new PageResult<Client>(
            results as IEnumerable<Client>, request.ODataProperties().NextLink,
            resultCount);
        }

        public UserAnayticsModel GetUserAnalytics(Guid userId, Guid? caseId)
        {
            var userAnalytics = new UserAnayticsModel();
            var numberOfCasesCreatedByAUser = _innostaxDb.Cases.Count(x => x.CreatedBy == userId);
            var taskDelayed = caseId == null ? _innostaxDb.Tasks.Where(x => x.AssignedTo == userId && x.DueDate < _commonService.GetUtcDateTime()).ToList() : _innostaxDb.Tasks.Where(x => x.AssignedTo == userId && x.CaseId == caseId && x.DueDate < DateTime.Now).ToList();
            var timeSheetForAUser = _innostaxDb.TimeSheets.Where(x => x.UserId == userId).ToList();
            var expensesOccuredForAUser = _innostaxDb.CaseExpenses.Where(x => x.CreatedBy == userId);
            userAnalytics.TotalCasesCreatedByAUser = numberOfCasesCreatedByAUser;
            userAnalytics.ListOfTaskDelayed = Mapper.Map<List<Models.Task>>(taskDelayed);
            userAnalytics.ListOfCaseExpense = Mapper.Map<List<Models.CaseExpense>>(expensesOccuredForAUser);
            userAnalytics.ListOfTimesheet = Mapper.Map<List<Models.TimeSheet>>(timeSheetForAUser);
            userAnalytics.UserName = _userService.GetUser(userId).Data.UserName;
            return userAnalytics;
        }

        public void SaveAnalyticsReport(AnalyticsConfiguration newConfiguration)
        {           
            var existingConfiguration = _innostaxDb.AnalyticsConfigurations.FirstOrDefault(x => x.UserId == _userService.UserId && x.Id == newConfiguration.Id);
            if(existingConfiguration == null)
            {
                existingConfiguration = new Common.Database.Models.AnalyticsConfiguration();
                existingConfiguration.UserId = _userService.UserId;
                existingConfiguration.AnalyticsType = newConfiguration.AnalyticsType;                
            }
            existingConfiguration.Configuration = newConfiguration.Configuration;
            existingConfiguration.CreationDateTime = _commonService.GetUtcDateTime();
            existingConfiguration.ReportName = newConfiguration.ReportName;
            _innostaxDb.AnalyticsConfigurations.AddOrUpdate(existingConfiguration);                             
            _innostaxDb.SaveChangesWithErrors();
        }

        public AnalyticsConfiguration GetAnalyticsReportById(Guid userId, Guid reportId)
        {
            var existingConfig = _innostaxDb.AnalyticsConfigurations.FirstOrDefault(x => x.UserId == userId && x.Id == reportId);                                   
            return Mapper.Map<AnalyticsConfiguration>(existingConfig);            
        }

        public List<AnalyticsConfiguration> GetAllAnalyticsReportsForUser(Guid userId)
        {
            var existingAnalyticsReports = _innostaxDb.AnalyticsConfigurations.Where(x => x.UserId == userId).ToList();
            return Mapper.Map<List<AnalyticsConfiguration>>(existingAnalyticsReports);                        
        }

        public string ExportCurrentAnalyticsToExcel(string[] currentColumns, string analyticsType,DateTime? startDate,DateTime? endDate)
       {   
            var dataTable =new DataTable();
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();                            
                modelBuilder.EntitySet<Models.Case>("Case");
                ODataQueryOptions<Models.Case> options = new ODataQueryOptions<Models.Case>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Case)), new HttpRequestMessage());                     
                var caseAnalyticsResult = GetCaseAnalytics(options, new HttpRequestMessage(), startDate, endDate).Items.ToList();            
            switch (analyticsType)
            {
                case "Case": dataTable = ConvertCaseAnalyticsToDataTable(caseAnalyticsResult, currentColumns); break;
                case "Task": dataTable = ConvertTaskAnalyticsToDataTable(caseAnalyticsResult, currentColumns); break;
                case "Expense": dataTable = ConvertExpenseAnalyticsToDataTable(caseAnalyticsResult, currentColumns); break;
                case "Timesheet": dataTable = ConvertTimesheetAnalyticsToDataTable(caseAnalyticsResult, currentColumns); break;
                case "Report":dataTable = ConvertReportAnalyticsToDataTable(caseAnalyticsResult, currentColumns);break;
                case "Client":
                    ODataModelBuilder clientModelBuilder = new ODataConventionModelBuilder();
                    clientModelBuilder.EntitySet<Models.Client>("Client");
                    ODataQueryOptions<Models.Client> clientOptions = new ODataQueryOptions<Models.Client>(new ODataQueryContext(clientModelBuilder.GetEdmModel(), typeof(Models.Client)), new HttpRequestMessage());
                    var clientAnalyticsResult = GetClientAnalytics(clientOptions, new HttpRequestMessage(),startDate,endDate).Items.ToList();
                    dataTable = ConvertClientAnalyticsToDataTable(clientAnalyticsResult,currentColumns);break;
            }                              
            var fileName = string.Concat("Analytics",Guid.NewGuid().ToString(),".xlsx");            
            ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PivotDataExcel", fileName);
            wbook.Worksheets.Add(dataTable, "tab1");
            wbook.SaveAs(fullPath);
            return fileName;
        }

        public DataTable ConvertClientAnalyticsToDataTable(List<Models.Client> clientAnalyticsResult, string[] currentColumns)
        {
            DataTable table = new DataTable();         
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Client clientData in clientAnalyticsResult)
            {              
                DataRow row = table.NewRow();
                for (int counter = 0; counter < currentColumns.Count(); counter++)
                {                                    
                  row[currentColumns[counter]] = fetchClientTypeValue(clientData, currentColumns[counter]);                    
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public DataTable ConvertCaseAnalyticsToDataTable(List<Models.Case> data, string[] currentColumns)
        {           
            DataTable table = new DataTable();
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {               
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Case caseData in data)
            {
                    var commonFields = new List<String> { "CaseNumber", "CaseNickname","Nickname","Client", "Country", "ICIAssignedUser", "SubjectName", "FinalReportSentToClientDate" };               
                    DataRow row = table.NewRow();
                for (int counter = 0; counter < currentColumns.Count(); counter++)
                {
                    if (commonFields.Contains(currentColumns[counter]))
                    {
                        row[currentColumns[counter]] = fetchCommonValue(caseData, currentColumns[counter]);
                    }
                    else
                    {
                        row[currentColumns[counter]] = fetchCaseTypeValue(caseData, currentColumns[counter]);
                    }
                }                    
                    table.Rows.Add(row);              
            }
            return table;
        }

        public DataTable ConvertTaskAnalyticsToDataTable(List<Models.Case> data, string[] currentColumns)
        {
            DataTable table = new DataTable();
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Case caseData in data)
            {
                var commonFields = new List<String> { "CaseNumber", "CaseNickname", "Nickname", "Client", "Country", "ICIAssignedUser", "SubjectName", "FinalReportSentToClientDate" };
                foreach (var task in caseData.Tasks)
                {
                    DataRow row = table.NewRow();
                    for (int counter = 0; counter < currentColumns.Count(); counter++)
                    {
                        if (commonFields.Contains(currentColumns[counter]))
                        {
                            row[currentColumns[counter]] = fetchCommonValue(caseData, currentColumns[counter]);
                        }
                        else
                        {
                            row[currentColumns[counter]] = fetchTaskTypeValue(task, currentColumns[counter]);
                        }
                    }

                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public DataTable ConvertReportAnalyticsToDataTable(List<Models.Case> data, string[] currentColumns)
        {
            DataTable table = new DataTable();
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Case caseData in data)
            {
                var commonFields = new List<String> { "CaseNumber", "CaseNickname", "Nickname", "Client", "Country" };
                foreach (var report in caseData.Reports)
                {
                    DataRow row = table.NewRow();
                    for (int counter = 0; counter < currentColumns.Count(); counter++)
                    {
                        if (commonFields.Contains(currentColumns[counter]))
                        {
                            row[currentColumns[counter]] = fetchCommonValue(caseData, currentColumns[counter]);
                        }
                        else
                        {
                            row[currentColumns[counter]] = fetchReportTypeValue(report, currentColumns[counter]);
                        }
                    }

                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public DataTable ConvertExpenseAnalyticsToDataTable(List<Models.Case> data, string[] currentColumns)
        {
            DataTable table = new DataTable();
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Case caseData in data)
            {
                var commonFields = new List<String> { "CaseNumber", "CaseNickname", "Nickname", "Client", "Country" , "ICIAssignedUser", "SubjectName", "FinalReportSentToClientDate" };
                foreach (var expense in caseData.Expenses)
                {
                    DataRow row = table.NewRow();
                    for (int counter = 0; counter < currentColumns.Count(); counter++)
                    {
                        if (commonFields.Contains(currentColumns[counter]))
                        {
                            row[currentColumns[counter]] = fetchCommonValue(caseData, currentColumns[counter]);
                        }
                        else
                        {                          
                                row[currentColumns[counter]] = fetchExpenseTypeValue(expense, currentColumns[counter]);                            
                        }
                    }

                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public DataTable ConvertTimesheetAnalyticsToDataTable(List<Models.Case> data, string[] currentColumns)
        {
            DataTable table = new DataTable();
            for (int counter = 0; counter < currentColumns.Count(); counter++)
            {
                table.Columns.Add(currentColumns[counter], typeof(String));
            }
            foreach (Models.Case caseData in data)
            {
                var commonFields = new List<String> { "CaseNumber", "CaseNickname", "Nickname", "Client", "Country", "ICIAssignedUser", "SubjectName", "FinalReportSentToClientDate" };
                foreach (var timesheet in caseData.Timesheets)
                {
                    DataRow row = table.NewRow();
                    for (int counter = 0; counter < currentColumns.Count(); counter++)
                    {
                        if (commonFields.Contains(currentColumns[counter]))
                        {
                            row[currentColumns[counter]] = fetchCommonValue(caseData, currentColumns[counter]);
                        }
                        else
                        {                           
                            row[currentColumns[counter]] = fetchTimesheetTypeValue(timesheet, currentColumns[counter]);                            
                        }
                    }

                    table.Rows.Add(row);
                }
            }
            return table;
        }

        private object fetchExpenseTypeValue(Models.CaseExpense expenseItem, string expenseItemToBeFetched)
        {
            string expenseItemName = string.Empty;
            switch (expenseItemToBeFetched)
            {
                case "Id": expenseItemName = expenseItem.Id.ToString(); break;              
                case "Quantity": expenseItemName = expenseItem.Quantity.ToString("f"); break;
                case "Cost":  expenseItemName = expenseItem.Cost.ToString("f"); break;
                case "CreatedDate": expenseItemName = expenseItem.CreationDateTime.ToString(); break;
                case "CreatedBy": expenseItemName = expenseItem.CreatedByUser.FirstName + " " + expenseItem.CreatedByUser.LastName; break;
                case "VendorName": expenseItemName = expenseItem.Vendor!= null ? expenseItem.Vendor.VendorName : "NA"; break;
                case "OriginalCurrencyCost": expenseItemName = expenseItem.OriginalCurrencyCost != string.Empty ? expenseItem.OriginalCurrencyCost:"0"; break;
                case "PaymentType": expenseItemName = expenseItem.ExpensePaymentType.ToString(); break;
                case "InvoiceApproved": expenseItemName = expenseItem.InvoiceApproved.ToString(); break;               
            }
            return expenseItemName;
        }

        private object fetchTimesheetTypeValue(Models.TimeSheet timesheetItem, string timeSheetItemToBeFetched)
        {
            string timeSheetItemName = string.Empty;
            switch (timeSheetItemToBeFetched)
            {
                case "Id" :timeSheetItemName= timesheetItem.Id.ToString();break;
                case "Description": timeSheetItemName = timesheetItem.Description.Description.ToString(); break;
                case "endTime": timeSheetItemName = (timesheetItem.EndTime!= null ? timesheetItem.EndTime.Value.ToString("hh:mm"):"NA"); break;
                case "startTime": timeSheetItemName = timesheetItem.StartTime.ToString("hh:mm"); break;
                case "Hours": timeSheetItemName = ((timesheetItem.EndTime - timesheetItem.StartTime).HasValue)?(timesheetItem.EndTime - timesheetItem.StartTime).Value.TotalHours.ToString("f"):'0'.ToString(); break;
                case "Cost": timeSheetItemName = timesheetItem.Cost.ToString("f"); break;
                case "CreatedDate": timeSheetItemName = timesheetItem.CreationDateTime.ToString(); break;
                case "CreatedBy": timeSheetItemName = timesheetItem.CreatedByUser.FirstName + " " + timesheetItem.CreatedByUser.LastName; break;
                case "TimesheetUserName": timeSheetItemName = timesheetItem.User.FirstName + " " + timesheetItem.User.LastName; break;
                case "TimesheetUser": timeSheetItemName = timesheetItem.User.Role.Name; break;
            }
            return timeSheetItemName;
        }

        private string fetchCommonValue(Models.Case caseItem, string caseItemToBeFetched)
        {
            string caseItemName = string.Empty;
            switch (caseItemToBeFetched)
            {
                case "Client":
                    caseItemName = caseItem.Client.ClientName; break;
                case "Country": caseItemName = caseItem.PrimaryReportCountryName != null ? caseItem.PrimaryReportCountryName : "NA"; break;
                case "CaseNumber": caseItemName = caseItem.CaseNumber; break;
                case "CaseNickname":
                case "Nickname": caseItemName = caseItem.NickName; break;            
                case "SubjectName": caseItemName = (caseItem.MatterName != "" && caseItem.MatterName != null ? caseItem.MatterName : "NA"); break;
                case "ICIAssignedUser": caseItemName = (caseItem.ICIAssignedUser!=null ? string.Concat(caseItem.ICIAssignedUser.FirstName," ",caseItem.ICIAssignedUser.LastName) : "NA"); break;
                case "FinalReportSentToClientDate": caseItemName = (caseItem.FinalReportSentToClientDate.HasValue==true ? caseItem.FinalReportSentToClientDate.Value.ToString() : "NA"); break;
            }
            return caseItemName;
        }

        private string fetchCaseTypeValue(Models.Case caseItem,string caseItemToBeFetched)
        {
            string caseItemName = string.Empty;
            switch (caseItemToBeFetched)
            {                
                case "Speed" :caseItemName = caseItem.Speed.ToString(); break;               
                case "Margin" :caseItemName = caseItem.Margin.ToString("f"); break;
                case "IsNFC" :caseItemName = caseItem.IsNFCCase.ToString(); break;
                case "CaseStatus" :caseItemName = (caseItem.CaseStatus!=null ? caseItem.CaseStatus.Status : "NA"); break;
                case "ActualRetail" :caseItemName = (caseItem.ActualRetail==0 ? "0" : caseItem.ActualRetail.ToString("f")); break;
                case "CreatedBy" :caseItemName = caseItem.CreatedByUser.FirstName + " " + caseItem.CreatedByUser.LastName; break;
                case "CreatedDate" :caseItemName = caseItem.CreationDateTime.ToString(); break;
                case "ClosedBy" :caseItemName = (caseItem.UserWhoClosedCase!= null ? caseItem.UserWhoClosedCase.FirstName + " " + caseItem.UserWhoClosedCase.LastName : "NA"); break;
                case "ClosedDate" :caseItemName = (caseItem.ClosingDateTime!=null ? caseItem.ClosingDateTime.ToString() : "NA"); break;
                case "CaseAge" :caseItemName = (caseItem.CaseAge.HasValue== true? caseItem.CaseAge.Value.ToString(): "0"); break;
                case "FinalReportSentToClient": caseItemName = caseItem.FinalReportSentToClient.ToString();break;
                case "FinalReportSentToClientDate": caseItemName = (caseItem.FinalReportSentToClientDate.HasValue==true? caseItem.FinalReportSentToClientDate.Value.ToString():"NA").ToString();break;
                case "AccountCode": caseItemName = (caseItem.BusinessUnit != null ? caseItem.BusinessUnit.AccountCode.ToString() : "NA");break;
                case "InformationPending": caseItemName = caseItem.InformationPending.ToString();break;
                case "Reports": caseItemName = (caseItem.Reports.FirstOrDefault(x => x.IsPrimary == true)!= null? caseItem.Reports.FirstOrDefault(x => x.IsPrimary == true).ReportType:"NA");break;
                case "InHouseTurnTime": caseItemName = (caseItem.InHouseTurnTime.HasValue == true ? caseItem.InHouseTurnTime.Value.ToString():"NA");break;
                case "SalesRepresentative": caseItemName = (caseItem.Client.SalesRepresentative != null ? string.Concat(caseItem.Client.SalesRepresentative.FirstName, " ", caseItem.Client.SalesRepresentative.LastName) : "NA");break;
                case "ICIErrorType":caseItemName = (caseItem.ICIError.HasValue == true ? caseItem.ICIError.Value.ToString() : "NA");break;
                case "ICIAssignedUser": caseItemName = (caseItem.ICIAssignedUser != null ? string.Concat(caseItem.ICIAssignedUser.FirstName, " ", caseItem.ICIAssignedUser.LastName) : "NA");break;
                case "ReportToClientAheadOfTime":caseItemName= (caseItem.ReportToClientAheadOfTime.HasValue==true?caseItem.ReportToClientAheadOfTime.Value.ToString():"NA");break;
                case "CaseCancelled":caseItemName = caseItem.CaseCancelled.ToString();break;
                case "ClientCancelled":caseItemName = caseItem.ClientCancelled.HasValue == true ? caseItem.ClientCancelled.Value.ToString() : "NA";break;
                case "BusinessUnit": caseItemName=(caseItem.BusinessUnit!= null ? caseItem.BusinessUnit.BusinessUnit : "NA"); break;
                case "SubjectName": caseItemName=(caseItem.MatterName!="" && caseItem.MatterName != null ? caseItem.MatterName : "NA"); break;
                case "ContactName": caseItemName=(caseItem.ClientContactUser!= null ? caseItem.ClientContactUser.ContactName : "NA"); break;
                case "NFCBudget": caseItemName=(caseItem.NFCBudget.HasValue== true ? caseItem.NFCBudget.Value.ToString() : "NA"); break;
                case "FinalReportSentToSalesRep": caseItemName = caseItem.FinalReportSentToSalesRep.ToString(); break;
                case "FinalReportSentToSalesRepDate": caseItemName = (caseItem.FinalReportSentToSalesRepDate.HasValue == true ? caseItem.FinalReportSentToSalesRepDate.Value.ToString() : "NA"); break;
                case "IHIQuality": caseItemName = (caseItem.IHIQuality.HasValue==true ? caseItem.IHIQuality.Value.ToString(): "NA"); break;
                case "IHIAssignedUser": caseItemName = (caseItem.IHIAssignedUser!=null ? caseItem.IHIAssignedUser.FirstName + " " + caseItem.IHIAssignedUser.LastName : "NA"); break;
                case "IHIBonusCase": caseItemName = (caseItem.IHIBonusCase.HasValue==true ? caseItem.IHIBonusCase.ToString() : "NA"); break;
                case "AdverseReport": caseItemName = (caseItem.AdverseReport.HasValue==true ? caseItem.AdverseReport.Value.ToString() : "NA"); break;
                case "CaseReviewedDate": caseItemName = (caseItem.CaseReviewedDate.HasValue == true ? caseItem.CaseReviewedDate.Value.ToString() : "NA"); break;
                case "CaseReviewedByUser": caseItemName = (caseItem.ReviewedByUser != null ? caseItem.ReviewedByUser.FirstName + " " + caseItem.ReviewedByUser.LastName : "NA"); break;
                case "TurnTimeForICIBusinessDays": caseItemName = (caseItem.TurnTimeForICIBusinessDays.HasValue == true ? caseItem.TurnTimeForICIBusinessDays.Value.ToString() : "NA"); break;
                case "CaseDueDate": caseItemName = (caseItem.CaseDueDate.HasValue==true ? caseItem.CaseDueDate.Value.ToString() : "NA"); break;
                case "DDSupervisorWhoApprovedCase": caseItemName = (caseItem.DDSupervisorWhoApprovedCaseByUser != null ? caseItem.DDSupervisorWhoApprovedCaseByUser.FirstName+" "+ caseItem.DDSupervisorWhoApprovedCaseByUser.LastName : "NA"); break;
                case "DDSupervisorApprovedCaseDate": caseItemName = (caseItem.DDSupervisorApprovedCaseDate.HasValue == true ? caseItem.DDSupervisorApprovedCaseDate.Value.ToString() : "NA"); break;
            }
            return caseItemName;
        }

        private string fetchTaskTypeValue(Models.Task taskItem, string taskItemToBeFetched)
        {
            string taskItemName = string.Empty;
            switch (taskItemToBeFetched)
            {
                case "Id": taskItemName = taskItem.TaskId.ToString(); break;
                case "Title": taskItemName = taskItem.Title; break;
                case "Status": taskItemName = ((TaskStatusType)taskItem.Status).ToString(); break;
                case "Priority": taskItemName = ((TaskPriority)taskItem.Priority).ToString(); break;
                case "DueDate": taskItemName = taskItem.DueDate.ToString(); break;
                case "CompletionDate": taskItemName = taskItem.CompletionDate != null ? taskItem.CompletionDate.ToString() : "NA"; break;
                case "AssignedTo": taskItemName = taskItem.AssignedToUserDetails.FirstName + " " + taskItem.AssignedToUserDetails.LastName; break;
                case "CreatedBy": taskItemName = taskItem.CreatedByUserDetails.FirstName + " " + taskItem.CreatedByUserDetails.LastName; break;
                case "CreatedDate": taskItemName = taskItem.CreatedOn.ToString(); break;
                case "OriginalDueDate": taskItemName = taskItem.OriginalDueDate.ToString(); break;
            }
            return taskItemName;
        }

        private string fetchReportTypeValue(Models.Report reportItem, string reportItemToBeFetched)
        {
            string reportItemName = string.Empty;
            switch (reportItemToBeFetched)
            {
                case "ReportType": reportItemName = reportItem.ReportType;break;
            }
            return reportItemName;
        }

        private string fetchClientTypeValue(Models.Client clientItem, string clientItemToBeFetched)
        {
            string clientItemName = string.Empty;
            switch (clientItemToBeFetched)
            {
                case "Name": clientItemName = clientItem.ClientName; break;
                case "SalesRepresentative":
                    clientItemName = (clientItem.SalesRepresentative != null ? clientItem.SalesRepresentative.FirstName + " " + clientItem.SalesRepresentative.LastName : "NA");break;
                case "LatestCaseOrderDate": clientItemName = (clientItem.LatestCaseOrderDate.HasValue == true ? clientItem.LatestCaseOrderDate.Value.ToString() : "NA");break;
            }
            return clientItemName;
        }
    }
}
