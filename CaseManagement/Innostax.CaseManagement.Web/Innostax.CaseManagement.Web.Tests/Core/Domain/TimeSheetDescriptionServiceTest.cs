﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TimeSheetDescriptionServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private TimeSheetDescriptionService sut;
        private ErrorHandlerService _errorHandlerService;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            _errorHandlerService = new ErrorHandlerService();
            sut = new TimeSheetDescriptionService(innostaxDbMock.Object,_errorHandlerService);           
        }

        [TestMethod]
        public void GivenCalling_GetAllTimeSheetDescriptions_WhenDataIsPresent_MappedResultReturn()
        {
            var timeSheetDescriptions = new List<TimeSheetDescription> { new TimeSheetDescription { Id = Guid.NewGuid(), Description = "TestDescription1" }, new TimeSheetDescription { Id = Guid.NewGuid(), Description = "TestDescription2" } };
            innostaxDbMock.Setup(c => c.TimeSheetDescriptions).ReturnsDbSet(timeSheetDescriptions);
            var actual = sut.GetAllTimeSheetDescriptions();
            Assert.AreEqual(actual.Count, 2);
        }

        [TestMethod]
        public void GivenCalling_GetAllTimeSheetDescriptions_WhenDataIsNotPresent_MappedResultReturn()
        {
            var timeSheetDescriptions = new List<TimeSheetDescription>();
            innostaxDbMock.Setup(c => c.TimeSheetDescriptions).ReturnsDbSet(timeSheetDescriptions);
            var actual = sut.GetAllTimeSheetDescriptions();
            Assert.AreEqual(actual.Count, 0);
        }
    }
}
