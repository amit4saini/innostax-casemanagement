﻿using Innostax.Common.Database.Database;
using System;
using System.Linq;
using System.Data.Entity;
using Innostax.CaseManagement.Models;
using System.Collections.Generic;
using Innostax.Common.Database.Enums;
using System.Globalization;

namespace Innostax.CaseManagement.Core.Domain
{
    public class SendNotificationToSubscriberService: IObserver
    {
        private INotificationService _notificationService;
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;     
        public SendNotificationToSubscriberService(INotificationService notificationService, IInnostaxDb innostaxDb, IUserService userService)
        {
            _notificationService = notificationService;
            _innostaxDb = innostaxDb;
            _userService = userService;
        }

        internal Common.Database.Models.Case GetCaseDetails(Guid caseId)
        {
            var caseDetails = _innostaxDb.Cases.Include(y => y.CreatedByUser).Include(x=>x.CaseStatus).Include(x=>x.Client).FirstOrDefault(x => x.CaseId == caseId && x.IsDeleted != true);
            return caseDetails;                         
        }

        public List<Guid> Execute(Guid caseId, string status)
        {
            //var caseDetails = GetCaseDetails(caseId);
            //var notificationDetails = new Notification();
            //List<Guid> listOfUserToBeNotified=new List<Guid>();                  
            //var URL = "/case-management/cases/"+caseDetails.CaseNumber+ "/caseDetails";                          
            //return _notificationService.SaveNotification(GetNotificationText(caseDetails,status), ListOfUserToBeNotified(caseDetails,status), URL);         
            return new List<Guid> { };  
        }

        private List<Guid> ListOfUserToBeNotified(Common.Database.Models.Case caseDetails,string status)
        {
            var listOfUserToBeNotified = new List<Guid>();   
            switch (status)
            {    
                case "APPROVED":if (caseDetails.Client.SalesRepresentativeId.HasValue) {   listOfUserToBeNotified.Add(caseDetails.Client.SalesRepresentativeId.Value) ; } break;
                case "NFC_APPROVED": if (caseDetails.Client.SalesRepresentativeId.HasValue) { listOfUserToBeNotified.Add(caseDetails.Client.SalesRepresentativeId.Value ); } break;
                case "CLOSED":
                    var billingRole = _innostaxDb.Roles.FirstOrDefault(x => x.Name == Roles.Billing.ToString());
                    var billingUserIdList = _innostaxDb.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(billingRole.Id)).Select(x => x.Id).ToList();
                    listOfUserToBeNotified = billingUserIdList;
                    break;
            }
            if (listOfUserToBeNotified.Count == 0)
            {
                if (caseDetails.ReviewedBy.HasValue)
                { listOfUserToBeNotified.Add(caseDetails.ReviewedBy.Value); }
                else
                {
                    var adminRole = _innostaxDb.Roles.FirstOrDefault(x => x.Name == Roles.DDSupervisor.ToString());
                    var adminIdList = _innostaxDb.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(adminRole.Id)).Select(x => x.Id).ToList();
                    listOfUserToBeNotified = adminIdList;
                }
            }
            return listOfUserToBeNotified;
        }

        private string GetNotificationText(Common.Database.Models.Case caseDetails, string status)
        {
            var notificationText = string.Empty;
            var currentUser = _userService.GetCurrentUser();
            string currentUserName = currentUser.FirstName + currentUser.LastName;                          
            switch (status)
            {
                case "SENT_FOR_APPROVAL":
                    notificationText = string.Format(Innostax.Common.Constants.NotificationMessages.CaseNotification.CASE_APPROVAL_REQUESTED, currentUserName, caseDetails.NickName + " " + caseDetails.CaseNumber); break;
                case "NFC_APPROVED":
                    notificationText = string.Format(Innostax.Common.Constants.NotificationMessages.CaseNotification.CASE_SENT_TO_NFC, currentUserName, caseDetails.NickName + " " + caseDetails.CaseNumber);
                        break;                    
                case "APPROVED":
                    notificationText = string.Format(Innostax.Common.Constants.NotificationMessages.CaseNotification.CASE_APPROVED, currentUserName, caseDetails.NickName + " " + caseDetails.CaseNumber); break;
                case "CLOSED":
                    notificationText= string.Format(Innostax.Common.Constants.NotificationMessages.CaseNotification.CASE_READY_FOR_INVOICE, caseDetails.CaseNumber); break;

            }
            if (notificationText == string.Empty)
            {
                notificationText = Common.Constants.NotificationMessages.CaseNotification.CASE_STATUS_CHANGED;
                var caseStatus = caseDetails.CaseStatus.Status.ToLower().Replace("_", " ");
                caseStatus = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(caseStatus);
                notificationText = string.Format(notificationText, caseDetails.CaseNumber, caseDetails.NickName, caseStatus);
            }
            return notificationText;
        }
    }
}
