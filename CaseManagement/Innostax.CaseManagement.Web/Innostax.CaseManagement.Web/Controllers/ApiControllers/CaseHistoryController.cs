﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseHistoryController : ApiController
    {
        // GET: CaseHistory
        private readonly ICaseHistoryService _caseHistoryService;
        public CaseHistoryController(ICaseHistoryService caseHistoryService)
        {
            _caseHistoryService = caseHistoryService;
        }

        [HasPermission(new Permission[] { Permission.VIEW_HISTORY })]
        [Route("api/CaseHistory/{caseId}/Case/{historyFilter}")]
        public async System.Threading.Tasks.Task<PageResult<CaseHistory>> Get(ODataQueryOptions<CaseHistory> options,Guid caseId,HistoryFilter historyFilter)
        {
            return await _caseHistoryService.GetHistoryDetailsForACase(options,Request,caseId, historyFilter);
        }

        [System.Web.Http.Route("api/CaseHistory/{caseId}/{userId}")]
        [HasPermission(new Permission[] { Permission.VIEW_HISTORY })]
        public List<CaseHistory> Get(Guid caseId, Guid userId)
        {
            return _caseHistoryService.GetCaseHistoryDetailsForAUser(caseId, userId);
        }
    }
}