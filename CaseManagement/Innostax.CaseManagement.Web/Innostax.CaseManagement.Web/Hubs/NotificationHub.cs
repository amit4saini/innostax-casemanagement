﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;

namespace Innostax.CaseManagement.Web.Hubs
{
    public interface INotificationHub
    {
        void Send(string name, List<string> message);
    }

    public class NotificationHub : Hub, INotificationHub
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        public NotificationHub(IInnostaxDb innostaxDb, IUserService userService)
        {
            _innostaxDb = innostaxDb;
            _userService = userService;
        }
        public void Send(string userId, List<string> message)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            // Call the addNewMessageToPage method to update clients.
            hubContext.Clients.User(userId).sendNotification(message);
        }
    }
}