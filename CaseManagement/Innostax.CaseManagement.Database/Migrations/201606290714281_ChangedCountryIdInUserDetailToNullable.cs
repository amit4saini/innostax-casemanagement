namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangedCountryIdInUserDetailToNullable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserDetails", new[] { "CountryId" });
            AlterColumn("dbo.UserDetails", "CountryId", c => c.Int());
            CreateIndex("dbo.UserDetails", "CountryId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserDetails", new[] { "CountryId" });
            AlterColumn("dbo.UserDetails", "CountryId", c => c.Int(nullable: false));
            CreateIndex("dbo.UserDetails", "CountryId");
        }
    }
}
