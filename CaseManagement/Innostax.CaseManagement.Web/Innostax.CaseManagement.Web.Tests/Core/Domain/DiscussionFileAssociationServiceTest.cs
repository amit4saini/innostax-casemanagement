﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using System.Collections.Generic;
using AutoMapper;
using System.Web.Http;
using Innostax.Common.Database.Models;
using System.Net;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionFileAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscussionFileAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<IUserService> _userService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICommonService> _commonService;
        Guid DiscussionId;

        [TestInitialize]
        public void TestInitialize()
        {
            DiscussionId = System.Guid.NewGuid();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            _userService = new Mock<IUserService>();
            _commonService = new Mock<ICommonService>();
            sut = new DiscussionFileAssociationService(innostaxDbMock.Object, errorHandlerService,_userService.Object, _caseHistoryService.Object,_commonService.Object);

        }

        private Models.DiscussionFileAssociation GetDummyDiscussionFileAssociation()
        {

            var dummyAssociation = new Models.DiscussionFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                DiscussionId = DiscussionId
            };
            return dummyAssociation;
        }
        private Models.DiscussionFileAssociation GetInvalidDummyDiscussionAssociation()
        {

            var dummyAssociation = new Models.DiscussionFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                DiscussionId = Guid.NewGuid()
            };
            return dummyAssociation;
        }

        public List<UploadedFile> GetDummyFiles()
        {
            var dummyFiles = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new UploadedFile
                {
                    FileId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyFiles;
        }

        public List<Models.FileModel> GetDomainModelDummyFiles()
        {
            var dummyFiles = new List<Models.FileModel>
            {
                new Models.FileModel
                {
                    FileId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new Models.FileModel
                {
                    FileId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyFiles;
        }
        public List<Innostax.Common.Database.Models.Discussion> GetDummyDiscussion()
        {
            var dummyDiscussion = new List<Innostax.Common.Database.Models.Discussion>
            {
                new Innostax.Common.Database.Models.Discussion
                {
                     Id = DiscussionId
                },
                new Innostax.Common.Database.Models.Discussion
                {
                   Id = new Guid("02561DAC-DAF0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyDiscussion;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenUploadedTableIsEmpty_ThenThrowsNotFoundRequest()
        {
            try
            {
                var dummyFiles = new List<UploadedFile>();
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(dummyFiles);
                innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(GetDummyDiscussion());
                innostaxDbMock.Setup(c => c.DiscussionFileAssociations).ReturnsDbSet(new Innostax.Common.Database.Models.DiscussionFileAssociation[0]);
                sut.Save(GetDummyDiscussionFileAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenDiscussionTableIsEmpty_ThenThrowsBadRequest()
        {
            try
            {
                var dummyDiscussion = new List<Innostax.Common.Database.Models.Discussion>();
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyFiles());
                innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(dummyDiscussion);
                sut.Save(GetDummyDiscussionFileAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        public void GivenCallingSave_WhenDataIsValid_ThenMappedResultReturn()
        {
            var dummyDiscussionFileAssociation = new List<Innostax.Common.Database.Models.DiscussionFileAssociation>();
            innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyFiles());
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(GetDummyDiscussion());
            innostaxDbMock.Setup(c => c.DiscussionFileAssociations).ReturnsDbSet(dummyDiscussionFileAssociation);
            try
            {
                sut.Save(GetDummyDiscussionFileAssociation());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCallingGetDiscussionAssociatedFiles_ThenMappedResultReturn()
        {
            var dummyDiscussionFileAssociation = new List<Innostax.Common.Database.Models.DiscussionFileAssociation>();
            innostaxDbMock.Setup(c => c.DiscussionFileAssociations).ReturnsDbSet(dummyDiscussionFileAssociation);
            try
            {
                var result = sut.GetDiscussionAssociatedFiles(Guid.NewGuid());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }
    }
}
