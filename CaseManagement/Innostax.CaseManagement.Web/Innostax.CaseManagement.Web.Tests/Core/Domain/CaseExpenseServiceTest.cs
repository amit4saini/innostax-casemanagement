﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseExpenseServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private CaseExpenseService sut;
        private Mock<IUserService> _userService;
        private ErrorHandlerService _errorHandler;
        private Mock<ICommonService> _commonService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        Guid clientId1;
        Guid clientId2;
        Guid caseExpenseId;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            caseExpenseId = System.Guid.NewGuid();
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandler = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            sut = new CaseExpenseService(
                innostaxDbMock.Object,
                 _userService.Object, _errorHandler,
                 _commonService.Object, _caseHistoryService.Object
                );           
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_OWN };
            _userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            _commonService.Setup(x => x.ValidateAccess(Validate.CASE, It.IsAny<Innostax.Common.Database.Enums.Permission>(),It.IsAny<Guid>()));
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId1, ClientName = "DummyClient1" },
                    ClientId=clientId1,
                    IsDeleted=false
                },
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId2, ClientName = "DummyClient2" },
                    ClientId=clientId2,
                    IsDeleted=false
                }
            };
            return dummyCase;
        }
     
        private Models.CaseExpense GetDummyCaseExpense()
        {
            var caseExpense = new Models.CaseExpense
            {
                Id = Guid.NewGuid(),
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
                Note = "Nothing",
                Quantity = 1,
                Cost = 17           
        };
            return caseExpense;
        }
        private List<CaseExpense> GetDummyCaseExpenseModel()
        {
            var caseExpense = new List<CaseExpense> {
                new CaseExpense
            {
                Id = Guid.NewGuid(),
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
                Note = "Nothing",
                Quantity = 1,
                Cost = 17        
            },
                  new CaseExpense
            {
                Id = caseExpenseId,
                CaseId = new Guid("16834A66-39FA-E511-937B-FCAA14866D0F"),
                Note = "Nothing",
                Quantity = 1,
                Cost = 17,   
            }};
            return caseExpense;
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_Save_WhenCaseIdIsNotPresent_ThenThrowNotBadRequestException()
        {
            try
            {
                var dummyCases = GetDummyCase();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);
                sut.Save(GetDummyCaseExpense());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_Delete_WhenDataIsValid_MappedResultReturn()
        {          
                var dummyCases = GetDummyCase();
                var notPresent = Guid.NewGuid();
                innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(dummyCases);     
                innostaxDbMock.Setup(c => c.CaseExpenses).ReturnsDbSet(GetDummyCaseExpenseModel());
            try
            {
                sut.Delete(caseExpenseId);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
