﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Core.Services.Implementation;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionMessageServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscussionMessageService sut;
        private ErrorHandlerService _errorHandler;
        private Mock<IUserService> _userService;
        private Mock<IDiscussionMessageFileAssociationService> _discussionMessageFileAssociationService;
        private Mock<IAzureFileManager> _azureFileManager;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<INotificationService> _notificationService;
        private Mock<ICaseService> _caseService;
        private Mock<IDiscussionNotifierService> _discussionNotifierService;
        private Mock<IMailSenderService> _mailSenderService;
        private Mock<ICommonService> _commonService;
        ODataQueryOptions<Models.DiscussionMessage> options;
        private Mock<IDiscussionMessageTaggedUserService> _discussionMessageTaggedUserService;
        Guid discussionId;
        Guid caseId;

        [TestInitialize]
        public void TestInitialize()
        {
            discussionId = System.Guid.NewGuid();
            caseId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _errorHandler = new ErrorHandlerService();
            _userService = new Mock<IUserService>();
            _discussionMessageTaggedUserService = new Mock<IDiscussionMessageTaggedUserService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _notificationService = new Mock<INotificationService>();
            _discussionMessageFileAssociationService = new Mock<IDiscussionMessageFileAssociationService>();
            _caseService = new Mock<ICaseService>();
            _mailSenderService = new Mock<IMailSenderService>();
            _discussionNotifierService = new Mock<IDiscussionNotifierService>();
            _azureFileManager = new Mock<IAzureFileManager>();
            _commonService = new Mock<ICommonService>();
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.DiscussionMessage>("Discussion");
            options = new ODataQueryOptions<Models.DiscussionMessage>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.DiscussionMessage)), new HttpRequestMessage());
            sut = new DiscussionMessageService(
                innostaxDbMock.Object,
                _errorHandler, _userService.Object,_discussionMessageFileAssociationService.Object,_azureFileManager.Object, _caseHistoryService.Object,_mailSenderService.Object,_discussionNotifierService.Object,_caseService.Object, _notificationService.Object,_commonService.Object, _discussionMessageTaggedUserService.Object
                );   
            _discussionMessageFileAssociationService.Setup(x => x.Save(It.IsAny<Models.DiscussionMessageFileAssociation>()));
            _discussionMessageFileAssociationService.Setup(x => x.GetDiscussionMessageAssociatedFiles(It.IsAny<Guid>())).Returns(
            new List<Models.FileModel>
            {
                new Models.FileModel
                {
                    FileId=new Guid()
                }
            });
            _userService.SetupGet(x => x.UserId).Returns(new Guid("5435CC1E-E515-E611-8B84-FCAA14866CAF"));            
        }
        private List<DiscussionMessage> GetDummyDiscussionMessageModel()
        {
            var dummyDiscussionMessages = new List<DiscussionMessage>
            {
                new DiscussionMessage
                {
                    Id=new Guid("5435CC1E-E515-E611-8B84-FCAA14866CAF"),
                    DiscussionId = discussionId,
                    Message="random",
                    IsArchive =false,
                    IsDeleted=false,CreatedByUser=new Innostax.Common.Database.Models.UserManagement.User { },
                    Discussion = new Discussion {CaseId = caseId }
                },
                new DiscussionMessage
                {
                    Id=Guid.NewGuid(),
                    DiscussionId = discussionId,
                    Message="hello",
                    IsArchive =false,
                    IsDeleted=false,CreatedByUser=new Innostax.Common.Database.Models.UserManagement.User { },
                    Discussion = new Discussion {CaseId =caseId }
                }
            };
            return dummyDiscussionMessages;
        }
        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetAllDiscussionMessagesByDiscussionId_WhenNoDiscussionMessageIsPresentForDiscussionId_ThenThrowNotFoundRequest()
        {
            var dummyDiscussionMessages = GetDummyDiscussionMessageModel();
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            var actual = await sut.GetAllDiscussionMessagesByDiscussionId(options, new HttpRequestMessage(), Guid.NewGuid());
            Assert.AreEqual(actual.Count, 0);
        }
        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetAllDiscussionByCaseId_WhenDiscussionsArePresent_ThenMappedResultReturn()
        {
            var dummyDiscussionMessages = GetDummyDiscussionMessageModel();            
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(new List<Discussion> { new Discussion() });
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            var actual = await sut.GetAllDiscussionMessagesByDiscussionId(options, new System.Net.Http.HttpRequestMessage(), discussionId);
            Assert.IsNotNull(actual.Items);
        }

        [TestMethod]
        public void GivenCalling_ArchiveUnarchiveDiscussionMessage_WhenDiscussionMessagesArePresent_ThenMappedResultReturn()
        {
            var dummyDiscussionMessages = GetDummyDiscussionMessageModel();
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(new List<Discussion> {new Discussion() });
            sut.ArchiveUnarchiveDiscussionMessage(new Guid("5435CC1E-E515-E611-8B84-FCAA14866CAF"), true);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_DeleteDiscussionMessage_WhenDiscussionMessageIdIsNotPresent_ThenMappedResultReturn()
        {
            var dummyDiscussionMessages = GetDummyDiscussionMessageModel();
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            try
            {
                sut.ArchiveUnarchiveDiscussionMessage(Guid.NewGuid(), true);
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }
    }
}
