﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class Discount
    {
        public Guid DiscountId { get; set; }
        public double DiscountAmount { get; set; }
    }
}
