﻿using System;
using System.Collections.Generic;
using System.Linq;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using AutoMapper;
using System.Data.Entity.Migrations;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Data.Entity;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Models.EmailModels;
using Innostax.CaseManagement.Models.Account;
using System.Text.RegularExpressions;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskCommentService
    {
        List<Guid> Save(TaskComment taskComment);
        Task<List<TaskComment>> GetAllCommentsForTask(Guid taskId);
        void Delete(Guid taskCommentId);
    }

    public class TaskCommentService : ITaskCommentService
    {
        private IInnostaxDb _innostaxDb;
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IAzureFileManager _azureFileManager;
        private readonly ICommonService _commonService;
        private readonly ITaskCommentFileAssociationService _taskCommentFileAssociationService;
        private readonly IMailSenderService _mailSenderService;
        private readonly ITaskCommentTaggedUserService _taskCommentTaggedUsersService;
        private readonly INotificationService _notificationService;
        public TaskCommentService(IInnostaxDb innostaxDb, ITaskService taskService, IUserService userService, IErrorHandlerService errorHandlerService, IAzureFileManager azureFileManager, ITaskCommentFileAssociationService taskCommentFileAssociationService,ICommonService commonService, IMailSenderService mailSenderService, ITaskCommentTaggedUserService taskCommentTaggedUsers, INotificationService notificationService)
        {
            _innostaxDb = innostaxDb;
            _taskService = taskService;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
            _azureFileManager = azureFileManager;
            _commonService = commonService;
            _taskCommentFileAssociationService = taskCommentFileAssociationService;
            _mailSenderService = mailSenderService;
            _taskCommentTaggedUsersService = taskCommentTaggedUsers;
            _notificationService = notificationService;
        }
       
        public void SendEmail(Common.Database.Models.Task taskData, Common.Database.Enums.TaskCommentAction action, Guid taskCommentId)
        {
            var taskCommentEmailDetails = new TaskCommentEmailModel
            {
                TaskTitle = taskData.Title,
                CaseNumber = taskData.Case.CaseNumber,
                CommentedBy = _userService.GetUser(_userService.UserId).Data.UserName,
                Time = _commonService.GetUtcDateTime().ToString("dd MMMM,yyyy hh:mm"),
                TaggedUserEmailList = _taskCommentTaggedUsersService.GetTaskCommentTaggedUsers(taskCommentId).Select(x => x.UserName).ToList(),
                AssignedToUser = taskData.AssignedToUser.UserName,
                TaskCommentAction = action.ToString()
            };
            _mailSenderService.SendTaskCommentDetails(taskCommentEmailDetails);
        }

        public List<Guid> Save(TaskComment taskComment)
        {
            Common.Database.Enums.TaskCommentAction action;
            var existingTask = _innostaxDb.Tasks.Include(y=>y.Case).Include(y=> y.AssignedToUser).FirstOrDefault(x => x.TaskId == taskComment.TaskId && x.IsDeleted != true);
            if (existingTask == null)
            {
                _errorHandlerService.ThrowBadRequestError(Innostax.Common.Constants.ErrorMessages.Common.TaskCommentErrors.TASK_COMMENT_NOT_ASSOCIATED);        
            }
            _commonService.ValidateAccess(Validate.TASK, Permission.CASE_UPDATE_ALL, existingTask.TaskId);
            var existingTaskComment = _innostaxDb.TaskComments.Include(y=>y.Task).FirstOrDefault(x => x.Id == taskComment.Id);
            var mappedComment = Mapper.Map<Innostax.Common.Database.Models.TaskComment>(taskComment);    
            if (taskComment.Id == Guid.Empty)
            {
                mappedComment.CreatedBy = _userService.UserId;
                mappedComment.CreationDateTime = _commonService.GetUtcDateTime();
                mappedComment.IsDeleted = false;
                action = Common.Database.Enums.TaskCommentAction.POSTED;
            }
            else
            {
                if (existingTaskComment.CreatedBy != _userService.UserId)
                {
                    string userNotAuthorized = Common.Constants.ErrorMessages.Common.TaskCommentErrors.NOT_AUTHORIZED;
                    _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorized);
                }
                action = Common.Database.Enums.TaskCommentAction.EDITED;
                mappedComment.CreationDateTime = existingTaskComment.CreationDateTime;
                mappedComment.CreatedBy = existingTaskComment.CreatedBy;
            }
            mappedComment.LastEditedBy = _userService.UserId;
            mappedComment.LastEditedDateTime = _commonService.GetUtcDateTime();
            _innostaxDb.TaskComments.AddOrUpdate(mappedComment);
            _innostaxDb.SaveChangesWithErrors();

            taskComment.TaggedUsersEmailList = new List<string>();
            if (taskComment.Comment != null)
            {
                var regex = new Regex((@"@\[(.*?)\]"));
                var matchesFound = regex.Matches(taskComment.Comment);
                List<User> usersList = new List<User>();

                foreach (Match eachMatch in matchesFound)
                {
                    string userEmail = string.Empty;
                    userEmail = eachMatch.Value;
                    userEmail = userEmail.Replace("@[~", string.Empty).Replace("]", string.Empty);
                    taskComment.TaggedUsersEmailList.Add(userEmail);
                }
                if (taskComment.TaggedUsersEmailList.Count > 0)
                {
                    foreach (var eachUserName in taskComment.TaggedUsersEmailList)
                    {
                        usersList.Add(_userService.GetUserByEmail(eachUserName));
                    }                    
                    _taskCommentTaggedUsersService.SaveTaskCommentTaggedUsers(mappedComment.Id, usersList.Select(x => x.Id).ToList());
                }
            }
                          
            if (taskComment.TaskCommentFiles != null)
            {
                TaskCommentFileAssociation taskCommentFileAssociation = new TaskCommentFileAssociation();
                taskCommentFileAssociation.Files = taskComment.TaskCommentFiles.ToList();
                taskCommentFileAssociation.TaskCommentId = mappedComment.Id;
                _taskCommentFileAssociationService.Save(taskCommentFileAssociation);
             }

            //SendEmail(existingTask, action, mappedComment.Id);
            return SaveTaskCommentNotifications(existingTask, action, mappedComment.Id);
        }

        public async Task<List<TaskComment>> GetAllCommentsForTask(Guid taskId)
        {
            var mappedTaskComments = Mapper.Map<List<TaskComment>>(_innostaxDb.TaskComments.Include(y=>y.Task).Where(x => x.TaskId == taskId && x.IsDeleted == false).Include(x => x.CreatedByUser).ToList());
            if(mappedTaskComments.Count > 0)
            {
                _commonService.ValidateAccess(Validate.TASK, Permission.CASE_READ_ALL, mappedTaskComments.First().TaskId);
            }
            Dictionary<Guid,string> profilePicKeyUrlList = new Dictionary<Guid,string>();
            foreach (var eachComment in mappedTaskComments)
            {
                eachComment.TaskCommentFiles = _taskCommentFileAssociationService.GetTaskCommentAssociatedFiles(eachComment.Id);
                eachComment.CreatedByUser.ProfilePicUrl = string.Empty;
                Guid profilePicKey = eachComment.CreatedByUser.ProfilePicKey ?? Guid.Empty;
                if (profilePicKey != Guid.Empty)
                {
                    if(profilePicKeyUrlList.ContainsKey(profilePicKey))
                    {
                        eachComment.CreatedByUser.ProfilePicUrl = profilePicKeyUrlList[profilePicKey];
                    }
                    else
                    { 
                        eachComment.CreatedByUser.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
                        profilePicKeyUrlList.Add(profilePicKey, eachComment.CreatedByUser.ProfilePicUrl);
                    }
                }
                else
                    eachComment.CreatedByUser.ProfilePicUrl = Common.Constants.InnostaxConstants.PROFILE_PIC_URL;
            }
            mappedTaskComments = mappedTaskComments.OrderByDescending(x => x.CreationDateTime).ToList();
            return mappedTaskComments;
        }

        public void Delete(Guid taskCommentId)
        {
            var existingTaskComment = _innostaxDb.TaskComments.Include(y=> y.Task).Include(y=>y.Task.AssignedToUser).FirstOrDefault(x => x.Id == taskCommentId && x.IsDeleted != true);
            if (existingTaskComment == null)
            {
                _errorHandlerService.ThrowResourceNotFoundError(Innostax.Common.Constants.ErrorMessages.Common.TaskCommentErrors.TASK_COMMENT_NOT_PRESENT);
            }
            _commonService.ValidateAccess(Validate.TASK, Permission.CASE_DELETE_ALL, existingTaskComment.TaskId);
            _taskCommentFileAssociationService.RemoveTaskCommentFileAssociation(taskCommentId);
            _taskCommentTaggedUsersService.DeleteTaskCommentTaggedUsers(existingTaskComment.Id);
            
            existingTaskComment.IsDeleted = true;
            _innostaxDb.SaveChangesWithErrors();
           // SendEmail(existingTaskComment.Task, Common.Database.Enums.TaskCommentAction.ARCHIVED, existingTaskComment.Id);
           // return SaveTaskCommentNotifications(existingTaskComment.Task, Common.Database.Enums.TaskCommentAction.ARCHIVED, existingTaskComment.Id);
        }

        internal List<Guid> SaveTaskCommentNotifications( Common.Database.Models.Task taskData, TaskCommentAction action, Guid taskCommentId)
        {
            var message = Common.Constants.NotificationMessages.TaskCommentNotification.TASK_COMMENT;
            var commentedBy = _userService.GetUser(_userService.UserId).Data.UserName;
            message = string.Format(message, taskData.Title, action.ToString().ToLower(), commentedBy, taskData.Case.CaseNumber);
            var notificationModel = new Notification
            {
                NotificationText = message,
                ListOfUserId = _taskCommentTaggedUsersService.GetTaskCommentTaggedUsers(taskCommentId).Select(user => user.Id).ToList(),
                URL = _commonService.GetCaseRelatedUrl(NotificationUrl.TASK, taskData.Case.CaseNumber, taskData.TaskId)
            };
           return _notificationService.SaveNotification(notificationModel.NotificationText, notificationModel.ListOfUserId, notificationModel.URL);
        }

    }
}
