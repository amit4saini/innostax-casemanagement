﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System.Threading.Tasks;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Web.Hubs;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class TaskCommentController:ApiController
    {
        private readonly ITaskCommentService _taskCommentService;
        private readonly ITaskCommentTaggedUserService _taskCommentTaggedUsers;
        private readonly INotificationService _notificationService;
        private readonly INotificationHub _notificationHub;

        public TaskCommentController(ITaskCommentService taskCommentService, ITaskCommentTaggedUserService taskCommentTaggedUsers, INotificationService notificationService, INotificationHub notificationHub)
        {
            _taskCommentService = taskCommentService;
            _taskCommentTaggedUsers = taskCommentTaggedUsers;
            _notificationService = notificationService;
            _notificationHub = notificationHub;
        }

        [HttpPost]      
        public void Post(TaskComment taskComment)
        {
            List<Guid> usersList = _taskCommentService.Save(taskComment);
            SendNotificationToSignalR(usersList);
        }

        [HttpGet]      
        public async Task<List<TaskComment>> Get(Guid taskId)
        {
            return await _taskCommentService.GetAllCommentsForTask(taskId);
        }

        [HttpPut]
        public void Put(TaskComment taskComment, Guid id)
        {
            taskComment.Id = id;
             List<Guid> usersList = _taskCommentService.Save(taskComment);
             SendNotificationToSignalR(usersList);
        }

        [HttpDelete]    
        public void Delete(Guid id)
        {
            _taskCommentService.Delete(id);
            //List<Guid> usersList = _taskCommentService.Delete(id);
            // SendNotificationToSignalR(usersList);
        }

        internal void SendNotificationToSignalR(List<Guid> usersList)
        {
            foreach (var userId in usersList)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }
    }
}