﻿
namespace Innostax.Common.Database.Enums
{
    public enum ExpensePaymentType
    {
        Account,
        CreditCardPayment,
        Invoice
    }
}
