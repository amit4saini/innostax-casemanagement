﻿using AutoMapper;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITaskCommentTaggedUserService
    {
        void SaveTaskCommentTaggedUsers(Guid taskCommentId, List<Guid> taggedUserIdsList);
        List<Models.Account.User> GetTaskCommentTaggedUsers(Guid taskCommentId);
        void DeleteTaskCommentTaggedUsers(Guid taskCommentId);
    }

    public class TaskCommentTaggedUserService : ITaskCommentTaggedUserService
    {
        private IInnostaxDb _innostaxDb;

        public TaskCommentTaggedUserService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        } 

        public void SaveTaskCommentTaggedUsers(Guid taskCommentId, List<Guid> taggedUserIdsList)
        {
            var existingtaggedUsersList = _innostaxDb.TaskCommentTaggedUsers.Where(x => x.TaskCommentId == taskCommentId).ToList();
            if (existingtaggedUsersList != null)
            {
                existingtaggedUsersList.ForEach(x => _innostaxDb.TaskCommentTaggedUsers.Remove(x));
            }
            foreach (var eachTaggedUser in taggedUserIdsList.Distinct())
            {
                var newTaskCommentTag = new TaskCommentTaggedUser
                {
                    TaskCommentId = taskCommentId,
                    UserId = eachTaggedUser
                };
                _innostaxDb.TaskCommentTaggedUsers.Add(newTaskCommentTag);
            }          
                _innostaxDb.SaveChangesWithErrors();
        }

        public List<Models.Account.User> GetTaskCommentTaggedUsers(Guid taskCommentId)
        {
            var existingTaggedUsersList = _innostaxDb.TaskCommentTaggedUsers.Include(y => y.User).Where(x => x.TaskCommentId == taskCommentId).Select(y=>y.User).ToList();
            var usersList = new List<Models.Account.User>();
            usersList = Mapper.Map<List<Models.Account.User>>(existingTaggedUsersList);
            return usersList;
        }

        public void DeleteTaskCommentTaggedUsers(Guid taskCommentId)
        {
            var existingTaggedUsersList = _innostaxDb.TaskCommentTaggedUsers.Where(x => x.TaskComment.Id == taskCommentId).ToList();
            existingTaggedUsersList.ForEach(x => _innostaxDb.TaskCommentTaggedUsers.Remove(x));                     
        }
    }
}

