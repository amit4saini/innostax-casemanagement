namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTaskDescriptionTaggedUsersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskDescriptionTaggedUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        TaskId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TaskId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskDescriptionTaggedUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.TaskDescriptionTaggedUsers", "TaskId", "dbo.Tasks");
            DropIndex("dbo.TaskDescriptionTaggedUsers", new[] { "TaskId" });
            DropIndex("dbo.TaskDescriptionTaggedUsers", new[] { "UserId" });
            DropTable("dbo.TaskDescriptionTaggedUsers");
        }
    }
}
