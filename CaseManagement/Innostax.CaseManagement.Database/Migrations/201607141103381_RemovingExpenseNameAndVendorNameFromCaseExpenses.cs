namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovingExpenseNameAndVendorNameFromCaseExpenses : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CaseExpenses", "ExpenseName");
            DropColumn("dbo.CaseExpenses", "VendorName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CaseExpenses", "VendorName", c => c.String());
            AddColumn("dbo.CaseExpenses", "ExpenseName", c => c.String());
        }
    }
}
