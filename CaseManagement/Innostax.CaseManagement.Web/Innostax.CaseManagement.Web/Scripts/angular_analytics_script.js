﻿﻿function displayAnalytics(selectedAnalytics, selectedRows, selectedCols, analyticsTypeEnum, analyticsType) {
    if (analyticsType == 0)
        window.caseAnalytics = selectedAnalytics;
    else if (analyticsType == 1)
        window.taskAnalytics = selectedAnalytics;
    else if (analyticsType == 2)
        window.expenseAnalytics = selectedAnalytics;
    else if (analyticsType == 3)
        window.timesheetAnalytics = selectedAnalytics;
    else if (analyticsType == 4) {
      window.reportAnalytics = selectedAnalytics;
    }
    else if (analyticsType == 5)
    {
        window.reportAnalytics = selectedAnalytics;
    }
    var selectedFields = {
        rows: selectedRows,
        cols: selectedCols
    };
    switch (analyticsType) {
        case analyticsTypeEnum['Case']: $("#case-analytics").pivotUI(selectedAnalytics, selectedFields); break;
        case analyticsTypeEnum['Task']: $("#task-analytics").pivotUI(selectedAnalytics, selectedFields); break;
        case analyticsTypeEnum['Expense']: $("#expense-analytics").pivotUI(selectedAnalytics, selectedFields); break;
        case analyticsTypeEnum['Timesheet']: $("#timesheet-analytics").pivotUI(selectedAnalytics, selectedFields); break;
        case analyticsTypeEnum['Report']: $("#report-analytics").pivotUI(selectedAnalytics, selectedFields); break;
        case analyticsTypeEnum['Client']: $("#client-analytics").pivotUI(selectedAnalytics, selectedFields); break;
    }
}

function saveAnalyticsReportConfiguration() {
    $("#analytics_loader").show();
    var currentAnalyticsConfiguration = getActiveAnalyticsConfiguration();
    var data = JSON.stringify({ Id: selectedAnalyticsReportId, AnalyticsType: activeAnalyticsTab, Configuration: JSON.stringify(currentAnalyticsConfiguration), ReportName: analyticsReportName });
    $.ajax({
        url: '/api/AnalyticsConfiguration',
        type: 'POST',
        data: data,
        cache: false,
        dataType: "application/json",
        contentType: "application/json",
        processData: false,
        success: function (data, textStatus, jqXHR) {
            $("#analytics_loader").hide();
            angular.element('#analytics_view').scope().analyticsController.getAllAnalyticsReportsForUser();
            toastr.success("Configuration saved successfully");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#analytics_loader").hide();
            toastr.error("Something went wrong, we are trying to resolve it");
        }
    });
};

function restoreInitialConfiguration() {
    $("#analytics_loader").show();
    var caseAnalyticsData = window.caseAnalytics;
    var taskAnalyticsData = window.taskAnalytics;
    var expenseAnalyticsData = window.expenseAnalytics;
    var timesheetAnalyticsData = window.timesheetAnalytics;
    var reportAnalyticsData = window.reportAnalytics;
    var initialPivotTableConfig = { "cols": [], "rows": [], "vals": [], "unusedAttrsVertical": 85, "autoSortUnusedAttrs": false, "aggregatorName": "Count", "rendererName": "Table" };

    switch (activeAnalyticsTab) {
        case "Case":
            var initialCaseConfig = initialPivotTableConfig;
            initialCaseConfig.cols = caseCols;
            initialCaseConfig.rows = caseRows;
            var currentCaseConfig = $("#case-analytics").data("pivotUIOptions");
            currentCaseConfig = mapSavedConfigToCurrentConfig(currentCaseConfig, initialCaseConfig);
            $("#case-analytics").pivotUI(caseAnalyticsData, currentCaseConfig, true);
            break;
        case "Task":
            var initialTaskConfig = initialPivotTableConfig;
            initialTaskConfig.cols = taskCols;
            initialTaskConfig.rows = taskRows;
            var currentTaskConfig = $("#task-analytics").data("pivotUIOptions");
            currentTaskConfig = mapSavedConfigToCurrentConfig(currentTaskConfig, initialTaskConfig);
            $("#task-analytics").pivotUI(taskAnalyticsData, currentTaskConfig, true);
            break;
        case "Expense":
            var initialExpenseConfig = initialPivotTableConfig;
            initialExpenseConfig.cols = expenseCols;
            initialExpenseConfig.rows = expenseRows;
            var currentExpenseConfig = $("#expense-analytics").data("pivotUIOptions");
            currentExpenseConfig = mapSavedConfigToCurrentConfig(currentExpenseConfig, initialExpenseConfig);
            $("#expense-analytics").pivotUI(expenseAnalyticsData, currentExpenseConfig, true);
            break;
        case "Timesheet":
            var initialTimesheetConfig = initialPivotTableConfig;
            initialTimesheetConfig.cols = timesheetCols;
            initialTimesheetConfig.rows = timesheetRows;
            var currentTimesheetConfig = $("#timesheet-analytics").data("pivotUIOptions");
            currentTimesheetConfig = mapSavedConfigToCurrentConfig(currentTimesheetConfig, initialTimesheetConfig);
            $("#timesheet-analytics").pivotUI(timesheetAnalyticsData, currentTimesheetConfig, true);
            break;
        case "Report":
            var initialReportConfig = initialPivotTableConfig;
            initialReportConfig.cols = reportCols;
            initialReportConfig.rows = reportRows;
            var currentReportConfig = $("#report-analytics").data("pivotUIOptions");
            currentTimesheetConfig = mapSavedConfigToCurrentConfig(currentReportConfig, initialReportConfig);
            $("#report-analytics").pivotUI(reportAnalyticsData, currentReportConfig, true);
            break;
        case "Client":
            var initialReportConfig = initialPivotTableConfig;
            initialReportConfig.cols = reportCols;
            initialReportConfig.rows = reportRows;
            var currentReportConfig = $("#client-analytics").data("pivotUIOptions");
            currentTimesheetConfig = mapSavedConfigToCurrentConfig(currentReportConfig, initialReportConfig);
            $("#client-analytics").pivotUI(reportAnalyticsData, currentReportConfig, true);
            break;
    }
    $("#analytics_loader").hide();
}

function restoreAnalyticsReportConfiguration() {
    $("#analytics_loader").show();
    var caseAnalyticsData = window.caseAnalytics;
    var taskAnalyticsData = window.taskAnalytics;
    var expenseAnalyticsData = window.expenseAnalytics;
    var timesheetAnalyticsData = window.timesheetAnalytics;
    var reportAnalyticsData = window.reportAnalytics;
    $.ajax({
        url: '/api/Analytics/' + user.UserId + '/' + selectedAnalyticsReportId + '/Reports',
        type: 'GET',
        cache: false,
        dataType: "text",
        contentType: "text",
        processData: false,
        success: function (data, textStatus, jqXHR) {
            receivedData = JSON.parse(data);
            if (receivedData == null) {
                $("#analytics_loader").hide();
                return;
            }
            switch (receivedData.analyticsType) {
                case "Case":
                    var currentConfig = $("#case-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#case-analytics").pivotUI(caseAnalyticsData, currentConfig, true);
                    break;
                case "Task":
                    var currentConfig = $("#task-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#task-analytics").pivotUI(taskAnalyticsData, currentConfig, true);
                    break;
                case "Expense":
                    var currentConfig = $("#expense-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#expense-analytics").pivotUI(expenseAnalyticsData, currentConfig, true);
                    break;
                case "Timesheet":
                    var currentConfig = $("#timesheet-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#timesheet-analytics").pivotUI(timesheetAnalyticsData, currentConfig, true);
                    break;
                case "Report":
                    var currentConfig = $("#report-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#report-analytics").pivotUI(reportAnalyticsData, currentConfig, true);
                    break;
                case "Client":
                    var currentConfig = $("#client-analytics").data("pivotUIOptions");
                    var savedConfig = JSON.parse(receivedData.configuration);
                    currentConfig = mapSavedConfigToCurrentConfig(currentConfig, savedConfig);
                    $("#client-analytics").pivotUI(reportAnalyticsData, currentConfig, true);
                    break;
            }
            $("#analytics_loader").hide();
            toastr.success("Configuration restored successfully");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#analytics_loader").hide();
            toastr.error("Something went wrong, we are trying to resolve it");
        }
    });
};

function getActiveAnalyticsConfiguration() {
    var activeConfiguration = null;
    var caseConfig = $("#case-analytics").data("pivotUIOptions");
    var taskConfig = $("#task-analytics").data("pivotUIOptions");
    var expenseConfig = $("#expense-analytics").data("pivotUIOptions");
    var timesheetConfig = $("#timesheet-analytics").data("pivotUIOptions");
    var reportConfig = $("#report-analytics").data("pivotUIOptions");
    var clientConfig = $("#client-analytics").data("pivotUIOptions");;

    switch (activeAnalyticsTab) {
        case "Case":
            activeConfiguration = caseConfig;
            break;
        case "Task":
            activeConfiguration = taskConfig;
            break;
        case "Expense":
            activeConfiguration = expenseConfig;
            break;
        case "Timesheet":
            activeConfiguration = timesheetConfig;
            break;
        case "Report":
            activeConfiguration = reportConfig;
            break;
        case "Client":
            activeConfiguration = clientConfig;
            break;
    }
    return activeConfiguration;
}

function exportToExcel() { 
    var currentAnalyticsConfiguration = getActiveAnalyticsConfiguration();
    var config = jQuery.extend(true, {}, currentAnalyticsConfiguration);
    //push cols into rows
    for (var count = 0; count < config.cols.length; count++) {
        config.rows.push(config.cols[count]);
    }
    var data = JSON.stringify(config.rows);
    var caseStartTime = analyticsStartDate;
    var caseEndTime = analyticsEndDate;
    $.ajax({
        url: '/api/AnalyticsConfiguration/' + activeAnalyticsTab + '/ExportToExcel?startDate=' + caseStartTime + '&endDate=' + caseEndTime,
        type: 'POST',
        data: data,
        cache: false,
        dataType: "json",
        contentType: "application/json",
        processData: false,
        success: function (data, textStatus, jqXHR) {
            window.location.href = "/api/File?fileName=" + data;
            $("#analytics_loader").hide();
            toastr.success("File exported successfully");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#analytics_loader").hide();
            toastr.error("Something went wrong, we are trying to resolve it");
        }
    });
}

function mapSavedConfigToCurrentConfig(currentConfig, savedConfig) {
    currentConfig.unusedAttrsVertical = savedConfig.unusedAttrsVertical;
    currentConfig.autoSortUnusedAttrs = savedConfig.autoSortUnusedAttrs;
    currentConfig.aggregatorName = savedConfig.aggregatorName;
    currentConfig.rendererName = savedConfig.rendererName;
    currentConfig.vals = savedConfig.vals;
    currentConfig.cols = savedConfig.cols;
    currentConfig.rows = savedConfig.rows;
    currentConfig.menuLimit = savedConfig.menuLimit;
    currentConfig.onRefresh = savedConfig.onRefresh;
    return currentConfig;
}

function displayClientAnalytics(clientAnalytics) {
    //    $("#client-analytics").pivotUI(
    //    clientAnalytics,
    //    {
    //        rows: ["CaseNumber", "CaseNickName", ],
    //        cols: ["ClientName", "ClientContactUser"]
    //    }
    //);
}
function displayUserAnalytics(userAnalytics) {
    //    $("#user-analytics").pivotUI(
    //    userAnalytics,
    //    {
    //        rows: ["UserName", "CasesCreated", "DelayedTasks", "Timesheets", "Expenses"],
    //        cols: []
    //    }
    //);
}