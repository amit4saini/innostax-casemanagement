namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EditedDiscussionAndDiscussionMessageAndNotificationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discussions", "IsNotes", c => c.Boolean(nullable: false));
            AddColumn("dbo.Notifications", "URL", c => c.String());
            DropColumn("dbo.DiscussionMessages", "IsNotes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DiscussionMessages", "IsNotes", c => c.Boolean(nullable: false));
            DropColumn("dbo.Notifications", "URL");
            DropColumn("dbo.Discussions", "IsNotes");
        }
    }
}
