namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIHIQualityDescriptionInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "IHIQualityDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "IHIQualityDescription");
        }
    }
}
