﻿using System;
using System.Collections.Generic;
using Innostax.CaseManagement.Models.Account;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionMessage
    {
        public Guid Id { get; set; }
        public Guid DiscussionId { get; set; }
        public Discussion Discussion { get; set; }
        public string Message { get; set; }
        public Guid CreatedBy { get; set; } 
        public User CreatedByUser { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime LastEditedDateTime { get; set; }
        public Guid? EditedBy { get; set; } 
        public bool IsArchive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public FileModel[] Files { get; set; }
        public List<UploadedImages> Images { get; set; }      
    }
}
