﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseExpenseService
    {
        void Save(CaseExpense caseExpenseData);
        void Delete(Guid caseExpenseId);
        List<CaseExpense> GetCaseExpenseByCaseId(Guid caseId);
        void UpdateCaseExpensesList(List<CaseExpense> caseExpenseList);
    }

    public class CaseExpenseService : ICaseExpenseService
    {
        private IInnostaxDb _innostaxDb;
        private IUserService _userService;
        private IErrorHandlerService _errorHandlerService;
        private ICommonService _commonService;
        private ICaseHistoryService _caseHistoryService;

        public CaseExpenseService(IInnostaxDb innostaxDb, IUserService userService, IErrorHandlerService errorHandlerService, ICommonService commonService, ICaseHistoryService caseHistoryService)
        {
            _errorHandlerService = errorHandlerService;
            _innostaxDb = innostaxDb;
            _userService = userService;
            _commonService = commonService;
            _caseHistoryService = caseHistoryService;
        }

        public List<CaseExpense> GetCaseExpenseByCaseId(Guid caseId)
        {
            var result = new List<CaseExpense>();
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_READ_ALL, caseId);
            result = Mapper.Map<List<Models.CaseExpense>>(_userService.UserPermissions.Contains(Permission.VIEW_ALL_CASE_EXPENSE) ? _innostaxDb.CaseExpenses.Include(x => x.Vendor).Where(x => x.CaseId == caseId && x.IsDeleted == false).ToList(): _innostaxDb.CaseExpenses.Include(x => x.Vendor).Where(x => x.CaseId == caseId && x.IsDeleted == false && x.CreatedBy == _userService.UserId).ToList());
            return result;
        }

        public void Save(CaseExpense caseExpenseData)
        {
            var isNewCaseExpense = false;
            bool caseInvoiceApproved = false;
            _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_UPDATE_ALL, caseExpenseData.CaseId);
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseExpenseData.CaseId && x.IsDeleted == false);
            if (existingCase == null)
            {
                string caseNotFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(caseNotFoundError);
            }
            var currentUserId = _userService.UserId;
            var existingCaseExpense = _innostaxDb.CaseExpenses.FirstOrDefault(x => x.Id == caseExpenseData.Id);
            if (existingCaseExpense == null)
            {
                existingCaseExpense = new Innostax.Common.Database.Models.CaseExpense();
                isNewCaseExpense = true;
            }
            if ((existingCaseExpense.InvoiceApproved != caseExpenseData.InvoiceApproved) && caseExpenseData.InvoiceApproved == true) { caseInvoiceApproved = true; }
            existingCaseExpense = Mapper.Map<Common.Database.Models.CaseExpense>(caseExpenseData);
            if (isNewCaseExpense)
            {
                existingCaseExpense.CreatedBy = currentUserId;
                existingCaseExpense.CreationDateTime = _commonService.GetUtcDateTime();
            }
            string expensePaymentType = string.Concat(caseExpenseData.ExpensePaymentType.ToString(), " for a vendor ", existingCaseExpense.Vendor.VendorName);
            existingCaseExpense.LastEditedBy = currentUserId;
            existingCaseExpense.LastEditedDateTime = _commonService.GetUtcDateTime();
            existingCaseExpense.DeletedOn = null;
            existingCaseExpense.DeletedBy = null;
            if (existingCaseExpense.Vendor.VendorId == Guid.Empty || existingCaseExpense.Vendor.VendorId == null)
            {
                var newVendor = new Innostax.Common.Database.Models.Vendor();
                newVendor.VendorName = caseExpenseData.Vendor.VendorName;
                newVendor.CreatedOn = _commonService.GetUtcDateTime();
                _innostaxDb.Vendors.Add(newVendor);
                existingCaseExpense.VendorId = newVendor.VendorId;
            }
            existingCaseExpense.Vendor = null;
            _innostaxDb.CaseExpenses.AddOrUpdate(existingCaseExpense);
            _innostaxDb.SaveChangesWithErrors();
            if (isNewCaseExpense)
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_EXPENSE_ADDED, expensePaymentType);
            else
                if (caseInvoiceApproved) { _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_EXPENSE_APPROVED, expensePaymentType); }
            else { _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_EXPENSE_EDITED, expensePaymentType); }
        }

        public void Delete(Guid caseExpenseId)
        {
            Result result = new Result();
            var existingCaseExpense = _innostaxDb.CaseExpenses.FirstOrDefault(x => x.Id == caseExpenseId && (x.IsDeleted != true));
            if (existingCaseExpense != null)
            {
                _commonService.ValidateAccess(Validate.CASE, Common.Database.Enums.Permission.CASE_DELETE_ALL, existingCaseExpense.CaseId);
                existingCaseExpense.IsDeleted = true;
                existingCaseExpense.DeletedBy = _userService.UserId;
                existingCaseExpense.DeletedOn = _commonService.GetUtcDateTime();
                _innostaxDb.SaveChangesWithErrors();
                var expenseName = ((ExpensePaymentType)existingCaseExpense.ExpensePaymentType).ToString();
                _caseHistoryService.SaveCaseHistory(existingCaseExpense.CaseId, Common.Database.Enums.Action.CASE_EXPENSE_DELETED, expenseName);
            }
        }  

        public void UpdateCaseExpensesList(List<CaseExpense> caseExpenseList)
        {
            foreach (var eachCaseExpenseData in caseExpenseList)
            {
                Save(eachCaseExpenseData);
            }
        }
     }
    }