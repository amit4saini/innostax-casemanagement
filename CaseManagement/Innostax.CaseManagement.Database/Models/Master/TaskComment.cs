﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innostax.Common.Database.Models.UserManagement;

namespace Innostax.Common.Database.Models
{
    public class TaskComment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid TaskId { get; set; }
        [ForeignKey("TaskId")]
        public Task Task { get; set; }

        public string Comment { get; set; }

        public Guid CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public DateTime CreationDateTime { get; set; }

        public Guid LastEditedBy { get; set; }
        [ForeignKey("LastEditedBy")]
        public User LastEditedByUser { get; set; }

        public DateTime LastEditedDateTime { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }

        public DateTime? DeletionDateTime { get; set; }
    }
}
