﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class VendorServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private VendorService sut;
        Guid vendorId;

        [TestInitialize]
        public void TestInitialize()
        {
            vendorId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            sut = new VendorService(innostaxDbMock.Object);           
        }

        private List<Vendor> GetDummyVendors()
        {
            var dummyVendor = new List<Vendor>
            {
                new Vendor
                {
                    VendorName = "Test",
                    VendorId = vendorId,
                    CreatedOn = DateTime.Now
                }
            };
            return dummyVendor;
        }
        
        [TestMethod]
        public void GivenCalling_GetAllVendors_WhenVendorsAreFound_ReturnMappedResults()
        {
            var dummyVendors = GetDummyVendors();
            innostaxDbMock.Setup(c => c.Vendors).ReturnsDbSet(dummyVendors);
            var actual = sut.GetAllVendors();
            Assert.AreEqual(actual.Count, 1);
        }

        [TestMethod]
        public void GivenCalling_GetAllVendors_WhenVendorsAreNotFound_ReturnEmptySet()
        {
            var dummyVendors = new Innostax.Common.Database.Models.Vendor[0];
            innostaxDbMock.Setup(c => c.Vendors).ReturnsDbSet(dummyVendors);
            var actual = sut.GetAllVendors();
            Assert.AreEqual(actual.Count, 0);
        }
    }
}
