﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseSubStatusController : ApiController
    {
        private readonly ICaseSubStatusService _caseSubStatusService;
        public CaseSubStatusController(ICaseSubStatusService caseSubStatusService)
        {
            _caseSubStatusService = caseSubStatusService;
        }

        [System.Web.Mvc.HttpPut]
        [HasPermission(new Permission[] { Permission.CASE_UPDATE_ALL, Permission.CASE_UPDATE_OWN })]
        public void Put(SubStatus subStatus, Guid caseId)
        {
            _caseSubStatusService.ChangeCaseSubStatus(subStatus, caseId);
        }
    }
}
