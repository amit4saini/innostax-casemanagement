﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class ClientIndustryList
    {
        public List<string> ClientIndustry()
        {
            var ClientIndustries = new List<string>{
                "Oil/Gas",
                "Chemicals",
                "Technology",
                "Air/Aerospace",
                "Customs Broker",
                "Machinery",
                "Metals",
                "Legal Services",
                "Services/Consulting",
                "Logistics",
                "Investigative Company",
                "Shipping",
                "Health",
                "Finance/Insurance",
                "Agriculture",
                "Mining",
                "Construction",
                "Food/Drink",
                "Textile",
                "Media/Printing/Publishing",
                "Other",
                 };
            return ClientIndustries;
        }
    }
}
