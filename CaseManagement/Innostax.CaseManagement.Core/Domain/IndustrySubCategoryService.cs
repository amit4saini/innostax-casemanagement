﻿using Innostax.CaseManagement.Models;
using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Database;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IIndustrySubCategoryService
    {
        List<Innostax.CaseManagement.Models.IndustrySubCategory> GetIndustrySubCategory();
    }

    public class IndustrySubCategoryService : IIndustrySubCategoryService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        public IndustrySubCategoryService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Innostax.CaseManagement.Models.IndustrySubCategory> GetIndustrySubCategory()
        {
            var result = new List<Innostax.CaseManagement.Models.IndustrySubCategory>();
            var industrySubCategories = _innostaxDb.IndustrySubCategories.ToList();
            if (industrySubCategories.Count() == 0)
            {
                string subCategoryNotFoundError = Common.Constants.ErrorMessages.Common.IndustrySubCategoryError.SUBCATEGORY_NOT_FOUND;
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(subCategoryNotFoundError)
                };
                throw new HttpResponseException(response);
            }
            result = new List<IndustrySubCategory>();
            foreach (var industrySubCategory in industrySubCategories)
            {
                var mappedIndustrySubCategory = Mapper.Map<IndustrySubCategory>(industrySubCategory);
                result.Add(mappedIndustrySubCategory);
            }
            return result;
        }
    }
}
