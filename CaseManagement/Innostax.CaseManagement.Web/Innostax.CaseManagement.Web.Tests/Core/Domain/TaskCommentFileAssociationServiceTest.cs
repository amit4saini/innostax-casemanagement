﻿using AutoMapper;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class TaskCommentFileAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ITaskCommentFileAssociationService sut;
        private IErrorHandlerService errorHandlerService;
        private Mock<IUserService> userService;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICommonService> _commonService;
        Guid TaskCommentId;

        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            userService = new Mock<IUserService>();
            _commonService = new Mock<ICommonService>();
            TaskCommentId = Guid.NewGuid();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            sut = new TaskCommentFileAssociationService(innostaxDbMock.Object, errorHandlerService, userService.Object,_caseHistoryService.Object, _commonService.Object);                
        }
        
        public List<UploadedFile> GetUploadedFiles()
        {
            var dummyUploadedFiles = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new UploadedFile
                {
                    FileId = new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyUploadedFiles;
        }

        public List<Models.FileModel> GetDomainModelDummyFiles()
        {
            var dummyFiles = new List<Models.FileModel>
            {
                new Models.FileModel
                {
                     FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new Models.FileModel
                {
                    FileId = new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyFiles;
        }

        private Models.TaskCommentFileAssociation GetTaskCommentFileAssociation()
        {
            var dummyAssociation = new Models.TaskCommentFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                TaskCommentId = TaskCommentId,
                IsDeleted = false
            };
            return dummyAssociation;
        }

        private List<TaskCommentFileAssociation> GetDataBaseTaskCommentFileAssociation()
        {
            var dummyTaskCommentAssociation = new List<TaskCommentFileAssociation>
            {
                new TaskCommentFileAssociation
                {
                    TaskCommentId = TaskCommentId,
                    IsDeleted = false,
                    UploadedFile=new UploadedFile { FileName="test"}
                }
            };
            return dummyTaskCommentAssociation;
        }

        private Models.TaskCommentFileAssociation GetDummyInvalidTaskCommentsAssociation()
        {
            var dummyTaskComment = new Models.TaskCommentFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                TaskCommentId = Guid.NewGuid()
            };
            return dummyTaskComment;
        }

        public List<TaskComment> GetDummyTaskComment()
        {
            var dummyTaskComment = new List<TaskComment>
            {
                new TaskComment
                {
                    Id = TaskCommentId,
                    IsDeleted = false,
                    Task=new Innostax.Common.Database.Models.Task { CaseId=Guid.NewGuid()}
                },
                new TaskComment
                {
                    Id = new Guid("02561DAC-DAF0-E511-AA98-FCAA14866D0F"),
                    IsDeleted = false,
                    Task=new Innostax.Common.Database.Models.Task { CaseId=Guid.NewGuid()}
                }
            };
            return dummyTaskComment;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenUploadedFileTableIsEmpty_ThenThrowsNotFoundRequest()
        {
            try
            {
                var dummyFiles = new List<UploadedFile>();
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(dummyFiles);
                innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(GetDummyTaskComment());
                innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(new TaskCommentFileAssociation[0] );
                sut.Save(GetTaskCommentFileAssociation());
            }
            catch(HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, System.Net.HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenTaskCommentTableIsEmpty_ThenThrowBadRequest()
        {
            try
            {
                var dummyTaskComment = new List<TaskComment>(); 
                innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(dummyTaskComment);
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetUploadedFiles());
                sut.Save(GetTaskCommentFileAssociation());
            }
            catch(HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, System.Net.HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        public void GivenCallingSave_WhenDataIsValid_ReturnMappedResult()
        {
            var dummyTaskCommentFileAssociation = new List<TaskCommentFileAssociation>();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(GetDummyTaskComment());
            innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetUploadedFiles());
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(dummyTaskCommentFileAssociation);
            try
            {
                sut.Save(GetTaskCommentFileAssociation());
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got:" + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCallingGetTaskCommentAssociatedFiles_ThenReturnMappedResult()
        {
            var dummyTaskCommentFileAssociation = new List<TaskCommentFileAssociation>();
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(dummyTaskCommentFileAssociation);
            try
            {
                sut.GetTaskCommentAssociatedFiles(Guid.NewGuid());
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got:" + ex.Message);
            }
        }
        
        [TestMethod]
        public void GivenCallingRemoveTaskCommentFileAssociation_WhenTaskCommentIdIsPresent_ReturnResults()
        {
            var dummyTaskCommentFileAssociation = GetDataBaseTaskCommentFileAssociation();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(GetDummyTaskComment());
            innostaxDbMock.Setup(c => c.TaskCommentFileAssociations).ReturnsDbSet(dummyTaskCommentFileAssociation);
            try
            {
                sut.RemoveTaskCommentFileAssociation(TaskCommentId);
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got:" + ex.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingRemoveTaskCommentFileAssociation_WhenTaskCommentIdIsNotPresent_ReturnTaskCommentIdError()
        {
            var dummyTaskComment = new List<TaskComment>();
            innostaxDbMock.Setup(c => c.TaskComments).ReturnsDbSet(dummyTaskComment);
            try
            {
                sut.RemoveTaskCommentFileAssociation(Guid.NewGuid());
            }
            catch(HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, System.Net.HttpStatusCode.BadRequest);
                throw;
            }
            
        }
    }
}
