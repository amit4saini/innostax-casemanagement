﻿using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IIndustryService
    {
        List<Innostax.CaseManagement.Models.Industry> GetAllIndustries();
        Innostax.CaseManagement.Models.Industry GetIndustry(string id);
    }

    public class IndustryService : IIndustryService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public IndustryService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Innostax.CaseManagement.Models.Industry> GetAllIndustries()
        {
            var result = new List<Innostax.CaseManagement.Models.Industry>();
            var allIndustries = _innostaxDb.Industries.ToList();
            if (allIndustries.Count == 0)
            {
                string industryNotFoundError = Common.Constants.ErrorMessages.Common.IndustryError.INDUSTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(industryNotFoundError);
            }
            result = new List<Innostax.CaseManagement.Models.Industry>();
            foreach (var industry in allIndustries)
            {
                result.Add(Mapper.Map<Industry>(industry));
            }
            return result;
        }

        public Innostax.CaseManagement.Models.Industry GetIndustry(string id)
        {
            int industryId;
            if (!Int32.TryParse(id, out industryId))
            {
                string invalidIndustryIdError = Common.Constants.ErrorMessages.Common.IndustryError.INVALID_INDUSTRY_ID;
                _errorHandlerService.ThrowBadRequestError(invalidIndustryIdError);
            }
            var result = new Innostax.CaseManagement.Models.Industry();
            result = new Innostax.CaseManagement.Models.Industry();
            var industry = _innostaxDb.Industries.FirstOrDefault(x => x.Id == industryId);
            if (industry == null)
            {
                string industryNotFoundError = Common.Constants.ErrorMessages.Common.IndustryError.INDUSTRY_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(industryNotFoundError);
            }
            var mappedIndustry = Mapper.Map<Industry>(industry);
            result = mappedIndustry;
            return result;
        }
    }
}
