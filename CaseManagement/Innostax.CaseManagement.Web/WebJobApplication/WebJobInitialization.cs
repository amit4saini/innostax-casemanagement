﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.CaseManagement.Web.DependencyResolution;
using Innostax.CaseManagement.Web.App_Start;

namespace WebJobApplication
{
    public class WebJobInitialization
    {
        public void AutomapperInitialization()
        {
            AutomapperConfig.Configure();
        }
    }
}
