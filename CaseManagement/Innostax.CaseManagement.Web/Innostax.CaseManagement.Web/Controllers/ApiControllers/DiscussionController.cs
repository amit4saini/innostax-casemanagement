﻿using System.Web.Http;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Core.Domain;
using System;
using System.Web.Http.OData.Query;
using System.Threading.Tasks;
using Innostax.CaseManagement.Web.Hubs;
using Innostax.Common.Database.Enums;
using Innostax.CaseManagement.Web.Attributes;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class DiscussionController : ApiController
    {
        private readonly IDiscussionService _discussionService;
        private INotificationHub _notificationHub;
        private INotificationService _notificationService;
        public DiscussionController(IDiscussionService discussionService, INotificationHub notificationHub, INotificationService notificationService)
        {
            _discussionService = discussionService;
            _notificationHub = notificationHub;
            _notificationService = notificationService;
        }

        // POST api/<controller>       
        [HttpPost]
        [HasPermission(new Permission[] { Permission.CAN_CREATE_DISCUSSION })]
        public void Post(Discussion discussionData)
        {
            var listOfUserIdToBeNotified=_discussionService.Save(discussionData);
            foreach (var userId in listOfUserIdToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [HttpPut]
        [HasPermission(new Permission[] { Permission.CAN_EDIT_DISCUSSION })]
        public void Put(Guid id, Discussion discussionData)
        {
            discussionData.Id = id;
             _discussionService.Save(discussionData);
        }

        [HttpDelete]
        [HasPermission(new Permission[] { Permission.CAN_DELETE_A_DISCUSSION })]
        public void Delete(Guid id)
        {
            _discussionService.ArchiveUnarchiveDiscussion(id, true);
        }

        [HttpGet]
        [Route("api/Case/{caseId}/Discussions")]
        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_DISCUSSIONS, Permission.CAN_VIEW_OWN_DISCUSSIONS })]
        public async Task<DiscussionPageResult> Get(ODataQueryOptions<Discussion> options, Guid caseId)
        {
            return await _discussionService.GetAllDiscussionsByUserIdOrCaseId(options, Request, caseId);
        }
		
        [HttpGet]
        public Discussion Get(Guid Id)
        {
            return _discussionService.GetDiscussionByDiscussionId(Id);
        }                    

        [HttpGet]
        [Route("api/Search/Discussions")]
        public async System.Threading.Tasks.Task<DiscussionPageResult> SearchDiscussions(ODataQueryOptions<Discussion> options, Guid caseId, string keyword)
        {
            return await _discussionService.SearchDiscussionsByKeyword(options, Request, caseId, keyword);
        }
    }
}