namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingDDSupervisorFieldToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "DDSupervisorId", c => c.Guid());
            AddColumn("dbo.CaseReportAssociations", "StateId", c => c.Int());
            CreateIndex("dbo.Cases", "DDSupervisorId");
            CreateIndex("dbo.CaseReportAssociations", "StateId");
            AddForeignKey("dbo.Cases", "DDSupervisorId", "dbo.Users", "Id");
            AddForeignKey("dbo.CaseReportAssociations", "StateId", "dbo.States", "StateId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaseReportAssociations", "StateId", "dbo.States");
            DropForeignKey("dbo.Cases", "DDSupervisorId", "dbo.Users");
            DropIndex("dbo.CaseReportAssociations", new[] { "StateId" });
            DropIndex("dbo.Cases", new[] { "DDSupervisorId" });
            DropColumn("dbo.CaseReportAssociations", "StateId");
            DropColumn("dbo.Cases", "DDSupervisorId");
        }
    }
}
