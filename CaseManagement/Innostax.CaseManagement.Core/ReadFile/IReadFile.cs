﻿namespace Innostax.CaseManagement.Core.ReadFile
{
    public interface IReadFile
    {
        string ReadFile(string filePath);
    }
}
