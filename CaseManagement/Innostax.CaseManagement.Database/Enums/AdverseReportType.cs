﻿
namespace Innostax.Common.Database.Enums
{
    public enum AdverseReportType
    {
        Not_Adverse,
        Adverse,
        Very_Adverse,
        NA

    }
}
