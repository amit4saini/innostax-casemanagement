﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData.Query;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class MyController : ApiController
    {
        private readonly IDiscussionService _discussionService;
        public MyController(IDiscussionService discussionService)
        {
            _discussionService = discussionService;
        }

        [HttpGet]
        [System.Web.Http.Route("api/My/Discussions")]
        public async Task<DiscussionPageResult> GetDiscussionsListForCurrentUser(ODataQueryOptions<Discussion> options)
        {
            return await _discussionService.GetAllDiscussionsByUserIdOrCaseId(options, Request, new Guid());
        }
    }
}
