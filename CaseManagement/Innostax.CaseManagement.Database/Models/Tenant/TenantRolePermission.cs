﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;

namespace Innostax.Common.Database.Models
{
    public class TenantRolePermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RolePermissionId { get; set; }       
        public Permission Permission { get; set; }
        public Guid RoleId { get; set; }
        [ForeignKey("RoleId")]
        public TenantRole Role { get; set; }
    }
}
