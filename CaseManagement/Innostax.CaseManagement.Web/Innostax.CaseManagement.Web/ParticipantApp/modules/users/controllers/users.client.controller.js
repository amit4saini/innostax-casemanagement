﻿'use strict';

angular.module('users').controller('UserController', ['Users',
    function (users) {
        return users;
    }
]).factory('HttpInterceptor', ['$q', '$window', 'usSpinnerService', function ($q, $window, usSpinnerService) {
    return function (promise) {
        return promise.then(function (response) {
            // do something on success
            // todo hide the spinner
            //alert('stop spinner');
            usSpinnerService.stop('spinner');
            return response;

        }, function (response) {
            // do something on error
            // todo hide the spinner
            //alert('stop spinner');
            usSpinnerService.stop('spinner');
            return $q.reject(response);
        });
    };
}]);;;
