﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Innostax.Common.Database.Models;

namespace Innostax.Common.Database.Database
{
    public interface ITenantDb : IDisposable, IObjectContextAdapter
    {
        int SaveChangesWithErrors();
        DbSet<TenantUser> TenantUsers { get; set; }

        DbSet<TenantRole> TenantRoles { get; set; }

        DbSet<TenantUserRole> TenantUserRoles { get; set; }

        DbSet<TenantUserLogin> TenantUserLogins { get; set; }

        DbSet<TenantRolePermission> TenantRolePermissions { get; set; }

    }
}

