namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class removingNotesTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Notes", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.Notes", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "DeletedBy", "dbo.Users");
            DropForeignKey("dbo.Notes", "LastEditedBy", "dbo.Users");
            DropIndex("dbo.Notes", new[] { "CaseId" });
            DropIndex("dbo.Notes", new[] { "CreatedBy" });
            DropIndex("dbo.Notes", new[] { "LastEditedBy" });
            DropIndex("dbo.Notes", new[] { "DeletedBy" });
            DropTable("dbo.Notes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        NoteId = c.Guid(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(),
                        LastEditedBy = c.Guid(),
                        LastEditedDateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.NoteId);
            
            CreateIndex("dbo.Notes", "DeletedBy");
            CreateIndex("dbo.Notes", "LastEditedBy");
            CreateIndex("dbo.Notes", "CreatedBy");
            CreateIndex("dbo.Notes", "CaseId");
            AddForeignKey("dbo.Notes", "LastEditedBy", "dbo.Users", "Id");
            AddForeignKey("dbo.Notes", "DeletedBy", "dbo.Users", "Id");
            AddForeignKey("dbo.Notes", "CreatedBy", "dbo.Users", "Id");
            AddForeignKey("dbo.Notes", "CaseId", "dbo.Cases", "CaseId");
        }
    }
}
