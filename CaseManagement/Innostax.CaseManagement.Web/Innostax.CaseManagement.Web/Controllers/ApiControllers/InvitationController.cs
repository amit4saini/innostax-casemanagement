﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Mvc;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class InvitationController : ApiController
    {
        private readonly IInvitationService _invitationService;
        private readonly IUserService _userService;
        private readonly IErrorHandlerService _errorHandlerService;
        public InvitationController(IInvitationService invitationService, IUserService userService, IErrorHandlerService errorHandlerService)
        {
            _invitationService = invitationService;
            _userService = userService;
            _errorHandlerService = errorHandlerService;
        }

        [System.Web.Http.HttpPost]
        [HasPermission(new Permission[] { Permission.INVITE_ALL,Permission.INVITE_NFCUSER })]
        public async System.Threading.Tasks.Task Post(InvitationModel invitation)
        {
            if (invitation == null || invitation.User == null) { return; }
            var newUser = invitation.User;
            if (invitation.User.Id == null || invitation.User.Id == Guid.Empty)
            {
                newUser = _userService.SaveUser(invitation.User);
                var userManager = HttpContext.Current.GetOwinContext().GetUserManager<UserManager>();
                string code = await userManager.GeneratePasswordResetTokenAsync(newUser.Id);
                var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
                var link = url.Action("SetPassword", "Account", new { userId = newUser.Id, code = code }, protocol: "http");
                _invitationService.SendInvitationToUser(invitation.User, link);
                invitation.User.Id = newUser.Id;
            }
            _invitationService.CreateUserAssociation(invitation);
        }
    }
}
