﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscussionFileAssociationService
    {
        void Save(Models.DiscussionFileAssociation requestData);
        void RemoveDiscussionFileAssociation(Guid discussionId);
        List<Models.FileModel> GetDiscussionAssociatedFiles(Guid discussionId);
    }

    public class DiscussionFileAssociationService: IDiscussionFileAssociationService
    {
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IUserService _userService;
        private readonly ICaseHistoryService _caseHistoryService;
        private readonly ICommonService _commonService;

        public DiscussionFileAssociationService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ICaseHistoryService caseHistoryService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _caseHistoryService = caseHistoryService;
            _commonService = commonService;
        }

        public void Save(Models.DiscussionFileAssociation requestData)
        {
            var existingDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == requestData.DiscussionId && x.IsDeleted != true);
            if (existingDiscussion != null)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_UPDATE_ALL, requestData.DiscussionId);
                var existingDiscussionFileAssociations = _innostaxDb.DiscussionFileAssociations.Include(y=>y.UploadedFile).Where(x => x.DiscussionId == requestData.DiscussionId && x.IsDeleted != true).ToList();
                if (existingDiscussionFileAssociations.Count > 0)
                {
                    foreach (var eachAssociation in existingDiscussionFileAssociations)
                    {
                        if(requestData.Files.FirstOrDefault(x => x.FileId == eachAssociation.FileId) == null)
                        {
                            var fileName = eachAssociation.UploadedFile.FileName;                           
                            _innostaxDb.DiscussionFileAssociations.Remove(eachAssociation);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_DELETED,fileName, existingDiscussion.Title);
                            _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.FILE_DELETED, historyMessage);
                        }
                    }
                }

                if (requestData.Files.Count > 0)
                {
                    foreach (var eachFile in requestData.Files)
                    {
                        if (_innostaxDb.DiscussionFileAssociations.FirstOrDefault(x => x.FileId == eachFile.FileId && x.DiscussionId == requestData.DiscussionId && x.IsDeleted != true) == null)
                        {
                            SaveDiscussionAssociatedFiles(eachFile, requestData);
                            var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_ADDED, eachFile.FileName, existingDiscussion.Title);
                            _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.FILE_ADDED, historyMessage);
                        }
                    }
                }             
                    _innostaxDb.SaveChangesWithErrors();           
            }
            else
            {
                var invalidDiscussionIdError = Common.Constants.ErrorMessages.DiscussionManagement.DiscussionError.DISCUSSIONS_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(invalidDiscussionIdError);
            }
        }

        public void RemoveDiscussionFileAssociation(Guid discussionId)
        {
            var existingDiscussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionId && x.IsDeleted != true);
            if (existingDiscussion != null)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_DELETE_ALL, discussionId);
                var discussionFileAssociations = _innostaxDb.DiscussionFileAssociations.Include(y=>y.UploadedFile).Where(x => x.DiscussionId == discussionId && x.IsDeleted != true).ToList();
                foreach (var discussionFileAssociation in discussionFileAssociations)
                {
                    discussionFileAssociation.IsDeleted = true;
                    _innostaxDb.SaveChangesWithErrors();
                    var historyMessage = string.Format(Common.Constants.CaseHistory.DISCUSSION_FILE_DELETED, discussionFileAssociation.UploadedFile.FileName, existingDiscussion.Title);
                    _caseHistoryService.SaveCaseHistory(existingDiscussion.CaseId, Common.Database.Enums.Action.FILE_DELETED,historyMessage);
                }             
                          
            }
            else
            {
                var invalidDiscussionIdError = Common.Constants.ErrorMessages.DiscussionManagement.DiscussionError.DISCUSSIONS_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(invalidDiscussionIdError);
            }
        }

        public List<Models.FileModel> GetDiscussionAssociatedFiles(Guid discussionId)
        {
            var result = new List<Models.FileModel>();
            var currentDiscussionFileAssociations = _innostaxDb.DiscussionFileAssociations.Include(y=>y.Discussion).Where(x => x.DiscussionId == discussionId && x.IsDeleted != true).ToList();
            if(currentDiscussionFileAssociations.Count > 0)
            {
                _commonService.ValidateAccess(Validate.DISCUSSION, Permission.CASE_READ_ALL, discussionId);
            }
            foreach (var eachAssociation in currentDiscussionFileAssociations)
            {
                var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == eachAssociation.FileId && x.IsDeleted != true);
                var mappedFile = Mapper.Map<CaseManagement.Models.FileModel>(existingFile);
                mappedFile.FileUrl = existingFile.FileUrl;
                mappedFile.FileName = existingFile.FileName;
                var associatedUser = _userService.GetUser(existingFile.UserId).Data;
                mappedFile.UserName = associatedUser.FirstName + " " + associatedUser.LastName;
                result.Add(mappedFile);
            }
            return result.OrderByDescending(x=>x.CreateDateTime).ToList();
        }

        internal void SaveDiscussionAssociatedFiles(FileModel file, Models.DiscussionFileAssociation requestData)
        {
            var existingFile = _innostaxDb.UploadFiles.FirstOrDefault(x => x.FileId == file.FileId && x.IsDeleted != true);
            if (existingFile == null)
            {
                string fileNotFoundError = Common.Constants.ErrorMessages.Common.FileUploadError.NO_FILE_EXISTS;
                _errorHandlerService.ThrowResourceNotFoundError(fileNotFoundError);
            }
            var mappedDiscussionFileAssociation = Mapper.Map<Common.Database.Models.DiscussionFileAssociation>(requestData);
            mappedDiscussionFileAssociation.FileId = file.FileId;
            var newDiscussionFileAssociation = _innostaxDb.DiscussionFileAssociations;
            newDiscussionFileAssociation.Add(mappedDiscussionFileAssociation);        
                _innostaxDb.SaveChangesWithErrors();        
        }
    }
}
