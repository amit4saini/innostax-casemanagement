﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionFileAssociation
    {
        public Guid DiscussionId { get; set; }
        public List<FileModel> Files { get; set; }
        public Discussion Discussion { get; set; }
    }
}
