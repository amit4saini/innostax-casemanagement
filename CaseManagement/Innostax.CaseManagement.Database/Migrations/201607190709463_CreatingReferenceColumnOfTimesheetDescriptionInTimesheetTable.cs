namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatingReferenceColumnOfTimesheetDescriptionInTimesheetTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TimeSheets", "DescriptionId", c => c.Guid());
            CreateIndex("dbo.TimeSheets", "DescriptionId");
            AddForeignKey("dbo.TimeSheets", "DescriptionId", "dbo.TimeSheetDescriptions", "Id");
            DropColumn("dbo.TimeSheets", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TimeSheets", "Description", c => c.String());
            DropForeignKey("dbo.TimeSheets", "DescriptionId", "dbo.TimeSheetDescriptions");
            DropIndex("dbo.TimeSheets", new[] { "DescriptionId" });
            DropColumn("dbo.TimeSheets", "DescriptionId");
        }
    }
}
