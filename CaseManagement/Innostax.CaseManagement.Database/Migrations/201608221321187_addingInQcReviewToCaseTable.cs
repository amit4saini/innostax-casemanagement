namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingInQcReviewToCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "InQCReview", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "InQCReview");
        }
    }
}
