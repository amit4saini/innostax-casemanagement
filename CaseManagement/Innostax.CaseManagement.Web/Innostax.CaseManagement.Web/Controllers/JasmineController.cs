using System.Web.Mvc;

namespace Innostax.CaseManagement.Web.Controllers
{
    public class JasmineController : Controller
    {
        public ViewResult Run()
        {
            return View("SpecRunner");
        }
    }
}
