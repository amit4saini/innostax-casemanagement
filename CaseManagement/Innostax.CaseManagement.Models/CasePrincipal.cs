﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class CasePrincipal
    {
        public Guid Id { get; set; }
        public string PrincipalName { get; set; }
    }
}
