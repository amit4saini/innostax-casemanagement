namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesMadeInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "TotalInvestigativePeriod", c => c.Int());
            AddColumn("dbo.Cases", "TotalICIInvestigativePeriod", c => c.Int());
            AddColumn("dbo.Cases", "IHIAheadOfTime", c => c.Int());
            AddColumn("dbo.Cases", "ReportToClientAheadOfTime", c => c.Int());
            AddColumn("dbo.Cases", "ICICorpRegistrationTurnTime", c => c.Int());
            AddColumn("dbo.Cases", "DateClientCancelled", c => c.DateTime());
            AddColumn("dbo.Cases", "CancellationReason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "CancellationReason");
            DropColumn("dbo.Cases", "DateClientCancelled");
            DropColumn("dbo.Cases", "ICICorpRegistrationTurnTime");
            DropColumn("dbo.Cases", "ReportToClientAheadOfTime");
            DropColumn("dbo.Cases", "IHIAheadOfTime");
            DropColumn("dbo.Cases", "TotalICIInvestigativePeriod");
            DropColumn("dbo.Cases", "TotalInvestigativePeriod");
        }
    }
}
