﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Innostax.Common.Database.Models.RolesAndPermissionManagement;
using Innostax.Common.Database.Models.UserManagement;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Models.Master;

namespace Innostax.Common.Database.Database
{
    public interface IInnostaxDb : IDisposable, IObjectContextAdapter
    {
        int SaveChangesWithErrors();

        DbSet<User> Users { get; set; }

        DbSet<Role> Roles { get; set; }

        DbSet<UserRole> UserRoles { get; set; }

        DbSet<UserLogin> UserLogins { get; set; }

        DbSet<RolePermission> RolePermissions { get; set; }
        DbSet<UploadedFile> UploadFiles { get; set; }

        DbSet<Client> Clients { get; set; }

        DbSet<Country> Countries { get; set; }

        DbSet<State> States { get; set; }

        DbSet<Case> Cases { get; set; }

        DbSet<Industry> Industries { get; set; }

        DbSet<Region> Regions { get; set; }

        DbSet<Report> Reports { get; set; }

        DbSet<IndustrySubCategory> IndustrySubCategories { get; set; }

        DbSet<CaseReportAssociation> CaseReportAssociations { get; set; }

        DbSet<CaseFileAssociation> CaseFileAssociations { get; set; }

        DbSet<ClientIndustry> ClientIndustries { get; set; }

        DbSet<Discussion> Discussions { get; set; }

        DbSet<Task> Tasks { get; set; }

        DbSet<CaseExpense> CaseExpenses { get; set; }

        DbSet<TimeSheet> TimeSheets { get; set; }

        DbSet<Notification> Notifications { get; set; }

        DbSet<Discount> Discounts { get; set; }

        DbSet<CasePrincipal> CasePrincipals { get; set; }
    
        DbSet<TaskFileAssociation> TaskFileAssociations { get; set; }
        DbSet<CaseHistory> CaseHistory { get; set; }

        DbSet<DiscussionMessage> DiscussionMessages { get; set; }
        DbSet<DiscussionFileAssociation> DiscussionFileAssociations { get; set; }
        DbSet<DiscussionMessageFileAssociation> DiscussionMessageFileAssociations { get; set; }
        DbSet<DiscussionNotifier> DiscussionNotifiers { get; set; }

        DbSet<TimeSheetDescription> TimeSheetDescriptions { get; set; }

        DbSet<ClientContactUser> ClientContactUsers { get; set; }
		
        DbSet<TaskActivity> TaskActivities { get; set; }
		
        DbSet<ReportPrice> ReportPrices { get; set; }

        DbSet<TaskComment> TaskComments { get; set; }
		
		DbSet<TaskCommentTaggedUser> TaskCommentTaggedUsers { get; set; }

        DbSet<CaseStatus> CaseStatus { get; set; }
		   
        DbSet<SubStatus> SubStatus { get; set; }      
        DbSet<ContactUserBusinessUnit> ContactUserBusinessUnits { get; set; }
        DbSet<ELMAH_Error> ELMAH_Error { get; set; }
        DbSet<ParticipantCaseAssociation> ParticipantCaseAssociation { get; set; }
        DbSet<ICIUserCaseAssociation> ICIUserCaseAssociations { get; set; }
        DbSet<UserDetail> UserDetails { get; set; }
        DbSet<TaskCommentFileAssociation> TaskCommentFileAssociations { get; set; }
        DbSet<Vendor> Vendors { get; set; }
        DbSet<TaskDescriptionTaggedUser> TaskDescriptionTaggedUsers { get; set; }
        DbSet<DiscussionTaggedUser> DiscussionTaggedUsers { get; set; }
        DbSet<DiscussionMessageTaggedUser> DiscussionMessageTaggedUsers { get; set; }
        DbSet<NFCUserCaseAssociation> NFCUserCaseAssociations { get; set; }
        DbSet<AnalyticsConfiguration> AnalyticsConfigurations { get; set; }
        DbSet<Tenant> Tenants { get; set; }
    }
}

