﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    class SubStatusList
    {
        public List<string> SubStatus()
        {
            var SubStatusList = new List<string> {
                "stage1~START",
                "stage2~START",
                "Requested~UNPROCESSED",
                "stage3~SEND_TO_SALES_REPRESENTATIVE",
                "stage4~IN_PROGRESS",
                "stage5~IN_PROGRESS",
                "Available~IN_PROGRESS",
                "stage6~SEND_TO_SALES_REPRESENTATIVE",
                "stage7~SEND_TO_SALES_REPRESENTATIVE",
                "stage8~SEND_TO_SALES_REPRESENTATIVE",
                "Completed~SEND_TO_SALES_REPRESENTATIVE",
                "SentForApproval~SEND_TO_SALES_REPRESENTATIVE",
                "Approved~SEND_TO_SALES_REPRESENTATIVE",
                "NFCApproved~SEND_TO_SALES_REPRESENTATIVE",
                "Closed~CLOSED"
            };
            return SubStatusList;
        }
    }
}
