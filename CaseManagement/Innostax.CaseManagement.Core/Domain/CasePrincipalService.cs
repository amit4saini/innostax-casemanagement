﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICasePrincipalService
    {
        List<Models.CasePrincipal> GetCaseAssociatedPrincipals(Guid caseId);
        void Save(CasePrincipal[] principalData, Guid caseId);
    }

    public class CasePrincipalService : ICasePrincipalService
    {
        private IInnostaxDb _innostaxDb;
        private ICaseHistoryService _caseHistoryService;

        public CasePrincipalService(IInnostaxDb innostaxDb, ICaseHistoryService caseHistoryService)
        {
            _innostaxDb = innostaxDb;
            _caseHistoryService = caseHistoryService;
        }

        public List<Models.CasePrincipal> GetCaseAssociatedPrincipals(Guid caseId)
        {
            var result = new List<Models.CasePrincipal>();
            var allCaseAssociatedPrincipals = _innostaxDb.CasePrincipals
                .Where(x => x.CaseId == caseId).ToList();
            result = new List<Models.CasePrincipal>();
            if (allCaseAssociatedPrincipals.Count > 0)
            {
                foreach (var casePrincipal in allCaseAssociatedPrincipals)
                {
                    var mappedPrincipal = Mapper.Map<Models.CasePrincipal>(casePrincipal);
                    result.Add(mappedPrincipal);
                }
            }
            return result;
        }

        public void Save(CasePrincipal[] principalData, Guid caseId)
        {
            var existingCasePrincipal = _innostaxDb.CasePrincipals.Where(x => x.CaseId == caseId).ToList();
            if (existingCasePrincipal.Count > 0)
            {
                foreach (var eachExistingCasePrincipal in existingCasePrincipal)
                {
                    if (principalData.FirstOrDefault(x => x.PrincipalName == eachExistingCasePrincipal.PrincipalName) == null)
                    {
                        _innostaxDb.CasePrincipals.Remove(eachExistingCasePrincipal);
                        _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_PRINCIPAL_DELETED,null);
                    }
                }
            }
            foreach (var eachCasePrincipal in principalData)
            {
                var casePrincipal = new Common.Database.Models.CasePrincipal();
                casePrincipal.CaseId = caseId;
                casePrincipal.PrincipalName = eachCasePrincipal.PrincipalName;
                if (_innostaxDb.CasePrincipals.FirstOrDefault(x => x.PrincipalName == eachCasePrincipal.PrincipalName && x.CaseId == caseId) == null)
                {
                    _innostaxDb.CasePrincipals.Add(casePrincipal);
                    _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_PRINCIPAL_ADDED,null);
                }
            }     
                _innostaxDb.SaveChangesWithErrors();
        }
    }
}
