namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingDateReportReceivedFromNFCInCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "DateReportReceivedFromNFC", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "DateReportReceivedFromNFC");
        }
    }
}
