﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class CasePrincipal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public string PrincipalName { get; set; }
    }
}
