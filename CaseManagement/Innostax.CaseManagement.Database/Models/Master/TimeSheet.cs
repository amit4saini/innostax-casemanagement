﻿using Innostax.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class TimeSheet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid CaseId { get; set; }

        [ForeignKey("CaseId")]
        public Case Case { get; set; }

        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public Guid? DescriptionId { get; set; }

        [ForeignKey("DescriptionId")]
        public TimeSheetDescription Description { get; set; }

        public string CustomDescription { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public Decimal Cost { get; set; }    

        public DateTime CreationDateTime { get; set; }

        public Guid? CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public Guid? LastEditedBy { get; set; }

        [ForeignKey("LastEditedBy")]
        public User LastEditedByUser { get; set; }

        public DateTime? LastEditedDateTime { get; set; }

        public Boolean IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }

        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }
    }
}
