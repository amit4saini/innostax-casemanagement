﻿namespace Innostax.CaseManagement.Models
{
    public class InvitationModel
    {
        public Account.User User { get; set; }
        public ICIUserCaseAssociation ICICaseUser { get; set; }
        public ParticipantCaseAssociation ParticipantUser { get; set; }
        public NFCUserCaseAssociation NFCCaseUser { get; set; }
    }
}
