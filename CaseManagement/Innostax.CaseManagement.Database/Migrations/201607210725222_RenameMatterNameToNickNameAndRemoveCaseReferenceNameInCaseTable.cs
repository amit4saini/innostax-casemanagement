namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameMatterNameToNickNameAndRemoveCaseReferenceNameInCaseTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Cases", "MatterName", "NickName");
            DropColumn("dbo.Cases", "CaseReferenceName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "CaseReferenceName", c => c.String());
            RenameColumn("dbo.Cases", "NickName", "MatterName");
        }
    }
}
