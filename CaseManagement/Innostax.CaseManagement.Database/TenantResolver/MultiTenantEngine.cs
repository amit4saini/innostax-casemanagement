﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Models;
using Innostax.Common.Infrastructure;
using Innostax.Common.Infrastructure.Cache;
using Innostax.Common.Database.Database;
using StructureMap;

namespace Innostax.Common.Database.TenantResolver
{
    public interface IMultiTenantEngine
    {
        System.Threading.Tasks.Task BeginRequest(IOwinContext context);
        System.Threading.Tasks.Task EndRequest(IOwinContext context);
    }

    public class MultiTenantEngine : IMultiTenantEngine
    {
        private const string TENANT_DATABASE_OBJECT_PREFIX = "tenant_";
        private const string CURRENT_TENANT_CONTEXT = "current_tenant_context";
        private readonly ITenantIdentificationStrategy _tenantIdentificationStrategy;
        private readonly ITenantResolver _tenantResolver;
        private readonly ICacheManager _cacheManager;

        public MultiTenantEngine(ITenantResolver tenantResolver, 
            ICacheManager cacheManager,
            ITenantIdentificationStrategy tenantIdentificationStrategy)
        {
            this._tenantIdentificationStrategy = tenantIdentificationStrategy;
            this._tenantResolver = tenantResolver;
            this._cacheManager = cacheManager;
        }

        public async System.Threading.Tasks.Task BeginRequest(IOwinContext context)
        {
            Console.WriteLine("Host begin request.");

            var instance = await GetTenantInstance(context.Request);                    
            if (instance != null)
            {
                context.Set(CURRENT_TENANT_CONTEXT, instance);
                //var container = new Container(_ =>
                //{
                //_.For<ITenantDb>().Use<TenantDb>.ct<string>("connectionString").Is(someValueAtRunTime); (@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Innostax.mdf;Initial Catalog=Innostax;Integrated Security=True; MultipleActiveResultSets=True;"));
                //});
             

            }
        }

        public System.Threading.Tasks.Task EndRequest(IOwinContext context)
        {
            Console.WriteLine("Host end request.");
            return System.Threading.Tasks.Task.FromResult(0);
        }

        private async System.Threading.Tasks.Task<TenantDb> GetTenantInstance(IOwinRequest request)
        {
            var identifier = _tenantIdentificationStrategy.Invoke(request);

            if (identifier == null)
            {
                throw new ArgumentException("The identification strategy did not return an identifier for the request.");
            }

            var instance = _cacheManager.Get(TENANT_DATABASE_OBJECT_PREFIX + identifier) as TenantDb;

            if (instance != null)
            {
                Console.WriteLine("Tenant instance already running.");
                return instance;
            }


            Console.WriteLine("Tenant instance not found. Resolving tenant.");

            var tenant = await _tenantResolver.Resolve(identifier);

            if (tenant != null)
            {
                Console.WriteLine("Tenant found with identifier {0}", identifier);
                Console.WriteLine("Starting Tenant Instance");

                // instance = new TenantInstance(tenant);

                _cacheManager.set(TENANT_DATABASE_OBJECT_PREFIX + identifier, tenant, 3600);
                
            }

            return instance;
        }
    }
}
