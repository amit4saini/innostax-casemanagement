﻿using System.Collections.Generic;

namespace Innostax.Common.Database.MigrationSeedData
{
    public class TimeSheetDescriptionList
    {
        public List<string> TimeSheetDescription()
        {
            var timeSheetDescription = new List<string>
            {
                "Research",
                "Reviewing client-provided information",
                "Reviewing local agent report",
                "Revisions",
                "References",
                "Updating report",
                "Logistics (UTC setting up interviews, locating vendors, etc.)",
                "Quote/Consultation",
                "Supplemental Research (eliminate name match, answer client question, etc.)",
                "Reviewing (includes re-review)",
                "Other (Manual)"
            };
            return timeSheetDescription;
        }
    }
}
