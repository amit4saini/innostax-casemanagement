﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class SubStatus
    {
        public Guid Id { get; set; }
        public Guid StatusId { get; set; }
        public string Name { get; set; }
    }
}
