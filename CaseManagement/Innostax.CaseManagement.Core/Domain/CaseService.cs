﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http;
using Innostax.Common.Database.Enums;
using System.Data;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;
using Innostax.CaseManagement.Models.Account;
using System.Net;
using System.Web.Http;
using System.Text;
using Innostax.CaseManagement.Models.EmailModels;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseService
    {
        List<Guid> Save(Case caseData);
        PageResult<Models.Account.User> GetAllUsersForCase(ODataQueryOptions<User> options, HttpRequestMessage request, Guid id);
        Innostax.CaseManagement.Models.Case GetCaseInfoById(Guid caseId);
        void ArchiveUnarchiveCase(Guid id, bool isArchive);
        PageResult<Case> SearchCasesByKeyword(ODataQueryOptions<Case> options, HttpRequestMessage request, string keyword);
        void AssignUserToCase(Guid caseId, Guid userId);       
        Case GetCaseDetailsFromCaseNumber(string caseNumber);
        List<Guid> GetListOfUserIdAssociatedWithCase(Guid caseId);
        Case CalculateCaseMargin(Case caseDetails);
        Case CalculateICITurnTime(Case caseDetails);
        PageResult<Case> GetAllCases(ODataQueryOptions<Case> options, HttpRequestMessage request);
        bool CheckIfCaseHasOpenTasks(Guid caseId);
        Case CalculateReportToClientAheadOfTime(Case caseDetails);
    }

    public class CaseService : ICaseService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IClientService _clientService;
        private readonly IUserService _userService;
        private readonly ICaseReportAssociationService _caseReportAssociationService;
        private readonly IReportService _reportService;
        private readonly IFileService _fileUploadService;
        private readonly ICaseFileAssociationService _caseFileAssociationService;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly INotificationService _notificationService;
        private readonly ICasePrincipalService _casePrincipalService;
        private readonly ICommonService _commonService;
        private ICaseHistoryService _caseHistoryService;
        private IMailSenderService _mailSenderService;
        private readonly ITaskService _taskService;
        private readonly IParticipantCaseAssociationService _participantCaseAssociationService;
        private readonly ICaseStatusService _caseStatusService;

        public CaseService(IInnostaxDb innostaxDb, IUserService userService, IClientService clientService,
            ICaseReportAssociationService caseReportAssociationService, IReportService reportService,
            IFileService fileUploadService, ICaseFileAssociationService caseFileAssociationService,
            IErrorHandlerService errorHandlerService,
             INotificationService notificationService,
             ICasePrincipalService casePrincipalService, 
             ICommonService commonService, 
             ICaseHistoryService caseHistoryService, 
             IMailSenderService mailSenderService,
             ITaskService taskService,
             IParticipantCaseAssociationService participantCaseAssociationService, ICaseStatusService caseStatusService)
        {
            _fileUploadService = fileUploadService;
            _innostaxDb = innostaxDb;
            _userService = userService;
            _clientService = clientService;
            _caseReportAssociationService = caseReportAssociationService;
            _reportService = reportService;
            _caseFileAssociationService = caseFileAssociationService;
            _errorHandlerService = errorHandlerService;
            _notificationService = notificationService;
            _casePrincipalService = casePrincipalService;
            _commonService = commonService;
            _caseHistoryService = caseHistoryService;
            _mailSenderService = mailSenderService;
            _taskService = taskService;
            _participantCaseAssociationService = participantCaseAssociationService;
            _caseStatusService = caseStatusService;
        }

        public Guid GetUserId()
        {
            Guid result = new Guid();
            var userId = _userService.UserId;
            if (userId == new Guid())
            {
                string userNotAuthorizeError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.USER_NOT_AUTHORIZE;
                _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizeError);
            }
            result = userId;
            return result;
        }

        public void ValidateCaseId(Guid caseId)
        {
            var caseDetails = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId);
            if (caseDetails == null)
            {
                string caseIdNotValidError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(caseIdNotValidError);
            }
        }

        public bool CheckIfCaseHasOpenTasks(Guid caseId)
        {
            var hasOpenTask = false;
            var listOfTask = _taskService.GetTasksByCaseIdAndUserId(new CasesAndUsersIdModel { CasesIdList= new Guid[] { caseId}  });
            foreach (var task in listOfTask)
            {
                if (task.Status != TaskStatusType.COMPLETED)
                {
                    hasOpenTask = true;
                    break;
                }
            }
            return hasOpenTask;
        }

        public List<Guid> Save(Case caseData)
        {
            caseData = CheckForNullableValues(caseData);
            bool isNewCase = false;
            bool isCaseStatusChanged = false;
            bool sendOnHoldStatusChangeNotification = false;            
            var listOfUserId = new List<Guid>();
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseData.CaseId);                     
            Common.Database.Enums.Action currentAction = new Common.Database.Enums.Action();
            if (existingCase == null)
            {
                currentAction = Common.Database.Enums.Action.CASE_ADDED;
                existingCase = new Innostax.Common.Database.Models.Case();
                isNewCase = true;
            }
            else
            {
                _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_UPDATE_ALL, existingCase.CaseId);
            }
            if(caseData.AssignedTo != Guid.Empty && caseData.AssignedTo != null)
            {
                var assignedToUserDetails = _userService.GetUser(caseData.AssignedTo.Value);                
                if (assignedToUserDetails.Data.Role.Name == Roles.Investigator.ToString())
                    caseData.IHIAssignedUserId = caseData.AssignedTo;
            }
            if (!isNewCase)
            {
                if (existingCase.StatusId != caseData.StatusId) { isCaseStatusChanged = true; }          
                if (existingCase.IsNFCCase != caseData.IsNFCCase && caseData.IsNFCCase && caseData.Client.SalesRepresentativeId != null)
                {                    
                        caseData.CaseAssignedToNFCDate = _commonService.GetUtcDateTime();
                        caseData.UserWhoAssignedToNFC = _userService.UserId;                    
                    SendNfcNotificationToSalesRepresentative(caseData);       
                }
                var currentUser = _userService.GetCurrentUser();
                if (isCaseStatusChanged && caseData.StatusId.HasValue)
                {
                    listOfUserId.AddRange( _caseStatusService.ChangeCaseStatus(existingCase.CaseId, caseData.StatusId.Value));
                }
                if (caseData.CaseStatus.Status == "SENT_FOR_REVIEW" && caseData.InQC == true)
                {
                    caseData.ReviewedBy = _userService.UserId;
                    caseData.CaseReviewedDate = _commonService.GetUtcDateTime();
                }
                else if (caseData.CaseStatus.Status == "APPROVED")
                {
                    caseData.DDSupervisorWhoApprovedCase = _userService.UserId;
                    caseData.DDSupervisorApprovedCaseDate = _commonService.GetUtcDateTime();
                }
                if (existingCase.OnHold != caseData.OnHold)
                {
                    if (caseData.OnHold)
                    {
                        caseData.CasePlacedOnHoldDate = _commonService.GetUtcDateTime();
                        caseData.UserIdWhoPlacedCaseOnHold = _userService.UserId;
                    }
                    else
                    {
                        caseData.DateCaseRemovedFromHold = _commonService.GetUtcDateTime();
                        caseData.UserIdWhoRemovedCaseFromHold = _userService.UserId;
                    }
                }
                if ((existingCase.CaseCancelled != caseData.CaseCancelled) && caseData.CaseCancelled == true)
                {
                    caseData.UserIdWhoCancelledCase = _userService.UserId;
                    caseData.CaseCancelledDate = _commonService.GetUtcDateTime();
                }
                if (existingCase.CaseStatus !=null && existingCase.CaseStatus.Status =="CLOSED")
                {
                    caseData.UserIdWhoClosedCase = _userService.UserId;
                    caseData.DateCaseClosed = _commonService.GetUtcDateTime();
                }
                if ((existingCase.InQC != caseData.InQC) && caseData.InQC == true)
                {
                    caseData.UserIdWhoConductedQCReview = _userService.UserId;
                    caseData.DDSupervisorBeganQCReviewDate = _commonService.GetUtcDateTime();
                }
                if ((existingCase.FinalReportSentToSalesRep != caseData.FinalReportSentToSalesRep) && caseData.FinalReportSentToSalesRep)
                {
                    caseData.UserIdWhoSentFinalReportToSalesRep = _userService.UserId;
                    caseData.FinalReportSentToSalesRepDate = _commonService.GetUtcDateTime();
               }
                if ((existingCase.FinalReportSentToClient != caseData.FinalReportSentToClient) && caseData.FinalReportSentToClient)
                {
                    caseData.UserIdWhoSentFinalReportToClient = _userService.UserId;
                    caseData.FinalReportSentToClientDate = _commonService.GetUtcDateTime();
                }
                //if (isCaseStatusChanged && (caseData.CaseStatus.Status == "APPROVED" || caseData.CaseStatus.Status == "NFC_APPROVED") && currentUser.Role.Name == Roles.DDSupervisor.ToString())
                //{ caseData.CaseApprovedBy = _userService.UserId; }   
                if (caseData.OnHold != existingCase.OnHold)
                {
                    sendOnHoldStatusChangeNotification = true;
                    //send OnHold status change notification to case associated IHI users and DDSupervisor/SalesRep                   
                    var caseAssociatedUsersIdList = GetListOfUserIdAssociatedWithCase(caseData.CaseId);
                    var allCaseAssociatedUsers = _userService.GetUsersByUserId(caseAssociatedUsersIdList);

                    foreach (var eachUser in allCaseAssociatedUsers)
                    {
                        if (currentUser.Role.Name == Roles.DDSupervisor.ToString())
                        {
                            if (eachUser.Role.Name == Roles.Investigator.ToString() || eachUser.Role.Name == Roles.Sales_Rep.ToString())
                                listOfUserId.Add(eachUser.Id);
                        }
                        else if (currentUser.Role.Name == Roles.Sales_Rep.ToString())
                        {
                            if (eachUser.Role.Name == Roles.Investigator.ToString() || eachUser.Role.Name == Roles.DDSupervisor.ToString())
                                listOfUserId.Add(eachUser.Id);
                        }
                    }
                }
                if (caseData.IsICIReportReceived && existingCase.DateReportReceivedFromICI == null)
                    caseData.DateReportReceivedFromICI = DateTime.Now;
                if (caseData.IsICIReportTurnTimeEndDateAdded && existingCase.ICIReportTurnTimeEndDate == null)
                    caseData.ICIReportTurnTimeEndDate = DateTime.Now;
            }
            existingCase = Mapper.Map<Innostax.Common.Database.Models.Case>(caseData);
            existingCase.ClientId = caseData.Client.ClientId;
            existingCase.Client = null;
            if (!isNewCase)
            {
                currentAction = Common.Database.Enums.Action.CASE_EDITED;
            }
            else
            {
                existingCase.AssignedTo = null;
                existingCase.CreationDateTime = _commonService.GetUtcDateTime();
                existingCase.CreatedBy = _userService.UserId;
                existingCase.CaseNumber = GetNewCaseNumber("D");
            }      
            if (existingCase.DDSupervisorWhoApprovedCase == new Guid())
            {
                existingCase.DDSupervisorWhoApprovedCase = null;
            }
            existingCase.LastEditedDateTime = _commonService.GetUtcDateTime();
            existingCase.SubStatus = null;
            _innostaxDb.Cases.AddOrUpdate(existingCase);        
                _innostaxDb.SaveChangesWithErrors();
              if (isNewCase)
            {
                existingCase.Client = Mapper.Map<Common.Database.Models.Client>(caseData.Client);
                listOfUserId = SaveNotification(new Guid(), existingCase, isNewCase);
                // SendNewCaseEmailNotification(caseData);            
                var currentUserDetails = _userService.GetCurrentUser();
                if (currentUserDetails.Role.Name == Roles.Client_Participant.ToString())
                {
                    var participantCaseAssociation = new ParticipantCaseAssociation();
                    participantCaseAssociation.CaseId = existingCase.CaseId;
                    participantCaseAssociation.UserId = _userService.UserId;
                    participantCaseAssociation.User = currentUserDetails;
                    participantCaseAssociation.IsActive = true;
                    _participantCaseAssociationService.Save(participantCaseAssociation);
                    return new List<Guid>();
                }
            }
              if(!isNewCase && listOfUserId.Count > 0 && sendOnHoldStatusChangeNotification)
            {                
               //    SendOnHoldStatusChangeNotification(caseData, listOfUserId);
            }
            _caseHistoryService.SaveCaseHistory(existingCase.CaseId, currentAction, existingCase.CaseNumber);
            var caseReportAsssociationModel = new CaseReportAssociation();
            caseReportAsssociationModel.CaseId = existingCase.CaseId;
            if (caseData.Reports != null || caseData.ReportsCountries!= null || caseData.OtherReportTypes != null)
            {
                caseReportAsssociationModel.Reports = AssignCountriesToReports(caseData.Reports, caseData.ReportsCountries, caseData.OtherReportTypes);
           _caseReportAssociationService.Save(caseReportAsssociationModel);
            }           
            caseData.CaseId = existingCase.CaseId;
            if (caseData.Files != null)
            {
                _caseFileAssociationService.Save(caseData);
            }
            if (existingCase.SubjectType == 0 && existingCase.IsSpecificPrincipal == true)
                _casePrincipalService.Save(caseData.PrincipalNames, existingCase.CaseId);
            return listOfUserId;
        }

        //private void SendOnHoldStatusChangeNotification(Case currentCase, List<Guid> usersToBeNotified)
        //{
        //    string notificationText = string.Empty;
        //    var currentUser = _userService.GetCurrentUser();
        //    var URL = _commonService.GetCaseRelatedUrl(NotificationUrl.CASE, currentCase.CaseNumber, null);
        //    if (currentCase.OnHold)
        //    {
        //        notificationText = Common.Constants.NotificationMessages.CaseNotification.CASE_SET_ON_HOLD;
        //        notificationText = string.Format(notificationText, currentCase.CaseNumber, currentCase.NickName, currentUser.FirstName + currentUser.LastName);
        //    }
        //    else
        //    {
        //        notificationText = Common.Constants.NotificationMessages.CaseNotification.CASE_NOT_ON_HOLD;
        //        notificationText = string.Format(notificationText, currentCase.CaseNumber, currentCase.NickName);
        //    }
        //    _notificationService.SaveNotification(notificationText, usersToBeNotified, URL);
        //}

        private void SendNfcNotificationToSalesRepresentative(Case caseDetails)
        {
            var notification = new Innostax.CaseManagement.Models.Notification();
            var listOfUserId = new List<Guid>();
            var currentUser = _userService.GetCurrentUser();
            var notificationText = string.Empty;
            var adminName = currentUser.FirstName + " " + currentUser.LastName;
            var URL = _commonService.GetCaseRelatedUrl(NotificationUrl.CASE, caseDetails.CaseNumber, null);
            notificationText = Common.Constants.NotificationMessages.CaseNotification.CASE_SET_AS_NFC;
            listOfUserId.Add(caseDetails.Client.SalesRepresentativeId.Value);
            notificationText = string.Format(notificationText, adminName, caseDetails.CaseNumber, caseDetails.NickName);
            _notificationService.SaveNotification(notificationText, listOfUserId, URL);
        }
     

        private void SendNewCaseEmailNotification(Case caseDetails)
        {
            var newCaseEmailModel = new NewCaseEmailModel();
            newCaseEmailModel.CaseNumber = caseDetails.CaseNumber;
            if (caseDetails.Client.SalesRepresentativeEmail != null)
            {
                newCaseEmailModel.User = Mapper.Map<User>(caseDetails.Client.SalesRepresentative) ;
            }
            _mailSenderService.SendNewCaseDetails(newCaseEmailModel);
        }         
               
        internal List<Report> AssignCountriesToReports(Report[] reports, Country[] countries, String[] otherReportsNames)
        {
            for (var count = 0; count < reports.Length; count++)
            {
                if (reports[count] != null)
                {
                    if (count == 0)
                        reports[count].IsPrimary = true;
                    else
                        reports[count].IsPrimary = false;

                    if (countries != null && countries.Length > count)
                    {
                        if (countries[count] == null)
                        {
                            reports[count].CountryId = null;
                            reports[count].CountryName = null;
                            reports[count].StateId = null;
                            reports[count].StateName = null;

                        }
                        else
                        {
                            reports[count].CountryId = countries[count].CountryId;
                            reports[count].CountryName = countries[count].CountryName;
                            if (countries[count].StateId.HasValue)
                            {
                                reports[count].StateId = countries[count].StateId.Value;
                                reports[count].StateName = countries[count].StateName;
                            }
                        }                        
                    }
                    if (reports[count].ReportType == "Other")
                    {
                        if (otherReportsNames.Length > count)
                        {
                            reports[count].OtherReportTypeName = otherReportsNames[count];
                            reports[count].IsOtherReportType = true;
                        }
                    }
                    else
                        reports[count].IsOtherReportType = false;
                }
            }
            return reports.ToList();
        }

        internal Case CheckForNullableValues(Case caseData)
        {
            if (caseData.Client != null)
            {
                if (caseData.Client.IndustryId == 0)
                {
                    caseData.Client.IndustryId = null;
                }
                if (caseData.Client.IndustrySubCategoryId == 0)
                {
                    caseData.Client.IndustrySubCategoryId = null;
                }
            }
            if (caseData.StateOfEmploymentId == 0)
            {
                caseData.StateOfEmploymentId = null;
            }
            if (caseData.StateOfResidenceId == 0)
            {
                caseData.StateOfResidenceId = null;
            }
            return caseData;
        }

        public List<Guid> GetListOfUserIdAssociatedWithCase(Guid caseId)
        {
            var listOfUserId = new List<Guid>();
            var caseDetails = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == caseId);
            listOfUserId.Add(caseDetails.CreatedBy);
            string userId = string.Empty;
            if (caseDetails.AssignedTo != null)
            {
                userId = caseDetails.AssignedTo.ToString();
                listOfUserId.Add(Guid.Parse(userId));
            }
            var caseTasks = _innostaxDb.Tasks.Where(x => x.CaseId == caseId).ToList();
            foreach (var caseTask in caseTasks)
            {
                listOfUserId.Add(caseTask.AssignedTo);
                listOfUserId.Add(caseTask.CreatedBy);
            }
            listOfUserId = listOfUserId.Distinct().ToList();
            return listOfUserId;
        }

        public string GetNewCaseNumber(string caseType)
        {
            var latestCase = _innostaxDb.Cases.OrderByDescending(x => x.CreationDateTime).FirstOrDefault();
            DateTime time = _commonService.GetUtcDateTime();
            string format = "MMddyy";
            var newCaseNumber = new StringBuilder();
            if (latestCase == null)
            {
                var defaultCaseNumber = string.Empty;
                defaultCaseNumber = _commonService.GetDefaultCaseNumber();
                newCaseNumber.Append(GetCaseNumberInitial(caseType)).Append(time.ToString(format)).Append("-").Append(defaultCaseNumber);
                return newCaseNumber.ToString();
            }
            var latestCaseNumber = latestCase.CaseNumber;
            latestCaseNumber = Regex.Replace(latestCaseNumber, @"\D+-(.*?)-", string.Empty);
            newCaseNumber.Append(GetCaseNumberInitial(caseType)).Append(time.ToString(format)).Append("-").Append((Convert.ToInt32(latestCaseNumber) + 1).ToString());
            return newCaseNumber.ToString();
        }

        private string GetCaseNumberInitial(string caseType)
        {
            var returnval = string.Empty;
            if (caseType == "D")
                returnval = "DD-";
            else
                returnval = "C";
            return returnval;
        }

        public void ArchiveUnarchiveCase(Guid caseId, bool isArchive)
        {
            _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_DELETE_ALL, caseId);
            var existingCase = _innostaxDb.Cases.Where(x => x.CaseId == caseId).FirstOrDefault();

            if (existingCase == null)
            {
                string caseIdNotValidError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(caseIdNotValidError);
            }
            existingCase.IsDeleted = isArchive;
            existingCase.DeletedBy = _userService.UserId;
            existingCase.LastEditedDateTime = _commonService.GetUtcDateTime();
            _innostaxDb.SaveChangesWithErrors();
            if (isArchive)
            {
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_ARCHIVED, existingCase.CaseNumber);
            }
            else
            {
                _caseHistoryService.SaveCaseHistory(existingCase.CaseId, Common.Database.Enums.Action.CASE_UNARCHIVED, existingCase.CaseNumber);
            }
            _caseReportAssociationService.DeleteCaseReportAssociation(caseId, isArchive);
            _caseFileAssociationService.ArchiveCaseFileAssociation(caseId, isArchive);
            _taskService.DeleteCaseAssociatedTasks(caseId, isArchive);
        }

        public PageResult<Case> GetAllCases(ODataQueryOptions<Case> options, HttpRequestMessage request)
        {
           var allCases = _commonService.GetAuthorizedCasesForUser().ToList();
            var mappedCases = Mapper.Map<List<Case>>(allCases);
            foreach (var eachCase in mappedCases)
            {
                if (eachCase.IsDeleted != true)
                {
                    eachCase.Reports = _reportService.GetCaseAssociatedReports(eachCase.CaseId).ToArray();
                    if (eachCase.Reports.Length != 0)
                    {
                        var primaryReport = eachCase.Reports.FirstOrDefault(x => x.IsPrimary == true);
                        if (primaryReport != null)
                        {
                            eachCase.PrimaryReportType = primaryReport.ReportType;
                            eachCase.PrimaryReportCountryName = primaryReport.CountryName;
                        }
                    }
                }
            }
            IQueryable results = options.ApplyTo(mappedCases.AsQueryable());
            var resultCount = mappedCases.Count;
            if(options.Filter != null)
            {
                resultCount = (options.Filter.ApplyTo(mappedCases.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Case>).ToList().Count;
            }                     
            return new PageResult<Case>(
            results as IEnumerable<Case>, request.ODataProperties().NextLink,
            resultCount);
        }

        internal List<Common.Database.Models.Case> GetCasesForSalesRepresentative(Guid salesRepresentativeId)
        {
            var result = new List<Common.Database.Models.Case>();
            var existingClients = _innostaxDb.Clients.Where(x => x.SalesRepresentativeId == _userService.UserId && x.IsDeleted == false).ToList();
            if(existingClients != null || existingClients.Count > 0)
            {
                foreach(var eachClient in existingClients)
                {
                    var existingCases = _innostaxDb.Cases
                        .Include(x => x.ClientContactUser)
                        .Include(x => x.AssignedToUser)
                        .Include(x => x.DDSupervisorUserDetails)
                        .Include(x => x.CreatedByUser)
                        .Include(x => x.SubStatus)
                        .Include(x => x.Client)
                        .Include(x => x.Client.SalesRepresentative)
                        .Where(x => x.ClientId == eachClient.ClientId && x.IsDeleted == false).ToList();
                    if (existingCases != null || existingCases.Count > 0)
                    {
                        foreach (var eachCase in existingCases)
                        {
                            result.Add(eachCase);
                        }
                    }
                }
            }
            return result;
        }

        public Innostax.CaseManagement.Models.Case GetCaseInfoById(Guid caseId)
        {
            var result = new Innostax.CaseManagement.Models.Case();

            var userId = _userService.UserId;
            var existingCase = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.IHIAssignedUser)
                .Include(x => x.StateOfResidence).Include(x => x.StateOfEmployment).Include(x => x.CreatedByUser).Include(x => x.AssignedToUser).Include(x => x.DDSupervisorUserDetails).Include(x => x.ClientContactUser).Include(x => x.CaseStatus).Include(x => x.BusinessUnit)
                .Include( x => x.UserWhoCancelledCase).Include(x => x.UserWhoPlacedCaseOnHold)
                .FirstOrDefault(x => x.CaseId == caseId);
            if (existingCase == null)
            {
                string caseIdNotValidError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(caseIdNotValidError);
            }
            _commonService.ValidateAccess(Validate.CASE, Permission.CASE_READ_ALL, existingCase.CaseId);
            var existingCaseReports = _reportService.GetCaseAssociatedReports(caseId).ToArray();
            var existingCaseFiles = _caseFileAssociationService.GetCaseAssociatedFiles(caseId);
            var existingCasePrimaryReportFiles = new List<FileModel>();
            result = new Models.Case();
            var existingCaseClient = _clientService.GetClientDetailsFromClientId(existingCase.ClientId);
            result = Mapper.Map<Models.Case>(existingCase);
            result.Client = existingCaseClient;
            result.Reports = existingCaseReports;
            result.Files = existingCaseFiles;
            result.PrimaryReportFiles = existingCasePrimaryReportFiles.ToArray();
            if (existingCase.DateReportReceivedFromICI != null)
                result.IsICIReportReceived = true;
            if (existingCase.ICIReportTurnTimeEndDate != null)
                result.IsICIReportTurnTimeEndDateAdded = true;
            if (result.Reports.Length != 0)
            {
                var primaryReport = result.Reports.FirstOrDefault(x => x.IsPrimary == true);
                if (primaryReport != null)
                {
                    result.PrimaryReportType = primaryReport.ReportType;
                    result.PrimaryReportCountryName = primaryReport.CountryName;
                }
            }
            if (existingCase.Client.SalesRepresentativeId != Guid.Empty)
                result.Client.SalesRepresentative = Mapper.Map<User>(_innostaxDb.Users.FirstOrDefault(x => x.Id == existingCase.Client.SalesRepresentativeId));
            if (existingCase.SubjectType == 0 && existingCase.IsSpecificPrincipal == true)
            {
                var existingCasePrincipals = _casePrincipalService.GetCaseAssociatedPrincipals(caseId);
                result.PrincipalNames = existingCasePrincipals.ToArray();
            }
            if (existingCase.StateOfEmploymentId != null && existingCase.StateOfEmploymentId != 0)
            {
                result.StateOfEmploymentName = existingCase.StateOfEmployment.StateName;
            }
            if (existingCase.StateOfResidenceId != null && existingCase.StateOfResidenceId != 0)
            {
                result.StateOfResidenceName = existingCase.StateOfResidence.StateName;
            }
            if(existingCase.ClientContactUser != null)
            {
                result.ClientContactUser.ContactUserBusinessUnits = new List<ContactUserBusinessUnit>();               
                var contactUserBusinessUnits = _innostaxDb.ContactUserBusinessUnits.Where(x => x.ClientContactUserId == existingCase.ClientContactUserId).ToList();                
                result.ClientContactUser.ContactUserBusinessUnits = Mapper.Map<List<ContactUserBusinessUnit>>(contactUserBusinessUnits);
            }
            var currentUserId = _userService.UserId;
            var currentUserRole = _userService.GetUser(currentUserId).Data.Role.Name;
            var authorizedDiscussions = _commonService.GetAuthorizedDiscussions(caseId).Where(x=>x.IsDeleted != true);
            var authorizedTasks = _commonService.GetAuthorizedTasks(caseId).Where(x => x.IsDeleted != true);
            result.CaseAssociatedTasksCount = authorizedTasks.ToList().Count;
            if (_userService.GetCurrentUser().Role.Name == Roles.Client_Participant.ToString())
            {
                result.CaseAssociatedDiscussionsCount = authorizedDiscussions.Where(x => x.IsNotes == false).ToList().Count;
                result.CaseDiscussionReportCount = authorizedDiscussions.Where(x => x.IsNotes == true).ToList().Count;
            }
            else
            {
                result.CaseAssociatedDiscussionsCount = authorizedDiscussions.ToList().Count;
            }
            result.CaseAssociatedTimesheetsCount = (_userService.UserPermissions.Contains(Permission.CASE_ALL_TIMESHEET))? _innostaxDb.TimeSheets.Where(x => x.CaseId == existingCase.CaseId && x.IsDeleted == false).ToList().Count : _innostaxDb.TimeSheets.Where(x => x.CaseId == existingCase.CaseId && x.IsDeleted == false && x.CreatedBy == currentUserId).ToList().Count;
            result.CaseAssociatedExpensesCount = (_userService.UserPermissions.Contains(Permission.VIEW_ALL_CASE_EXPENSE)) ? _innostaxDb.CaseExpenses.Where(x => x.CaseId == existingCase.CaseId && x.IsDeleted == false).ToList().Count: _innostaxDb.CaseExpenses.Where(x => x.CaseId == existingCase.CaseId && x.IsDeleted == false && x.CreatedBy == currentUserId).ToList().Count;
            var discussionAssociatedFileCount = _innostaxDb.DiscussionFileAssociations.Include(y=>y.Discussion).Where(x => authorizedDiscussions.Contains(x.Discussion) && x.Discussion.IsDeleted == false).ToList().Count;
            var discussionMessageAssociatedFileCount = _innostaxDb.DiscussionMessageFileAssociations.Include(y=>y.DiscussionMessage.Discussion).Where(x => authorizedDiscussions.Contains(x.DiscussionMessage.Discussion) && x.DiscussionMessage.Discussion.IsDeleted == false).ToList().Count;
            var taskFileCount = _innostaxDb.TaskFileAssociations.Where(x => authorizedTasks.Contains(x.Task) && x.IsDeleted != true && x.Task.IsDeleted != true).ToList().Count();
            var taskCommentFileCount = _innostaxDb.TaskCommentFileAssociations.Where(x => authorizedTasks.Contains(x.TaskComment.Task) && x.IsDeleted != true && x.TaskComment.IsDeleted != true && x.TaskComment.Task.IsDeleted != true).ToList().Count();
            result.CaseAllFilesCount = existingCaseFiles.ToArray().Length + discussionAssociatedFileCount + discussionMessageAssociatedFileCount+ taskFileCount+taskCommentFileCount;
            return result;
        }   

        public PageResult<Case> SearchCasesByKeyword(ODataQueryOptions<Case> options, HttpRequestMessage request, string keyword)
        {
            IQueryable<Common.Database.Models.Case> cases;         
            cases = _commonService.GetAuthorizedCasesForUser().Where(x => x.IsDeleted != true);
            var caseIdList = cases.Select(x => x.CaseId).ToList();
            cases = cases.Include(x => x.Client).Include(x => x.Client.SalesRepresentative).Include(x => x.BusinessUnit).Include(x => x.ClientContactUser).Where(x => (x.AdditionalSalesRepNotes ?? "").Contains(keyword) || (x.CaseNumber ?? "").Contains(keyword) || (x.ClientContactUser.ContactName ?? "").Contains(keyword) || (x.ClientPO ?? "").Contains(keyword) || (x.ClientProvidedInformation ?? "").Contains(keyword) || (x.HoldReason ?? "").Contains(keyword) || (x.NickName ?? "").Contains(keyword) || (x.ReportSubject ?? "").Contains(keyword) || (x.WhyProBonoGratis ?? "").Contains(keyword) || (x.StateOfEmployment.Country.CountryName ?? "").Contains(keyword) || (((x.Client.ClientName ?? "").Contains(keyword) || (x.Client.SalesRepresentative.FirstName ?? "").Contains(keyword) || (x.Client.SalesRepresentative.LastName ?? "").Contains(keyword) || ((x.Client.SalesRepresentative.FirstName + " " + x.Client.SalesRepresentative.LastName) ?? "").Contains(keyword) || (x.Client.ClientName ?? "").Contains(keyword) || (x.Client.Notes ?? "").Contains(keyword) || (x.Client.Industry.Name ?? "").Contains(keyword) || (x.Client.IndustrySubCategory.Name ?? "").Contains(keyword) || (x.Client.SalesRepresentativeEmail ?? "").Contains(keyword) || (x.ClientContactUser.EmailAddress ?? "").Contains(keyword) || (x.ClientContactUser.PhoneNumber ?? "").Contains(keyword) || (x.ClientContactUser.Street ?? "").Contains(keyword) || (x.ClientContactUser.ZipCode ?? "").Contains(keyword) || (x.ClientContactUser.PositionHeldbyContact ?? "").Contains(keyword) || (x.ClientContactUser.City ?? "").Contains(keyword)) && x.IsDeleted == false && x.Client.IsDeleted == false));
            var searchCaseReportResult = _innostaxDb.CaseReportAssociations.Include(y => y.Case).Include(x => x.Case.Client).Include(x => x.Case.Client.SalesRepresentative).Include(x => x.Case.ClientContactUser).Include(x => x.Country).Where(y => caseIdList.Contains(y.CaseId) && ((y.Report.Description ?? "").Contains(keyword) || (y.Report.ReportType ?? "").Contains(keyword) || (y.Country.CountryName ?? "").Contains(keyword)) && y.IsDeleted == false && y.Case.IsDeleted == false && y.Case.Client.IsDeleted == false).ToList().Select(x => x.Case).ToList();   
            var searchCasePrincipalSearchResult = _innostaxDb.CasePrincipals.Include(y => y.Case).Include(x => x.Case.Client).Include(x => x.Case.Client.SalesRepresentative).Include(x => x.Case.ClientContactUser).Where(x => (caseIdList.Contains(x.CaseId) && (x.PrincipalName ?? "").Contains(keyword)) && x.Case.IsDeleted == false && x.Case.Client.IsDeleted == false).ToList().Select(x => x.Case).ToList();
            var searchResultUnion = cases.ToList().Union(searchCaseReportResult.Union(searchCasePrincipalSearchResult)).Distinct();
            var mappedSearchResult = Mapper.Map<List<Case>>(searchResultUnion);
            foreach (var eachCase in mappedSearchResult)
            {
                if (eachCase.IsDeleted != true)
                {
                    eachCase.Reports = _reportService.GetCaseAssociatedReports(eachCase.CaseId).ToArray();
                    if (eachCase.Reports.Length != 0)
                    {
                        var primaryReport = eachCase.Reports.FirstOrDefault(x => x.IsPrimary == true);
                        if (primaryReport != null)
                        {
                            eachCase.PrimaryReportType = primaryReport.ReportType;
                            eachCase.PrimaryReportCountryName = primaryReport.CountryName;
                        }
                    }
                }
            }
            IQueryable results = options.ApplyTo(mappedSearchResult.AsQueryable());
            var resultCount = mappedSearchResult.Count;
            if (options.Filter != null)
            {
                resultCount = (options.Filter.ApplyTo(mappedSearchResult.AsQueryable(), new ODataQuerySettings()) as IEnumerable<Case>).ToList().Count;
            }
            return new PageResult<Case>(
            results as IEnumerable<Case>, request.ODataProperties().NextLink,
            resultCount);           
        }

        public void AssignUserToCase(Guid caseId, Guid userId)
        {
            var existingCase = _innostaxDb.Cases.Include(y => y.CreatedByUser).FirstOrDefault(x => x.CaseId == caseId);
            if (existingCase == null)
                _errorHandlerService.ThrowBadRequestError(Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID);
            else
            {
                existingCase.AssignedTo = userId;
                var user = _userService.GetUser(userId);
                if (user.Data.Role.Name == Roles.In_Country_Investigator.ToString())
                {
                    existingCase.ICIAssignedUserId = userId;
                    existingCase.CaseAssignedToICIDate = _commonService.GetUtcDateTime();
                    existingCase.CaseAssignedToICIByUserId = _userService.UserId;
                }
                else if (user.Data.Role.Name == Roles.Investigator.ToString())
                {
                    existingCase.IHIAssignedUserId = userId;
                    existingCase.CaseAssignedToIHIDate = _commonService.GetUtcDateTime();
                    existingCase.CaseAssignedToIHIByUserId = _userService.UserId;
                }     
                _innostaxDb.SaveChangesWithErrors();                 
               var listOfUserIdToBeNotified= SaveNotification(userId, existingCase,false);
                var existingUser = _innostaxDb.Users.FirstOrDefault(x => x.Id == userId);
                if (existingUser != null)
                {
                    _caseHistoryService.SaveCaseHistory(caseId, Common.Database.Enums.Action.CASE_ASSIGNED, existingUser.FirstName + " " + existingUser.LastName);
                }
            }
        }

        private void SendAssignedCaseEmailNotification(Case existingCase)
        {
            var assignCaseEmailModel = new AssignCaseEmailModel();
            assignCaseEmailModel.CaseNumber = existingCase.CaseNumber;
            var currentUser = _userService.GetCurrentUser();
            var assignedUser = new User();
            if (existingCase.AssignedTo != Guid.Empty)
                assignedUser = _userService.GetUser(existingCase.AssignedTo ?? Guid.Empty).Data;
            else
                _errorHandlerService.ThrowBadRequestError(Innostax.Common.Constants.ErrorMessages.CaseManagement.CaseAssignment.IMPROPER_CASE_ASSIGNMENT);
            assignCaseEmailModel.AssigneeName = currentUser.FirstName+" "+currentUser.LastName;
            assignCaseEmailModel.AssignedUserMail = assignedUser.Email;
            assignCaseEmailModel.AssignedUserName = assignedUser.FirstName + " " + assignedUser.LastName;
            _mailSenderService.SendAssignedCaseDetails(assignCaseEmailModel);
        }

        private List<Guid> SaveNotification(Guid userId,Common.Database.Models.Case caseDetail ,bool isNewCase)
        {            
            var notification = new Innostax.CaseManagement.Models.Notification();
            var listOfUserId = new List<Guid>();
            var notificationText = string.Empty;
            var userName = string.Empty;
            var URL = _commonService.GetCaseRelatedUrl(NotificationUrl.CASE, caseDetail.CaseNumber,null);          
            if (isNewCase)
            {
                var userDetails = _userService.GetUser(caseDetail.CreatedBy);
                userName = string.Concat(userDetails.Data.FirstName, " ", userDetails.Data.LastName);
                var adminRoleId = _innostaxDb.Roles.FirstOrDefault(y => y.Name == Roles.DDSupervisor.ToString());
                var participantRole = _innostaxDb.Roles.FirstOrDefault(y => y.Name == Roles.Client_Participant.ToString());
                //var createdByUserRole = _innostaxDb.UserRoles.FirstOrDefault(y => y.UserId == caseDetail.CreatedBy);
                if (participantRole != null )
                {
                    if (participantRole.Id == userDetails.Data.Role.Id)
                    {
                        notificationText = Common.Constants.NotificationMessages.CaseNotification.NEW_CASE_REQUESTED_BY_PARTICIPANT;
                        if(caseDetail.Client != null)
                        listOfUserId.Add(caseDetail.Client.SalesRepresentativeId.Value);
                        caseDetail.Client = null;
                    }
                    else
                    {
                        notificationText = Common.Constants.NotificationMessages.CaseNotification.NEW_CASE_CREATED;
                        listOfUserId = _innostaxDb.UserRoles.Where(x => x.RoleId == adminRoleId.Id).Select(x => x.UserId).ToList();
                        caseDetail.Client = null;
                    }
                }
                notificationText = string.Format(notificationText, caseDetail.NickName, userName);               
            }
            else
            {
                userName = string.Concat(caseDetail.CreatedByUser.FirstName, " ", caseDetail.CreatedByUser.LastName);
                notificationText = Common.Constants.NotificationMessages.CaseNotification.CASE_ASSIGNED_TO_USER;
                notificationText = String.Format(notificationText, userName, caseDetail.CaseNumber, caseDetail.NickName);
                listOfUserId.Add(userId);
                caseDetail.Client = null;
            }          
            return _notificationService.SaveNotification(notificationText,listOfUserId,URL);            
        }       

        public Case GetCaseDetailsFromCaseNumber(string caseNumber)
        {
            var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.CaseNumber == caseNumber);           
            if (existingCase == null)
            {
                string caseIdNotValidError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASEID_NOT_VALID;
                _errorHandlerService.ThrowBadRequestError(caseIdNotValidError);
            }
            var caseDetails = GetCaseInfoById(existingCase.CaseId);
            caseDetails = CalculateCaseMargin(caseDetails);
            return caseDetails;
        }

        public PageResult<Models.Account.User> GetAllUsersForCase(ODataQueryOptions<User> options, HttpRequestMessage request, Guid id)
        {
            var caseAssociatedUsers = new List<User>();
            var userIdList = new List<Guid>();
            var caseDetails = _innostaxDb.Cases.FirstOrDefault(x => x.CaseId == id && x.IsDeleted != true);
            if (caseDetails == null)
            {
                string noCaseFoundError = Common.Constants.ErrorMessages.CaseManagement.CaseCreation.CASE_NOT_FOUND;
                _errorHandlerService.ThrowBadRequestError(noCaseFoundError);
            }
            userIdList.Add(caseDetails.CreatedBy);
            if (caseDetails.AssignedTo.HasValue)
            {
                userIdList.Add(caseDetails.AssignedTo.Value);
            }           
            var caseHistoryUserName = _innostaxDb.CaseHistory.Where(x => x.CaseId == id && x.Action == Common.Database.Enums.Action.CASE_ASSIGNED).Select(x=>x.NewValue).ToList();          
            var userDetailsFromUserName=_userService.GetUserByUserName(caseHistoryUserName);
            if (userDetailsFromUserName.Count != 0)
            {
                    userIdList= userDetailsFromUserName.Select(x=>x.Id).ToList();
            }                     
            var caseTasks = _innostaxDb.Tasks.Include(x => x.Case).Where(x => x.Case.CaseId == id && x.Case.IsDeleted != true && x.IsDeleted != true);
            if (caseTasks != null)
            {
                userIdList.AddRange(caseTasks.Select(x => x.AssignedTo).ToList());
                userIdList.AddRange(caseTasks.Select(x => x.CreatedBy).ToList());
            }
            var listOfDDSupervisor = _innostaxDb.UserRoles.Where(x => x.RoleId == (_innostaxDb.Roles.FirstOrDefault(y => y.Name == "DDSupervisor")).Id).ToList();
            if (listOfDDSupervisor != null)
            {
                userIdList.AddRange(listOfDDSupervisor.Select(x => x.UserId).ToList());
                
            }
            var taskDescriptionTaggedUserIdList = _innostaxDb.TaskDescriptionTaggedUsers.Where(x => x.Task.CaseId == id && x.Task.IsDeleted != true).Select(x => x.UserId).ToList();
            var taskCommentTagUserIdList = _innostaxDb.TaskCommentTaggedUsers.Where(x => x.TaskComment.Task.CaseId == id && x.TaskComment.IsDeleted != true && x.TaskComment.Task.IsDeleted != true).Select(x => x.UserId).ToList();
            userIdList.AddRange(taskDescriptionTaggedUserIdList);
            userIdList.AddRange(taskCommentTagUserIdList);
            var discussionUsers = _innostaxDb.DiscussionNotifiers.Include(y => y.Discussion.Case).Where(x => x.Discussion.Case.CaseId == id && x.Discussion.Case.IsDeleted != true && x.Discussion.IsArchive != true && x.Discussion.IsDeleted != true).Select(x => x.UserId);
            if (discussionUsers != null)
            {
                userIdList.AddRange(discussionUsers);
            }
            var caseParticipantUsers = _innostaxDb.ParticipantCaseAssociation.Where(x => x.CaseId == id && x.IsActive == true).Select(x=>x.UserId).ToList();
            if (caseParticipantUsers != null)
            {
                userIdList.AddRange(caseParticipantUsers);
            }
            var distinctUserIdList= userIdList.Distinct().ToList();
            foreach (var userId in distinctUserIdList)
            {
                var userDetails= _userService.GetUser(userId).Data;
                if(userDetails != null)
                    {
                    caseAssociatedUsers.Add(userDetails);
                }
            }
            IQueryable results = options.ApplyTo(caseAssociatedUsers.AsQueryable());
            return new PageResult<User>(
           results as IEnumerable<User>, request.ODataProperties().NextLink,
           caseAssociatedUsers.Count);      
        }

        public Case CalculateCaseMargin(Case caseDetails)
        {
            decimal directCaseExpense = 0;
            decimal laborExpense = 0;
            var caseExpensesList = _innostaxDb.CaseExpenses.Where(x => x.CaseId == caseDetails.CaseId && x.IsDeleted == false).ToList();
            foreach (var eachExpense in caseExpensesList)
            {
                directCaseExpense += eachExpense.Quantity * eachExpense.Cost;
            }
            var caseAssociatedTimesheets = _innostaxDb.TimeSheets.Where(x => x.CaseId == caseDetails.CaseId && x.IsDeleted == false).ToList();
            foreach (var eachTimesheet in caseAssociatedTimesheets)
            {
                if(eachTimesheet.EndTime != null)
                {
                    var totalTimeSpent = eachTimesheet.EndTime.Value.Subtract(eachTimesheet.StartTime);
                    double totalHoursSpent = totalTimeSpent.TotalHours;
                    var timesheetTotalCost = eachTimesheet.Cost * System.Convert.ToDecimal(totalHoursSpent);
                    laborExpense += timesheetTotalCost;
                }               
            }
            if (caseDetails.ActualRetail > 0)
                caseDetails.Margin = ((caseDetails.ActualRetail - (directCaseExpense + laborExpense)) / caseDetails.ActualRetail) * 100;
            return caseDetails;
        }

        public Case CalculateICITurnTime(Case caseDetails)
        {
            if (caseDetails.DateReportReceivedFromICI.HasValue && caseDetails.CaseAssignedToICIDate.HasValue)
            {
                TimeSpan difference = caseDetails.DateReportReceivedFromICI.Value - caseDetails.CaseAssignedToICIDate.Value;
                caseDetails.TurnTimeForICIBusinessDays = (int)difference.TotalDays;
            }            
            return caseDetails;
        }

        public Case CalculateReportToClientAheadOfTime(Case caseDetails)
        {
            if (caseDetails.FinalReportSentToClient && caseDetails.CaseDueDate.HasValue && caseDetails.FinalReportSentToClientDate.HasValue)
            {
                var timeSpan = caseDetails.CaseDueDate.Value - caseDetails.FinalReportSentToClientDate.Value;
                caseDetails.ReportToClientAheadOfTime = (int)timeSpan.TotalDays;
            }
            return caseDetails;
        }
    }
}



