﻿using AutoMapper;
using Innostax.CaseManagement.Models;

namespace Innostax.CaseManagement.Core.Automapper
{
    public class AutoMapperCoreConfiguration : Profile
    {

        protected override void Configure()
        {
            CreateMap<Common.Database.Models.Client, Client>();

            CreateMap<Client, Common.Database.Models.Client>()       
                .ForMember(y => y.CreationDateTime, z => z.Ignore())
                .ForMember(y => y.ClientId, z => z.Ignore());

            CreateMap<Innostax.Common.Database.Models.Case, Innostax.CaseManagement.Models.Case>();
            CreateMap<Innostax.Common.Database.Models.ClientIndustry, Innostax.CaseManagement.Models.ClientIndustry>();
            CreateMap<Innostax.CaseManagement.Models.ClientIndustry,Innostax.Common.Database.Models.ClientIndustry>();
            CreateMap<Innostax.CaseManagement.Models.Case, Innostax.Common.Database.Models.Case>();
            CreateMap<Innostax.CaseManagement.Models.Industry, Innostax.Common.Database.Models.Industry>();
            CreateMap<Innostax.Common.Database.Models.Industry,Innostax.CaseManagement.Models.Industry>();
            CreateMap<Innostax.CaseManagement.Models.Report, Innostax.Common.Database.Models.Report>();
            CreateMap<Innostax.Common.Database.Models.Report, Innostax.CaseManagement.Models.Report>();
            CreateMap<Innostax.Common.Database.Models.IndustrySubCategory, Innostax.CaseManagement.Models.IndustrySubCategory>();
            CreateMap<Innostax.CaseManagement.Models.IndustrySubCategory,Innostax.Common.Database.Models.IndustrySubCategory>();
            CreateMap<Innostax.CaseManagement.Models.State, Innostax.Common.Database.Models.State>();
            CreateMap<Innostax.Common.Database.Models.State,Innostax.CaseManagement.Models.State>();
            CreateMap<Innostax.CaseManagement.Models.Country, Innostax.Common.Database.Models.Country>();
            CreateMap<Innostax.Common.Database.Models.Country, Innostax.CaseManagement.Models.Country>();
            CreateMap<Innostax.Common.Database.Models.Region, Innostax.CaseManagement.Models.Region>();
            CreateMap<Innostax.Common.Database.Models.Discount, Innostax.CaseManagement.Models.Discount>();
            CreateMap<Common.Database.Models.Country, Country>();
            CreateMap<Common.Database.Models.State, State>();
            //To have the latest updates of the client for auto population
            CreateMap<Innostax.CaseManagement.Models.Case, Innostax.Common.Database.Models.Client>();
            CreateMap<Innostax.Common.Database.Models.CaseReportAssociation, Innostax.CaseManagement.Models.CaseReportAssociation>();
            CreateMap<Innostax.CaseManagement.Models.CaseReportAssociation,Innostax.Common.Database.Models.CaseReportAssociation>();           
            CreateMap<Innostax.Common.Database.Models.CaseFileAssociation, Innostax.CaseManagement.Models.CaseFileAssociation>();                 
            CreateMap<CaseManagement.Models.FileModel, Innostax.Common.Database.Models.UploadedFile>();
            CreateMap<Innostax.Common.Database.Models.UploadedFile, CaseManagement.Models.FileModel>();
            CreateMap<Innostax.CaseManagement.Models.Discussion, Innostax.Common.Database.Models.Discussion>();
            CreateMap<Innostax.Common.Database.Models.Discussion, Models.Discussion>();
            CreateMap<Innostax.CaseManagement.Models.CaseFileAssociation, Innostax.Common.Database.Models.CaseFileAssociation>();
            CreateMap<Innostax.Common.Database.Models.Task, Innostax.CaseManagement.Models.Task>()
                .ForMember(x => x.AssignedToUserDetails, y => y.MapFrom(src => src.AssignedToUser))
                .ForMember(x => x.CreatedByUserDetails, y => y.MapFrom(src => src.CreatedByUser));
            CreateMap<Innostax.CaseManagement.Models.Task, Innostax.Common.Database.Models.Task>();                               
            CreateMap<Innostax.CaseManagement.Models.Account.User, Innostax.Common.Database.Models.UserManagement.User>();
            CreateMap<Innostax.Common.Database.Models.UserManagement.User, Innostax.CaseManagement.Models.Account.User>();
            CreateMap<CaseExpense, Common.Database.Models.CaseExpense>();
            CreateMap<Common.Database.Models.CaseExpense, CaseExpense>();              
            CreateMap<TimeSheet, Common.Database.Models.TimeSheet>();
            CreateMap<Common.Database.Models.TimeSheet, TimeSheet>();                
            CreateMap<Innostax.Common.Database.Models.RolesAndPermissionManagement.Role, Innostax.CaseManagement.Models.Role>();
            CreateMap<Innostax.CaseManagement.Models.Role, Innostax.Common.Database.Models.RolesAndPermissionManagement.Role>();
            CreateMap<Innostax.Common.Database.Models.UserManagement.User, Innostax.CaseManagement.Models.Account.User>();
            CreateMap<Innostax.CaseManagement.Models.Account.User, Innostax.Common.Database.Models.UserManagement.User>();
            CreateMap<Notification, Common.Database.Models.Notification>()
                .ForMember(y => y.CreatedOn, z => z.Ignore())
                .ForMember(y => y.CreatedBy, z => z.Ignore());
            CreateMap<Common.Database.Models.Notification, Notification>();
            CreateMap<Innostax.Common.Database.Models.CasePrincipal, Innostax.CaseManagement.Models.CasePrincipal>();
            CreateMap<Innostax.CaseManagement.Models.CasePrincipal, Innostax.Common.Database.Models.CasePrincipal>();
            CreateMap<Innostax.Common.Database.Models.TaskFileAssociation,Innostax.CaseManagement.Models.TaskFileAssociation>();
            CreateMap<Innostax.CaseManagement.Models.TaskFileAssociation,Innostax.Common.Database.Models.TaskFileAssociation>();
            CreateMap<DiscussionMessage, Common.Database.Models.DiscussionMessage>();
            CreateMap<Common.Database.Models.DiscussionMessage, DiscussionMessage>();
            CreateMap<Innostax.Common.Database.Models.DiscussionFileAssociation, Innostax.CaseManagement.Models.DiscussionFileAssociation>();
            CreateMap<Innostax.CaseManagement.Models.DiscussionFileAssociation,Innostax.Common.Database.Models.DiscussionFileAssociation>();
            CreateMap<Innostax.Common.Database.Models.DiscussionMessageFileAssociation, Innostax.CaseManagement.Models.DiscussionMessageFileAssociation>();
            CreateMap<Innostax.CaseManagement.Models.DiscussionMessageFileAssociation, Innostax.Common.Database.Models.DiscussionMessageFileAssociation>();
            CreateMap<Innostax.CaseManagement.Models.CaseHistory, Innostax.Common.Database.Models.CaseHistory>();
            CreateMap<Innostax.Common.Database.Models.CaseHistory,Innostax.CaseManagement.Models.CaseHistory>();
            CreateMap<Innostax.CaseManagement.Models.TimeSheetDescription, Innostax.Common.Database.Models.TimeSheetDescription>();
            CreateMap<Innostax.Common.Database.Models.TimeSheetDescription, Innostax.CaseManagement.Models.TimeSheetDescription>();
            CreateMap<Innostax.Common.Database.Models.ClientContactUser, Innostax.CaseManagement.Models.ClientContactUser>()
                .ForMember(y => y.CountryName, z => z.MapFrom(src => src.Country.CountryName))
                .ForMember(y => y.StateName, z => z.MapFrom(src => src.State.StateName))
                .ForMember(y => y.Country, z => z.MapFrom(src => src.Country))
                .ForMember(y => y.State, z => z.MapFrom(src => src.State));
            CreateMap<Innostax.CaseManagement.Models.ClientContactUser, Innostax.Common.Database.Models.ClientContactUser>();
            CreateMap<Innostax.Common.Database.Models.ReportPrice, Innostax.CaseManagement.Models.ReportPrice>();
            CreateMap<Innostax.CaseManagement.Models.ReportPrice, Innostax.Common.Database.Models.ReportPrice>();
            CreateMap<Innostax.CaseManagement.Models.TaskComment, Innostax.Common.Database.Models.TaskComment>();
            CreateMap<Innostax.Common.Database.Models.TaskComment, Innostax.CaseManagement.Models.TaskComment>();
            CreateMap<Innostax.Common.Database.Models.TaskActivity, Innostax.CaseManagement.Models.TaskActivity>();
            CreateMap<Innostax.CaseManagement.Models.TaskActivity, Innostax.Common.Database.Models.TaskActivity>();
            CreateMap<Innostax.Common.Database.Models.SubStatus, Innostax.CaseManagement.Models.SubStatus>();
            CreateMap<Innostax.CaseManagement.Models.SubStatus, Innostax.Common.Database.Models.SubStatus>();
            CreateMap<Innostax.CaseManagement.Models.ContactUserBusinessUnit, Innostax.Common.Database.Models.ContactUserBusinessUnit>();
            CreateMap<Innostax.Common.Database.Models.ContactUserBusinessUnit, Innostax.CaseManagement.Models.ContactUserBusinessUnit>();
            CreateMap<Innostax.Common.Database.Models.ParticipantCaseAssociation, Innostax.CaseManagement.Models.ParticipantCaseAssociation>();
            CreateMap< Innostax.CaseManagement.Models.ParticipantCaseAssociation, Innostax.Common.Database.Models.ParticipantCaseAssociation>();
            CreateMap<Innostax.CaseManagement.Models.ICIUserCaseAssociation, Innostax.Common.Database.Models.ICIUserCaseAssociation>();
            CreateMap<Innostax.Common.Database.Models.ICIUserCaseAssociation, Innostax.CaseManagement.Models.ICIUserCaseAssociation>();
            CreateMap<Innostax.CaseManagement.Models.UserDetail, Innostax.Common.Database.Models.UserDetail>();
            CreateMap<Innostax.Common.Database.Models.UserDetail, Innostax.CaseManagement.Models.UserDetail>();
            CreateMap<Innostax.CaseManagement.Models.TaskCommentFileAssociation, Innostax.Common.Database.Models.TaskCommentFileAssociation>();
            CreateMap<Innostax.Common.Database.Models.TaskCommentFileAssociation, Innostax.CaseManagement.Models.TaskCommentFileAssociation>();
            CreateMap<Innostax.Common.Database.Models.Vendor, Innostax.CaseManagement.Models.Vendor>();
            CreateMap<Innostax.CaseManagement.Models.Vendor, Innostax.Common.Database.Models.Vendor>();
            CreateMap<Innostax.Common.Database.Models.NFCUserCaseAssociation, Innostax.CaseManagement.Models.NFCUserCaseAssociation>();
            CreateMap<Innostax.CaseManagement.Models.NFCUserCaseAssociation, Innostax.Common.Database.Models.NFCUserCaseAssociation>();
            CreateMap<Innostax.CaseManagement.Models.CaseStatus, Innostax.Common.Database.Models.CaseStatus>();
            CreateMap<Innostax.Common.Database.Models.CaseStatus,Innostax.CaseManagement.Models.CaseStatus>();
            CreateMap<Innostax.CaseManagement.Models.AnalyticsConfiguration, Innostax.Common.Database.Models.AnalyticsConfiguration>();
            CreateMap<Innostax.Common.Database.Models.AnalyticsConfiguration, Innostax.CaseManagement.Models.AnalyticsConfiguration>();
            CreateMap<Innostax.CaseManagement.Models.Tenant, Innostax.Common.Database.Models.Master.Tenant>();
            CreateMap<Innostax.Common.Database.Models.Master.Tenant, Innostax.CaseManagement.Models.Tenant>();
        }
    }
}
