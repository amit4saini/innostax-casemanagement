﻿'use strict';

// Setting up route
angular.module('tasks').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

      // Redirect to 404 when route not found
      $urlRouterProvider.otherwise('not-found');

      // Home state routing
      $stateProvider
        .state('tasks-list', {
            url: '/case-management/tasks',
            templateUrl: '/webapp/modules/tasks/views/assigned-tasks.view.html'
        });
  }
]);
