﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using System.Collections.Generic;
using AutoMapper;
using System.Web.Http;
using Innostax.Common.Database.Models;
using System.Net;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionMessageFileAssociationServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscussionMessageFileAssociationService sut;
        private ErrorHandlerService errorHandlerService;
        private Mock<ICaseHistoryService> _caseHistoryService; 
        private Mock<IUserService> _userService;
        private Mock<ICommonService> _commonService;
        Guid DiscussionMessageId;

        [TestInitialize]
        public void TestInitialize()
        {
            DiscussionMessageId = System.Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();
            _userService = new Mock<IUserService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _commonService = new Mock<ICommonService>();
            sut = new DiscussionMessageFileAssociationService(innostaxDbMock.Object, errorHandlerService,_userService.Object, _caseHistoryService.Object, _commonService.Object);
        }

        private Models.DiscussionMessageFileAssociation GetDummyDiscussionMessageFileAssociation()
        {

            var dummyAssociation = new Models.DiscussionMessageFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                DiscussionMessageId = DiscussionMessageId
            };
            return dummyAssociation;
        }
        private Models.DiscussionMessageFileAssociation GetInvalidDummyDiscussionAssociation()
        {

            var dummyAssociation = new Models.DiscussionMessageFileAssociation
            {
                Files = new List<Models.FileModel> { new Models.FileModel { FileId = new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F") } },
                DiscussionMessageId = Guid.NewGuid()
            };
            return dummyAssociation;
        }

        public List<UploadedFile> GetDummyFiles()
        {
            var dummyFiles = new List<UploadedFile>
            {
                new UploadedFile
                {
                    FileId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new UploadedFile
                {
                    FileId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyFiles;
        }

        public List<Models.FileModel> GetDomainModelDummyFiles()
        {
            var dummyFiles = new List<Models.FileModel>
            {
                new Models.FileModel
                {
                    FileId= new Guid("0CC4622F-C9F0-E511-AA98-FCAA14866D0F")
                },
                new Models.FileModel
                {
                    FileId= new Guid("20C4622F-C9F0-E511-AA98-FCAA14866D0F")
                }
            };
            return dummyFiles;
        }
        public List<Innostax.Common.Database.Models.DiscussionMessage> GetDummyDiscussionMessage()
        {
            var dummyDiscussionMessage = new List<Innostax.Common.Database.Models.DiscussionMessage>
            {
                new Innostax.Common.Database.Models.DiscussionMessage
                {
                     Id = DiscussionMessageId,
                     Discussion=new Innostax.Common.Database.Models.Discussion { Title="",CaseId=Guid.NewGuid()}
                },
                new Innostax.Common.Database.Models.DiscussionMessage
                {
                   Id = new Guid("02561DAC-DAF0-E511-AA98-FCAA14866D0F"),
                    Discussion=new Innostax.Common.Database.Models.Discussion { Title="",CaseId=Guid.NewGuid()}
                }
            };
            return dummyDiscussionMessage;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenUploadedTableIsEmpty_ThenThrowsNotFoundRequest()
        {
            try
            {
                var dummyFiles = new List<UploadedFile>();
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(dummyFiles);
                innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(GetDummyDiscussionMessage());
                innostaxDbMock.Setup(c => c.DiscussionMessageFileAssociations).ReturnsDbSet(new Innostax.Common.Database.Models.DiscussionMessageFileAssociation[0]);
                sut.Save(GetDummyDiscussionMessageFileAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingSave_WhenDiscussionMessageTableIsEmpty_ThenThrowsBadRequest()
        {
            try
            {
                var dummyDiscussionMessage = new List<Innostax.Common.Database.Models.DiscussionMessage>();
                innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyFiles());
                innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessage);
                sut.Save(GetDummyDiscussionMessageFileAssociation());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        public void GivenCallingSave_WhenDataIsValid_ThenMappedResultReturn()
        {
            var dummyDiscussionMessageAssociation = new List<Innostax.Common.Database.Models.DiscussionMessageFileAssociation>();
            innostaxDbMock.Setup(c => c.UploadFiles).ReturnsDbSet(GetDummyFiles());
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(GetDummyDiscussionMessage());
            innostaxDbMock.Setup(c => c.DiscussionMessageFileAssociations).ReturnsDbSet(dummyDiscussionMessageAssociation);
            try
            {
                sut.Save(GetDummyDiscussionMessageFileAssociation());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCallingGetDiscussionAssociatedFiles_ThenMappedResultReturn()
        {
            var dummyDiscussionMessageFileAssociation = new List<Innostax.Common.Database.Models.DiscussionMessageFileAssociation>();
            innostaxDbMock.Setup(c => c.DiscussionMessageFileAssociations).ReturnsDbSet(dummyDiscussionMessageFileAssociation);
            try
            {
                var result = sut.GetDiscussionMessageAssociatedFiles(Guid.NewGuid());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }
    }
}