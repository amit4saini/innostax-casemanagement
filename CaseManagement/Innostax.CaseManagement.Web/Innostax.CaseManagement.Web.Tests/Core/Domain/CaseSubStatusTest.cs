﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using System.Web.Http;
using Innostax.CaseManagement.Models;
using System.Collections.Generic;
using System.Net;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class CaseSubStatusTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ErrorHandlerService _errorHandlerService;
        private ICaseSubStatusService sut;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICommonService> _commonService;
        private Guid caseId;
        [TestInitialize]
        public void TestInitialize()
        {         
            innostaxDbMock = new Mock<InnostaxDb>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _errorHandlerService = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            sut = new CaseSubStatusService(
                innostaxDbMock.Object, _errorHandlerService, _caseHistoryService.Object, _commonService.Object
                );
            caseId = Guid.NewGuid();    
        }

        public SubStatus GetDummySubStatus()
        {
            return new SubStatus {Id=Guid.NewGuid(),Name="Test",StatusId=Guid.NewGuid() };
        }

        [TestMethod]      
        public void GivenCalling_ChangeCaseSubStatus_WhenCaseIsPresent_ThenThrowNoException()
        {
            innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(new List<Innostax.Common.Database.Models.Case> { new Innostax.Common.Database.Models.Case { CaseId = caseId } });
            sut.ChangeCaseSubStatus(GetDummySubStatus(),caseId);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_AssignUserToCase_WhenCasesAreNotPresent_ThenThrowCaseNotFound()
        {
            try
            {
                innostaxDbMock.Setup(x => x.Cases).ReturnsDbSet(new List<Innostax.Common.Database.Models.Case> { new Innostax.Common.Database.Models.Case { CaseId = caseId } });
                sut.ChangeCaseSubStatus(GetDummySubStatus(), Guid.NewGuid());
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;           
            }
        }
    }
}
