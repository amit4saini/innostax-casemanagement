﻿namespace Innostax.Common.Database.Enums
{
    public enum CaseFileType
    {
        CASEFILE,
        DISCUSSIONFILE,
        REPORTFILE,
        TASKFILE
    }
}
