﻿using System.Collections.Generic;
using System.Linq;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IDiscountService
    {
        List<Discount> GetDiscounts();
    }

    public class DiscountService : IDiscountService
    {
        private IInnostaxDb _innostaxDb;
        private IErrorHandlerService _errorHandlerService;

        public DiscountService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Discount> GetDiscounts()
        {
            List<Discount> result = new List<Discount>();
            result = new List<Discount>();
            var discountList = _innostaxDb.Discounts.ToList();
            if (discountList.Count == 0)
            {
                string discountNotFoundError = Common.Constants.ErrorMessages.Common.DiscountError.DISCOUNT_NOT_FOUND;
                _errorHandlerService.ThrowResourceNotFoundError(discountNotFoundError);
            }
            foreach (var discount in discountList)
            {
                var currentDiscount = Mapper.Map<Discount>(discount);
                result.Add(currentDiscount);
            }
            return result;
        }
    }
}
