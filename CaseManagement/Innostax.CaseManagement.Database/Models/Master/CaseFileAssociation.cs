﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class CaseFileAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public Guid FileId { get; set; }

        [ForeignKey("FileId")]
        public UploadedFile UploadedFile { get; set; }
        public bool IsDeleted { get; set; }
    }
}
