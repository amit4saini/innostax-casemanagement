﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using AutoMapper;
using System.Collections.Generic;
using Innostax.Common.Database.Models;
using System.Web.Http;
using System.Net;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ClientIndustryTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private ClientIndustryService sut;        
        Guid clientId1;
        Guid clientId2;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            sut = new ClientIndustryService(
                innostaxDbMock.Object
                );          
        }  
        private List<ClientIndustry> GetDummyClientIndustry()
        {
            var dummyClientIndustry = new List<ClientIndustry>
            {
                new ClientIndustry
                {
                   Name="IndustryA"
                },
                new ClientIndustry
                {
                   Name="IndustryB"
                }
            };
            return dummyClientIndustry;
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCalling_GetAllClientIndustry_WhenClientIndustryTableIsEmpty_ThenThrowNotFound()
        {
            try
            {
                var dummyClientIndustry = new List<ClientIndustry>();          
                innostaxDbMock.Setup(c => c.ClientIndustries).ReturnsDbSet(dummyClientIndustry);
                 sut.GetAllClientIndustry();
            }
            catch (HttpResponseException e)
            {
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]
        public void GivenCalling_GetAllClientIndustry_WhenClientIndustryTableIsNotEmpty_ThenReturnResultTrue()
        {
            
            var dummyClientIndustry = GetDummyClientIndustry();
            innostaxDbMock.Setup(c => c.ClientIndustries).ReturnsDbSet(dummyClientIndustry);
            try
            {
                var result = sut.GetAllClientIndustry();
            }
            catch(Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }      
        }
    }
}
