﻿using AutoMapper;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ISubStatusService
    {
        List<Innostax.CaseManagement.Models.SubStatus> GetAllSubStatus();
        List<Models.SubStatus> GetSubStatusByStatusId(Guid statusId);
    }

    public class SubStatusService : ISubStatusService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public SubStatusService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        public List<Innostax.CaseManagement.Models.SubStatus> GetAllSubStatus()
        {
            var result = new List<Innostax.CaseManagement.Models.SubStatus>();
            var allSubStatus = _innostaxDb.SubStatus.ToList();
            if (allSubStatus.Count() > 0)
            {
                allSubStatus.ForEach(x => result.Add(Mapper.Map<Innostax.CaseManagement.Models.SubStatus>(x)));
            }
            return result;
        }

        public List<Models.SubStatus> GetSubStatusByStatusId(Guid statusId)
        {
            var result = new List<Innostax.CaseManagement.Models.SubStatus>();
            var allSubStatus = _innostaxDb.SubStatus.Where(x => x.StatusId == statusId).ToList();
            if (allSubStatus.Count() > 0)
            {
                allSubStatus.ForEach(x => result.Add(Mapper.Map<Innostax.CaseManagement.Models.SubStatus>(x)));
            }
            return result;
        }
    }
}
