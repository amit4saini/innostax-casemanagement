﻿using Innostax.Common.Database.Models.UserManagement;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class DiscussionMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid DiscussionId { get; set; }
        [ForeignKey("DiscussionId")]
        public Discussion Discussion { get; set; }

        public string Message { get; set; }
        public DateTime CreationDateTime { get; set; }
        public Guid CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public DateTime LastEditedDateTime { get; set; }

        public Guid? EditedBy { get; set; }
        [ForeignKey("EditedBy")]
        public User EditedByUser { get; set; }

        [DefaultValue(false)]
        public bool IsArchive { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public User DeleteByUser { get; set; }
    }
}

