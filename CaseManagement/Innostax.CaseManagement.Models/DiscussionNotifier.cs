﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class DiscussionNotifier
    {
        public Guid Id { get; set; }
        public Guid DiscussionId { get; set; }
        public Guid UserId { get; set; }
        public bool IsRead { get; set; }
    }
}
