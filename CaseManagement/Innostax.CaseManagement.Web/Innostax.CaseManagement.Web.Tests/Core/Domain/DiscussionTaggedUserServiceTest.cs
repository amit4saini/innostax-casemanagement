﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Web.Tests.Core.Domain;
using Innostax.Common.Database;
using Innostax.Common.Database.Models;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionTaggedUserServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscussionTaggedUserService sut;
        Guid discussionId;
        Guid userId;

        [TestInitialize]
        public void TestInitialize()
        {
            discussionId = Guid.NewGuid();
            userId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            sut = new DiscussionTaggedUserService(
                innostaxDbMock.Object);
        }

        private List<DiscussionTaggedUser> GetDummyDiscussionTaggedUsers()
        {
            var dummyDiscussionTaggedUsers = new List<DiscussionTaggedUser>
            {
                new DiscussionTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                   DiscussionId=discussionId,
                   User=GetDummyUser()
                },
                new DiscussionTaggedUser
                {
                    Id=Guid.NewGuid(),
                    UserId = Guid.NewGuid(),
                    DiscussionId=discussionId
                }
            };
            return dummyDiscussionTaggedUsers;
        }
        private Innostax.Common.Database.Models.UserManagement.User GetDummyUser()
        {
            return new Innostax.Common.Database.Models.UserManagement.User
            {
                Id=userId,
            };
        }

        [TestMethod]
        public void GivenCalling_SaveDiscussionTaggers_WhenDiscussionTaggedUserIsNotFound_ThenReturnEmptyResult()
        {
            try
            {
                var dummyDiscussionTaggedUsers = GetDummyDiscussionTaggedUsers();
                var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                innostaxDbMock.Setup(c => c.DiscussionTaggedUsers).ReturnsDbSet(dummyDiscussionTaggedUsers);
                sut.SaveDiscussionTaggedUsers(Guid.NewGuid(), listOfTaggedUsers);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCalling_SaveDiscussionTaggers_WhenDiscussionTaggedUserIsFound_ThenReturnEmptyResult()
        {
            try
            {
                var dummyDiscussionTaggedUsers = GetDummyDiscussionTaggedUsers();
                var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                innostaxDbMock.Setup(c => c.DiscussionTaggedUsers).ReturnsDbSet(dummyDiscussionTaggedUsers);
                sut.SaveDiscussionTaggedUsers(discussionId, listOfTaggedUsers);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_GetDiscussionTaggedUser_WhenDiscussionTaggedUserIsNotFound_ThenReturnEmptyResult()
        {          
                var dummyDiscussionTaggedUsers = GetDummyDiscussionTaggedUsers();
                var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
                innostaxDbMock.Setup(c => c.DiscussionTaggedUsers).ReturnsDbSet(new List<DiscussionTaggedUser> { });
               innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new List<Innostax.Common.Database.Models.UserManagement.User> { });
                var result=sut.GetDiscussionTaggedUsers(discussionId);
                Assert.AreEqual(result.Count,0);
         }                   

     [TestMethod]
    public void GivenCalling_GetDiscussionTaggedUser_WhenDiscussionTaggedUserIsFound_ThenReturnResult()
    {
        var dummyDiscussionTaggedUsers = GetDummyDiscussionTaggedUsers();
        var listOfTaggedUsers = new List<Guid>
            {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
        innostaxDbMock.Setup(c => c.DiscussionTaggedUsers).ReturnsDbSet(GetDummyDiscussionTaggedUsers());
        innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(new List<Innostax.Common.Database.Models.UserManagement.User> { GetDummyUser() });
        var result = sut.GetDiscussionTaggedUsers(discussionId);
        Assert.IsNotNull(result);
    }
}

}  

