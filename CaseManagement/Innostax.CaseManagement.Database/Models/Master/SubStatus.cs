﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class SubStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid StatusId { get; set; }

        [ForeignKey("StatusId")]
        public CaseStatus CaseStatus { get; set; }
        public string Name { get; set; }
    }
}
