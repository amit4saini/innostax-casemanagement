﻿using System;
using System.Collections.Generic;
using Innostax.CaseManagement.Models.Account;
using Innostax.Common.Database.Enums;
using System.ComponentModel;

namespace Innostax.CaseManagement.Models
{
    public class Case
    {
        public Guid CaseId { get; set; }
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        public Guid? SubStatusId { get; set; }
        public string ClientName { get; set; }  
        public Report[] Reports { get; set; }
        public string PrimaryReportType { get; set; }
        public string PrimaryReportCountryName { get; set; }
        public int CaseAssociatedTasksCount { get; set; }
        public int CaseAssociatedDiscussionsCount { get; set; }
        public int CaseAssociatedTimesheetsCount { get; set; }
        public int CaseAssociatedExpensesCount { get; set; }
        public int CaseDiscussionReportCount { get; set; }
        public int CaseAllFilesCount { get; set; }
        public int RegionId { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Discussion> Discussions { get; set; }
        public List<FileModel> Files { get; set; }
        public List<CaseExpense> Expenses { get; set; }
        public List<TimeSheet> Timesheets { get; set; }
        public string CaseNumber { get; set; }               
        public FileModel[] PrimaryReportFiles { get; set; }
        public string ReportSubject { get; set; }
        public string NickName { get; set; }
        public string ClientPO { get; set; }

        public int? SubjectType { get; set; }

        public int NumberOfPrincipals { get; set; }

        public bool IsSpecificPrincipal { get; set; }

        public CasePrincipal[] PrincipalNames { get; set; }

        public bool ConsentReceived { get; set; }

        public bool AgreementReceived { get; set; }

        public bool FCRACertificationEmailReceivedFromClient { get; set; }

        public int? StateOfEmploymentId { get; set; }    

        public int? StateOfResidenceId { get; set; }     

        public bool SalaryGreaterThan75k { get; set; }

        public bool References { get; set; }

        public bool Reputation { get; set; }

        public bool CreditReport { get; set; }

        public decimal ActualRetail { get; set; }

        public decimal Discount { get; set; }

        public bool IsProBonoGratis { get; set; }

        public string WhyProBonoGratis { get; set; }

        public string ClientProvidedInformation { get; set; }
        public bool ClientProvidedAttachments { get; set; }
        public string AdditionalSalesRepNotes { get; set; }
        public string HoldReason { get; set; }      
        public int? CaseAge { get; set; }       
        public bool? ClientCancelled { get; set; }
        public Guid? DDSupervisorWhoApprovedCase { get; set; }
        public User DDSupervisorWhoApprovedCaseByUser { get; set; }
        public DateTime? DDSupervisorApprovedCaseDate { get; set; }//Intake Date
        public Guid? IHIAssignedUserId { get; set; }      
        public User IHIAssignedUser { get; set; }
        public DateTime? IHIDueDate { get; set; }
        public bool? ICIAssigned { get; set; }        
        public Guid? AssignedTo { get; set; }
        public bool IsNFCCase { get; set; }
        public bool? CaseAcceptedByNFC { get; set; }
        public DateTime? NFCDueDate { get; set; }
        public DateTime? CaseAcceptedByNFCDate { get; set; }
        public decimal? NFCBudget { get; set; }
        public bool? ReportReceivedFromNFC { get; set; }
        public DateTime? DateReportReceivedFromNFC { get; set; }
        public bool? InvoiceReceivedFromNFC { get; set; }
        public DateTime? DateInvoiceReceivedFromNFC { get; set; }
        public Country[] ReportsCountries { get; set; }
        public String[] OtherReportTypes { get; set; }
        public string StateOfEmploymentName { get; set; }
        public string StateOfResidenceName { get; set; }
        public DateTime? LastEditedDateTime { get; set; }
        public DateTime CreationDateTime { get; set; }
        public Guid CreatedBy { get; set; }
        public User CreatedByUser { get; set; }
        public User AssignedToUser { get; set; }
        public Guid? ClientContactUserId { get; set; }       
        public ClientContactUser ClientContactUser { get; set; }
        public SubStatus SubStatus { get; set; }
        public Speed Speed { get; set; }
        public bool IsDeleted { get; set; }
        public decimal Margin { get; set; }
        public DateTime? ClosingDateTime { get; set; }     
        public Guid? BusinessUnitId { get; set; }
        public ContactUserBusinessUnit BusinessUnit { get; set; }
        public Guid? ReviewedBy { get; set; }  
        public User ReviewedByUser { get; set; }
        public Guid? StatusId { get; set; }
        public CaseStatus CaseStatus { get; set; }
        public string MatterName { get; set; }
        public bool IsEligibleForInvoice { get; set; }
        public DateTime? DateReportReceivedFromICI { get; set; }
        public int? TurnTimeForICIBusinessDays { get; set; }
        public AdverseReportType? AdverseReport { get; set; }
        public ICIErrorType? ICIError { get; set; }
        public string ICIErrorDescription { get; set; }
        public bool? IHIError { get; set; }
        public string IHIErrorDescription { get; set; }
        public IHIQuality? IHIQuality { get; set; }
        public string IHIQualityDescription { get; set; }
        public bool? IHIBonusCase { get; set; }
        public bool InQC { get; set; }
        public bool OnHold { get; set; }
        public int? InHouseTurnTime { get; set; }
        public int? NumberOfDaysLate { get; set; }
        public DateTime? CaseReviewedDate { get; set; }
        public int? TotalInvestigativePeriod { get; set; }
        public int? TotalICIInvestigativePeriod { get; set; }
        public int? IHIAheadOfTime { get; set; }
        public int? ReportToClientAheadOfTime { get; set; }
        public int? ICICorpRegistrationTurnTime { get; set; }
        public DateTime? CaseCancelledDate { get; set; }
        public string CancellationReason { get; set; }
        public DateTime? EstimatedCompletionDate { get; set; }
        public DateTime? CaseAssignedToNFCDate { get; set; }
        public Guid? UserWhoAssignedToNFC { get; set; }
        public User UserWhoAssignedToNFCUser { get; set; }
        public DateTime? NFCToKrellerAssignedDate { get; set; }
        public Guid? NFCToKrellerAssignedByUserId { get; set; }
        public User NFCToKrellerAssignedByUser { get; set; }
        public DateTime? CaseAssignedToICIDate { get; set; }
        public Guid? CaseAssignedToICIByUserId { get; set; }
        public User CaseAssignedToICIByUser { get; set; }
        public DateTime? CaseAssignedToIHIDate { get; set; }
        public DateTime? ICIConfirmedReceiptDate { get; set; }
        public Guid? ICIConfirmationMarkedByUserId { get; set; }
        public User ICIConfirmationMarkedByUser { get; set; }
        public DateTime? InitialContactWithReferencesDate { get; set; }
        public Guid? UserIdWhoMadeInitialContactWithReferences { get; set; }
        public User UserWhoMadeInitialContactWithReferences { get; set; }
        public DateTime? RegistrationDetailsReceivedDate { get; set; }
        public DateTime? DateReportDueFromICI { get; set; }
        public Guid? UserIdWhoMarkedReportFromICIAsReceived { get; set; }
        public User UserWhoMarkedReportFromICIAsReceived { get; set; }
        public DateTime? IHIReviewOfICIReportCompletedDate { get; set; }
        public Guid? UserIdWhoCompletedReviewOfICIReport { get; set; }
        public User UserWhoCompletedReviewOfICIReport { get; set; }
        public DateTime? DateIHISubmittedReportForReview { get; set; }
        public Guid? IHIUserIdWhoSubmittedReport { get; set; }
        public User IHIUserWhoSubmittedReport { get; set; }
        public DateTime? DDSupervisorBeganQCReviewDate { get; set; }
        public Guid? UserIdWhoConductedQCReview { get; set; }
        public User UserWhoConductedQCReview { get; set; }
        public DateTime? DDSupervisorApprovedQCReviewDate { get; set; }
        public Guid? UserIdWhoApprovedQCReviewOfReport { get; set; }
        public User UserWhoApprovedQCReviewOfReport { get; set; }
        public Guid? UserIdWhoSentFinalReportToSalesRep { get; set; }
        public User UserWhoSentFinalReportToSalesRep { get; set; }
        public Guid? UserIdWhoSentFinalReportToClient { get; set; }
        public User UserWhoSentFinalReportToClient { get; set; }
        public Guid? UserIdWhoPlacedCaseOnHold { get; set; }
        public User UserWhoPlacedCaseOnHold { get; set; }
        public DateTime? DateCaseRemovedFromHold { get; set; }
        public Guid? UserIdWhoRemovedCaseFromHold { get; set; }      
        public User UserWhoRemovedCaseFromHold { get; set; }
        public DateTime? DateCaseClosed { get; set; }
        public Guid? UserIdWhoClosedCase { get; set; }
        public User UserWhoClosedCase { get; set; }
        public DateTime? CaseDueDate { get; set; }
        public DateTime? CasePlacedOnHoldDate { get; set; }
        public bool CaseCancelled { get; set; }
        [DefaultValue(true)]
        public bool InformationPending { get; set; }
        public bool UpdateRequested { get; set; }
        public Guid? CaseAssignedToIHIByUserId { get; set; }
        public User CaseAssignedToIHIByUser { get; set; }
        public Guid? ICIAssignedUserId { get; set; }
        public User ICIAssignedUser { get; set; }
        public bool FinalReportSentToClient { get; set; }
        public bool FinalReportSentToSalesRep { get; set; }
        public DateTime? FinalReportSentToSalesRepDate { get; set; }
        public DateTime? FinalReportSentToClientDate { get; set; }
        public Guid? DDSupervisorId { get; set; }
        public User DDSupervisorUserDetails { get; set; }
        public Guid? UserIdWhoCancelledCase { get; set; }
        public User UserWhoCancelledCase { get; set; }
        public DateTime? ICIReportTurnTimeEndDate { get; set; }
        public bool IsICIReportReceived { get; set; }
        public bool IsICIReportTurnTimeEndDateAdded { get; set; }
    }
}
