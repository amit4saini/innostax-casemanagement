﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IVendorService
    {
        void Save(Vendor vendorData);
        List<Vendor> GetAllVendors();
    }

    public class VendorService:IVendorService
    {
        private IInnostaxDb _innostaxDb;
        public VendorService(IInnostaxDb innostaxDb)
        {
            _innostaxDb = innostaxDb;
        }

        public void Save(Vendor vendorData)
        {
            var existingVendor = _innostaxDb.Vendors.FirstOrDefault(x => x.VendorId == vendorData.VendorId);
            if(existingVendor == null)
            {
                existingVendor = new Common.Database.Models.Vendor();
                existingVendor.CreatedOn = DateTime.Now;
            }
            existingVendor.VendorName = vendorData.VendorName;
            _innostaxDb.Vendors.AddOrUpdate(existingVendor);
            _innostaxDb.SaveChangesWithErrors();
        }

        public List<Vendor> GetAllVendors()
        {
            var existingVendors = _innostaxDb.Vendors;
            var mappedVendors = Mapper.Map<List<Vendor>>(existingVendors.OrderBy(x=>x.CreatedOn).ToList());
            return mappedVendors;
        }
    }
}
