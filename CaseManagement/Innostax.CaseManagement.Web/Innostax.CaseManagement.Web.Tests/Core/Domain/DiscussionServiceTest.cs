﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using AutoMapper;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Query;
using System.Web.Http.OData;
using System.Net.Http;
using Innostax.CaseManagement.Core.Services.Implementation;
using System.Linq;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class DiscussionServiceTest
    {
        private Mock<InnostaxDb> innostaxDbMock;
        private DiscussionService sut;
        private Mock<IUserService> _userService;
        private Mock<IDiscussionFileAssociationService> _discussionFileAssociationService;
        private Mock<IDiscussionNotifierService> _discussionNotifierService;
        private Mock<IDiscussionMessageService> _discussionMessageService;
        private Mock<IDiscussionMessageFileAssociationService> _discussionMessageFileAssociationService;
        private Mock<IAzureFileManager> _azureFileManager;
        private Mock<ICaseHistoryService> _caseHistoryService;
        private Mock<ICaseService> _caseService;
        private Mock<ICommonService> _commonService;
        private Mock<INotificationService> _notificationService;
        private ErrorHandlerService _errorHandler;
        private Mock<IDiscussionTaggedUserService> _discussionTaggedUserService;
        Guid discussionId;
        Guid caseId;
        Guid userId1;
        Guid userId2;
        ODataQueryOptions<Models.Discussion> options;

        [TestInitialize]
        public void TestInitialize()
        {
            var userId = new Guid("6D864895-58F2-E511-B4E9-FCAA14866D0F");
            discussionId = System.Guid.NewGuid();
            caseId = Guid.NewGuid();
            userId1 = Guid.NewGuid();
            userId2 = Guid.NewGuid();
            caseId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            _userService = new Mock<IUserService>();
            _errorHandler = new ErrorHandlerService();
            _caseService = new Mock<ICaseService>();
            _discussionTaggedUserService = new Mock<IDiscussionTaggedUserService>();
            _commonService = new Mock<ICommonService>();
            _discussionNotifierService = new Mock<IDiscussionNotifierService>();
            _discussionFileAssociationService = new Mock<IDiscussionFileAssociationService>();
            _discussionMessageService = new Mock<IDiscussionMessageService>();
            _caseHistoryService = new Mock<ICaseHistoryService>();
            _discussionMessageFileAssociationService = new Mock<IDiscussionMessageFileAssociationService>();
            _azureFileManager = new Mock<IAzureFileManager>();
            _notificationService = new Mock<INotificationService>();
            _userService.SetupGet(x => x.UserId).Returns(userId);
            _userService.Setup(x => x.GetCurrentUser()).Returns(new Models.Account.User { Role= new Models.Role {Name=Roles.Client_Participant.ToString() } });
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Discussion>("Discussion");
            options = new ODataQueryOptions<Models.Discussion>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Discussion)), new HttpRequestMessage());
            sut = new DiscussionService(
                innostaxDbMock.Object,
                 _userService.Object,
                 _errorHandler,
                 _discussionNotifierService.Object,
                 _discussionFileAssociationService.Object,
                 _discussionMessageService.Object,
                 _discussionMessageFileAssociationService.Object,
                 _azureFileManager.Object,_caseHistoryService.Object,_caseService.Object, _notificationService.Object, _commonService.Object,_discussionTaggedUserService.Object
                );
            _discussionFileAssociationService.Setup(x => x.Save(It.IsAny<Models.DiscussionFileAssociation>()));
            _discussionFileAssociationService.Setup(x => x.GetDiscussionAssociatedFiles(It.IsAny<Guid>())).Returns(
            new List<Models.FileModel>
            {
                { new Models.FileModel
                {
                    FileId=new Guid()
                }
            }
            });           
        }
        private List<Discussion> GetDummyDiscussionModel()
        {
            var dummyDiscussion = new List<Discussion>
            {
                new Discussion
                {
                    Id=discussionId,
                    Case = GetACase(),
                    CaseId = caseId,
                    CreatedBy = userId1,
                    Message="random",
                    Title="title",
                    IsDeleted=false,
                    IsArchive=false,
                    CreationDateTime=DateTime.Now,
                    
                },
                new Discussion
                {
                    Id=Guid.NewGuid(),
                    Case = GetACase(),
                    CaseId = caseId,
                    CreatedBy = userId1,
                    Message="random",
                    Title="title",
                    IsDeleted=false,
                    IsArchive=false,
                    CreationDateTime=DateTime.Now
                }
            };
            return dummyDiscussion;
        }
        private List<DiscussionMessage> GetDummyDiscussionMessageModel()
        {
            var dummyDiscussionMessage = new List<DiscussionMessage>
            {
                new DiscussionMessage
                {
                    Discussion = new Discussion { Id=discussionId,
                    Case = GetACase(),
                    CaseId = caseId,
                    CreatedBy = userId1,
                    Message="random",
                    Title="title",
                    IsDeleted=false,
                    IsArchive=false,
                    CreationDateTime=DateTime.Now,
                     },
                    Id=Guid.NewGuid(),
                    DiscussionId = discussionId,
                    CreatedBy = userId1,
                    Message="random",
                    IsDeleted=false,
                    IsArchive=false
                },
                new DiscussionMessage
                {
                    Discussion = new Discussion {Title="ttl", Message= "message", CaseId=caseId, CreatedBy = userId1, CreationDateTime=DateTime.Now },
                    Id=Guid.NewGuid(),
                    DiscussionId = discussionId,
                    CreatedBy = userId1,
                    Message="random",
                    IsDeleted=false,
                    IsArchive=false
                }
            };
            return dummyDiscussionMessage;
        }
        private Models.Discussion GetDummyDiscussion()
        {
            var discussion = new Models.Discussion
            {
                Id = Guid.NewGuid(),
                Message = "random",
                Title = "title",
                IsDeleted = false,
            };
            return discussion;
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId1 },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId2 }
                            };
            return dummyUser;
        }

        private List<Innostax.Common.Database.Models.DiscussionNotifier> GetDiscussionNotifier()
        {
            var dummyDiscussionNotifier = new List<DiscussionNotifier>
            {
                new DiscussionNotifier
                {
                    Id= Guid.NewGuid(),
                    Discussion = new Discussion {Title="title", Message= "msg", CaseId=caseId, CreatedBy = userId1, CreationDateTime=DateTime.Now  },
                    User = new Innostax.Common.Database.Models.UserManagement.User {Id = userId1 }
                }
            };
            return dummyDiscussionNotifier;
        }

        private List<Client> GetDummyClients()
        {
            var clientId1 = Guid.NewGuid();
            var clientId2 = Guid.NewGuid();
            var dummyClients = new List<Client>
            {
                new Client
                {
                    ClientId = clientId1,
                    IndustryId =1,
                    IndustrySubCategoryId = 1,
                    SalesRepresentativeId = Guid.NewGuid(),
                    ClientName = "clientName2",
                    Notes = "notes2",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User {FirstName = "firstName2", LastName = "lastName2", },
                    IsDeleted=false
                },
                new Client
                {
                    ClientId = clientId2,
                    IndustryId =1,
                    IndustrySubCategoryId = 1,
                    ClientName = "clientName2",
                    Notes = "notes2",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User {FirstName = "firstName2", LastName = "lastName2"},
                    IsDeleted=false
                }
            };
            return dummyClients;
        }
        private List<ClientContactUser> GetDummyClientContactUsers()
        {
            var dummyClientContactUsers = new List<ClientContactUser>
            {
                new ClientContactUser
                {
                    Id= Guid.NewGuid(),
                    CountryId=1,
                    StateId=1,
                    ClientId = Guid.NewGuid(),
                    Client = new Client {ClientName = "clientName", Notes = "notes", SalesRepresentativeEmail = "abc@bcd.com" },
                    PositionHeldbyContact = "positionHeldBy",
                    Street = "street",
                    City = "city",
                    ZipCode = "zipCode",
                    IsDeleted=false
                },
                new ClientContactUser
                {
                    Id=Guid.NewGuid(),
                    CountryId=1,
                    StateId=1,
                    Client = new Client {ClientName = "clientName", Notes = "notes" },
                    PositionHeldbyContact = "positionHeldBy",
                    Street = "street",
                    City = "city",
                    ZipCode = "zipCode",
                    IsDeleted=false
                }
            };
            return dummyClientContactUsers;
        }
        private Case GetACase()
        {
            return new Case
            {
                CaseId = caseId,
                IsDeleted = false,
                AdditionalSalesRepNotes = "notes",
                ClientPO = "clientPO",
                ClientProvidedInformation = "hgdjmd",
                HoldReason = "holdReason",
                NickName = "matter",    
                AssignedTo = userId1
            };
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                GetACase()                               
            };
            return dummyCase;
        }

        [TestMethod]
        [Ignore]
        public void GivenCalling_Save_WhenDataIsValid_ThenMappedResultReturn()
        {
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(GetDummyDiscussionModel());
            try
            {
                sut.Save(GetDummyDiscussion());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void GivenCalling_ArchiveUnarchiveDiscussion_WhenDataIsValid_MappedResultReturn()
        {

            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(GetDummyDiscussionModel());
            try
            {
                sut.ArchiveUnarchiveDiscussion(discussionId, true);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetAllDiscussionByUserIdOrCaseId_WhenNoDiscussionIsPresentForCaseId_ThenThrowNotFoundRequest()
        {
            var dummyDiscussions = GetDummyDiscussionModel();
            var dummyUsers = GetDummyUser();
            var dummyDiscussionMessages = new List<DiscussionMessage>
            {
                new DiscussionMessage { Id = Guid.NewGuid(), DiscussionId = dummyDiscussions[0].Id, IsDeleted = false },
                new DiscussionMessage { Id = Guid.NewGuid(), DiscussionId = dummyDiscussions[1].Id, IsDeleted = false }
            };
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(dummyDiscussions);
            var actual = await sut.GetAllDiscussionsByUserIdOrCaseId(options, new HttpRequestMessage(), Guid.NewGuid());
            Assert.AreEqual(actual.Result.Count, 0);
        }

        [TestMethod]
        public async System.Threading.Tasks.Task GivenCalling_GetAllDiscussionByUserIdOrCaseId_WhenDiscussionsArePresent_ThenMappedResultReturn()
        {
            var dummyDiscussions = GetDummyDiscussionModel();
            var dummyDiscussionMessages = GetDummyDiscussionMessageModel();
            var dummyUsers = GetDummyUser();
            var DiscussionMessages = new List<DiscussionMessage>
            {
                new DiscussionMessage { Id = Guid.NewGuid(), DiscussionId = dummyDiscussions[0].Id, IsDeleted = false },
                new DiscussionMessage { Id = Guid.NewGuid(), DiscussionId = dummyDiscussions[1].Id, IsDeleted = false }
            };
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(dummyDiscussions);
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(dummyDiscussionMessages);
            innostaxDbMock.Setup(c => c.DiscussionNotifiers).ReturnsDbSet(GetDiscussionNotifier());
            var actual = await sut.GetAllDiscussionsByUserIdOrCaseId(options, new System.Net.Http.HttpRequestMessage(), caseId);
            Assert.IsNotNull(actual.Result.Items);
        }

        [TestMethod]
        public void GivenCalling_SearchDiscussionsByKeyword_WhenDiscussionIsFound_ReturnDiscussion()
        {
            var dummyDiscussions = GetDummyDiscussionModel();
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(dummyDiscussions);
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(GetDummyDiscussionMessageModel());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(GetDummyClients());
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(new List<Case> { new Case() {CaseId=caseId, CaseNumber="DD-11"} });
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(GetDummyClientContactUsers());
            _commonService.Setup(x => x.GetAuthorizedCasesForUser()).Returns(GetDummyCase().AsQueryable());
            _commonService.Setup(x => x.GetAuthorizedDiscussions(null)).Returns(GetDummyDiscussionModel().AsQueryable());
            _discussionNotifierService.Setup(x => x.GetNotifiersListForDiscussion(It.IsAny<Guid>())).Returns(new List<Guid> {  });
            var discussionData = sut.SearchDiscussionsByKeyword(options, new HttpRequestMessage(), caseId, "title");
            Assert.AreEqual(discussionData.Result.TotalDiscussionCount,2);
        }

        [TestMethod]
        public void GivenCalling_SearchDiscussionsByKeyword_WhenDiscussionIsNotFound_ThenResultIsEmpty()
        {
            var dummyDiscussions = GetDummyDiscussionModel();
            innostaxDbMock.Setup(c => c.Discussions).ReturnsDbSet(dummyDiscussions);
            innostaxDbMock.Setup(c => c.DiscussionMessages).ReturnsDbSet(GetDummyDiscussionMessageModel());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(GetDummyUser());
            var discussionData = sut.SearchDiscussionsByKeyword(options, new HttpRequestMessage(), Guid.NewGuid(), "fdhvbj");
            Assert.AreEqual(discussionData.Result.TotalDiscussionCount, 0);
        }
    }
}
