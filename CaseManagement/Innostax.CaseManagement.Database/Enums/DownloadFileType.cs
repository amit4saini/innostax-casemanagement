﻿namespace Innostax.Common.Database.Enums
{
    public enum DownloadFileType
    {
        CaseFile,
        ProfilePic,
        DiscussionFile,
        TaskFile
    }
}
