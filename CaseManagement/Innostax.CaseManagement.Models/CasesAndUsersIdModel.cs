﻿using System;

namespace Innostax.CaseManagement.Models
{
    public class CasesAndUsersIdModel
    {
        public Guid[] CasesIdList { get; set; }
        public Guid[] UsersIdList { get; set; }
    }
}
