﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Innostax.Common.Database.Enums;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICaseFilesService
    {
        List<CaseFileModel> GetAllCaseRelatedFiles(Guid caseId); 
    }

    public class CaseFilesService : ICaseFilesService
    {
        private IInnostaxDb _innostaxDb;
        private readonly ICaseFileAssociationService _caseFileAssociationService;
        private readonly IDiscussionFileAssociationService _discussionFileAssociationService;
        private readonly IDiscussionMessageFileAssociationService _discussionMessageFileAssociationService;
        private readonly ICommonService _commonService;
        public CaseFilesService(IInnostaxDb innostaxDb, 
            ICaseFileAssociationService caseFileAssociationService,
            IDiscussionFileAssociationService discussionFileAssociationService,
            IDiscussionMessageFileAssociationService discussionMessageFileAssociationService, ICommonService commonService)
        {
            _innostaxDb = innostaxDb;
            _caseFileAssociationService = caseFileAssociationService;
            _discussionFileAssociationService = discussionFileAssociationService;
            _discussionMessageFileAssociationService = discussionMessageFileAssociationService;
            _commonService = commonService;
        }

        public List<CaseFileModel> GetAllCaseRelatedFiles(Guid caseId)
        {
            var result = new List<CaseFileModel>();    
            _commonService.ValidateAccess(Validate.CASE,Common.Database.Enums.Permission.CASE_READ_ALL, caseId);
            var allCaseFiles = _caseFileAssociationService.GetCaseAssociatedFiles(caseId);
            if (allCaseFiles != null || allCaseFiles.Count > 0)
            {
                foreach (var eachCaseFile in allCaseFiles)
                {
                    var caseFileModel = new CaseFileModel();
                    caseFileModel = MapValuesToCaseFileModel(eachCaseFile);
                    caseFileModel.FileType = CaseFileType.CASEFILE;
                    result.Add(caseFileModel);
                }
            }
            result.AddRange(GetTaskRelatedFiles(caseId));
            var authorizedDiscussions = _commonService.GetAuthorizedDiscussions(caseId);         
            var allCaseRelatedDiscussions = authorizedDiscussions.Where(x => x.IsDeleted == false).ToList();
            if (allCaseRelatedDiscussions != null || allCaseRelatedDiscussions.Count > 0)
            {
                foreach (var eachDiscussion in allCaseRelatedDiscussions)
                {
                    var allDiscussionFiles = _discussionFileAssociationService.GetDiscussionAssociatedFiles(eachDiscussion.Id);
                    var alldiscussionMessage = _innostaxDb.DiscussionMessages.Where(x => x.DiscussionId == eachDiscussion.Id && x.IsDeleted == false).ToList();
                    foreach (var eachDiscussionFile in allDiscussionFiles)
                    {
                        var caseFileModel = new CaseFileModel();
                        caseFileModel = MapValuesToCaseFileModel(eachDiscussionFile);
                        caseFileModel.DiscussionId = eachDiscussion.Id;
                        caseFileModel.DiscussionName = eachDiscussion.Title;
                        caseFileModel.DiscussionMessageCount = alldiscussionMessage.Count;
                        caseFileModel.FileType = CaseFileType.DISCUSSIONFILE;
                        result.Add(caseFileModel);
                    }
                    if (alldiscussionMessage != null || alldiscussionMessage.Count > 0)
                    {
                        foreach (var eachDiscussionMessage in alldiscussionMessage)
                        {
                            var discussionMessageFiles = _discussionMessageFileAssociationService.GetDiscussionMessageAssociatedFiles(eachDiscussionMessage.Id);
                            foreach (var eachDiscussionMessageFile in discussionMessageFiles)
                            {
                                var caseFileModel = new CaseFileModel();
                                caseFileModel = MapValuesToCaseFileModel(eachDiscussionMessageFile);
                                caseFileModel.DiscussionId = eachDiscussion.Id;
                                caseFileModel.DiscussionName = eachDiscussion.Title;
                                caseFileModel.DiscussionMessageCount = alldiscussionMessage.Count;
                                caseFileModel.FileType = CaseFileType.DISCUSSIONFILE;
                                result.Add(caseFileModel);
                            }
                        }
                    }
                }
            }
            return result.OrderByDescending(x=>x.FileUploadedTime).ToList();
        }

        internal List<CaseFileModel> GetTaskRelatedFiles(Guid caseId)
        {
            var result = new List<CaseFileModel>();
            result.AddRange(GetListOfTaskFiles(caseId));          
            return result;
        }

        internal List<CaseFileModel> GetListOfTaskFiles(Guid caseId)
        {
            var result = new List<CaseFileModel>();
            var authorizedTasks = _commonService.GetAuthorizedTasks(caseId).Where(x=>x.IsDeleted == false);
            var allTaskFilesForACase = _innostaxDb.TaskFileAssociations.Include(y => y.UploadedFile).Include(x => x.Task).Include(x=>x.UploadedFile.User).Where(x => authorizedTasks.Contains(x.Task) && x.Task.IsDeleted != true && x.IsDeleted != true).ToList();
            var listOfTaskId = authorizedTasks.Select(x => x.TaskId).ToList();          
                foreach (var eachTaskFile in allTaskFilesForACase)
                {
                    var taskFile = Mapper.Map<Models.FileModel>(eachTaskFile.UploadedFile);
                    taskFile.UserName = eachTaskFile.UploadedFile.User.FirstName + " " + eachTaskFile.UploadedFile.User.LastName;
                    var caseFileModel = new CaseFileModel();
                    caseFileModel = MapValuesToCaseFileModel(taskFile);
                    caseFileModel.TaskId = eachTaskFile.TaskId;
                    caseFileModel.TaskName = eachTaskFile.Task.Title;              
                    caseFileModel.FileType = CaseFileType.TASKFILE;
                    result.Add(caseFileModel);
                }            
            result.AddRange(GetListOfTaskCommentFile(listOfTaskId));
            return result;
        }

        internal List<CaseFileModel> GetListOfTaskCommentFile(List<Guid> listOfTaskId)
        {
            var result = new List<CaseFileModel>();
            var allTaskCommentFiles = _innostaxDb.TaskCommentFileAssociations.Include(y => y.TaskComment).Include(y=>y.UploadedFile.User).Include(y => y.TaskComment.Task).Include(y => y.UploadedFile).Where(x => listOfTaskId.Contains(x.TaskComment.TaskId) && x.IsDeleted != true).ToList();           
                foreach (var eachTaskCommentFile in allTaskCommentFiles)
                {
                    var taskFile = Mapper.Map<Models.FileModel>(eachTaskCommentFile.UploadedFile);
                    taskFile.UserName = eachTaskCommentFile.UploadedFile.User.FirstName + " " + eachTaskCommentFile.UploadedFile.User.LastName;
                    var caseFileModel = new CaseFileModel();
                    caseFileModel = MapValuesToCaseFileModel(taskFile);
                    caseFileModel.TaskId = eachTaskCommentFile.TaskComment.Task.TaskId;
                    caseFileModel.FileType = CaseFileType.TASKFILE;
                    caseFileModel.TaskName = eachTaskCommentFile.TaskComment.Task.Title;
                    result.Add(caseFileModel);
                }            
            return result;
        }

        internal CaseFileModel MapValuesToCaseFileModel(FileModel value)
        {
            var caseFileModel = new CaseFileModel();
            caseFileModel.FileId = value.FileId;
            caseFileModel.FileName = value.FileName;
            caseFileModel.FileUploadedTime = value.CreateDateTime;
            caseFileModel.FileUploadedBy = value.UserName;
            return caseFileModel;
        }
    }
}
