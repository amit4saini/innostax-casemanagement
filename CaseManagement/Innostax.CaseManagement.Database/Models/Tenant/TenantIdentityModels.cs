﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Innostax.Common.Database.Models
{
    public class TenantUser : IdentityUser<Guid, TenantUserLogin, TenantUserRole, TenantUserClaim>
    {
        public TenantUser()
        {
            CreationDateTime = DateTime.UtcNow;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<TenantUser, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime LastEditedDateTime { get; set; }
        public Guid? ProfilePicKey { get; set; }
        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public TenantUser DeletedByUser { get; set; }


    }

    public class TenantUserIdentityStore : UserStore<TenantUser, TenantRole, Guid,
        TenantUserLogin, TenantUserRole, TenantUserClaim>
    {
        public TenantUserIdentityStore(TenantDb context)
            : base(context)
        {
        }
    }

    public class TenantRoleStore : RoleStore<TenantRole, Guid, TenantUserRole>
    {
        public TenantRoleStore(TenantDb context)
            : base(context)
        {
        }
    }
}
