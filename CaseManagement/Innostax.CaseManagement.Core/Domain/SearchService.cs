﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ISearchService
    {
        Task<PageResult<Case>> SearchByKeyword(ODataQueryOptions<Case> options, HttpRequestMessage request, string keyword);
    }

    public class SearchService : ISearchService
    {
        private IInnostaxDb _innostaxDb;
        private ICaseService _caseService;
        private IDiscussionService _discussionService;
        private ITaskService _taskService;
        private ICommonService _commonService;
        private IUserService _userService;

        public SearchService(IInnostaxDb innostaxDb, ICaseService caseService, ITaskService taskService, IDiscussionService discussionService, ICommonService commonService,IUserService userService)
        {
            _innostaxDb = innostaxDb;
            _caseService = caseService;
            _discussionService = discussionService;
            _taskService = taskService;
            _commonService = commonService;
            _userService = userService;
        }

        public async Task<PageResult<Case>> SearchByKeyword(ODataQueryOptions<Case> options, HttpRequestMessage request, string keyword)
        {
            var matchedCases = SearchCase(keyword, new HttpRequestMessage());
            var matchedTasks = SearchTask(keyword, new HttpRequestMessage());
            var matchedFiles = new List<CaseFileAssociation>();
            if (!(_userService.GetCurrentUser().Role.Name == Roles.Sales_Rep.ToString() || _userService.GetCurrentUser().Role.Name == Roles.Billing.ToString()))
            {
                matchedFiles = SearchCaseFile(keyword);
            }           
            var matchedDiscussions = await SearchDiscussion(keyword, new HttpRequestMessage());
            var casesFromMatchedTasks = matchedTasks.Select(x => x.Case).ToList();
            var casesFromMatchedFiles = matchedFiles.Select(x => x.Case).ToList();
            var casesFromMatchedDiscussions = matchedDiscussions.Select(x => x.Case).ToList();
            matchedTasks.ForEach(x => x.Case = null);
            matchedFiles.ForEach(x => x.Case = null);
            matchedDiscussions.ForEach(x => x.Case = null);
            IEnumerable<Case> allCases = matchedCases.Concat(casesFromMatchedTasks).Concat(casesFromMatchedFiles).Concat(casesFromMatchedDiscussions).GroupBy(x=>x.CaseId).Select(x=>x.First());
            var searchResult = allCases.Select(x => { x.Tasks = matchedTasks.Where(c => c.CaseId == x.CaseId).ToList(); x.Discussions = matchedDiscussions.Where(c => c.CaseId == x.CaseId).ToList(); x.Files = matchedFiles.Where(c => c.CaseId == x.CaseId).Select(c=>c.UploadedFile).ToList(); return x; }).ToList();
            var result = options.ApplyTo(searchResult.AsQueryable());
            return new PageResult<Case>(
       result as IEnumerable<Case>, request.ODataProperties().NextLink,
       searchResult.Count); 
        }

        internal List<Case> SearchCase(string keyword, HttpRequestMessage request)
        {
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Case>("Case");
            ODataQueryOptions<Models.Case> options= new ODataQueryOptions<Models.Case>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Case)), request); 
            return _caseService.SearchCasesByKeyword(options, request,keyword).Items.ToList();    
        }

        internal List<Models.Task> SearchTask(string keyword, HttpRequestMessage request)
        {
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Task>("Task");
            ODataQueryOptions<Models.Task> options = new ODataQueryOptions<Models.Task>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Task)), request);
            return _taskService.SearchTasksByKeyword(options, request, keyword, null).Result.Items.ToList();
        }

        internal async Task<List<Discussion>> SearchDiscussion(string keyword, HttpRequestMessage request)
        {
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Discussion>("Discussion");
            ODataQueryOptions<Models.Discussion> options = new ODataQueryOptions<Models.Discussion>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Discussion)), request);
            var asyncResult = await _discussionService.SearchDiscussionsByKeyword(options, request, null, keyword);
            return asyncResult.Result.Items.ToList();
        }
              
        internal List<CaseFileAssociation> SearchCaseFile(string keyword)
        {
            var cases = _commonService.GetAuthorizedCasesForUser().Where(x => x.IsDeleted != true);
            var caseAssociatedFilesSearchResult = _innostaxDb.CaseFileAssociations.Include(y=>y.Case).Include(y=>y.Case.Client).Include(y=>y.Case.Client.SalesRepresentative).Include(y=>y.Case.ClientContactUser).Include(y=>y.UploadedFile).Where(x => cases.Contains(x.Case) && x.IsDeleted == false && x.Case.IsDeleted == false && ((x.UploadedFile.FileContent ?? "").Contains(keyword) || (x.UploadedFile.FileName ?? "").Contains(keyword))).ToList();
            var listOfCaseFiles = Mapper.Map<List<CaseFileAssociation>>(caseAssociatedFilesSearchResult);
            listOfCaseFiles.ForEach(x => x.UploadedFile.Type = DownloadFileType.CaseFile);
            listOfCaseFiles.AddRange(SearchDiscussionFile(keyword));
            listOfCaseFiles.AddRange(SearchTaskFile(keyword));
            listOfCaseFiles.AddRange(SearchDiscussionMessageFile(keyword));
            listOfCaseFiles.AddRange(SearchTaskCommentFile(keyword));          
            return listOfCaseFiles;
        }

        internal List<CaseFileAssociation> SearchDiscussionFile(string keyword)
        {
            var discussions = _commonService.GetAuthorizedDiscussions(null).Where(x => x.IsDeleted != true);
            var discussionAssociatedFilesSearchResult = _innostaxDb.DiscussionFileAssociations.Include(y=>y.Discussion).Include(y => y.Discussion.Case).Include(y => y.Discussion.Case.Client).Include(y => y.Discussion.Case.Client.SalesRepresentative).Include(y => y.Discussion.Case.ClientContactUser).Include(y => y.UploadedFile).Where(x => discussions.Contains(x.Discussion) && x.IsDeleted == false && x.Discussion.Case.IsDeleted == false && ((x.UploadedFile.FileContent ?? "").Contains(keyword) || (x.UploadedFile.FileName ?? "").Contains(keyword))).ToList();
            var listOfCaseAssociatedDiscussionFiles = new List<CaseFileAssociation>();
            foreach (var discussionFile in discussionAssociatedFilesSearchResult)
            {
                var caseFileAssociation = new CaseFileAssociation();
                caseFileAssociation.Case = Mapper.Map<Case>(discussionFile.Discussion.Case);
                caseFileAssociation.CaseId = discussionFile.Discussion.CaseId;
                caseFileAssociation.UploadedFile = Mapper.Map<FileModel>(discussionFile.UploadedFile);
                listOfCaseAssociatedDiscussionFiles.Add(caseFileAssociation);
            }
            listOfCaseAssociatedDiscussionFiles.ForEach(x => x.UploadedFile.Type = DownloadFileType.DiscussionFile);
            return listOfCaseAssociatedDiscussionFiles;
        }

        internal List<CaseFileAssociation> SearchTaskFile(string keyword)
        {
            var tasks = _commonService.GetAuthorizedTasks(null).Where(x => x.IsDeleted != true);
            var taskAssociatedFilesSearchResult = _innostaxDb.TaskFileAssociations.Include(y => y.Task.Case).Include(y => y.Task.Case.Client).Include(y => y.Task.Case.Client.SalesRepresentative).Include(y => y.Task.Case.ClientContactUser).Include(y => y.UploadedFile).Where(x => tasks.Contains(x.Task)  && x.IsDeleted == false && x.Task.Case.IsDeleted == false && ((x.UploadedFile.FileContent ?? "").Contains(keyword) || (x.UploadedFile.FileName ?? "").Contains(keyword))).ToList();
            var listOfCaseAssociatedTaskFiles = new List<CaseFileAssociation>();
            foreach (var taskFile in taskAssociatedFilesSearchResult)
            {
                var caseFileAssociation = new CaseFileAssociation();
                caseFileAssociation.Case = Mapper.Map<Case>(taskFile.Task.Case);
                caseFileAssociation.CaseId = taskFile.Task.CaseId;
                caseFileAssociation.UploadedFile = Mapper.Map<FileModel>(taskFile.UploadedFile);
                listOfCaseAssociatedTaskFiles.Add(caseFileAssociation);
            }
            listOfCaseAssociatedTaskFiles.ForEach(x => x.UploadedFile.Type = DownloadFileType.TaskFile);
            return listOfCaseAssociatedTaskFiles;
        }

        internal List<CaseFileAssociation> SearchTaskCommentFile(string keyword)
        {
            var tasks = _commonService.GetAuthorizedTasks(null).Where(x => x.IsDeleted != true);
            var taskCommentAssociatedFilesSearchResult =_innostaxDb.TaskCommentFileAssociations.Include(y => y.TaskComment.Task.Case).Include(y => y.TaskComment.Task.Case.Client).Include(y => y.TaskComment.Task.Case.Client.SalesRepresentative).Include(y => y.TaskComment.Task.Case.ClientContactUser).Include(y => y.UploadedFile).Where(x => tasks.Contains(x.TaskComment.Task) && x.IsDeleted == false && x.TaskComment.Task.Case.IsDeleted == false && ((x.UploadedFile.FileContent ?? "").Contains(keyword) || (x.UploadedFile.FileName ?? "").Contains(keyword))).ToList();
            var listOfCaseAssociatedTaskCommentFiles = new List<CaseFileAssociation>();
            foreach (var taskCommentFile in taskCommentAssociatedFilesSearchResult)
            {
                var caseFileAssociation = new CaseFileAssociation();
                caseFileAssociation.Case = Mapper.Map<Case>(taskCommentFile.TaskComment.Task.Case);
                caseFileAssociation.CaseId = taskCommentFile.TaskComment.Task.CaseId;
                caseFileAssociation.UploadedFile = Mapper.Map<FileModel>(taskCommentFile.UploadedFile);
                listOfCaseAssociatedTaskCommentFiles.Add(caseFileAssociation);
            }
            listOfCaseAssociatedTaskCommentFiles.ForEach(x => x.UploadedFile.Type = DownloadFileType.TaskFile);
            return listOfCaseAssociatedTaskCommentFiles;
        }

        internal List<CaseFileAssociation> SearchDiscussionMessageFile(string keyword)
        {
            var discussions = _commonService.GetAuthorizedDiscussions(null).Where(x => x.IsDeleted != true);
            var caseAssociatedFilesSearchResult = _innostaxDb.DiscussionMessageFileAssociations.Include(y => y.DiscussionMessage.Discussion.Case).Include(y => y.DiscussionMessage.Discussion.Case.Client).Include(y => y.DiscussionMessage.Discussion.Case.Client.SalesRepresentative).Include(y => y.DiscussionMessage.Discussion.Case.ClientContactUser).Include(y => y.UploadedFile).Where(x => discussions.Contains(x.DiscussionMessage.Discussion) && x.IsDeleted == false && x.DiscussionMessage.Discussion.Case.IsDeleted == false && ((x.UploadedFile.FileContent ?? "").Contains(keyword) || (x.UploadedFile.FileName ?? "").Contains(keyword))).ToList();
            var listOfCaseAssociatedDiscussionMessageFiles = new List<CaseFileAssociation>();
            foreach (var discussionMessageFile in caseAssociatedFilesSearchResult)
            {
                var caseFileAssociation = new CaseFileAssociation();
                caseFileAssociation.Case = Mapper.Map<Case>(discussionMessageFile.DiscussionMessage.Discussion.Case);
                caseFileAssociation.CaseId = discussionMessageFile.DiscussionMessage.Discussion.CaseId;
                caseFileAssociation.UploadedFile = Mapper.Map<FileModel>(discussionMessageFile.UploadedFile);
                listOfCaseAssociatedDiscussionMessageFiles.Add(caseFileAssociation);
            }
            listOfCaseAssociatedDiscussionMessageFiles.ForEach(x => x.UploadedFile.Type = DownloadFileType.DiscussionFile);
            return listOfCaseAssociatedDiscussionMessageFiles;
        }
    }
}
