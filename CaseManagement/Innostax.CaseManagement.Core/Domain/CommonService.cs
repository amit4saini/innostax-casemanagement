﻿using Innostax.Common.Database.Database;
using System;
using System.Linq;
using Innostax.Common.Database.Enums;
using System.Web.Configuration;
using Innostax.CaseManagement.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using System.Data.Entity;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ICommonService
    {
        string GetCaseRelatedUrl(NotificationUrl type, string caseNumber, Guid? id);
        DateTime GetUtcDateTime();
        string GetDefaultCaseNumber();
        string GetCaseCountryName(Case caseDetails);
        void ValidateAccess(Common.Database.Enums.Validate validate, Common.Database.Enums.Permission permission, Guid caseId);
        IQueryable<Common.Database.Models.Case> GetAuthorizedCasesForUser();
        IQueryable<Innostax.Common.Database.Models.Task> GetAuthorizedTasks(Guid? caseId);
        IQueryable<Innostax.Common.Database.Models.Discussion> GetAuthorizedDiscussions(Guid? caseId);
    } 

    public class CommonService : ICommonService
    {
        private readonly IUserService _userService;
        private readonly IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;

        public CommonService(IUserService userService, IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService)
        {
            _userService = userService;
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
        }

        internal bool ValidateCaseAccess(Guid caseId)
        {
            var currentUserId = _userService.UserId;
            bool accessGranted = false;
            var requestedCase = _innostaxDb.Cases.Include(x => x.Client).Include(x => x.Client.SalesRepresentative).FirstOrDefault(x => x.CaseId == caseId);
            if(requestedCase != null)
            {
                accessGranted = requestedCase.CreatedBy == currentUserId || requestedCase.AssignedTo == currentUserId || requestedCase.Client.SalesRepresentativeId == currentUserId;
            }
            if(!accessGranted)
            {
                var tasksAssociatedUserIdList = _innostaxDb.Tasks.Where(x => x.CaseId == caseId && x.IsDeleted != true ).Select(x=>x.AssignedTo).ToList();
                if (tasksAssociatedUserIdList.Contains(currentUserId))
                    accessGranted = true;
            }
            if (!accessGranted)
            {
                var existingParticipantCaseAssociation = _innostaxDb.ParticipantCaseAssociation.Where(x => x.CaseId == caseId && x.UserId == _userService.UserId && x.IsActive == true ).ToList();
                if (existingParticipantCaseAssociation.Count != 0)
                    accessGranted = true;
            }
            if (!accessGranted)
            {
                var existingNFCUserCaseAssociation = _innostaxDb.NFCUserCaseAssociations.Where(x => x.CaseId == caseId && x.NFCUserId == _userService.UserId && x.IsActive == true ).ToList();
                if (existingNFCUserCaseAssociation.Count != 0)
                    accessGranted = true;
            }
            if (!accessGranted)
            {
                var existingParticipantCaseAssociation = _innostaxDb.ICIUserCaseAssociations.Where(x => x.CaseId == caseId && x.ICIUserId == _userService.UserId && x.IsActive == true ).ToList();
                if (existingParticipantCaseAssociation.Count != 0)
                    accessGranted = true;
            }
            if (!accessGranted)
            {
                var isIndiscussionNotifierList = _innostaxDb.DiscussionNotifiers.Where(x => x.Discussion.CaseId == caseId && x.UserId == currentUserId ).Count();
                if(isIndiscussionNotifierList != 0)
                accessGranted = true;
            }        
            return accessGranted;
        }

        public void ValidateAccess(Common.Database.Enums.Validate validate, Common.Database.Enums.Permission permission, Guid id)
        {
            var userNotAuthorizedError = Innostax.Common.Constants.ErrorMessages.CaseManagement.CaseCreation.USER_NOT_AUTHORIZE;
            if (!_userService.UserPermissions.Contains(permission))
            {
                switch (validate)
                {
                    case Validate.CASE:
                        if (!ValidateCaseAccess(id))
                        {
                            _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizedError);
                        }
                        break;
                    case Validate.DISCUSSION:
                        if (!ValidateDiscussionAccess(id))
                        {
                            _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizedError);
                        }
                        break;
                    case Validate.TASK:
                        if (!ValidateTaskAccess(id))
                        {
                            _errorHandlerService.ThrowForbiddenRequestError(userNotAuthorizedError);
                        }
                        break;
                }
            }          
        }

        internal bool ValidateTaskAccess(Guid taskId)
        {
            var currentUser = _userService.UserId;
            bool accessGranted = false;
            var requestedTask = _innostaxDb.Tasks.FirstOrDefault(x => x.TaskId == taskId && x.IsDeleted == false && (x.AssignedTo == currentUser || x.CreatedBy == currentUser ));
            if (requestedTask != null)
            {
                accessGranted = true;
            }
            return accessGranted;
        }

        public bool ValidateDiscussionAccess(Guid discussionId)
        {
            var currentUser = _userService.UserId;
            bool accessGranted = false;
            var requestedDiscussion = _innostaxDb.DiscussionNotifiers.FirstOrDefault(x => x.DiscussionId == discussionId && x.UserId == currentUser && x.Discussion.IsDeleted == false );
            if(requestedDiscussion != null)
            {
                accessGranted = true;
            }
            if (!accessGranted)
            {
                var discussion = _innostaxDb.Discussions.FirstOrDefault(x => x.Id == discussionId && x.CreatedBy == currentUser && x.IsDeleted == false);
                if (discussion != null) { accessGranted=true;}          
            }
            if (!accessGranted)
            {
                var discusssionTaggedUser = _innostaxDb.DiscussionTaggedUsers.FirstOrDefault(x => x.DiscussionId == discussionId && x.UserId == currentUser && x.Discussion.IsDeleted == false);
                if (discusssionTaggedUser != null) { accessGranted = true; }
            }
            if (!accessGranted)
            {
                var discusssionMessageTaggedUser = _innostaxDb.DiscussionMessageTaggedUsers.FirstOrDefault(x => x.DiscussionMessage.DiscussionId == discussionId && x.UserId == currentUser && x.DiscussionMessage.Discussion.IsDeleted == false);
                if (discusssionMessageTaggedUser != null) { accessGranted = true; }
            }
            return accessGranted;
        }


        public string GetCaseRelatedUrl(NotificationUrl type, string caseNumber, Guid? id)        
        {
            string url=string.Empty;
            string urlPrefix = string.Concat("/case-management/cases/",caseNumber);
            switch (type)
            {
                case NotificationUrl.CASE: url = string.Concat(urlPrefix,"/caseDetails"); break;
                case NotificationUrl.DISCUSSION: url = String.Concat(urlPrefix, "/discussions/", id.ToString()); break;
                case NotificationUrl.DISCUSSIONMESSAGE: url = String.Concat(urlPrefix, "/discussions"); break;
                case NotificationUrl.TASK: url = String.Concat(urlPrefix, "/task/", id.ToString()); break;
            }
            return url;
        } 

        public DateTime GetUtcDateTime()
        {
            var existingDateTime = DateTime.UtcNow;
            return existingDateTime;
        }

        public string GetDefaultCaseNumber()
        {
            return WebConfigurationManager.AppSettings["DefaultCaseNumber"] == null ? "12000" : WebConfigurationManager.AppSettings["DefaultCaseNumber"];
        }

        public string GetCaseCountryName(Case caseDetails)
        {
            var existingCasePrimaryReport = _innostaxDb.CaseReportAssociations.FirstOrDefault(x => x.CaseId == caseDetails.CaseId && x.IsPrimaryReport == true);
            string caseCountryName = string.Empty;
            if(existingCasePrimaryReport != null)
            {
               var caseCountryDetails = _innostaxDb.Countries.FirstOrDefault(x => x.CountryId == existingCasePrimaryReport.CountryId);
                if(caseCountryDetails != null)
                caseCountryName = caseCountryDetails.CountryName;
            }
            return caseCountryName;
        }

        public IQueryable<Common.Database.Models.Case> GetAuthorizedCasesForUser()
        {
            IQueryable<Common.Database.Models.Case> allCases = null;
            var currentUser = _userService.GetCurrentUser();
            if (currentUser.Permissions.Contains(Permission.CASE_READ_ALL.ToString()))
            {
                allCases = _innostaxDb.Cases.Include(x => x.ClientContactUser).Include(x => x.AssignedToUser).Include(x => x.BusinessUnit).Include(x => x.CreatedByUser).Include(x => x.CaseStatus).Include(x => x.Client).Include(x => x.Client.SalesRepresentative).Where(x => x.Client.IsDeleted == false);
            }
            else if (currentUser.Role.Name == Roles.Sales_Rep.ToString())
            {
                allCases = _innostaxDb.Cases.Include(y => y.AssignedToUser).Include(x => x.CreatedByUser).Include(x => x.BusinessUnit).Include(x => x.CaseStatus).Include(x => x.Client).Include(x => x.Client.SalesRepresentative).Include(x => x.ClientContactUser).Where(x => (x.AssignedTo == currentUser.Id || (x.Client.SalesRepresentativeId == _userService.UserId && x.Client.IsDeleted == false)));
            }
            else if (currentUser.Role.Name == Roles.Client_Participant.ToString())
            {
                allCases = _innostaxDb.ParticipantCaseAssociation.Include(y => y.Case).Include(y => y.Case.ClientContactUser).Include(y => y.Case.Client).Include(y => y.Case.Client.SalesRepresentative).Include(y => y.Case.CaseStatus).Include(y => y.Case.AssignedToUser).Include(y => y.Case.ClientContactUser).Where(x => x.UserId == currentUser.Id && x.IsActive == true).Select(y => y.Case);
            }
            else if (currentUser.Role.Name == Roles.In_Country_Investigator.ToString())
            {
                allCases = _innostaxDb.ICIUserCaseAssociations.Include(y => y.Case).Include(y => y.Case.ClientContactUser).Include(y => y.Case.Client).Include(y => y.Case.Client.SalesRepresentative).Include(y => y.Case.CaseStatus).Include(y => y.Case.AssignedToUser).Include(y => y.Case.ClientContactUser).Where(x => x.ICIUserId == currentUser.Id && x.IsActive == true).Select(y => y.Case);
            }
            else if (currentUser.Role.Name == Roles.NFC.ToString())
            {
                allCases = _innostaxDb.NFCUserCaseAssociations.Include(y => y.Case).Include(y => y.Case.ClientContactUser).Include(y => y.Case.Client).Include(y => y.Case.Client.SalesRepresentative).Include(y => y.Case.CaseStatus).Include(y => y.Case.AssignedToUser).Include(y => y.Case.ClientContactUser).Where(x => x.NFCUserId == currentUser.Id && x.IsActive == true).Select(y => y.Case);
            }
            else if (currentUser.Role.Name == Roles.Billing.ToString())
            {
                allCases = _innostaxDb.Cases.Include(y => y.ClientContactUser).Include(y => y.Client).Include(y => y.Client.SalesRepresentative).Include(y => y.CaseStatus).Include(y => y.AssignedToUser).Include(y => y.ClientContactUser).Where(x => x.CaseStatus.Status == "READY_FOR_INVOICE" && x.IsDeleted != true);
            }
            if (currentUser.Role.Name != Roles.Billing.ToString())
            { 
            var taskAssociatedCaseIdLists = _innostaxDb.Tasks.Where(x => x.AssignedTo == currentUser.Id && x.IsDeleted == false).Select(x => x.CaseId);
            var casesAssociatedWithTask = _innostaxDb.Cases.Where(x => (x.CreatedBy == currentUser.Id || x.AssignedTo == currentUser.Id || taskAssociatedCaseIdLists.Contains(x.CaseId)) && (x.Client.IsDeleted == false)).Include(x => x.ClientContactUser).Include(x => x.AssignedToUser).Include(x => x.CreatedByUser).Include(x => x.SubStatus).Include(x => x.Client).Include(x => x.Client.SalesRepresentative);
               if (allCases != null)
               {
                  allCases = allCases.Union(casesAssociatedWithTask).Distinct();
               }
               else { allCases = casesAssociatedWithTask; }
           }
            return allCases;
        }

        public IQueryable<Innostax.Common.Database.Models.Discussion> GetAuthorizedDiscussions(Guid? caseId)
        {
            IQueryable<Innostax.Common.Database.Models.Discussion> authorizedDiscussions ;
            var currentUser = _userService.GetCurrentUser();
            if (caseId != null)
            {
                var cases = GetAuthorizedCasesForUser().Where(x => x.IsDeleted != true);
                authorizedDiscussions = (_userService.UserPermissions.Contains(Permission.CAN_VIEW_ALL_DISCUSSIONS)) ? _innostaxDb.Discussions.Where(x => x.CaseId == caseId) : (_innostaxDb.DiscussionNotifiers.Include(y => y.Discussion).Where(x => x.UserId == _userService.UserId).Select(x => x.Discussion).Where(x => x.CaseId == caseId).Union(_innostaxDb.Discussions.Where(x => x.CaseId == caseId && x.CreatedBy == _userService.UserId)).Union(_innostaxDb.DiscussionTaggedUsers.Include(y=>y.Discussion).Where(x=>x.Discussion.CaseId==caseId && x.UserId==_userService.UserId).Select(x=>x.Discussion)).Union(_innostaxDb.DiscussionMessageTaggedUsers.Include(y => y.DiscussionMessage.Discussion).Where(x => x.DiscussionMessage.Discussion.CaseId == caseId && x.UserId == _userService.UserId).Select(x => x.DiscussionMessage.Discussion))).Distinct();
            }
            else
            {
                authorizedDiscussions = (_userService.UserPermissions.Contains(Permission.CAN_VIEW_ALL_DISCUSSIONS)) ? _innostaxDb.Discussions : (_innostaxDb.DiscussionNotifiers.Include(y => y.Discussion).Where(x => x.UserId == _userService.UserId).Select(x => x.Discussion).Union(_innostaxDb.Discussions.Where(x=> x.CreatedBy == _userService.UserId)).Union(_innostaxDb.DiscussionTaggedUsers.Include(y => y.Discussion).Where(x => x.UserId == _userService.UserId).Select(x => x.Discussion)).Union(_innostaxDb.DiscussionMessageTaggedUsers.Include(y => y.DiscussionMessage.Discussion).Where(x => x.UserId == _userService.UserId).Select(x => x.DiscussionMessage.Discussion))).Distinct();
            }
            return authorizedDiscussions;
        }

        public IQueryable<Innostax.Common.Database.Models.Task> GetAuthorizedTasks(Guid? caseId)
        {
            IQueryable<Innostax.Common.Database.Models.Task> authorizedTasks;
            var currentUser = _userService.GetCurrentUser();
            if (caseId != null)
            {
                authorizedTasks = (_userService.UserPermissions.Contains(Permission.CAN_VIEW_ALL_TASKS)) ? _innostaxDb.Tasks.Where(x => x.CaseId == caseId) : _innostaxDb.Tasks.Where(x => x.CaseId == caseId && (x.AssignedTo == _userService.UserId || x.CreatedBy == _userService.UserId));
            }
            else
            {
                authorizedTasks = (_userService.UserPermissions.Contains(Permission.CAN_VIEW_ALL_TASKS)) ? _innostaxDb.Tasks : _innostaxDb.Tasks.Where(x => x.AssignedTo == _userService.UserId || x.CreatedBy == _userService.UserId);
            }
            return authorizedTasks;
        }
    }
}
