﻿'use strict';
describe('DiscussionController', function () {
    var CaseHeader;
    var Discussions;
    var toastr;
    var $uibModal;
    var $httpBackend;
    var $window;
    var ErrorHandlerService;
    var document;
    var usSpinnerService;
    var Enums;
    var Upload;
    var $location;
    var $anchorScroll;
    var $stateParams;
    var Authentication;
    var $global;
    var cbSuccess;
    var cbError;
    beforeEach(function () {
        module('case');
        module('case', function ($provide) {
            CaseHeader = {
                getCaseFromCaseNumber: function () { return true; },
                case: {
                    caseId: "1000",
                    caseNumber: "2000"
                }
            };
            $provide.value('CaseHeader', CaseHeader);
            $stateParams = {
                caseNumber: 1,
                discussionId: 1
            };
            toastr = {
                success: function (message) { },
                error: function (message) { }
            };
            spyOn(toastr, 'success');
            spyOn(toastr, 'error');
            $provide.value('toastr', toastr);
            $uibModal = {
                open: function () { return; }
            };
            $provide.value('$uibModal', $uibModal);
            $provide.value('document', document);
            $provide.value('usSpinnerService', usSpinnerService);
            $provide.value('$anchorScroll', $anchorScroll);
            Authentication = {
                user: {
                    Role:{
                        Name:"PArticipant"
                    }
                }
            }
            $provide.value('Authentication', Authentication);
            $provide.value('Upload', Upload);
            Enums =    {       
                SubjectType : {
                    'Company'      : 0,
                    'Individual'   : 1,
                    'Other'        : 2,          
                },
                DownloadFileType: {
                    'CaseFile': 0,
                    'ProfilePic': 1,
                    'DiscussionFile':2,
                },
                TaskStatus: {
                    'NotStarted': 0,
                    'InProgress': 1,
                    'PastDue': 2,
                    'Completed':3,
                },
                DiscussionAction: {
                    'Added': 0,
                    'Edited': 1,
                    'Marked as note': 2,
                    'Unmarked as note': 3,
                    'Archived': 4
                }
            };
            $anchorScroll = { function() { return; } };
            $provide.value('Enums', Enums);
            $window = {
                document: document,
                defaultPageSize: 5,
                location:{href:""}
            };
            $location = {
                hash: function () { return; },
                path: function (path) { return path; }
            };
            spyOn($location, 'path');
            $provide.value('$location', $location);
            $provide.value('$window', $window);
            ErrorHandlerService = {
                DisplayErrorMessage: function (response, status) {
                    return response;
                }
            };
            spyOn(ErrorHandlerService, 'DisplayErrorMessage');
            $provide.value('ErrorHandlerService', ErrorHandlerService);
            $provide.value('$stateParams', $stateParams);
            $global = {
                getExistingCaseUsers: function () { return;}
            }
            $provide.value('$global', $global);
            
        });
        inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            Discussions = $injector.get('CaseDiscussions', { CaseHeader: CaseHeader, toastr: toastr, $uibModal: $uibModal, Upload: Upload, usSpinnerService: usSpinnerService, ErrorHandlerService: ErrorHandlerService, Enums: Enums, $window: $window, $location: $location, $anchorScroll: $anchorScroll, $stateParams: $stateParams, Authentication: Authentication, $global: $global });
        });
        Discussions.modalInstance = {
            dismiss: function () { return; }
        };
        Discussions.allPages = {
            length: 0
        };
        Discussions.allDiscussionPages = {
            length: 0
        };
        Discussions.discussionAction = {
            'Added': 0,
            'Edited': 1,
            'Marked as note': 2,
            'Unmarked as note': 3,
            'Archived': 4
        }
    });
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    
    describe("getSelectedDiscussion", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                Discussions.selectedDiscussion = response;
            }  ;
            cbError = function (response, status) {
                ErrorHandlerService.DisplayErrorMessage(response, status);
                Discussions.selectedDiscussion = {};
            };
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/discussion/1").respond(200, { result: { items: { id: 1 } } });
            Discussions.getSelectedDiscussion(1,cbSuccess,cbError);
            $httpBackend.flush();
            expect(Discussions.allDiscussions).not.toBeNull();
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/discussion/1").respond(500, 'Error Message');
            Discussions.getSelectedDiscussion(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Error Message', 500);
        });
    });
    
    describe("getExistingDiscussionMessages", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { Discussions.allDiscussionMessages = response.items; }
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Discussions.allDiscussionMessages = []; };
        });
        it("Positive Test", function () {
            $httpBackend.expectGET("api/discussion/1/discussionMessages?$skip=0&$orderby=CreationDateTime").respond(200, { result: { items: { id: 1 } } });
            Discussions.getExistingDiscussionMessages(1,"?$skip=0&$orderby=CreationDateTime",cbSuccess,cbError);
            $httpBackend.flush();
            expect(Discussions.allDiscussionMessages).not.toBeNull();
        });
        it("Negative Test", function () {
            $httpBackend.expectGET("api/discussion/1/discussionMessages?$skip=0&$orderby=CreationDateTime").respond(500, 'Error Message');
            Discussions.getExistingDiscussionMessages(1,"?$skip=0&$orderby=CreationDateTime",cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Error Message', 500);
        });
    });
   
       
    describe("postDiscussionMessage", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                toastr.success("Message added");
            }
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            Discussions.message = { files: [] , createdBy: 1};
        });
        it("Positive Test", function () {
            $httpBackend.expectPOST("/api/DiscussionMessages").respond(200, { result: { items: { id: 1 } } });
            Discussions.postDiscussionMessage(Discussions.message, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Message added");
        });
        it("Negative Test", function () {
            $httpBackend.expectPOST("/api/DiscussionMessages").respond(500, 'Error Message');
            Discussions.postDiscussionMessage(Discussions.message, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Error Message', 500);
        });
    });
    describe("updateDiscussionMessage", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) {
                toastr.success("Message edited");
            }
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
            Discussions.message = {id:1, files: [], createdBy: 1 };
        });
        it("Positive Test", function () {
            $httpBackend.expectPUT("/api/DiscussionMessages/1").respond(200, { result: { items: { id: 1 } } });
            Discussions.updateDiscussionMessage(Discussions.message, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Message edited");
        });
        it("Negative Test", function () {
            $httpBackend.expectPUT("/api/DiscussionMessages/1").respond(500, 'Error Message');
            Discussions.updateDiscussionMessage(Discussions.message, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Error Message', 500);
        });
    });
    
    describe("deleteExistingDiscussionMessage", function () {
        beforeEach(function () {
            cbSuccess = function (response, status) { toastr.success("Message archived");}
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); };
        });
        it("Positive Test", function () {
            $httpBackend.expectDELETE("/api/DiscussionMessages/1").respond(200, 'Success');
            Discussions.deleteExistingDiscussionMessage(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(toastr.success).toHaveBeenCalledWith("Message archived");
        });
        it("Negative Test", function () {
            $httpBackend.expectDELETE("/api/DiscussionMessages/1").respond(500, 'Error Message');
            Discussions.deleteExistingDiscussionMessage(1, cbSuccess, cbError);
            $httpBackend.flush();
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Error Message', 500);
        });
    });
    
    describe("getExistingDiscussionsByKeyword", function () {
        beforeEach(function () {
            Discussions.keyword = "Dummy";
            Discussions.options = '&$top=1&$skip=0&$orderby=LastEditedDateTime desc';
            cbSuccess = function (response, status) {
                Discussions.allDiscussions = response.result.items;
            };
            cbError = function (response, status) { ErrorHandlerService.DisplayErrorMessage(response, status); Discussions.allDiscussions = []; };
        });

        it('Positive Test', function () {
            $httpBackend.expectGET('api/search/discussions?keyword=' + Discussions.keyword +'&caseId=1&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(200, { result :{ items: { caseName: "dummyText", client: { salesRepresentativeDetails: { firstName: "a" } }, reports: [], files: [] } }});
            Discussions.getExistingDiscussionsByKeyword(Discussions.keyword, 1, Discussions.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Discussions.allDiscussions).not.toBeNull();
        });
        it('Negative Test', function () {
            $httpBackend.expectGET('api/search/discussions?keyword=' + Discussions.keyword + '&caseId=1&$top=1&$skip=0&$orderby=LastEditedDateTime desc').respond(500, 'Some problem has occured');
            Discussions.getExistingDiscussionsByKeyword(Discussions.keyword,1, Discussions.options, cbSuccess, cbError);
            $httpBackend.flush();
            expect(Discussions.allDiscussions).toEqual([]);
            expect(ErrorHandlerService.DisplayErrorMessage).toHaveBeenCalledWith('Some problem has occured', 500);
        });
    });
});
