﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Innostax.Common.Database.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Innostax.CaseManagement.Models;

namespace Innostax.CaseManagement.Web.ViewModels.UserManagement
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            Permissions = new List<string>();   
        }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid UserId { get; set; }
        public string ProfilePicUrl { get; set; }
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        public List<string> Permissions { get; set; }
        public Role Role { get; set; }
    }
}