﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class ReportController : ApiController
    {
        private readonly IReportService _reportService;
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        // GET api/<controller>
        [System.Web.Http.Route("api/Reports")]
        public List<Report> Get()
        {
            return _reportService.GetReports();
        }

        public Report Get(Guid id)
        {
            return _reportService.GetReportById(id);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/Case/{caseId}/Reports")]
        public List<Report> GetReportsByCaseId(Guid caseId)
        {
            return _reportService.GetCaseAssociatedReports(caseId);
        }
    }
}