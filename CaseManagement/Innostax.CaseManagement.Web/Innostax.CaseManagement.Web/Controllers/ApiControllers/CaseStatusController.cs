﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.CaseManagement.Web.Hubs;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class CaseStatusController : ApiController
    {
        private ICaseStatusService _caseStatusService;
        private INotificationHub _notificationHub;
        private INotificationService _notificationService;
        private ICaseService _caseService;
        public CaseStatusController(ICaseStatusService caseStatusService, INotificationService notificationService, INotificationHub notificationHub, ICaseService caseService)
        {
            _caseStatusService = caseStatusService;
            _notificationService = notificationService;
            _notificationHub = notificationHub;
            _caseService = caseService;
        }

        [HasPermission(new Permission[] { Permission.CASE_UPDATE_ALL, Permission.CASE_UPDATE_OWN })]
        [System.Web.Mvc.HttpPost]
        public void Post(Guid caseId,Guid statusId)
        {
            var listOfUserAssociatedWithCase = _caseStatusService.ChangeCaseStatus(caseId, statusId);
            foreach (var userId in listOfUserAssociatedWithCase)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [System.Web.Mvc.HttpGet]
        public List<CaseStatus> Get()
        {
           return _caseStatusService.GetCaseStatusList();
        }
      
    }
}
