﻿using AutoMapper;
using Innostax.CaseManagement.Models;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface IContactUserBusinessUnitService
    {
        void Save(List<ContactUserBusinessUnit> contactUserBusinessUnitData, Guid clientContactUserId);
        void Delete(Guid clientContactUserId);
        List<ContactUserBusinessUnit> GetContactUserBusinessUnitDetails(Guid clientContactUserId);
    }

    public class ContactUserBusinessUnitService:IContactUserBusinessUnitService
    {
        private IInnostaxDb _innostaxDb;
        private IErrorHandlerService _errorHandlerService;
        private ICommonService _commonService;

        public ContactUserBusinessUnitService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, ICommonService commonService)
        {  
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _commonService = commonService;
        }

        public void Save(List<ContactUserBusinessUnit> contactUserBusinessUnitData, Guid clientContactUserId)
        {
            List<string> businessUnitsToBeRemoved = new List<string>();
            var existingContactUserBusinessUnits = _innostaxDb.ContactUserBusinessUnits.Where(x => x.ClientContactUserId == clientContactUserId).ToList();
            foreach (var contactUserBusinessUnit in existingContactUserBusinessUnits)
            {
                if (contactUserBusinessUnitData.FirstOrDefault(x => x.Id == contactUserBusinessUnit.Id) == null)
                {
                    var existingCase = _innostaxDb.Cases.FirstOrDefault(x => x.BusinessUnitId == contactUserBusinessUnit.Id && x.IsDeleted != true);
                    if (existingCase != null)
                    {
                        businessUnitsToBeRemoved.Add(string.Concat(contactUserBusinessUnit.BusinessUnit, " associated with case number: ", existingCase.CaseNumber));
                    }
                    else
                    {
                        _innostaxDb.ContactUserBusinessUnits.Remove(contactUserBusinessUnit);
                        _innostaxDb.SaveChangesWithErrors();
                    }
                }
            }
            if (contactUserBusinessUnitData != null)
            {
                var mappedContactUserBusinessUnit = Mapper.Map<List<Innostax.Common.Database.Models.ContactUserBusinessUnit>>(contactUserBusinessUnitData);
                foreach (var eachBusinessUnit in mappedContactUserBusinessUnit)
                {
                    eachBusinessUnit.ClientContactUser = null;
                    if (_innostaxDb.ContactUserBusinessUnits.FirstOrDefault(x => x.ClientContactUserId == eachBusinessUnit.ClientContactUserId && x.BusinessUnit == eachBusinessUnit.BusinessUnit && x.AccountCode == eachBusinessUnit.AccountCode) == null)
                    {
                        eachBusinessUnit.CreationDateTime = _commonService.GetUtcDateTime();
                        _innostaxDb.ContactUserBusinessUnits.AddOrUpdate(eachBusinessUnit);
                        _innostaxDb.SaveChangesWithErrors();
                    }
                }
            }
        }


        public void Delete(Guid clientContactUserId)
        {
            var existingContactUserBusinessUnits = _innostaxDb.ContactUserBusinessUnits.Where(x => x.ClientContactUserId == clientContactUserId).ToList();
            foreach (var contactUserBusinessUnit in existingContactUserBusinessUnits)
            {
                _innostaxDb.ContactUserBusinessUnits.Remove(contactUserBusinessUnit);
                _innostaxDb.SaveChangesWithErrors();
            }
        }

        public List<ContactUserBusinessUnit> GetContactUserBusinessUnitDetails(Guid clientContactUserId)
        {
            var existingContactUserBusinessUnits = _innostaxDb.ContactUserBusinessUnits.Where(x => x.ClientContactUserId == clientContactUserId).ToList();
            var mappedContactUserBusinessUnits = Mapper.Map<List<ContactUserBusinessUnit>>(existingContactUserBusinessUnits);
            return mappedContactUserBusinessUnits.OrderBy(x=>x.CreationDateTime).ToList();
        }
    }
}
