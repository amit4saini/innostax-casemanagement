namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingNFCUserToDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NFCUserCaseAssociations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Guid(nullable: false),
                        NFCUserId = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId)
                .ForeignKey("dbo.Users", t => t.NFCUserId)
                .Index(t => t.CaseId)
                .Index(t => t.NFCUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NFCUserCaseAssociations", "NFCUserId", "dbo.Users");
            DropForeignKey("dbo.NFCUserCaseAssociations", "CaseId", "dbo.Cases");
            DropIndex("dbo.NFCUserCaseAssociations", new[] { "NFCUserId" });
            DropIndex("dbo.NFCUserCaseAssociations", new[] { "CaseId" });
            DropTable("dbo.NFCUserCaseAssociations");
        }
    }
}
