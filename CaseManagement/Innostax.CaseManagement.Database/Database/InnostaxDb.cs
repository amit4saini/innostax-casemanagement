using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Text;
using Innostax.Common.Database.Models;
using Innostax.Common.Database.Models.RolesAndPermissionManagement;
using Innostax.Common.Database.Models.UserManagement;
using Innostax.Common.Database.Models.Master;

namespace Innostax.Common.Database.Database
{
    public class InnostaxDb : DbContext, IInnostaxDb
    {
        public InnostaxDb()
            : base("DefaultConnection")
        {

        }
        public virtual DbSet<UploadedFile> UploadFiles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Case> Cases { get; set; }
        public virtual DbSet<Industry> Industries { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<CaseFileAssociation> CaseFileAssociations { get; set; }
        public virtual DbSet<IndustrySubCategory> IndustrySubCategories { get; set; }
        public virtual DbSet<ClientIndustry> ClientIndustries { get; set; }
        public virtual DbSet<Discussion> Discussions { get; set; }
        public virtual DbSet<Task> Tasks { get; set; } 
        public virtual DbSet<CaseExpense> CaseExpenses { get; set; }
        public virtual DbSet<TimeSheet> TimeSheets { get; set; }
        public virtual DbSet<CaseReportAssociation> CaseReportAssociations { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<CasePrincipal> CasePrincipals { get; set; }    
        public virtual DbSet<TaskFileAssociation> TaskFileAssociations { get; set; }
        public virtual DbSet<DiscussionMessage> DiscussionMessages { get; set; }
        public virtual DbSet<DiscussionFileAssociation> DiscussionFileAssociations { get; set; }
        public virtual DbSet<DiscussionMessageFileAssociation> DiscussionMessageFileAssociations { get; set; }
        public virtual DbSet<DiscussionNotifier> DiscussionNotifiers { get; set; }
        public virtual DbSet<CaseHistory> CaseHistory { get; set; }
        public virtual DbSet<TimeSheetDescription> TimeSheetDescriptions { get; set; }
        public virtual DbSet<ClientContactUser> ClientContactUsers { get; set; }
        public virtual DbSet<TaskActivity> TaskActivities { get; set; }
        public virtual DbSet<ReportPrice> ReportPrices { get; set; }
        public virtual DbSet<TaskComment> TaskComments { get; set; }
		public virtual DbSet<TaskCommentTaggedUser> TaskCommentTaggedUsers { get; set; }
        public virtual DbSet<CaseStatus> CaseStatus { get; set; }
        public virtual DbSet<ContactUserBusinessUnit> ContactUserBusinessUnits { get; set; }
        public virtual DbSet<SubStatus> SubStatus { get; set; }	
        public virtual DbSet<ELMAH_Error> ELMAH_Error { get; set; }
        public virtual DbSet<ParticipantCaseAssociation> ParticipantCaseAssociation { get; set; }
        public virtual DbSet<ICIUserCaseAssociation> ICIUserCaseAssociations { get; set; }
        public virtual DbSet<TaskCommentFileAssociation> TaskCommentFileAssociations { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<Vendor> Vendors { get; set; }
        public virtual DbSet<TaskDescriptionTaggedUser> TaskDescriptionTaggedUsers { get; set; }
        public virtual DbSet<DiscussionTaggedUser> DiscussionTaggedUsers { get; set; }
        public virtual DbSet<DiscussionMessageTaggedUser> DiscussionMessageTaggedUsers { get; set; }
        public virtual DbSet<NFCUserCaseAssociation> NFCUserCaseAssociations { get; set; }
        public virtual DbSet<AnalyticsConfiguration> AnalyticsConfigurations { get; set; }
        public virtual DbSet<Tenant> Tenants { get; set; }

        public static InnostaxDb Create()
        {
            return new InnostaxDb();
        }

        public int SaveChangesWithErrors()
        {
            try
            {
                return SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb, ex
                );
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
    }
}