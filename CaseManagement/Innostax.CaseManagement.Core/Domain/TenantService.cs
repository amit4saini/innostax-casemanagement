﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Migrations;
using Innostax.Common.Infrastructure.Cache;
using System.Web;

namespace Innostax.CaseManagement.Core.Domain
{
    public interface ITenantService
    {
        PageResult<Models.Tenant> GetAllTenants(ODataQueryOptions<Models.Tenant> options, HttpRequestMessage request);
        void Save(Models.Tenant tenantDetails);
        void Delete(Guid tenantId);
        Models.Tenant GetTenantDetailsByTenantId(Guid tenantId);
    }

    public class TenantService : ITenantService
    {
        private IInnostaxDb _innostaxDb;
        private readonly IErrorHandlerService _errorHandlerService;
        private readonly IUserService _userService;
        private readonly ITenantDb _tenantDb;

        public TenantService(IInnostaxDb innostaxDb, IErrorHandlerService errorHandlerService, IUserService userService, ITenantDb tenantDb)
        {
            _innostaxDb = innostaxDb;
            _errorHandlerService = errorHandlerService;
            _userService = userService;
            _tenantDb= System.Web.HttpContext.Current.GetOwinContext().Get<Common.Database.Database.TenantDb>("current_tenant_context");
        }

        public PageResult<Models.Tenant> GetAllTenants(ODataQueryOptions<Models.Tenant> options, HttpRequestMessage request)
        {                     
            var listOfTenants = _innostaxDb.Tenants.ToList();
            var test = _tenantDb.TenantRoles.ToList();
            var mappedListOfTenants = Mapper.Map<List<Models.Tenant>>(listOfTenants);        
            IQueryable results = options.ApplyTo(mappedListOfTenants.AsQueryable());
            return new PageResult<Models.Tenant>(
             results as IEnumerable<Models.Tenant>, request.ODataProperties().NextLink,
             mappedListOfTenants.Count);
        }

        public void Save(Models.Tenant tenantDetails)
        {
            bool isNewTenant = false;
            var existingTenant = _innostaxDb.Tenants.FirstOrDefault(x => x.TenantId == tenantDetails.TenantId);
            if (existingTenant == null)
            {
                tenantDetails.TenantId = Guid.NewGuid();
                existingTenant = new Common.Database.Models.Master.Tenant();
                tenantDetails.CreatedDateTime = DateTime.Now;
                tenantDetails.CreatedByUserId = _userService.UserId;
                tenantDetails.Dbname = Guid.NewGuid().ToString();
                tenantDetails.Serverip = @"(LocalDb)\MSSQLLocalDB";
                tenantDetails.UserId = "";
                tenantDetails.Password = "";
                isNewTenant = true;
            }
            else
            {
                tenantDetails.Dbname = existingTenant.Dbname;
            }
            tenantDetails.LastEditedDateTime = DateTime.Now;
            tenantDetails.LastUpdatedByUserId = _userService.UserId;
            var mappedResult = Mapper.Map<Common.Database.Models.Master.Tenant>(tenantDetails);
            _innostaxDb.Tenants.AddOrUpdate(mappedResult);
            _innostaxDb.SaveChangesWithErrors();
            if (isNewTenant)
            {
                var connectionString = GetConnectionStringForATenant(tenantDetails);
                using (var db = new TenantDb(connectionString))
                {
                    if (!db.Database.Exists())
                    {
                        db.Database.Create();
                    }
                }
            }
        }

        public void Delete(Guid tenantId)
        {
            var existingTenant = _innostaxDb.Tenants.FirstOrDefault(x => x.TenantId == tenantId && x.IsDeleted == false);
            if (existingTenant == null)
            {
                var tenantDoesNotExist = Innostax.Common.Constants.ErrorMessages.TenantManagement.TenantError.TENANT_DOES_NOT_EXIST;
                _errorHandlerService.ThrowBadRequestError(tenantDoesNotExist);
            }
            existingTenant.IsDeleted = true;
            _innostaxDb.SaveChangesWithErrors();
        }

        public Models.Tenant GetTenantDetailsByTenantId(Guid tenantId)
        {
            var existingTenant = _innostaxDb.Tenants.FirstOrDefault(x => x.TenantId == tenantId && x.IsDeleted == false);
            if (existingTenant == null)
                _errorHandlerService.ThrowBadRequestError(Innostax.Common.Constants.ErrorMessages.TenantManagement.TenantError.TENANT_DOES_NOT_EXIST);
            return Mapper.Map<Models.Tenant>(existingTenant);
        }

        public string GetConnectionStringForATenant(Models.Tenant tenant)
        {        
            var connectionString = @"Server="+tenant.Serverip+";Initial Catalog=" + tenant.Dbname+ ";User ID ="+tenant.UserId+ ";Password ="+tenant.Password+";Integrated Security=True;MultipleActiveResultSets=True; Connection Timeout=30";
            return connectionString;
        }

   }
}
