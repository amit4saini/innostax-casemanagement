namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingContactUserBusinessUnitTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "StatusId", c => c.Guid());
            AddColumn("dbo.ContactUserBusinessUnits", "CreationDateTime", c => c.DateTime());
            CreateIndex("dbo.Cases", "StatusId");
            AddForeignKey("dbo.Cases", "StatusId", "dbo.CaseStatus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "StatusId", "dbo.CaseStatus");
            DropIndex("dbo.Cases", new[] { "StatusId" });
            DropColumn("dbo.ContactUserBusinessUnits", "CreationDateTime");
            DropColumn("dbo.Cases", "StatusId");
        }
    }
}
