﻿using Innostax.CaseManagement.Models.Account;
using System;

namespace Innostax.CaseManagement.Models
{
    public class Client
    {        
        public Guid ClientId { get; set; }
        public string ClientName { get; set; }
        public string Notes { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryName { get; set; }
        public int? IndustrySubCategoryId { get; set; }
        public string IndustrySubCategoryName { get; set; }
        public Guid? SalesRepresentativeId { get; set; }
        public User SalesRepresentative { get; set; }
        public string SalesRepresentativeEmail { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public DateTime? LastEditedDateTime { get; set; }
        public bool IsDeleted { get; set; }
        public ClientContactUser[] ClientContactUsers { get; set; }       
        public DateTime? LatestCaseOrderDate { get; set; }
    }
}
