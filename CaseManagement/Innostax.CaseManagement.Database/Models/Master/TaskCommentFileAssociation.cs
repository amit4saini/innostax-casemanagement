﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innostax.Common.Database.Models
{
    public class TaskCommentFileAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid TaskCommentId { get; set; }

        [ForeignKey("TaskCommentId")]
        public TaskComment TaskComment { get; set; }
        public Guid FileId { get; set; }
    
        [ForeignKey("FileId")]
        public UploadedFile UploadedFile { get; set; }
        public bool IsDeleted { get; set; }
    }
}
