using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Innostax.Common.Database.Models.UserManagement;

namespace Innostax.Common.Database.Models
{
    public class Client
    {
        public Client()
        {
            CreationDateTime = DateTime.Now;
            LastEditedDateTime = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ClientId { get; set; }

        public string ClientName { get; set; } 
        public string Notes { get; set; }

        public int? IndustryId { get; set; }
        [ForeignKey("IndustryId")]
        public Industry Industry { get; set; }

        public int? IndustrySubCategoryId { get; set; }
        [ForeignKey("IndustrySubCategoryId")]
        public IndustrySubCategory IndustrySubCategory { get; set; }

        public Guid? SalesRepresentativeId { get; set; }
        [ForeignKey("SalesRepresentativeId")]
        public User SalesRepresentative { get; set; }

        public string SalesRepresentativeEmail { get; set; }
        
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public DateTime? DeletionDateTime { get; set; }

        public Guid? DeletedBy { get; set; }
        [ForeignKey("DeletedBy")]
        public User DeletedByUser { get; set; }

        public DateTime CreationDateTime { get; set; }

        public Guid CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }

        public DateTime LastEditedDateTime { get; set; }
    }
}