namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingMinimumSuggestedActualRetailFromCase : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cases", "MinimumSuggestedRetail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "MinimumSuggestedRetail", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
