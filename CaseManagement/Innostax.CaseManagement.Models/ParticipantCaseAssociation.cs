﻿using Innostax.CaseManagement.Models.Account;
using System;
namespace Innostax.CaseManagement.Models
{
    public class ParticipantCaseAssociation
    {
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }   
        public Case Case { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public bool IsActive { get; set; }
        public bool IsBeingAdded { get; set; }
    }
}