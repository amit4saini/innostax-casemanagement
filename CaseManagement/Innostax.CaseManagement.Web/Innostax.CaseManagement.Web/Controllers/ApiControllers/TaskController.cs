﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;
using Innostax.CaseManagement.Web.Attributes;
using Innostax.Common.Database.Enums;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Innostax.CaseManagement.Web.Hubs;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [System.Web.Http.Authorize]
    public class TaskController : ApiController
    {
        private readonly ITaskService _taskService;
        private INotificationHub _notificationHub;
        private INotificationService _notificationService;
        public TaskController(ITaskService taskService, INotificationService notificationService, INotificationHub notificationHub)
        {
            _taskService = taskService;
            _notificationService = notificationService;
            _notificationHub = notificationHub;
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("api/Case/{caseId}/Tasks")]
        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_TASKS, Permission.CAN_VIEW_OWN_TASKS })]
        public TaskPageResult Get(ODataQueryOptions<Models.Task> options, Guid caseId)
        {
            return _taskService.GetAllTasksByCaseId(options, Request, caseId);
        }
        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_TASKS, Permission.CAN_VIEW_OWN_TASKS })]
        [System.Web.Http.Route("api/Task/{userid}/Users")]
        public PageResult<Task> GetAllTasksForAUser(ODataQueryOptions<Models.Task> options, Guid userId)
        {
            return _taskService.GetAllTasksForAUser(options, Request, userId);
        }

        [System.Web.Mvc.HttpGet]
        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_TASKS, Permission.CAN_VIEW_OWN_TASKS })]
        [System.Web.Http.Route("api/Task/{userid}/RecentTasks")]
        public List<Task> GetRecentTasksForAUser(Guid userId)
        {
            return _taskService.GetRecentTasksForAUser(userId);
        }

        // POST api/<controller>
        [HasPermission(new Permission[] { Permission.CAN_CREATE_TASK })]
        [System.Web.Mvc.HttpPost]
        public void Post(Task task)
        {
            var listOfUserToBeNotified = _taskService.Save(task);
            foreach (var userId in listOfUserToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [System.Web.Mvc.HttpPut]
        [HasPermission(new Permission[] { Permission.CAN_EDIT_TASK })]
        public void Put(Guid id, Task task)
        {
            task.TaskId = id;
            var listOfUserToBeNotified = _taskService.Save(task);
            foreach (var userId in listOfUserToBeNotified)
            {
                var messageToNotify = _notificationService.GetMessageToNotifyUser(userId);
                _notificationHub.Send(userId.ToString().ToLower(), messageToNotify);
            }
        }

        [HttpDelete]
        [HasPermission(new Permission[] { Permission.CAN_DELETE_A_TASK })]
        public void Delete(Guid id)
        {
            _taskService.ArchiveUnarchiveTask(id, true);
        }

        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_TASKS, Permission.CAN_VIEW_OWN_TASKS })]
        public Task Get(Guid id)
        {
            return _taskService.GetTaskByTaskId(id);
        }

        [HttpPost]
        [Route("api/Task/GetTasksForCasesAndUsers")]
        [HasPermission(new Permission[] { Permission.CAN_VIEW_ALL_TASKS })]
        public List<Task> GetTasksByCaseIdAndUserId(CasesAndUsersIdModel casesAndUsersIdModel)
        {
            return _taskService.GetTasksByCaseIdAndUserId(casesAndUsersIdModel);
        }

        [HttpGet]
        [Route("api/Search/Tasks")]
        public TaskPageResult SearchTasks(ODataQueryOptions<Models.Task> options, string keyword, Guid id)
        {
            return _taskService.SearchTasksByKeyword(options, Request, keyword, id);
        }       
    }
}