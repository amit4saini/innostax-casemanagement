﻿using Innostax.Common.Infrastructure.Email;
using Innostax.Common.Database.Database;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Core.Services.Implementation;

namespace WebJobApplication
{
    public class TaskNotification
    {
        public void SendTaskNotification()
        {
            InnostaxDb innostaxDb = new InnostaxDb();
            IErrorHandlerService errorHandlerService = new ErrorHandlerService();          
            EmailService emailService = new EmailService();
            UserService userService = new UserService(innostaxDb, errorHandlerService);
            ICommonService commonService = new CommonService(userService, innostaxDb, errorHandlerService);
            AzureFileManager azureFileManager = new AzureFileManager(innostaxDb, commonService,userService);
            MailSenderService mailSenderService = new MailSenderService(userService, emailService);
            INotificationService notificationService = new NotificationService(innostaxDb, userService, errorHandlerService, azureFileManager, commonService);
            TaskNotificationService taskNotificationService = new TaskNotificationService(innostaxDb, mailSenderService, notificationService,commonService);
            taskNotificationService.SendNotification();
        }
    }
}
