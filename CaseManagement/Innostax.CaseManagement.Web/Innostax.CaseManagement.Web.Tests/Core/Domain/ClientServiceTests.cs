﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Should;
using AutoMapper;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Net.Http;
using System.Web.Http.OData.Builder;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class ClientServiceTests
    {

        private Mock<InnostaxDb> innostaxDbMock;    
        private ClientService sut;
        private Mock<IUserService> userService;
        private Mock<IReportService> reportService;
        private Mock<IClientContactUserService> clientContactUserService;
        private ErrorHandlerService errorHandlerService;
        private Mock<ICommonService> _commonService;
        ODataQueryOptions<Models.Client> options;
        ODataQueryOptions<Models.Case> caseOptions;
        Guid userId;
        Guid clientId1;
        Guid clientId2;
        Guid expectedClientId;
        Guid clientContactUserId;
        Guid clientBusinessUnitId;

        //// Arrange
        [TestInitialize]
        public void TestInitialize()
        {
            userId = Guid.NewGuid();
            expectedClientId = Guid.NewGuid();
            clientContactUserId = Guid.NewGuid();
            clientBusinessUnitId = Guid.NewGuid();
            innostaxDbMock = new Mock<InnostaxDb>();
            userService = new Mock<IUserService>();
            clientContactUserService = new Mock<IClientContactUserService>();
            clientId1 = Guid.NewGuid();
            clientId2 = Guid.NewGuid();
            var userPermission = new List<Innostax.Common.Database.Enums.Permission> { Innostax.Common.Database.Enums.Permission.CASE_CREATE, Innostax.Common.Database.Enums.Permission.CASE_DELETE_ALL, Innostax.Common.Database.Enums.Permission.CASE_READ_ALL, Innostax.Common.Database.Enums.Permission.CLIENT_READ_ALL };
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Models.Client>("Client");
            options = new ODataQueryOptions<Models.Client>(new ODataQueryContext(modelBuilder.GetEdmModel(), typeof(Models.Client)), new HttpRequestMessage());
            ODataModelBuilder modelBuilderForCase = new ODataConventionModelBuilder();
            modelBuilderForCase.EntitySet<Models.Case>("Case");
            caseOptions = new ODataQueryOptions<Models.Case>(new ODataQueryContext(modelBuilderForCase.GetEdmModel(), typeof(Models.Case)), new HttpRequestMessage());
            reportService = new Mock<IReportService>();
            errorHandlerService = new ErrorHandlerService();
            _commonService = new Mock<ICommonService>();
            userService.SetupGet(x => x.UserPermissions).Returns(userPermission);
            sut = new ClientService(innostaxDbMock.Object, userService.Object, reportService.Object, errorHandlerService, clientContactUserService.Object, _commonService.Object);
            clientContactUserService.Setup(x => x.GetClientAssociatedContactUsers(It.IsAny<Guid>())).Returns(new List<Models.ClientContactUser> { new Models.ClientContactUser() { } });           
        }
        private Industry GetDummyIndustry()
        {
            var dummyIndustry = new Industry { Id = 1, Name = "DummyIndustry" };
            return dummyIndustry;
        }
        private IndustrySubCategory GetDummyIndustrySubCategory()
        {
            var dummyIndustrySubCategory = new IndustrySubCategory { Id = 1, Name = "DummyIndustrySubCategory" };
            return dummyIndustrySubCategory;
        }
        private Country GetDummyCountry()
        {  
            var dummyCountry = 
        
                new Country {
                CountryId = 1,
                CountryName="India",
                RegionId=1,
                Region=GetDummyRegion()                                           
            };
            return dummyCountry;           
        }
        private List<Region> GetDummyRegionList()
        {
            var dummyRegion = new List<Region> {

                new Region
                {
                    RegionId=1,
                    RegionName="Asia"

                },
            new Region
            {
                RegionId = 2,
                RegionName = "Asia"

            }
        };
            return dummyRegion;
        }
        private Region GetDummyRegion()
        {
            var dummyRegion =
                new Region
                {
                    RegionId = 1,
                    RegionName = "Asia"

                };             
            return dummyRegion;
        }
        private State GetDummyState()
        {           
            var dummyState =

                new State
                {
                    Country=GetDummyCountry(),
                    CountryId = 1,
                    StateId = 1,
                    StateName="TEST"                  
                };
            return dummyState;
        }
        private List<Case> GetDummyCase()
        {
            var dummyCase = new List<Case>
            {
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId1, ClientName = "DummyClient1" },
                    ClientId=clientId1,
                    IsDeleted=false,                  
                    ClientContactUserId=clientContactUserId
                },
                new Case
                {
                    CaseId=Guid.NewGuid(),
                    Client = new Client {ClientId = clientId2, ClientName = "DummyClient2" },
                    ClientId=clientId2,
                    IsDeleted=false,                   
                    ClientContactUserId=Guid.NewGuid()
                }
            };
            return dummyCase;
        }
        private List<Client> GetDummyClients()
        {
            var clientId1 = Guid.NewGuid();
            var clientId2 = Guid.NewGuid();
            var dummyClients = new List<Client>
            {
                new Client
                {
                    ClientId = clientId1,
                    IndustryId =1,
                    Industry = GetDummyIndustry(),
                    IndustrySubCategory = GetDummyIndustrySubCategory(),
                    IndustrySubCategoryId = 1,
                    SalesRepresentativeId = userId,
                    ClientName = "clientName2",
                    Notes = "notes2",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User {FirstName = "firstName2", LastName = "lastName2", },
                    IsDeleted=false
                },
                new Client
                {
                    ClientId = clientId2,
                    IndustryId =1,
                    Industry = GetDummyIndustry(),
                    IndustrySubCategory = GetDummyIndustrySubCategory(),
                    IndustrySubCategoryId = 1,
                    ClientName = "clientName2",
                    Notes = "notes2",
                    SalesRepresentative = new Innostax.Common.Database.Models.UserManagement.User {FirstName = "firstName2", LastName = "lastName2"},
                    IsDeleted=false
                }
            };
            return dummyClients;
        }
        private List<ClientContactUser> GetDummyClientContactUsers()
        {
            var dummyClientContactUsers = new List<ClientContactUser>
            {
                new ClientContactUser
                {
                    Id=clientContactUserId,
                    CountryId=1,
                    Country=GetDummyCountry(),
                    StateId=1,
                    State=GetDummyState(),
                    ClientId = expectedClientId,
                    Client = new Client {ClientName = "clientName", Notes = "notes", SalesRepresentativeEmail = "abc@bcd.com" },
                    PositionHeldbyContact = "positionHeldBy",
                    Street = "street",
                    City = "city",
                    ZipCode = "zipCode",
                    IsDeleted=false
                },
                new ClientContactUser
                {
                    Id=Guid.NewGuid(),
                    CountryId=1,
                    Country=GetDummyCountry(),
                    StateId=1,
                    State=GetDummyState(),
                    Client = new Client {ClientName = "clientName", Notes = "notes" },
                    PositionHeldbyContact = "positionHeldBy",
                    Street = "street",
                    City = "city",
                    ZipCode = "zipCode",
                    IsDeleted=false
                }
            };
            return dummyClientContactUsers;
        }
        private List<ContactUserBusinessUnit> GetDummyContactUserBusinessUnit()
        {
            var dummyContactUserBusinessUnit = new List<ContactUserBusinessUnit>
            {
                new ContactUserBusinessUnit
                {
                    BusinessUnit = "businessUnit",
                    ClientContactUser = new ClientContactUser { PositionHeldbyContact ="positionHeldBy", Street = "street", City = "city", ZipCode = "zipCode" },
                }
            };
            return dummyContactUserBusinessUnit;
        }
        private List<Innostax.Common.Database.Models.UserManagement.User> GetDummyUser()
        {
            var dummyUser = new List<Innostax.Common.Database.Models.UserManagement.User>
                            {
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId },
                                new Innostax.Common.Database.Models.UserManagement.User { Id = userId }
                            };
            return dummyUser;
        }
        [TestMethod]    
        public void GivenCallingGetClientDetailsFromClientId_WhenClientIdIsNotFound_ThenMappedResultReturn()
        {
            try
            {                
                var dummyClients = new List<Client>();
                innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
                innostaxDbMock.Setup(c => c.Regions).ReturnsDbSet(GetDummyRegionList());
                innostaxDbMock.Setup(c => c.States).ReturnsDbSet(new List<State> { GetDummyState() });
                innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(new List<Country> { GetDummyCountry()});
                innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(new List<Industry> { GetDummyIndustry()});
                innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(new List<IndustrySubCategory> { GetDummyIndustrySubCategory()});           
                sut.GetClientDetailsFromClientId(Guid.NewGuid());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
        [TestMethod]
        public void GivenCallingGetAllCasesForAClient_WhenClientIdIsNotFound_ThenReturnsNull()
        {
           var dummyClients = new List<Client>();
           innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
           innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
           var caseData=sut.GetAllCasesForAClient(caseOptions, new HttpRequestMessage(),Guid.NewGuid());
           Assert.AreEqual(caseData.Count,0);
           
        }
        [TestMethod]
        public void GivenCallingGetAllCasesForAClient_WhenClientIdIsFound_ThenMappedResultReturns()
        {
            var dummyClients = new List<Client>();
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            var caseData = sut.GetAllCasesForAClient(caseOptions, new HttpRequestMessage(), clientId1);
            Assert.IsNotNull(caseData);
        }    
        [TestMethod]
        public void GivenCallingGetClientDetailsFromClientId_WhenClientIdHasResults_ThenMappedResultsAreReturned()
        {     
            var dummyClients = GetDummyClients();
            Client newexpectedClient = new Client
            {
                ClientId = expectedClientId,
                IndustryId = 1,
                IndustrySubCategoryId = 1
            };
            var dummy = new Innostax.Common.Database.Models.Country[1];
            dummy[0] = GetDummyCountry();
            var dummyState = new State[1];
            dummyState[0] = GetDummyState();
            var dummyIndustries = new List<Industry>();
            var dummyUsers = GetDummyUser();
            dummyIndustries.Add(GetDummyIndustry());
            var dummyIndustrySubCategories = new List<IndustrySubCategory>();
            newexpectedClient.Industry = GetDummyIndustry();
            newexpectedClient.IndustrySubCategory = GetDummyIndustrySubCategory();
            dummyClients.Add(newexpectedClient);         
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(GetDummyCase());
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(GetDummyClientContactUsers());          
            innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummy);
            innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
            innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustries);
            innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(dummyIndustrySubCategories);
            innostaxDbMock.Setup(c => c.Regions).ReturnsDbSet(GetDummyRegionList());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(new List<ClientContactUser> { new ClientContactUser() });
            // Act           
            var actual = sut.GetClientDetailsFromClientId(expectedClientId);
            // Assert
            actual.ClientId.ShouldEqual(expectedClientId);
        }

        [TestMethod]
        public void GivenCalling_GetAllClientsDetails_WhenThereIsNoData_ThenReturnsNoData()
        {
            var clientPresent = Guid.NewGuid();
            var dummyClients = new List<Client>();
            var dummy = new Innostax.Common.Database.Models.Country[1];
            dummy[0] = GetDummyCountry();
            var dummyState = new State[1];
            dummyState[0] = GetDummyState();
            var dummyIndustries = new List<Industry>();
            dummyIndustries.Add(GetDummyIndustry());
            var dummyIndustrySubCategories = new List<IndustrySubCategory>();
            dummyIndustrySubCategories.Add(GetDummyIndustrySubCategory());
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummy);
            innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
            innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustries);
            innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(dummyIndustrySubCategories);                         
            var clientData=sut.GetAllClientsDetails(options,new HttpRequestMessage());
            Assert.AreEqual(clientData.Count,0);
        }

        [TestMethod]
        public void GivenCalling_GetAllClientsDetails_WhenClientsExists_ThenListOfClientIsReturned()
        {
            // Arrange
            var innostaxDbMock = new Mock<InnostaxDb>();
            var clientId1 = Guid.NewGuid();
            var clientId2 = Guid.NewGuid();
            var dummy = new Innostax.Common.Database.Models.Country[1];
            dummy[0] = GetDummyCountry();
            var dummyState = new State[1];
            dummyState[0] = GetDummyState();
            var dummyClients = GetDummyClients();
            var dummyUsers = GetDummyUser();
            var dummyIndustries = new List<Industry>();
            dummyIndustries.Add(GetDummyIndustry());
            var dummyIndustrySubCategories = new List<IndustrySubCategory>();
            dummyIndustrySubCategories.Add(GetDummyIndustrySubCategory()); 
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.Countries).ReturnsDbSet(dummy);
            innostaxDbMock.Setup(c => c.States).ReturnsDbSet(dummyState);
            innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustries);
            innostaxDbMock.Setup(c => c.IndustrySubCategories).ReturnsDbSet(dummyIndustrySubCategories);
            innostaxDbMock.Setup(c => c.Regions).ReturnsDbSet(GetDummyRegionList());
            innostaxDbMock.Setup(c => c.Users).ReturnsDbSet(dummyUsers);
            sut = new ClientService(innostaxDbMock.Object, userService.Object, reportService.Object, errorHandlerService, clientContactUserService.Object, _commonService.Object);
            var clientData = sut.GetAllClientsDetails(options, new HttpRequestMessage()).Items;    
            Assert.IsNotNull(clientData);
        }

        [TestMethod]
        public void GivenCallingValidateClientId_WhenBadClientIdPAssed_ThenThrowsBadRequest()
        {
            try
            {         
                var dummyClients = new List<Client>();       
                innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
                sut.ValidateClientId("1");
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        [TestMethod]
        public void GivenCallingValidateClientId_WhenValidClientIdPassed()
        {        
            var dummyClients = new List<Client>();
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            // Act
            string clientid = Guid.NewGuid().ToString();
            var actual = sut.ValidateClientId(clientid);
            Assert.AreEqual(clientid, actual.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingDelete_WhenBadClientIdSent_ThenBadRequestAreReturned()
        {
            try
            {               
                var clientId1 = Guid.NewGuid();
                var dummyClients = GetDummyClients();
                innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
                sut.Delete(Guid.NewGuid());
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.BadRequest);
                throw;
            }
        }

        [TestMethod]
        public void WhenCalling_Delete_ToDeleteAClient_WhenClientExists_ThenReturnTrue()
        {     
            var clientId1 = Guid.NewGuid();
            var clientId2 = Guid.NewGuid();
            var clientPresent = Guid.NewGuid();
            var dummyClients = GetDummyClients();
            Client newexpectedClient = new Client
            {
                ClientId = clientPresent
            };
            dummyClients.Add(newexpectedClient);
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(new List<ClientContactUser> { new ClientContactUser() });
            innostaxDbMock.Setup(c => c.Cases).ReturnsDbSet(new List<Case> { new Case() });
            try
            {
                sut.Delete(clientPresent);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void WhenCalling_SearchClientsByKeyword_ToSearchAClient_WhenKeywordIsFound_ReturnClient()
        {
            var dummyClients = GetDummyClients();
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(GetDummyClientContactUsers());
            innostaxDbMock.Setup(c => c.ContactUserBusinessUnits).ReturnsDbSet(GetDummyContactUserBusinessUnit());
            var clientData = sut.SearchClientsByKeyword(options, new HttpRequestMessage(), "firstName");
            Assert.AreEqual(clientData.Count, 2);
        }

    [TestMethod]
        public void WhenCalling_SearchClientsByKeyword_ToSearchAClient_WhenKeywordNotFound_ThenResultIsEmpty()
        {
            var dummyClients = GetDummyClients();
            innostaxDbMock.Setup(c => c.Clients).ReturnsDbSet(dummyClients);
            innostaxDbMock.Setup(c => c.ClientContactUsers).ReturnsDbSet(GetDummyClientContactUsers());
            innostaxDbMock.Setup(c => c.ContactUserBusinessUnits).ReturnsDbSet(GetDummyContactUserBusinessUnit());
            var clientData = sut.SearchClientsByKeyword(options, new HttpRequestMessage(), "hfsjjsf");
            Assert.AreEqual(clientData.Count, 0);
        }
    }
}
