﻿namespace Innostax.Common.Database.Enums
{
    public enum NotificationUrl
    {
        CASE,
        DISCUSSION,
        DISCUSSIONMESSAGE,
        TASK
    }
}
