﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Core.Services.Implementation;
using Innostax.CaseManagement.Web;
using Innostax.Common.Database.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJobApplication
{
   public class NFCCaseNotification
    {
        public void SendNFCCaseNotification()
        {
            InnostaxDb innostaxDb = new InnostaxDb();
            IErrorHandlerService errorHandlerService = new ErrorHandlerService();           
            UserService userService = new UserService(innostaxDb, errorHandlerService);
            ICommonService commonService = new CommonService(userService, innostaxDb, errorHandlerService);
            AzureFileManager azureFileManager = new AzureFileManager(innostaxDb, commonService, userService);       
            INotificationService notificationService = new NotificationService(innostaxDb, userService, errorHandlerService, azureFileManager, commonService);
            NFCCaseNotificationService nfcCaseNotificationService = new NFCCaseNotificationService(innostaxDb, notificationService, commonService);
            nfcCaseNotificationService.SendNotification();
        }
    }
}
