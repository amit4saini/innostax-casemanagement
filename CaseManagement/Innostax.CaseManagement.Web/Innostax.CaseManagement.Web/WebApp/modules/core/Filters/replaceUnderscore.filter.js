﻿angular.module('core').filter('replaceUnderscore', function () {
    return function (roleName) {
        if (!angular.isString(roleName) && roleName.indexOf("_") == -1) {
            return roleName;
        }
        return roleName.replace(/_/g, ' ');
    };
});

