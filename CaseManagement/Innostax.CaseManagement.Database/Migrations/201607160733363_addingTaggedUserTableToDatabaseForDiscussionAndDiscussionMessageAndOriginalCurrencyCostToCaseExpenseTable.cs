namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingTaggedUserTableToDatabaseForDiscussionAndDiscussionMessageAndOriginalCurrencyCostToCaseExpenseTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DiscussionMessageTaggedUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        DiscussionMessageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DiscussionMessages", t => t.DiscussionMessageId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.DiscussionMessageId);
            
            CreateTable(
                "dbo.DiscussionTaggedUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        DiscussionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Discussions", t => t.DiscussionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.DiscussionId);
            
            AddColumn("dbo.CaseExpenses", "OriginalCurrencyCost", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DiscussionTaggedUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.DiscussionTaggedUsers", "DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.DiscussionMessageTaggedUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.DiscussionMessageTaggedUsers", "DiscussionMessageId", "dbo.DiscussionMessages");
            DropIndex("dbo.DiscussionTaggedUsers", new[] { "DiscussionId" });
            DropIndex("dbo.DiscussionTaggedUsers", new[] { "UserId" });
            DropIndex("dbo.DiscussionMessageTaggedUsers", new[] { "DiscussionMessageId" });
            DropIndex("dbo.DiscussionMessageTaggedUsers", new[] { "UserId" });
            DropColumn("dbo.CaseExpenses", "OriginalCurrencyCost");
            DropTable("dbo.DiscussionTaggedUsers");
            DropTable("dbo.DiscussionMessageTaggedUsers");
        }
    }
}
