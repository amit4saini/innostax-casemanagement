﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.Common.Database.Database;
using Innostax.Common.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoMapper;

namespace Innostax.CaseManagement.Web.Tests.Core.Domain
{
    [TestClass]
    public class IndustryTest
    {

        private Mock<InnostaxDb> innostaxDbMock;
        private IndustryService sut;
        private ErrorHandlerService errorHandlerService;
        //// Arrange
        [TestInitialize]
        public void TestInitialize()
        {
            innostaxDbMock = new Mock<InnostaxDb>();
            errorHandlerService = new ErrorHandlerService();           
            sut = new IndustryService(innostaxDbMock.Object, errorHandlerService);

        }

        private List<Industry> GetDummyIndustry()
        {
           
            var dummyIndustry = new List<Industry>
            {
                new Industry
                {
                    Id = 1
                },
                new Industry
                {
                    Id = 2
                }
            };
            return dummyIndustry;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingGetAllIndustry_WhenIndustryTableIsEmpty_ThenThrowsNotFound()
        {
            try
            {
                var dummyIndustry = new List<Industry>();
                innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustry);
                sut.GetAllIndustries();
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
        [TestMethod]        
        public void GivenCallingGetAllIndustry_WhenIndustryTableIsNotEmpty_ThenMappedResultIsReturned()
        {
            var dummyIndustry = GetDummyIndustry();
            innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustry);
            var result = sut.GetAllIndustries();
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GivenCallingGetIndustry_WhenIndustryTableDoesContainsSpecifiedId_ThenMappedResultReturn()
        {
            var dummyIndustry = GetDummyIndustry();
            string expectedId = "2";
            innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustry);
            var result = sut.GetIndustry(expectedId);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GivenCallingGetIndustry_WhenIndustryTableDoesNotContainsSpecifiedId_ThenThrowNotFound()
        {
            try
            {
                var dummyIndustry = GetDummyIndustry();
                string expectedId = "5";
                innostaxDbMock.Setup(c => c.Industries).ReturnsDbSet(dummyIndustry);
                var result = sut.GetIndustry(expectedId);
            }
            catch (HttpResponseException e)
            {
                // Assert
                Assert.AreEqual(e.Response.StatusCode, HttpStatusCode.NotFound);
                throw;
            }
        }
    }
}


    
