﻿namespace Innostax.Common.Database.Enums
{
    public enum Speed
    {
        Normal,
        Rush,
        SuperRush
    }
}
