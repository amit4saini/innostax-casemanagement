﻿using Innostax.CaseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Innostax.Common.Database.Database;
using System.IO;
using AutoMapper;

namespace Innostax.CaseManagement.Core.Domain
{
    public class TaskNotificationService
    {
        private IInnostaxDb _innostaxDb;
        private IMailSenderService _mailSenderService;
        private INotificationService _notificationService;
        private ICommonService _commonService;

        public TaskNotificationService(IInnostaxDb innostaxDb, IMailSenderService mailSenderService, INotificationService notificationService, ICommonService commonService)
        {    
            _innostaxDb = innostaxDb;
            _mailSenderService = mailSenderService;
            _notificationService = notificationService;
            _commonService = commonService;
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(baseDirectory, @"..\..\..\"));
            string connectionStringPath = Path.Combine(newPath, "Kreller.CaseManagement.Web", "App_Data");
            AppDomain.CurrentDomain.SetData("DataDirectory", connectionStringPath);
        }

        public List<DueTaskDetails> DueTasksDetails()
        {
            var dueTasksList = new List<DueTaskDetails>();                   
            var existingTasks = _innostaxDb.Tasks.Include(y => y.AssignedToUser).Include(x=>x.Case).Where(x=>x.IsDeleted == false && x.Status != Innostax.Common.Database.Enums.TaskStatusType.COMPLETED && (System.Data.Entity.DbFunctions.DiffDays(DateTime.UtcNow, x.DueDate)<= 2)).ToList();
            foreach (var task in existingTasks)
            {
                if (task.DueDate != null)
                {
                    if (task.DueDate.Date.AddDays(-1) == _commonService.GetUtcDateTime().Date || task.DueDate.Date.AddDays(-2) == _commonService.GetUtcDateTime().Date)
                    {
                        var dueTask = new DueTaskDetails();
                        dueTask.UserEmailAddress = task.AssignedToUser.Email;
                        dueTask.TaskName = task.Title;
                        dueTask.UserFirstName = task.AssignedToUser.FirstName;
                        dueTask.UserLastName = task.AssignedToUser.LastName;
                        dueTask.DueDate = task.DueDate;
                        dueTask.HasPassedDueDate = false;
                        dueTask.UserId = task.AssignedToUser.Id;
                        dueTask.TaskId = task.TaskId;
                        dueTask.Case = Mapper.Map<Models.Case>(task.Case);                       
                        dueTasksList.Add(dueTask);
                    }
                    if (task.DueDate.Date == _commonService.GetUtcDateTime().Date.AddDays(-1) )
                    {
                        var dueTask = new DueTaskDetails();
                        dueTask.UserEmailAddress = task.AssignedToUser.Email;
                        dueTask.TaskName = task.Title;
                        dueTask.UserFirstName = task.AssignedToUser.FirstName;
                        dueTask.UserLastName = task.AssignedToUser.LastName;
                        dueTask.DueDate = task.DueDate;
                        dueTask.TaskId = task.TaskId;
                        dueTask.Case = Mapper.Map<Case>(task.Case);
                        dueTask.UserId = task.AssignedToUser.Id;
                        dueTask.HasPassedDueDate = true;
                        dueTasksList.Add(dueTask);
                    }
                }
            }    
            return dueTasksList;
        }

        public void SendNotification()
        {
            var dueTasks = DueTasksDetails();
            foreach (var dueTask in dueTasks)
            {
                var notificationText = string.Empty;
                var URL = _commonService.GetCaseRelatedUrl(Common.Database.Enums.NotificationUrl.TASK, dueTask.Case.CaseNumber, dueTask.TaskId);
                if (dueTask.HasPassedDueDate)
                {
                    notificationText = Common.Constants.NotificationMessages.TaskNotification.TASK_DUE_DATE_PASSED;
                    notificationText = string.Format(notificationText, dueTask.TaskName, dueTask.Case.NickName, dueTask.DueDate);  
                    _notificationService.SaveWebJobNotification(notificationText,new List<Guid> { dueTask.UserId },URL);
                }
                else
                {
                    notificationText = Common.Constants.NotificationMessages.TaskNotification.TASK_PENDING;                  
                    notificationText = string.Format(notificationText, dueTask.TaskName, dueTask.Case.NickName, dueTask.DueDate);                   
                    _notificationService.SaveWebJobNotification(notificationText, new List<Guid> { dueTask.UserId }, URL);
                }                
            }
            _mailSenderService.SendDueTaskDetails(dueTasks);                                                  
        }
    }
}
