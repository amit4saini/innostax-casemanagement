﻿using System.Collections.Generic;
using System.Web.Http;
using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Models;

namespace Innostax.CaseManagement.Web.Controllers.ApiControllers
{
    [Authorize]
    public class DiscountController : ApiController
    {
        private readonly IDiscountService _discountService;
        public DiscountController(IDiscountService discountService)
        {
            _discountService = discountService;
        }

        //GET api/<controller>
        [Route("api/Discounts")]
        [System.Web.Mvc.HttpGet]
        public List<Discount> Get()
        {
            return _discountService.GetDiscounts();
        }
    }
}