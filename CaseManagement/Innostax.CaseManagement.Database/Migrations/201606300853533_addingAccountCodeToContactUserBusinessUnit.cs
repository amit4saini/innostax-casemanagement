namespace Innostax.Common.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addingAccountCodeToContactUserBusinessUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContactUserBusinessUnits", "AccountCode", c => c.String());
            DropColumn("dbo.ClientContactUsers", "AccountCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ClientContactUsers", "AccountCode", c => c.String());
            DropColumn("dbo.ContactUserBusinessUnits", "AccountCode");
        }
    }
}
