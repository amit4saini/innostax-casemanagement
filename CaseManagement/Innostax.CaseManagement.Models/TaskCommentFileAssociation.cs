﻿using System;
using System.Collections.Generic;

namespace Innostax.CaseManagement.Models
{
    public class TaskCommentFileAssociation
    {      
        public Guid Id { get; set; }
        public Guid TaskCommentId { get; set; }
        public TaskComment TaskComment { get; set; }
        public Guid FileId { get; set; }    
        public List<FileModel> Files { get; set; }
        public bool IsDeleted { get; set; }
    }
}
