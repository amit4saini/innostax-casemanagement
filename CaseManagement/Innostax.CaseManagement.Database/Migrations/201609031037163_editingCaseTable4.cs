namespace Innostax.Common.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editingCaseTable4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "CaseAssignedToIHIByUserId", c => c.Guid());
            AddColumn("dbo.Cases", "ICIAssignedUserId", c => c.Guid());
            CreateIndex("dbo.Cases", "CaseAssignedToIHIByUserId");
            CreateIndex("dbo.Cases", "ICIAssignedUserId");
            AddForeignKey("dbo.Cases", "CaseAssignedToIHIByUserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Cases", "ICIAssignedUserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "ICIAssignedUserId", "dbo.Users");
            DropForeignKey("dbo.Cases", "CaseAssignedToIHIByUserId", "dbo.Users");
            DropIndex("dbo.Cases", new[] { "ICIAssignedUserId" });
            DropIndex("dbo.Cases", new[] { "CaseAssignedToIHIByUserId" });
            DropColumn("dbo.Cases", "ICIAssignedUserId");
            DropColumn("dbo.Cases", "CaseAssignedToIHIByUserId");
        }
    }
}
