﻿namespace Innostax.Common.Database.Enums
{
    public enum TaskActivityAction
    {
        TASK_CREATED,
        TASK_ASSIGNED_TO,
        TASK_DUEDATE_CHANGED,
        TASK_STATUS_CHANGED
    }
}
