﻿using System;
using Innostax.Common.Database.Enums;
namespace Innostax.CaseManagement.Models
{
    public class CaseExpense
     {         
        public Guid Id { get; set; }   
        public Guid CaseId { get; set; }        
        public string Note { get; set; }
        public decimal Quantity { get; set; }     
        public decimal Cost { get; set; }            
        public DateTime CreationDateTime { get; set; }
        public Guid CreatedBy { get; set; }
        public Account.User CreatedByUser { get; set; }    
        public DateTime LastEditedDateTime { get; set; }
        public Guid LastEditedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Guid DeletedBy { get; set; }
        public Guid? VendorId { get; set; }
        public Vendor Vendor { get; set; } 
        public string OriginalCurrencyCost { get; set; }
        public ExpensePaymentType ExpensePaymentType { get; set; }
        public bool InvoiceApproved { get; set; }
     }
}
