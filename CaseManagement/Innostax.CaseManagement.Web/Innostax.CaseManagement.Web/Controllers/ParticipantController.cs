﻿using Innostax.CaseManagement.Core.Domain;
using Innostax.CaseManagement.Core.Services.Implementation;
using Innostax.CaseManagement.Web.ViewModels.UserManagement;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using Innostax.Common.Database.Enums;

namespace Innostax.CaseManagement.Web.Controllers
{
    [Authorize]
    public class ParticipantController : Controller
    {
        private IUserService _userService;
        private readonly IAzureFileManager _azureFileManager;
        public ParticipantController(IUserService userService, IAzureFileManager azureFileManager)
        {
            _userService = userService;
            _azureFileManager = azureFileManager;
        }
        [Route("participant")]
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            var userId = new Guid();
            var userViewModel = new UserViewModel();
            var userIdString = User.Identity.GetUserId();
            if (!Guid.TryParse(userIdString, out userId))
            {
                return RedirectToAction("Login", "Account");
            }
            var userDetailsResult = _userService.GetUser(userId);
            if (!userDetailsResult.IsSuccess)
            {
                return RedirectToAction("Login", "Account");
            }
            if (userDetailsResult.Data.Role.Name != Roles.Client_Participant.ToString())
            {
                return RedirectToAction("Login", "Account");
            }
            userViewModel.Email = userDetailsResult.Data.Email;
            userViewModel.UserId = userDetailsResult.Data.Id;
            userViewModel.Permissions = userDetailsResult.Data.Permissions.ToList();
            Guid profilePicKey = userDetailsResult.Data.ProfilePicKey ?? Guid.Empty;
            if (profilePicKey != Guid.Empty)
                userViewModel.ProfilePicUrl = await _azureFileManager.DownloadFile(profilePicKey, DownloadFileType.ProfilePic);
            else
                userViewModel.ProfilePicUrl = Common.Constants.InnostaxConstants.PARTICIPANT_PROFILE_PIC;
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            ViewBag.User = javaScriptSerializer.Serialize(userViewModel);
            ViewBag.DefaultPageSize = (WebConfigurationManager.AppSettings["DefaultPageSize"] == null) ||
                (WebConfigurationManager.AppSettings["DefaultPageSize"] == "") ? "10" : WebConfigurationManager.AppSettings["DefaultPageSize"];
            return View();
        }
    }
}